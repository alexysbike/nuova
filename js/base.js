var mensajesValidacion = {
  errores : {
    campoRequerido: '<div class="label label-danger">* Campo Requerido</div>',
    errorFormato: '<div class="label label-danger">* Error en Formato</div>',
    correoInvalido: '<div class="label label-danger">* Correo no v&aacute;lido</div>',
    correoUsado: '<div class="label label-danger">* Correo Electr&oacute;nico en uso</div>',
    correoDiferentes: '<div class="label label-danger">* Los Correos Electr&oacute;nicos deben de coincidir</div>',
    passwordsDiferentes: '<div class="label label-danger">* Las Contrase&ntilde;as deben de coincidir</div>',
    aliasUsado: '<div class="label label-danger">* Alias en uso</div>',
    nombreImagenUsado: '<div class="label label-danger">* Nombre de Imagen en uso</div>',
    faltaTallas: '<div class="label label-danger">* Debe haber por lo menos una talla</div>',
    tallaRequerida: '<div class="label label-danger">* Todos las tallas deben estar llenas</div>',
    presentacionRequerida: '<div class="label label-danger">* Todos las presentaciones deben estar llenas</div>',
    edadRequerida: '<div class="label label-danger">* Debes ser mayor de edad para poder registrarte</div>'
  }
};

$(function(){
  $("a#boton-micuenta").popover({title:$("a#boton-micuenta").attr("data-nombre")+' <a href="" class="pull-right boton-cerrar-popover" data-parent="a#boton-micuenta">X</a>', 
                                 content:'<div class="list-group" style="min-width:125px; margin:0"><a class="list-group-item text-center arreglo" href="'+base_url+'usuario/cuenta">Perfil</a><a class="list-group-item text-center arreglo" href="'+base_url+'usuario/cuenta/articulos">Mis Art&iacute;culos</a><a class="list-group-item text-center arreglo" href="'+base_url+'usuario/cuenta/ventas">Ventas</a><a class="list-group-item text-center arreglo" href="'+base_url+'usuario/cuenta/compras">Compras</a><a class="list-group-item text-center arreglo" href="'+base_url+'usuario/logout">Salir</a></div>',
                                 placement:"bottom",
                                 container:"body",
                                 html: true});
  $("a#boton-micuenta-comprador").popover({title:$("a#boton-micuenta").attr("data-nombre")+' <a href="" class="pull-right boton-cerrar-popover" data-parent="a#boton-micuenta">X</a>', 
                                 content:'<div class="list-group" style="min-width:125px; margin:0"><a class="list-group-item text-center arreglo" href="'+base_url+'usuario/cuenta">Perfil</a><a class="list-group-item text-center arreglo" href="'+base_url+'usuario/cuenta/compras">Compras</a><a class="list-group-item text-center arreglo" href="'+base_url+'usuario/logout">Salir</a></div>',
                                 placement:"bottom",
                                 container:"body",
                                 html: true});
  $("a#boton-login").popover({title:'Login <a href="" class="pull-right boton-cerrar-popover" data-parent="a#boton-login">X</a>', 
                                 content:'<div class="row"><div class="col-md-12"><form method="post" action="'+base_url+'usuario/login" role="form"><div class="form-group"><input type="email" name="correo" class="form-control" placeholder="Correo Electr&oacute;nico"></div><div class="form-group"><input type="password" name="password" class="form-control" placeholder="Contrase&ntilde;a"></div><button name="login" value="login" class="btn btn-primary btn-block">Login</button></form></div></div>',
                                 placement:"bottom",
                                 container:"body",
                                 html: true});
  $("body").on("click","a.boton-cerrar-popover", function (event){
    event.preventDefault();

    $($(this).attr("data-parent")).click();
  });
});