$(function(){
	var modalDatosPagos = $("div#modal-datos-pago");
	var formulario = $("form#formulario-reportar-pago");

	var modalEnviosCompra = $("div#modal-envios-compra");

	var modalVerPagos = $("div#modal-ver-pagos");

	var modalVerColecciones = $("div#modal-ver-colecciones");

	var modalVerCompra = $("div#modal-ver-compra");

	var modalCalificarCompra = $("div#modal-calificar-compra");

	var modalCalificarVenta = $("div#modal-calificar-venta");

	var modalVerCalificaciones = $("div#modal-ver-calificaciones");

	var modalCambiarContrasena = $("div#modal-cambiar-contrasena");

	var imagenHover = $("#imagen-hover-cuenta");

	var modalUpload = $("div#galeria-minima-upload-modal");
	var idImagen;

	var modalManejarDatosPago = $("div#modal-manejar-datos-pago");

	$(".popover-user").popover();
	$("input#fecha-pago").datepicker({format: "dd/mm/yyyy"});

	$("a#btn-ver-colecciones").click(function(event) {
		event.preventDefault();
		modalVerColecciones.find('div.modal-body').html(ajax_load);
		modalVerColecciones.modal("show");
		$.post(base_url+'articulo/manejar_colecciones/'+$(this).attr('data-idusuario'), function(data, textStatus, xhr) {
			modalVerColecciones.find('div.modal-body').html(data);
		});
	});

	modalVerColecciones.on('click', 'a#nueva-coleccion', function(event) {
		event.preventDefault();
		modalVerColecciones.find('legend#titulo-formulario-coleccion').html("Nueva Colecci&oacute;n");
		modalVerColecciones.find('input#nombre_coleccion').val("");
		modalVerColecciones.find('input#descripcion_coleccion').val("");
		modalVerColecciones.find('input#tipo').val("crear");
		modalVerColecciones.find('div.contenedor-colecciones').hide();
		modalVerColecciones.find('div.contenedor-nueva-coleccion').show();
	});

	modalVerColecciones.on('click', 'a.editar-coleccion', function(event) {
		event.preventDefault();
		modalVerColecciones.find('legend#titulo-formulario-coleccion').html("Nueva Colecci&oacute;n");
		modalVerColecciones.find('input#nombre_coleccion').val($(this).attr('data-nombre'));
		modalVerColecciones.find('input#descripcion_coleccion').val($(this).attr('data-descripcion'));
		modalVerColecciones.find('input#id_coleccion').val($(this).attr('data-idcoleccion'));
		modalVerColecciones.find('input#tipo').val("editar");
		modalVerColecciones.find('div.contenedor-colecciones').hide();
		modalVerColecciones.find('div.contenedor-nueva-coleccion').show();
	});	

	modalVerColecciones.on('click', 'a.eliminar-coleccion', function(event) {
		event.preventDefault();
		/* Act on the event */
		var btn = $(this);
		if(confirm("Estas Seguro que desea eliminar esta coleccion?")){
			modalVerColecciones.find('input#id_coleccion').val(btn.attr('data-idcoleccion'));
			modalVerColecciones.find('input#tipo').val("eliminar");
			modalVerColecciones.find('form#registro-coleccion').submit();
		}
	});

	modalVerColecciones.on('click', 'button#agregar-nueva-coleccion', function(event) {
		event.preventDefault();
		var error = false;
		var inputNombre = modalVerColecciones.find('input#nombre_coleccion');
		inputNombre.parent().find('.label').remove();
		if(inputNombre.val() == ""){
			inputNombre.parent().find('span#error-nombre').html(mensajesValidacion.errores.campoRequerido);
			error = true;
		}
		var inputDescripcion = modalVerColecciones.find('input#descripcion_coleccion'); 
		inputDescripcion.parent().find('.label').remove();
		if(inputDescripcion.val() == ""){
			inputDescripcion.parent().find('span#error-descripcion').html(mensajesValidacion.errores.campoRequerido);
			error = true;
		}

		if (error == false) {
			modalVerColecciones.find('form#registro-coleccion').submit();	
		};
	});

	modalVerColecciones.on('click', 'button#cancelar-nueva-coleccion', function(event) {
		event.preventDefault();
		modalVerColecciones.find('div.contenedor-nueva-coleccion').hide();
		modalVerColecciones.find('div.contenedor-colecciones').show();
	});

	$("table.table-list-compras tbody td.disenador, table.table-list-compras tbody span.btn-reportar-pago").click(function (event){
		event.preventDefault();
		var td = $(this);
		$.post(base_url+'usuario/get_acordion_datos_pago/'+$(this).find("a.disenador").attr("data-iddisenador"), function(data, textStatus, xhr) {
			modalDatosPagos.find('div.modal-body div#acordion-recibidor').html(data);
			modalDatosPagos.find('input#id-compra-datos').val(td.find("a.disenador").attr('data-idcompra'));
			modalDatosPagos.find('input#id-usuario-receptor').val(td.find("a.disenador").attr('data-iddisenador'));
			modalDatosPagos.find('a#n-pagos').html(td.find("a.disenador").attr('data-npagos')+" Pagos Reportados");
			modalDatosPagos.find("div.pagos-reportados").hide();
			modalDatosPagos.find("div.reportar-pago-formulario").show();
			modalDatosPagos.modal("show");
		});
	});

	$("table.table-list-ventas tbody span.btn-ver-pagos").click(function (event){
		event.preventDefault();
		var idCompra = $(this).find("a.comprador").attr("data-idcompra");
		
		modalVerPagos.find("div#contenedor-pagos-venta").html(ajax_load);
		modalVerPagos.modal("show");
		$.post(base_url+'articulo/get_acordion_pagos_reportados/'+idCompra, function(data, textStatus, xhr) {
			modalVerPagos.find("div#contenedor-pagos-venta").html(data);
		});

	});

	$("table.table-list-ventas tbody a.btn-detalle-compra, table.table-list-compras tbody a.btn-detalle-compra").click(function (event){
		event.preventDefault();
		var idCompra = $(this).attr("data-idcompra");
		
		modalVerCompra.find("div.modal-body").html(ajax_load);
		modalVerCompra.find("#modal-titulo").html($(this).attr("title"));
		modalVerCompra.modal("show");
		$.post(base_url+'articulo/detalles_compra/'+idCompra, function(data, textStatus, xhr) {
			modalVerCompra.find("div.modal-body").html(data);
		});

	});

	$("table.table-list-compras tbody a.btn-detalle-vendedor").click(function (event){
		event.preventDefault();
		var idUsuario = $(this).attr("data-idusuario");
		
		modalVerCompra.find("div.modal-body").html(ajax_load);
		modalVerCompra.find("#modal-titulo").html($(this).attr("title"));
		modalVerCompra.modal("show");
		$.post(base_url+'usuario/detalles_vendedor/'+idUsuario, function(data, textStatus, xhr) {
			modalVerCompra.find("div.modal-body").html(data);
		});

	});

	$("table.table-list-ventas tbody a.btn-detalle-comprador").click(function (event){
		event.preventDefault();
		var idUsuario = $(this).attr("data-idusuario");
		modalVerCompra.find("div.modal-body").html(ajax_load);
		modalVerCompra.find("#modal-titulo").html($(this).attr("title"));
		modalVerCompra.modal("show");
		$.post(base_url+'usuario/detalles_comprador/'+idUsuario, function(data, textStatus, xhr) {
			modalVerCompra.find("div.modal-body").html(data);
		});

	});

	$("a#n-pagos").click(function (event){
		event.preventDefault();
		var pagosReportados = modalDatosPagos.find("div#contenedor-pagos");
		var idCompra = modalDatosPagos.find("input#id-compra-datos").val();

		pagosReportados.html(ajax_load);

		$.post(base_url+'articulo/get_acordion_pagos_reportados/'+idCompra, function(data, textStatus, xhr) {
			pagosReportados.html(data);
		});

		$("div.reportar-pago-formulario").hide();
		$("div.pagos-reportados").show();
	});

	$("button#regresar-formulario").click(function (event){
		event.preventDefault();
		
		$("div.pagos-reportados").hide();
		$("div.reportar-pago-formulario").show();
		
	});

	$("body").on("click", "div#modal-datos-pago #accordion-datos-pago .panel-heading", function (event) {
		var panel = $(this).parent();
		var bancoSeleccionado = $("span#banco-seleccionado");
		var inputBancoSeleccionado = $("input#id-datos-pago");
		if (panel.hasClass('panel-success')) {
			panel.removeClass('panel-success').addClass('panel-default');
			bancoSeleccionado.html("-ninguno-");
			inputBancoSeleccionado.val("");
		} else{
			panel.removeClass('panel-default').addClass('panel-success');
			bancoSeleccionado.html($(this).attr("data-banco"));
			inputBancoSeleccionado.val($(this).attr("data-iddatospago"));
		};
		
		panel.parent().find(".panel").not(panel).removeClass('panel-success').addClass('panel-default');
	}).on('click', 'button#guardar-direccion-alt', function(event) {
		event.preventDefault();
		var idCompra = $(this).attr("data-idcompra");
		var direccionAlterna = $("textarea#direccion-alternativa").val();

		modalEnviosCompra.find('div#direccion-envio').html(ajax_load);
		$.post(base_url+'articulo/guardar_direccion_alt', {id_compra: idCompra, direccion_alt: direccionAlterna}, function(data, textStatus, xhr) {
			alert("Direccion Guardada");
			modalEnviosCompra.modal("hide");
		});
	}).on('click', 'button.marcar-verificado', function(event) {
		event.preventDefault();
		
		var boton = $(this);
		var idPago = boton.attr("data-idpago");
		var span = boton.find('span').detach();
		boton.attr("disabled", "disabled").html("Procesando...");
		$.post(base_url+'articulo/marcar_verificado/'+idPago, function(data, textStatus, xhr) {
			if (parseInt(data) == 0) {
				boton.removeAttr("disabled").removeClass('btn-danger').addClass('btn-success').html(span).find('span').removeClass('glyphicon-remove').addClass('glyphicon-ok').after(" Marcar como Verificado");
				$("span.label-verificado").removeClass('label-success').addClass('label-danger').html("Por Verificar");
			} else{
				boton.removeAttr("disabled").removeClass('btn-success').addClass('btn-danger').html(span).find('span').removeClass('glyphicon-ok').addClass('glyphicon-remove').after(" Desmarcar como Verificado");
				$("span.label-verificado").removeClass('label-danger').addClass('label-success').html("Verificado");
			};
		});
	});

	modalDatosPagos.on("shown.bs.modal", function(event){
		$(".chosen").chosen();
	});

	$.validator.addMethod('selectcheck', function (value, element) {
        return (value != '');
    }, "Campo requerido");

	formulario.validate({
		ignore: "",
		rules: {
	  		"banco-emisor": {
	  			selectcheck: true
	  		},
	  		"fecha-pago": { required: true },
	  		"monto-pago": { required: true },
	  		"id-datos-pago": { required: true }
	  	},
	  	messages: {
	  		"banco-emisor": {
	  			selectcheck: mensajesValidacion.errores.campoRequerido
	  		},
	  		"fecha-pago": {
	  			required: mensajesValidacion.errores.campoRequerido
	  		},
	  		"monto-pago": {
	  			required: mensajesValidacion.errores.campoRequerido	
	  		},
	  		"id-datos-pago": {
	  			required: mensajesValidacion.errores.campoRequerido	
	  		}
	  	},
	    validClass: "has-success",
	    success: function(label) {
    	    label.parent().addClass("has-success");
  	    },
  	     highlight: function(element, errorClass) {
  	      $(element).parent().addClass("has-error");
  	    }
	});

	$("a.btn-envios").click(function(event) {
		event.preventDefault();

		var idCompra = $(this).attr("data-idcompra");
		var idUsuario = $(this).attr("data-idusuario");

		modalEnviosCompra.find("div#direccion-envio").html(ajax_load);

		$.post(base_url+'articulo/get_direcciones_envio/'+idUsuario+"/"+idCompra, function(data, textStatus, xhr) {
			modalEnviosCompra.find("div#direccion-envio").html(data);
			modalEnviosCompra.modal("show");
		});
	});

	/*$("a#imagen-usuario-cuenta").hover(function() {
		imagenHover.show();
	}, function() {
		imagenHover.hide();
	});*/

	$("a#btn-cambiar-img-perfil").click(function(event) {
		/* Act on the event */
		event.preventDefault();
		idImagen = $(this).attr("data-idimagen");

		modalUpload.modal("show");
	});

	$("form#galeria-subir-imagen").validate({
			  rules: {
			  	"galeria-imagen-nombre": {
			  		required: true,
			  		remote: {url: base_url+"galeria/check_nombre",
    		    			type: "post",
    		    			data: {
    		    			  nombre: function() {
    		    			    return $( "#galeria-imagen-nombre" ).val();
    		    			  }
    		    			}
    		    		}
			  	}
			  },
			  messages: {
			  		"galeria-imagen-nombre": {
			  			required: mensajesValidacion.errores.campoRequerido,
			  			remote: mensajesValidacion.errores.nombreImagenUsado
			  		},
			  		"galeria-imagen": mensajesValidacion.errores.campoRequerido
			  	},
			  validClass: "has-success",
			  success: function(label) {
  			    label.parent().addClass("has-success");
  			  },
  			   highlight: function(element, errorClass) {
  			    $(element).parent().addClass("has-error");
  			  },
  			  submitHandler: function(form) {
  			  	var input = document.getElementById("galeria-imagen"),  
		      	formdata = false;  
		      
		  		if (window.FormData) {  
		    		formdata = new FormData();  
		  		}
		
		  		var i = 0, len = input.files.length, img, reader, file;  
		       
		    	for ( ; i < len; i++ ) {  
			      file = input.files[i];   
		    	}
		
		    	if ( window.FileReader ) {  
				  reader = new FileReader();    
				  reader.readAsDataURL(file);  
				}  
				if (formdata) {  
				  formdata.append("galeria-imagen", file);
				  formdata.append('galeria-nombre', $(form).find("#galeria-imagen-nombre").val());
				  formdata.append('galeria-descripcion', $(form).find("#galeria-imagen-descripcion").val());
				  formdata.append('galeria-album', 0);
				}
				$("div#galeria-minima-upload-modal").find('div#galeria-subir-resultado').html(ajax_load);
				$.ajax({  
				  url: base_url+"galeria/upload_image_perfil",  
				  type: "POST",  
				  data: formdata,  
				  processData: false,  
				  contentType: false,  
				  success: function (res) {  
				  	modalUpload.find('input#galeria-imagen').val('');
				  	modalUpload.find('input#galeria-imagen-nombre').val('');
				  	modalUpload.find('input#galeria-imagen-descripcion').val('');
				    location.href= base_url+"usuario/cuenta";
				  }  
				});
  			  }
			});

	$("a.btn-manejar-datos-pago").click(function  (event) {
		event.preventDefault();
		modalManejarDatosPago.find('div.contenedor-agregar-datos').hide();
		modalManejarDatosPago.find('div.contenedor-datos-pago').show();
		modalManejarDatosPago.modal("show");
	});

	modalManejarDatosPago.on('click', 'a#nuevo-datos-pago', function(event) {
		event.preventDefault();
		
		var parent = $(this).parent();
		modalManejarDatosPago.find('select#banco option').removeAttr('selected');
		modalManejarDatosPago.find('select#banco option[value=""]').attr("selected", "selected");
		modalManejarDatosPago.find('select#tipo-cuenta option').removeAttr('selected');
		modalManejarDatosPago.find('select#tipo-cuenta option[value=""]').attr("selected", "selected");
		modalManejarDatosPago.find('input#numero-cuenta').val("");
		modalManejarDatosPago.find('input#titular-cuenta').val("");
		modalManejarDatosPago.find('input#cedula-cuenta').val("");
		modalManejarDatosPago.find('input#correo-cuenta').val("");
		modalManejarDatosPago.find('input#tipo-guardar').val("nuevo");
		modalManejarDatosPago.find('input#id-datos-pago').val("");
		modalManejarDatosPago.find('div#contenedor-datos legend').html("Nuevo");
		modalManejarDatosPago.find('div.contenedor-datos-pago').hide();
		modalManejarDatosPago.find('div.contenedor-agregar-datos').show();
	});

	modalManejarDatosPago.on('click', 'button#cerrar-datos', function(event) {
		event.preventDefault();
		modalManejarDatosPago.find('div.contenedor-agregar-datos').hide();
		modalManejarDatosPago.find('div.contenedor-datos-pago').show();
	});

	modalManejarDatosPago.on('click', 'a.editar-datos-pago', function(event) {
		event.preventDefault();
		/* Act on the event */
		var parent = $(this).parent();
		modalManejarDatosPago.find('select#banco option').removeAttr('selected');
		modalManejarDatosPago.find('select#banco option[value="'+parent.find("span.banco").html()+'"]').attr("selected", "selected");
		modalManejarDatosPago.find('select#tipo-cuenta option').removeAttr('selected');
		modalManejarDatosPago.find('select#tipo-cuenta option[value="'+parent.find("span.tipo-cuenta").attr("value")+'"]').attr("selected", "selected");
		modalManejarDatosPago.find('input#numero-cuenta').val(parent.find("span.numero-cuenta").html());
		modalManejarDatosPago.find('input#titular-cuenta').val(parent.find("span.titular-cuenta").html());
		modalManejarDatosPago.find('input#cedula-cuenta').val(parent.find("span.cedula-cuenta").html());
		modalManejarDatosPago.find('input#correo-cuenta').val(parent.find("span.correo-cuenta").html());
		modalManejarDatosPago.find('input#tipo-guardar').val("editar");
		modalManejarDatosPago.find('input#id-datos-pago').val($(this).attr("data-iddatospago"));
		modalManejarDatosPago.find('div#contenedor-datos legend').html("Editar");
		modalManejarDatosPago.find('div.contenedor-datos-pago').hide();
		modalManejarDatosPago.find('div.contenedor-agregar-datos').show();
	});

	modalManejarDatosPago.on('click', 'a.eliminar-datos-pago', function(event) {
		event.preventDefault();
		
		modalManejarDatosPago.find('input#tipo-guardar').val("borrar");
		modalManejarDatosPago.find('input#id-datos-pago').val($(this).attr("data-iddatospago"));

		if (confirm("Seguro que desea borrar el registro?")) {
			modalManejarDatosPago.find("form#formulario-datos-pago").submit();
		};
	});

	$("a.btn-calificar-compra").click(function(event) {
		event.preventDefault();

		modalCalificarCompra.find('.modal-body').html(ajax_load);
		modalCalificarCompra.modal("show");
		$.post(base_url+'articulo/formulario_calificacion_compra', {id_compra:$(this).attr('data-idcompra'), id_usuario_vendedor:$(this).attr('data-idusuariovendedor')}, function(data, textStatus, xhr) {
			/*optional stuff to do after success */
			modalCalificarCompra.find('.modal-body').html(data);
		});
	});

	modalCalificarCompra.on('click', 'button.btn-calificar', function(event) {
		event.preventDefault();
		/* Act on the event */
		var error = false;
		var tipo = modalCalificarCompra.find("input[name=tipo]:checked").val();
		var recibido = modalCalificarCompra.find("input[name=recibido]:checked").val();
		var comentario = modalCalificarCompra.find('textarea[name=comentario]');
		modalCalificarCompra.find('div.label').remove();
		if (tipo == undefined) {
			modalCalificarCompra.find("span#error-tipo div.label").remove();
			modalCalificarCompra.find("span#error-tipo").html(mensajesValidacion.errores.campoRequerido);
			error = true;
		}
		if (recibido == undefined) {
			modalCalificarCompra.find("span#error-recibido div.label").remove();
			modalCalificarCompra.find("span#error-recibido").html(mensajesValidacion.errores.campoRequerido);
			error = true;
		}
		if (comentario.val() == "") {
			modalCalificarCompra.find("span#error-comentario div.label").remove();
			modalCalificarCompra.find("span#error-comentario").html(mensajesValidacion.errores.campoRequerido);
			error = true;
		};

		if (error == false) {
			modalCalificarCompra.find('form#calificar-compra').submit();
		};
	});

	$("a.btn-calificar-venta").click(function(event) {
		event.preventDefault();

		modalCalificarVenta.find('.modal-body').html(ajax_load);
		modalCalificarVenta.modal("show");
		$.post(base_url+'articulo/formulario_calificacion_venta', {id_compra:$(this).attr('data-idcompra'), id_usuario_comprador:$(this).attr('data-idusuariocomprador')}, function(data, textStatus, xhr) {
			/*optional stuff to do after success */
			modalCalificarVenta.find('.modal-body').html(data);
		});
	});

	modalCalificarVenta.on('click', 'button.btn-calificar', function(event) {
		event.preventDefault();
		/* Act on the event */
		var error = false;
		var tipo = modalCalificarVenta.find("input[name=tipo]:checked").val();
		var comentario = modalCalificarVenta.find('textarea[name=comentario]');
		modalCalificarVenta.find('div.label').remove();
		if (tipo == undefined) {
			modalCalificarVenta.find("span#error-tipo div.label").remove();
			modalCalificarVenta.find("span#error-tipo").html(mensajesValidacion.errores.campoRequerido);
			error = true;
		}
		if (comentario.val() == "") {
			modalCalificarVenta.find("span#error-comentario div.label").remove();
			modalCalificarVenta.find("span#error-comentario").html(mensajesValidacion.errores.campoRequerido);
			error = true;
		};

		if (error == false) {
			modalCalificarVenta.find('form#calificar-venta').submit();
		};
	});

	$("a.btn-ver-calificaciones").click(function(event) {
		/* Act on the event */
		event.preventDefault();

		modalVerCalificaciones.find('.modal-body').html(ajax_load);
		modalVerCalificaciones.modal("show");
		$.post(base_url+'articulo/ver_calificaciones', {id_compra:$(this).attr('data-idcompra'), por:$(this).attr('data-por')}, function(data, textStatus, xhr) {
			/*optional stuff to do after success */
			modalVerCalificaciones.find('.modal-body').html(data);
		});
	});

	$("a#btn-cambiar-contrasena").click(function(event) {
		event.preventDefault();
		modalCambiarContrasena.modal("show");
	});

	modalCambiarContrasena.on('click', '#btn-cambiar', function(event) {
		event.preventDefault();
		var actual = modalCambiarContrasena.find('input#actual').val();
		var nueva = modalCambiarContrasena.find('input#nueva').val();
		var confirmacion = modalCambiarContrasena.find('input#confirmacion').val();
		var error = false;

		modalCambiarContrasena.find('.label').remove();

		if (actual == '') {
			error = true;
			modalCambiarContrasena.find('#error-actual').html(mensajesValidacion.errores.campoRequerido);
		}

		if (nueva == '') {
			error = true;
			modalCambiarContrasena.find('#error-nueva').html(mensajesValidacion.errores.campoRequerido);
		}

		if (nueva != confirmacion) {
			error = true;
			modalCambiarContrasena.find('#error-confirmacion').html(mensajesValidacion.errores.passwordsDiferentes);
		}

		if (error == false) {
			$.post(base_url+'usuario/cambiar_contrasena', {actual:actual, nueva:nueva}, function(data, textStatus, xhr) {
				if (data == false) {
					modalCambiarContrasena.find('p#error-cambiar').show();
				}else{
					location.reload();
				}
			});
		}

	});

});