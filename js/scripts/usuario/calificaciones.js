$(function(){
	var modal = $("div#modal-ver-calificaciones");

	modal.on('click', 'input.ver-radio', function(event) {
		var ul = modal.find('ul#lista-calif');
		if ($(this).val() == "positivas") {
			ul.find('li.positiva').show();
			ul.find('li.negativa').hide();
			ul.find('li.neutral').hide();
		}else if ($(this).val() == "negativas") {
			ul.find('li.positiva').hide();
			ul.find('li.negativa').show();
			ul.find('li.neutral').hide();
		}else if ($(this).val() == "neutrales") {
			ul.find('li.positiva').hide();
			ul.find('li.negativa').hide();
			ul.find('li.neutral').show();
		}else {
			ul.find('li.positiva').show();
			ul.find('li.negativa').show();
			ul.find('li.neutral').show();
		}
	});
});