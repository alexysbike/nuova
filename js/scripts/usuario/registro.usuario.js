$(document).ready(function(){

	$.validator.addMethod("dateCheck", function (value, element) {
        var currVal = value;
	    if(currVal == '')
	        return false;

	    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
	    var dtArray = currVal.match(rxDatePattern); // is format OK?

	    if (dtArray == null) 
	        return false;

	    //Checks for dd/mm/yyyy format.
	    dtMonth = dtArray[3];
	    dtDay= dtArray[1];
	    dtYear = dtArray[5];        

	    if (dtMonth < 1 || dtMonth > 12) 
	        return false;
	    else if (dtDay < 1 || dtDay> 31) 
	        return false;
	    else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) 
	        return false;
	    else if (dtMonth == 2) 
	    {
	        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
	        if (dtDay> 29 || (dtDay ==29 && !isleap)) 
	                return false;
	    }

	    return true;
    }, 'Error en formato de fecha');

    $.validator.addMethod("ageCheck", function (value, element) {
        if (isValidDate(value)){
        var fechaActual = new Date()
		var diaActual = fechaActual.getDate();
		var mmActual = fechaActual.getMonth() + 1;
		var yyyyActual = fechaActual.getFullYear();
		FechaNac = value.split("/");
		var diaCumple = FechaNac[0];
		var mmCumple = FechaNac[1];
		var yyyyCumple = FechaNac[2];
		//retiramos el primer cero de la izquierda
		if (mmCumple.substr(0,1) == 0) {
		mmCumple= mmCumple.substring(1, 2);
		}
		//retiramos el primer cero de la izquierda
		if (diaCumple.substr(0, 1) == 0) {
		diaCumple = diaCumple.substring(1, 2);
		}
		var edad = yyyyActual - yyyyCumple;

		//validamos si el mes de cumpleaños es menor al actual
		//o si el mes de cumpleaños es igual al actual
		//y el dia actual es menor al del nacimiento
		//De ser asi, se resta un año
		if ((mmActual < mmCumple) || (mmActual == mmCumple && diaActual < diaCumple)) {
		edad--;
		}
		
		if (edad > 18){return true;}else{return false;}

		}else{ return true; }
    }, 'Debes ser mayor de edad para poder registrarte');

	$("input.date").datepicker({format: "dd/mm/yyyy"});

	var form = $("form#registro-usuario");

	form.validate({
	  rules: {
	  	"correo": {
	  		required: true,
	  		email: true,
	  		remote: {url: base_url+"usuario/check_correo",
        			type: "post",
        			data: {
        			  correo: function() {
        			    return $( "#correo" ).val();
        			  }
        			}
        		}
	  	},
	  	"confirmar-correo": {
	  			equalTo: '#correo',
	  			required: true
	  		},
	  	"password": "required",
	  	"confirmar-password": {
	  			equalTo: '#password',
	  			required: true
	  	}
	  },
	  messages: {
	  		"correo": {
	  			required: mensajesValidacion.errores.campoRequerido,
	  			email: mensajesValidacion.errores.correoInvalido,
	  			remote: mensajesValidacion.errores.correoUsado
	  		},
	  		"confirmar-correo": { equalTo: mensajesValidacion.errores.correosDiferentes, 
	  							  email: mensajesValidacion.errores.correoInvalido, 
	  							  required: mensajesValidacion.errores.campoRequerido },
	  		"password": mensajesValidacion.errores.campoRequerido,
	  		"confirmar-password": { equalTo: mensajesValidacion.errores.passwordsDiferentes, 
	  								required: mensajesValidacion.errores.campoRequerido }
	  	},
	  validClass: "has-success",
	  success: function(label) {
  	    label.parent().addClass("has-success");
  	  },
  	   highlight: function(element, errorClass) {
  	    $(element).parent().addClass("has-error");
  	  },
	  submitHandler: function(form) {
	    $(form).ajaxSubmit({target:"#columna-1",
							success:function(responseText, statusText, xhr, $form){
								$.post(base_url+"usuario/get_informacion/registro_perfil", function(data, textStatus, xhr) {
									$("#columna-2").html(data);

									$("input.date").datepicker({format: "dd/mm/yyyy"});
									var formEditar = $("form#editar-perfil");

									formEditar.validate({
									  rules: {
									  	"alias": {
									  		required: true,
									  		remote: {url: base_url+"usuario/check_alias",
									    			type: "post",
									    			data: {
									    			  alias: function() {
									    			    return $( "#alias" ).val();
									    			  }
									    			}
									    		}
									  	},
									  	"direccion": {
									  		required: true
									  	},
									  	"telefono": {
									  		required: true
									  	},
									  	"celular": {
									  		required: true
									  	},
									  	"ciudad": {
									  		required: true
									  	},
									  	"nombre": {
									  		required: true
									  	},
									  	"apellido": {
									  		required: true
									  	},
									  	"cedula": {
									  		required: true
									  	},
									  	"fecha-nacimiento": {
									  		required: true,
									  		dateCheck: true,
									  		ageCheck: true
									  	}
									  },
									  messages: {
									  		"alias": {
									  			required: mensajesValidacion.errores.campoRequerido,
									  			remote: mensajesValidacion.errores.aliasUsado
									  		},
									  		"direccion": {
									  			required: mensajesValidacion.errores.campoRequerido
									  		},
									  		"telefono": {
									  			required: mensajesValidacion.errores.campoRequerido
									  		},
									  		"celular": {
									  			required: mensajesValidacion.errores.campoRequerido
									  		},
									  		"ciudad": {
									  			required: mensajesValidacion.errores.campoRequerido
									  		},
									  		"nombre": {
									  			required: mensajesValidacion.errores.campoRequerido
									  		},
									  		"apellido": {
									  			required: mensajesValidacion.errores.campoRequerido
									  		},
									  		"cedula": {
									  			required: mensajesValidacion.errores.campoRequerido
									  		},
									  		"fecha-nacimiento": {
										  		required: mensajesValidacion.errores.campoRequerido,
										  		dateCheck: mensajesValidacion.errores.errorFormato,
										  		ageCheck: mensajesValidacion.errores.edadRequerida
										  	}
									  	},
									  validClass: "has-success",
									  success: function(label) {
  									    label.parent().addClass("has-success");
  									  },
  									   highlight: function(element, errorClass) {
  									    $(element).parent().addClass("has-error");
  									  }
									});
								});
							}});
	  }
	});

	$("button#crear-usuario").click(function(){
		//event.preventDefault();


		/*$("form#registro-usuario").ajaxSubmit({target:"#columna-1",
											 success:function(responseText, statusText, xhr, $form){
											 	$.post(base_url+"usuario/get_informacion/registro_perfil", function(data, textStatus, xhr) {
											 		$("#columna-2").html(data);
											 	});
											 }});*/
	});

	var formEditar = $("form#editar-perfil");

	if(formEditar != 0){
		formEditar.validate({
		  rules: {
		  	"alias": {
		  		required: true,
		  		remote: {url: base_url+"usuario/check_alias",
		    			type: "post",
		    			data: {
		    			  alias: function() {
		    			    return $( "#alias" ).val();
		    			  }
		    			}
		    		}
		  	},
		  	"direccion": {
		  		required: true
		  	},
		  	"telefono": {
		  		required: true
		  	},
		  	"celular": {
		  		required: true
		  	},
		  	"ciudad": {
		  		required: true
		  	},
		  	"nombre": {
		  		required: true
		  	},
		  	"apellido": {
		  		required: true
		  	},
		  	"cedula": {
		  		required: true
		  	},
		  	"fecha-nacimiento": {
		  		required: true,
		  		dateCheck: true,
		  		ageCheck: true
		  	}
		  },
		  messages: {
		  		"alias": {
		  			required: mensajesValidacion.errores.campoRequerido,
		  			remote: mensajesValidacion.errores.aliasUsado
		  		},
		  		"direccion": {
		  			required: mensajesValidacion.errores.campoRequerido
		  		},
		  		"telefono": {
		  			required: mensajesValidacion.errores.campoRequerido
		  		},
		  		"celular": {
		  			required: mensajesValidacion.errores.campoRequerido
		  		},
		  		"ciudad": {
		  			required: mensajesValidacion.errores.campoRequerido
		  		},
		  		"nombre": {
		  			required: mensajesValidacion.errores.campoRequerido
		  		},
		  		"apellido": {
		  			required: mensajesValidacion.errores.campoRequerido
		  		},
		  		"cedula": {
		  			required: mensajesValidacion.errores.campoRequerido
		  		},
		  		"fecha-nacimiento": {
			  		required: mensajesValidacion.errores.campoRequerido,
			  		dateCheck: mensajesValidacion.errores.errorFormato,
			  		ageCheck: mensajesValidacion.errores.edadRequerida
			  	}
		  	},
		  validClass: "has-success",
		  success: function(label) {
  		    label.parent().addClass("has-success");
  		  },
  		   highlight: function(element, errorClass) {
  		    $(element).parent().addClass("has-error");
  		  }
		});
	}
});

