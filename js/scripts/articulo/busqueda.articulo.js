$(function(){
	var formBusqueda = $("form#busqueda-pagina");

	$("ul#nav-ordenar a").click(function(event) {
		event.preventDefault();
		formBusqueda.find("input#ordenar").val($(this).attr('data-ordenar'));
		formBusqueda.submit();
	});

	$("ul#nav-genero a").click(function(event) {
		event.preventDefault();
		formBusqueda.find("input#filtrar").val($(this).attr('data-filtrar'));
		formBusqueda.find("input#valor").val($(this).attr('data-valor'));
		formBusqueda.submit();
	});

	$("ul#nav-categoria a").click(function(event) {
		event.preventDefault();
		formBusqueda.find("input#categoria").val($(this).attr('data-valor'));
		formBusqueda.submit();
	});

	$("ul.pagination a").click(function(event) {
		event.preventDefault();
		formBusqueda.find("input#pagina").val($(this).attr('data-pagina'));
		formBusqueda.submit();
	});
});