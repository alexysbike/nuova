$(function() {
	var modal = $("div#nueva-compra");
	
	//Movimientos de las cosas del articulo
	var presentacioTipo = $("input#presentacion-tipo").val();

	var tallaSelect = $("div#select-presentaciones select");
	var cantidadInput = $("input#cantidad");
	var min=0;
	var max=0;
	var unidadesDisponibles = $("p#unidades-disponibles");
	var nombrePresentacion = $("p#nombre-presentacion");
	var aEsconder = $("div#a-esconder");
	var contenedorImagenes = $("#carousel-imagenes");
	var mensajeError = $("span#mensaje-error-compra");
	var checkDireccionAlt = $("input#usar-direccion-alt");

	$('#thumbnails-articulo').Thumbelina({
                    $bwdBut:$('#thumbnails-articulo .left'),    // Selector to left button.
                    $fwdBut:$('#thumbnails-articulo .right')    // Selector to right button.
                });

	$("#thumbnails-articulo").on('click', 'img', function(event) {
		event.preventDefault();
		$("#carousel-imagenes").find("div:not("+$(this).attr("data-target")+")").hide();
		$("#carousel-imagenes").find("div"+$(this).attr("data-target")).show();
	});



	checkDireccionAlt.click(function (event) {
		$("div#contenedor-direccion-alt").toggle(this.checked);
	});

	$("ul#presentaciones a").click(function(event){
		event.preventDefault();
		var idPresentacion = $(this).parent().attr("data-idpresentacion");

		$("#thumbnails-articulo").find("li").hide();
		$("#thumbnails-articulo").find("li[data-idpresentacion='"+idPresentacion+"']").show();
		contenedorImagenes.find("div").hide();

		aEsconder.hide();

		modal.find('[name=id-presentacion]').val(idPresentacion);
		modal.find('span#confirmacion-presentacion').html($(this).attr("title"));

		nombrePresentacion.html($(this).attr("title"));
		tallaSelect.val("");
		unidadesDisponibles.html("Elija la talla");
		cantidadInput.val(0);
	});

	tallaSelect.change(function(event){
		max = parseInt($(this).find(":selected").attr("data-cantidad"));
		modal.find("[name=id-talla]").val($(this).find(":selected").val());
		modal.find("span#confirmacion-talla").html($(this).find(":selected").html());
		unidadesDisponibles.html(max+" Unidades Disponibles");
		cantidadInput.val(0);
	});

	$("button#mas-cantidad").click(function(event){
		event.preventDefault();

		var i = parseInt(cantidadInput.val());

		if (i<max) {
			cantidadInput.val(i+1);
		}else{
			cantidadInput.val(max);
		}
	});

	$("button#menos-cantidad").click(function(event){
		event.preventDefault();

		var i = parseInt(cantidadInput.val());

		if (i>min) {
			cantidadInput.val(i-1);
		}else{
			cantidadInput.val(min);
		}
	});

	cantidadInput.blur(function(){
		var i = cantidadInput.val();

		if(isNaN(i)==false){
			i = parseInt(i);
			if (i<min) {
				cantidadInput.val(min);
			}else if (i>max) {
				cantidadInput.val(max);
			}
		}else{
			cantidadInput.val(0);
		}
	});

	//Boton de Compra y WishList
	var verificarLogueo = function (){
		var idUsuarioLogueado = 0;

		$.post(base_url+'usuario/verificar_login', function(data, textStatus, xhr) {
			idUsuarioLogueado = data;
			if (idUsuarioLogueado == "0") {
				return false;
			} else{
				return idUsuarioLogueado;
			}
		});
	};

	$("button#boton-comprar").click(function(event) {
		event.preventDefault();
		var idUsuarioLogueado;
		var idDisenador = parseInt($("input#id-disenador").val());
		$.post(base_url+'usuario/verificar_login', function(data, textStatus, xhr) {
			idUsuarioLogueado = data;
			if (idUsuarioLogueado == 0) {
				alert("Debe de Estar Logueado para poder hacer la Compra");
				$("a#boton-login").click();
			} else{
				if (idUsuarioLogueado == idDisenador) {
					alert("No puede Comprar su propio Articulo");
				} else{
					if(presentacioTipo == "varias" && parseInt(modal.find("[name=id-presentacion]").val()) == 0){
						mensajeError.html("Debe Escojer una Presentaci&oacute;n");
						mensajeError.show();
					}else if(parseInt(modal.find('[name=id-talla]').val()) == 0){
						mensajeError.html("Debe Escojer una Talla");
						mensajeError.show();
					}else if(parseInt(cantidadInput.val()) == 0){
						mensajeError.html("La cantidad debe ser mayor que 0");
						mensajeError.show();
					}else{
						mensajeError.hide();
						modal.find('a#imagen-articulo-pequena').attr("style", 'background-image:url('+contenedorImagenes.find("div#carousel-imagenes>div.item:first>a").attr("href")+')');
						modal.find("[name=id-usuario-comprador]").val(parseInt(idUsuarioLogueado));
						modal.find("[name=articulo-cantidad]").val(cantidadInput.val());
						modal.find("span#confirmacion-cantidad").html(cantidadInput.val()+" Unidades");
						modal.find("span#confirmacion-total").html("Bs. "+(parseInt(cantidadInput.val())*parseFloat($("[name=articulo-precio]").val())));
						modal.modal('show');
					}
					
				}
			}
		});

	});

	$("a#marcar-favorito").click(function (event){
		event.preventDefault();
		var btn = $(this);
		$.post(base_url+"articulo/marcar_favorito/"+$(this).attr("data-idarticulo"),function (data){
			if (parseInt(data) == 0) {
				btn.attr("title", "Marcar Favorito").attr("style", "color: grey").find("span").removeClass("glyphicon-star").addClass("glyphicon-star-empty");
			} else{
				btn.attr("title", "Desmarcar Favorito").attr("style", "color: goldenrod").find("span").removeClass("glyphicon-star-empty").addClass("glyphicon-star");
			};
		});

	});

	$("div#contenedor-preguntas").on('click', 'a#btn-preguntar', function(event) {
		event.preventDefault();
		var param = {pregunta : $(this).parent().parent().find("textarea#pregunta").val(),
					 id_usuario : $(this).attr('data-idusuario'),
					 id_articulo : $(this).attr('data-idarticulo')};
		var aRemover = $(this).parent().parent();
		aRemover.html(ajax_load);
		$.post(base_url+'articulo/hacer_pregunta', param, function(data, textStatus, xhr) {
			var cont = aRemover.parent();
			aRemover.remove();
			cont.prepend(data);
		});
	});

	$("div#contenedor-preguntas").on('click', 'a.btn-responder', function(event) {
		event.preventDefault();
		var param = {respuesta : $(this).parent().parent().find("textarea.respuesta").val(),
					 id_pregunta : $(this).attr('data-idpregunta')};
		var contenedorRespuesta = $(this).parent().parent();
		contenedorRespuesta.html(ajax_load);
		$.post(base_url+'articulo/hacer_respuesta', param, function(data, textStatus, xhr) {
			contenedorRespuesta.html(data);
		});
	});

});