var formularioArticulo;
var idUsuario;
var nextPresentacion = 1;
$(function(){
	formularioArticulo = $("form#registro-articulo");
	idUsuario = formularioArticulo.attr('data-idusuario');
	
	$(".chosen").chosen();
	$(".color-picker").colorpicker();
	$("input.date").datepicker({format: "dd/mm/yyyy"});
	
	formularioArticulo.find("select#id_coleccion").change(function(event) {
		if ($(this).val() < 0) {
			coleccionWidget.nuevo(idUsuario,"id_coleccion");
		}
	});

	formularioArticulo.find('a#agregar-talla').click(function(event) {
		event.preventDefault();
		$(this).before('<div class="input-group"><input id="talla[]" name="talla[]" class="form-control" placeholder="Ingrese tallas del art&iacute;culo..." type="text" required=""><div class="input-group-btn"><button type="button" class="btn btn-default btn-eliminar-talla">Eliminar</button></div></div>');
	});

	formularioArticulo.on('click', 'button.btn-eliminar-talla', function(event) {
		event.preventDefault();
		$(this).parent().parent().remove();
	});

	formularioArticulo.find('ul#presentaciones-tipo a').click(function(event) {
		formularioArticulo.find('input#presentacion-tipo').val($(this).attr('data-tipo'));
	});

	formularioArticulo.find('div#presentaciones-varias a#btn-agregar-presentacion').click(function(event) {
		event.preventDefault();

		$(this).parent().parent().find('li.active').removeClass('active');
		nextPresentacion++;
		$(this).parent().before('<li class="active"><a href="#presentacion-'+nextPresentacion+'" data-toggle="tab">'+nextPresentacion+'</a></li>');
		$(this).parent().parent().parent().find("div.tab-content .active").removeClass('active').find('div.contenedor-imagenes').removeClass('contenedor-imagenes-activo');
		$(this).parent().parent().parent().find("div.tab-content").append('<div class="tab-pane active" id="presentacion-'+nextPresentacion+'"><div class="panel panel-default"><div class="panel-body"><a href="#" class="btn btn-danger btn-xs btn-eliminar-presentacion"><span class="glyphicon glyphicon-minus"></span> Eliminar Presentaci&oacute;n</a><div class="form-group"><label class="col-md-3 control-label">Nombre</label>  <div class="col-md-8"><input type="text" name="presentacion-nombre[]" class="form-control input-md" placeholder="Nombre de la Presentaci&oacute;n..."></div></div><div class="form-group"><label class="col-md-3 control-label">Color del Icono</label><div class="col-md-8"><div class="input-group color-picker"><input type="text" value="#ffffff" class="form-control" name="presentacion-color[]" /><span class="input-group-addon"><i></i></span></div></div></div><div class="form-group"><div id="contenedor-imagenes-'+nextPresentacion+'" class="col-md-10 col-md-offset-1 contenedor-imagenes contenedor-imagenes-activo"><input type="hidden" name="id_imagenes[]" class="selected-images" value=""><div class="panel panel-default"><div class="panel-heading"><strong>Imagenes</strong><div class="btn-group"><a href="#" class="btn btn-default galeria-boton-agregar-imagen" id="" data-estado="normal" title="Agregar Imagenes"><span class="glyphicon glyphicon-plus"></span></a><a href="#" class="btn btn-default opciones-imagen" id="galeria-boton-eliminar-imagen" data-estado="normal" title="Eliminar Imagenes" disabled="disabled"><span class="glyphicon glyphicon-minus"></span></a><a href="#" class="btn btn-default opciones-imagen" id="galeria-boton-detalles-imagen" data-estado="normal" title="Detalles" disabled="disabled"><span class="glyphicon glyphicon-list-alt"></span></a></div></div><div class="panel-body" style="overflow:auto; height:300px;"><div class="row"></div></div></div></div></div></div>').find('.color-picker').colorpicker();
		actualizarContenedorActivo();
	});

	formularioArticulo.on('click', 'div#presentaciones-varias a.btn-eliminar-presentacion', function(event) {
		event.preventDefault();
		var container = $(this).parent().parent().parent().parent().parent();
		container.find('li.active, div.tab-pane.active').remove();
		container.find('li:first, div.tab-pane:first').addClass('active').find('div.contenedor-imagenes').addClass('contenedor-imagenes-activo');
		actualizarContenedorActivo();
		actualizarImages();
	});

	$('ul#presentaciones-tipo a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		$($(e.relatedTarget).attr('href')).find('div.contenedor-imagenes').removeClass('contenedor-imagenes-activo') // previous tab
		if ($(e.target).attr('href')=="#presentaciones-varias") {
  			$($(e.target).attr('href')).find('div.tab-pane.active div.contenedor-imagenes').addClass('contenedor-imagenes-activo'); // activated tab
  		}else{
  			$($(e.target).attr('href')).find('div.contenedor-imagenes').addClass('contenedor-imagenes-activo'); // activated tab
  		}
  		actualizarContenedorActivo();
  		actualizarImages();
	});

	$('ul#presentaciones-varias-navs').on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
		$($(e.relatedTarget).attr('href')).find('div.contenedor-imagenes').removeClass('contenedor-imagenes-activo') // previous tab
		$($(e.target).attr('href')).find('div.contenedor-imagenes').addClass('contenedor-imagenes-activo'); // activated tab
  		actualizarContenedorActivo();
	})
});