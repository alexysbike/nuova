$(function() {
	var modal = $("div#agregar-inventario-detalles");

	var modalUpload = $("div#galeria-minima-upload-modal");

	modal.modal({show:false});

	/*$("li.talla-inventario-detalles").hover(function (event) {
		$(this).find("a.opcion-agregar").show();
	}, function  (event) {
		$(this).find("a.opcion-agregar").hide();
	});*/

	/*$("a.opcion-agregar").click(function(event){
		event.preventDefault();
		var idInventario = $(this).attr('id-inventario');
		modal.find(".modal-content").html(ajax_load);
		$.post(base_url+'articulo/modificar_inventario', {id_inventario:idInventario}, function(data, textStatus, xhr) {
			modal.find(".modal-content").html(data);
		});
		modal.modal("show");
	});*/

	$("a.btn-modificar-inventario").click(function(event){
		event.preventDefault();
		var idPresentacion = $(this).attr('data-idpresentacion');
		var idArticulo = $(this).attr('data-idarticulo');
		modal.find(".modal-content").html(ajax_load);
		$.post(base_url+'articulo/modificar_inventario', {id_presentacion:idPresentacion, id_articulo:idArticulo}, function(data, textStatus, xhr) {
			modal.find(".modal-content").html(data);
		});
		modal.modal("show");
	});	
});