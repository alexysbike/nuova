function preventBack(){window.history.forward(-1);}
    setTimeout("preventBack()", 0);
    window.onunload=function(){null};
var formularioArticulo;
var idUsuario;
var nextPresentacion = 1;
$(function(){
	formularioArticulo = $("form#registro-articulo");
	contenedorDatos = formularioArticulo.find("div#contenedor-datos");
	imgDatos = $("img#datos");
	contenedorTallas = formularioArticulo.find("div#contenedor-tallas");
	imgTallas = $("img#tallas");
	contenedorPresentaciones = formularioArticulo.find("div#contenedor-presentaciones");
	imgPresentaciones = $("img#presentaciones");
	idUsuario = formularioArticulo.attr('data-idusuario');
	tituloSeccion = $("h1#titulo-seccion");
	
	$(".chosen").chosen();
	$(".color-picker").colorpicker();
	$("input.date").datepicker({format: "dd/mm/yyyy"});
	
	formularioArticulo.find("select#id_coleccion").change(function(event) {
		if ($(this).val() < 0) {
			coleccionWidget.nuevo(idUsuario,"id_coleccion");
		}
	});

	formularioArticulo.on('click', 'a#agregar-talla', function(event) {
		event.preventDefault();
		$(this).before('<div class="input-group"><input id="talla[]" name="talla[]" class="form-control" placeholder="Ingrese tallas del art&iacute;culo..." type="text" required=""><div class="input-group-btn"><button type="button" class="btn btn-default btn-eliminar-talla">Eliminar</button></div></div>');
	});

	formularioArticulo.on('click', 'button.btn-eliminar-talla', function(event) {
		event.preventDefault();
		$(this).parent().parent().remove();
	});

	formularioArticulo.on('click', 'button#plantilla-basica', function(event) {
		event.preventDefault();
		$("div#inputs-tallas").html('<div class="col-md-6 col-md-offset-3"><div class="input-group"><input id="talla[]" name="talla[]" class="form-control" placeholder="Ingrese tallas del art&iacute;culo..." type="text" value="S"><div class="input-group-btn"><button type="button" class="btn btn-default btn-eliminar-talla">Eliminar</button></div></div><div class="input-group"><input id="talla[]" name="talla[]" class="form-control" placeholder="Ingrese tallas del art&iacute;culo..." type="text" value="M"><div class="input-group-btn"><button type="button" class="btn btn-default btn-eliminar-talla">Eliminar</button></div></div><div class="input-group"><input id="talla[]" name="talla[]" class="form-control" placeholder="Ingrese tallas del art&iacute;culo..." type="text" value="L"><div class="input-group-btn"><button type="button" class="btn btn-default btn-eliminar-talla">Eliminar</button></div></div><div class="input-group"><input id="talla[]" name="talla[]" class="form-control" placeholder="Ingrese tallas del art&iacute;culo..." type="text" value="XL"><div class="input-group-btn"><button type="button" class="btn btn-default btn-eliminar-talla">Eliminar</button></div></div><a href="#" id="agregar-talla"><strong>Agregar Talla</strong></a><br><span id="tallas-error"></span></div>');
	});

	formularioArticulo.on('click', 'button#plantilla-unica', function(event) {
		event.preventDefault();
		$("div#inputs-tallas").html('<div class="col-md-6 col-md-offset-3"><div class="input-group"><input id="talla[]" name="talla[]" class="form-control" placeholder="Ingrese tallas del art&iacute;culo..." type="text" required="required" value="UNICA"><div class="input-group-btn"><button type="button" class="btn btn-default btn-eliminar-talla">Eliminar</button></div></div><a href="#" id="agregar-talla"><strong>Agregar Talla</strong></a><br><span id="tallas-error"></span></div>');
	});


	formularioArticulo.on('click', 'button#plantilla-vacia', function(event) {
		event.preventDefault();
		$("div#inputs-tallas").html('<div class="col-md-6 col-md-offset-3"><div class="input-group"><input id="talla[]" name="talla[]" class="form-control" placeholder="Ingrese tallas del art&iacute;culo..." type="text" required="required"><div class="input-group-btn"><button type="button" class="btn btn-default btn-eliminar-talla">Eliminar</button></div></div><a href="#" id="agregar-talla"><strong>Agregar Talla</strong></a><br><span id="tallas-error"></span></div>');
	});

	formularioArticulo.on('click', 'input[name=presentacion-tipo]', function(event) {
		if ($(this).val() == "unica") {
			formularioArticulo.find('div#contenedor-presentaciones-varias').hide();
			//formularioArticulo.find('div#contenedor-presentacion-unica').show();
		}else{
			//formularioArticulo.find('div#contenedor-presentacion-unica').hide();
			formularioArticulo.find('div#contenedor-presentaciones-varias').show();
		}
	});

	formularioArticulo.on('click', 'a#agregar-presentacion', function(event) {
		event.preventDefault();
		/* Act on the event */

		$(this).before('<div class="row"><div class="col-md-4"><div class="input-group color-picker"><span class="input-group-addon"><i></i></span><input type="text" value="#ffffff" class="form-control" name="presentacion-color[]"/></div></div><div class="col-md-8"><div class="input-group"><input type="text" class="form-control input-md" name="presentacion-nombre[]" value="" placeholder="Nombre de la Presentaci&oacute;n..."><div class="input-group-btn"><button type="button" class="btn btn-default btn-eliminar-presentacion">Eliminar</button></div></div></div></div>').parent().find('.color-picker').colorpicker();;		
	});

	formularioArticulo.on('click', 'button.btn-eliminar-presentacion', function(event) {
		event.preventDefault();
		if (formularioArticulo.find('button.btn-eliminar-presentacion').length > 1){
			$(this).parent().parent().parent().parent().remove();
		}
	});

	formularioArticulo.on('click', 'button#btn-siguiente-datos', function(event){
		event.preventDefault();
		var error = false;
		var inputNombre = contenedorDatos.find("input#nombre");
		if (inputNombre.val() == "") {
			inputNombre.parent().find("div.label").remove();
			inputNombre.after(mensajesValidacion.errores.campoRequerido);
			inputNombre.parent().parent().removeClass("has-success").addClass("has-error");
			error = true;
		} else{
			inputNombre.parent().find("div.label").remove();
			inputNombre.parent().parent().removeClass("has-error").addClass("has-success");
		};

		var inputFecha = contenedorDatos.find("input#fecha");
		if (inputFecha.val() == "") {
			inputFecha.parent().find("div.label").remove();
			inputFecha.after(mensajesValidacion.errores.campoRequerido);
			inputFecha.parent().parent().removeClass("has-success").addClass("has-error");
			error = true;
		} else{
			console.log(isValidDate(inputFecha.val()));
			if (isValidDate(inputFecha.val())) {
				inputFecha.parent().find("div.label").remove();
				inputFecha.parent().parent().removeClass("has-error").addClass("has-success");
			} else{
				inputFecha.parent().find("div.label").remove();
				inputFecha.after(mensajesValidacion.errores.errorFormato);
				inputFecha.parent().parent().removeClass("has-success").addClass("has-error");
				error = true;
			};
		};

		var inputPrecio = contenedorDatos.find("input#precio");
		if (inputPrecio.val() == "") {
			inputPrecio.parent().parent().find("div.label").remove();
			inputPrecio.parent().after(mensajesValidacion.errores.campoRequerido);
			inputPrecio.parent().parent().parent().removeClass("has-success").addClass("has-error");
			error = true;
		} else{
			if (isDecimal(inputPrecio.val())) {
				inputPrecio.parent().parent().find("div.label").remove();
				inputPrecio.parent().parent().parent().removeClass("has-error").addClass("has-success");
			} else{
				inputPrecio.parent().parent().find("div.label").remove();
				inputPrecio.parent().after(mensajesValidacion.errores.errorFormato);
				inputPrecio.parent().parent().parent().removeClass("has-success").addClass("has-error");
				error = true;
			};
		};
		if (error == false) {
			tituloSeccion.html("Tallas");
			contenedorDatos.hide();
			imgDatos.hide();
			contenedorTallas.show();
			imgTallas.show();
		};
	});

	formularioArticulo.on('click', 'button#btn-regresar-tallas', function(event) {
		event.preventDefault();
		tituloSeccion.html("Datos Generales");
		contenedorTallas.hide();
		imgTallas.hide();
		contenedorDatos.show();
		imgDatos.show();
	});

	formularioArticulo.on('click', 'button#btn-siguiente-tallas', function(event) {
		event.preventDefault();
		var error = false;
		var inputsTallas = contenedorTallas.find("input[name='talla[]'],input[name='talla_existente[]']");
		if (inputsTallas.length == 0) {
			contenedorTallas.find("div#inputs-tallas div.label").remove();
			contenedorTallas.find("div#inputs-tallas span#tallas-error").html(mensajesValidacion.errores.faltaTallas);
			error = true;
		}else{
			contenedorTallas.find("div#inputs-tallas div.label").remove();
			inputsTallas.each(function (){
				if($(this).val()==""){
					contenedorTallas.find("div#inputs-tallas div.label").remove();
					contenedorTallas.find("div#inputs-tallas span#tallas-error").html(mensajesValidacion.errores.tallaRequerida);
					$(this).removeClass("has-success").addClass("has-error");
					error = true;
				}else{
					$(this).removeClass("has-error").addClass("has-success");
				}
			});
		}

		if (error == false) {
			tituloSeccion.html("Presentaciones");
			contenedorTallas.hide();
			imgTallas.hide();
			contenedorPresentaciones.show();
			imgPresentaciones.show();
		};
		
	});

	formularioArticulo.on('click', 'button#btn-regresar-presentaciones', function(event) {
		event.preventDefault();
		tituloSeccion.html("Tallas");
		contenedorPresentaciones.hide();
		imgPresentaciones.hide();
		contenedorTallas.show();
		imgTallas.show();
	});

	formularioArticulo.on('click', 'button#btn-crear-articulo', function(event) {
		event.preventDefault();
		var error = false;
		var tipoPresentacion = contenedorPresentaciones.find("input[name=presentacion-tipo]:checked").val();
		contenedorPresentaciones.find("span#presentaciones-error div.label").remove();
		if (tipoPresentacion != "unica") {
			var inputsPresentaciones = contenedorPresentaciones.find("input[name='presentacion-nombre[]']");
			inputsPresentaciones.each(function (){
				if($(this).val()==""){
					contenedorPresentaciones.find("span#presentaciones-error div.label").remove();
					contenedorPresentaciones.find("span#presentaciones-error").html(mensajesValidacion.errores.tallaRequerida);
					error = true;
				}
			});
		};
		if (error == false) {
			formularioArticulo.submit();
		};
	});
});