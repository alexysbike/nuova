$(function() {
	var modal = $("div#agregar-inventario-detalles");

	var modalUpload = $("div#galeria-minima-upload-modal");
	var idArticulo;
	var idPresentacion;
	var contenedorImg;

	modal.modal({show:false});

	$("span#label-status").popover({ 
                                 content:'Para Activar el art&iacute;culo por favor ir a "Mis Art&iacute;culos" y darle al boton "Activar"',
                                 placement:"right",
                                 container:"body",
                                 html: true,
                                 trigger: "hover"
                             });

	$("a.galeria-boton-agregar-imagen").click(function(event) {
		event.preventDefault();
		idArticulo = $(this).attr('data-idarticulo');
		idPresentacion = $(this).attr('data-idpresentacion');
		contenedorImg = $(this).parent().parent().parent().find("div.panel-body div.row");
		modalUpload.modal("show");
	});

	$("a.galeria-boton-eliminar-imagen").click(function(event) {
		event.preventDefault();
		var idImagen = $(this).attr("data-idimagen");
		var idArticuloFotos = $(this).attr("data-idarticulofotos");
		var imagen = $(this).parent().parent().parent().find("a.galeria-thumbnail.galeria-thumbnail-selected").parent();
		$(this).attr("disabled", "disabled");
		$.post(base_url+"articulo/eliminar_imagen", {id_imagen:idImagen, id_articulofotos:idArticuloFotos}, function(data, textStatus, xhr) {
			imagen.remove();
		});
	});

	$("body").on('click', 'a.galeria-thumbnail', function(event) {
		event.preventDefault();
		/* Act on the event */
		var parent = $(this).parent().parent().parent().parent();
		var botonEliminar = $(parent).find("a.galeria-boton-eliminar-imagen");
		var botonDetalles = $(parent).find("a.galeria-boton-detalles-imagen");
		contenedorImg = parent.find('div.panel-body div-row');
		if ($(this).hasClass('galeria-thumbnail-selected') == true) {
			botonDetalles.attr('disabled', 'disabled');
			botonEliminar.attr('disabled', 'disabled');
			botonEliminar.attr("data-idimagen", "");
			botonEliminar.attr("data-idarticulofotos", "");
			$(this).removeClass('galeria-thumbnail-selected');
		} else{
			parent.find('a.galeria-thumbnail').each(function(index, el) {
				$(this).removeClass('galeria-thumbnail-selected');
			});
			botonDetalles.removeAttr('disabled');
			botonDetalles.attr("url-img", $(this).attr("href"));
			botonDetalles.attr("href", $(this).attr("href"));
			botonEliminar.removeAttr('disabled');
			botonEliminar.attr("data-idimagen", $(this).attr("data-idimagen"));
			botonEliminar.attr("data-idarticulofotos", $(this).attr("data-idarticulofotos"));
			$(this).addClass('galeria-thumbnail-selected');

		};
	});

	$("form#galeria-subir-imagen").validate({
			  rules: {
			  	"galeria-imagen-nombre": {
			  		required: true,
			  		remote: {url: base_url+"galeria/check_nombre",
    		    			type: "post",
    		    			data: {
    		    			  nombre: function() {
    		    			    return $( "#galeria-imagen-nombre" ).val();
    		    			  }
    		    			}
    		    		}
			  	}
			  },
			  messages: {
			  		"galeria-imagen-nombre": {
			  			required: mensajesValidacion.errores.campoRequerido,
			  			remote: mensajesValidacion.errores.nombreImagenUsado
			  		},
			  		"galeria-imagen": mensajesValidacion.errores.campoRequerido
			  	},
			  validClass: "has-success",
			  success: function(label) {
  			    label.parent().addClass("has-success");
  			  },
  			   highlight: function(element, errorClass) {
  			    $(element).parent().addClass("has-error");
  			  },
  			  submitHandler: function(form) {
  			  	var input = document.getElementById("galeria-imagen"),  
		      	formdata = false;  
		      
		  		if (window.FormData) {  
		    		formdata = new FormData();  
		  		}
		
		  		var i = 0, len = input.files.length, img, reader, file;  
		       
		    	for ( ; i < len; i++ ) {  
			      file = input.files[i];   
		    	}
		
		    	if ( window.FileReader ) {  
				  reader = new FileReader();    
				  reader.readAsDataURL(file);  
				}  
				if (formdata) {  
				  formdata.append("galeria-imagen", file);
				  formdata.append('galeria-nombre', $(form).find("#galeria-imagen-nombre").val());
				  formdata.append('galeria-descripcion', $(form).find("#galeria-imagen-descripcion").val());
				  formdata.append('galeria-album', 0);
				}
				$("div#galeria-minima-upload-modal").find('div#galeria-subir-resultado').html(ajax_load);
				$.ajax({  
				  url: base_url+"galeria/upload_image_articulo/"+$(form).find("button#galeria-imagen-boton-subir").attr("id-usuario")+"/"+idArticulo+"/"+idPresentacion,  
				  type: "POST",  
				  data: formdata,  
				  processData: false,  
				  contentType: false,  
				  success: function (res) {  
				  	modalUpload.find('input#galeria-imagen').val('');
				  	modalUpload.find('input#galeria-imagen-nombre').val('');
				  	modalUpload.find('textarea#galeria-imagen-descripcion').val('');
				    modalUpload.find('div#galeria-subir-resultado').html($(res).find("div#resultado1").html());
				    contenedorImg.append($(res).find("div#resultado2").html());
				  }  
				});
  			  }
			});
});