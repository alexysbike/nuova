var coleccionWidget;
var modalWidgetColeccion;
var coleccion;
$(function(){
	$("body").append('<div id="modal-widget-coleccion" class="modal" role="dialog"></div>');
	modalWidgetColeccion = $("div#modal-widget-coleccion");
	modalWidgetColeccion.modal({keyboard: false, backdrop:'static', show: false});
	coleccionWidget = {
		nuevo: function(id_usuario, dataTrigger){
			modalWidgetColeccion.html('<div class="modal-content">'+ajax_load+'</div>');
			modalWidgetColeccion.modal("show");
			$.post(base_url+"articulo/get_widget/nueva_coleccion/"+id_usuario+"/"+dataTrigger, function(data, textStatus, xhr) {
				modalWidgetColeccion.html(data);
			});
		}

	}

	modalWidgetColeccion.on('click', 'button#wcb-crear-coleccion', function(event) {
		event.preventDefault();
		var dataTrigger = $("#"+$(this).attr('data-trigger'));
		modalWidgetColeccion.find("form#registro-coleccion").ajaxSubmit({dataType:'json',
			beforeSubmit: function(arr, $form, options){
				modalWidgetColeccion.find('div.modal-content').html(ajax_load);
			},
			success:function(responseJson, statusText, xhr, $form){
			 	coleccion = responseJson;
			 	dataTrigger.append('<option value="'+coleccion.id+'">'+coleccion.nombre+'</option>').val(coleccion.id);
			 	dataTrigger.trigger("chosen:updated");
			 	modalWidgetColeccion.modal('hide');
			}
		});
	}).on('click', 'button#wcb-cerrar-modal-nueva-coleccion', function(event) {
		event.preventDefault();
		dataTrigger = $("#"+$(this).attr('data-trigger'));
		dataTrigger.val(0);
		dataTrigger.trigger("chosen:updated");
		modalWidgetColeccion.modal('hide');
	});
});