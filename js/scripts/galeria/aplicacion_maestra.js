var contenedorAdicional;
var contenedorAplicacion;
var aplicacionGaleria;
var idUsuario;
var imagesData;
var images;
var initialRenderImages = function (data, container) {
	for (var i = data.length - 1; i >= 0; i--) {
		container.append('<div class="col-sm-6 col-md-2"><a id="'+data[i].id+'" href="#" class="thumbnail galeria-thumbnail" data-index="'+i+'"><div class="galeria-imagen" style="background-image:url('+base_url+'img/usuarios/'+idUsuario+'/'+data[i].archivo+')"></div></a></div>');
	};
}
var refreshRenderImages = function (data, container) {
	container.html("");
	for (var i = data.length - 1; i >= 0; i--) {
		container.append('<div class="col-sm-6 col-md-2"><a href="#" class="thumbnail galeria-thumbnail"><div class="galeria-imagen" style="background-image:url('+base_url+'img/usuarios/'+idUsuario+'/'+data[i].archivo+')"></div></a></div>');
	};
}
var habilitarOpcionesImagen = function () {
	contenedorAplicacion.find('.opciones-imagen').removeAttr('disabled');
}

var desabilitarOpcionesImagen = function () {
	contenedorAplicacion.find('.opciones-imagen').attr('disabled', 'disabled');
}

var detallesImagen = function (imageIndex) {
	contenedorAdicional.html(ajax_load);
	var objeto = {id_usuario:idUsuario,
				  archivo: images[imageIndex].archivo,
				  nombre_imagen: images[imageIndex].nombre_imagen,
				  descripcion: images[imageIndex].descripcion,
				  nombre_album: images[imageIndex].nombre_album,
				  mime: images[imageIndex].mime,
				  size: images[imageIndex].size};
	$.post(base_url+'galeria/adicional/detalles_imagen', objeto, function(data, textStatus, xhr) {
		contenedorAdicional.html(data);
	});
}

function mostrarAdicional () {
	contenedorAplicacion.addClass('col-md-9').removeClass('col-md-12');
	contenedorAdicional.show();
}
function ocultarAdicional () {
	contenedorAdicional.hide();
	contenedorAplicacion.addClass('col-md-12').removeClass('col-md-9');
}
$(function(){
	aplicacionGaleria = $("div#aplicacion-galeria");
	contenedorAdicional = $("div#aplicacion-galeria div#contenedor-adicional");
	contenedorAplicacion = $("div#aplicacion-galeria div#contenedor-aplicacion");
	idUsuario = aplicacionGaleria.attr("data-idusuario");

	contenedorAplicacion.on('click', 'a.galeria-thumbnail', function(event) {
		event.preventDefault();
		if ($(this).hasClass('galeria-thumbnail-selected') == true) {
			desabilitarOpcionesImagen();
			$(this).removeClass('galeria-thumbnail-selected');
		} else{
			contenedorAplicacion.find('a.galeria-thumbnail').each(function(index, el) {
				$(this).removeClass('galeria-thumbnail-selected');
			});
			if(contenedorAplicacion.find('a#galeria-boton-detalles-imagen').attr('data-estado') == "activo"){
				detallesImagen($(this).attr('data-index'));
			}
			habilitarOpcionesImagen();
			$(this).addClass('galeria-thumbnail-selected');

		};
		
	}).on("click","a#galeria-boton-upload",function(event){
		event.preventDefault();
		if($(this).attr("data-estado") == "normal"){
			$(this).attr("data-estado", "upload").attr("disabled","disabled");
			mostrarAdicional();
			contenedorAdicional.html(ajax_load);
			$.post(base_url+'galeria/adicional/upload_image', {id_usuario:idUsuario}, function(data, textStatus, xhr) {
				contenedorAdicional.html(data);
				contenedorAdicional.find(".chosen-select").chosen();
			});

		}
	}).on('click', 'a#galeria-boton-refresh', function(event) {
		event.preventDefault();
		$.post(base_url+'galeria/get_images_data/'+idUsuario, function(data, textStatus, xhr) {
			imagesData = data;
			images = imagesData;
			refreshRenderImages(images, contenedorAplicacion.find("div#contenedor-imagenes div.row"));
		}, "json");	
	}).on('click', 'a#galeria-boton-detalles-imagen', function(event) {
		event.preventDefault();
		if($(this).attr("data-estado") == "normal"){
			$(this).attr("data-estado", "activo");
			mostrarAdicional();
			var imageIndex = contenedorAplicacion.find(".galeria-thumbnail-selected").attr('data-index');
			detallesImagen(imageIndex);
		}else{
			$(this).attr("data-estado", "normal");
			ocultarAdicional();
		}
	});
	contenedorAdicional.on("click", "button#galeria-imagen-boton-cerrar", function(){
		contenedorAplicacion.find("a#galeria-boton-upload").attr("data-estado", "normal").removeAttr('disabled');
		ocultarAdicional();
	}).on('click', 'button#galeria-imagen-boton-subir', function(event) {
		event.preventDefault();
		var input = document.getElementById("galeria-imagen"),  
      	formdata = false;  
      
  		if (window.FormData) {  
    		formdata = new FormData();  
  		}

  		var i = 0, len = input.files.length, img, reader, file;  
       
    	for ( ; i < len; i++ ) {  
	      file = input.files[i];   
    	}

    	if ( window.FileReader ) {  
		  reader = new FileReader();    
		  reader.readAsDataURL(file);  
		}  
		if (formdata) {  
		  formdata.append("galeria-imagen", file);
		  formdata.append('galeria-nombre', contenedorAdicional.find("#galeria-imagen-nombre").val());
		  formdata.append('galeria-descripcion', contenedorAdicional.find("#galeria-imagen-descripcion").val());
		  formdata.append('galeria-album', contenedorAdicional.find("#galeria-imagen-album").val());
		}
		contenedorAdicional.html(ajax_load);
		$.ajax({  
		  url: base_url+"galeria/upload_image/"+idUsuario,  
		  type: "POST",  
		  data: formdata,  
		  processData: false,  
		  contentType: false,  
		  success: function (res) {  
		    contenedorAdicional.html(res);   
		  }  
		});
	});
});
$(document).ready(function() {
	$.post(base_url+'galeria/get_images_data/'+idUsuario, function(data, textStatus, xhr) {
			imagesData = data;
			images = imagesData;
			initialRenderImages(images, contenedorAplicacion.find("div#contenedor-imagenes div.row"));
		}, "json");	
});
