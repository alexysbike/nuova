var contenedores = new Array();
var contenedorActivo;
var contenedorAdicional;
var aplicacionGaleria;
var imagesData;
var images;

function get_img_data () {
	$.post(base_url+'galeria/get_images_data/'+idUsuario, function(data, textStatus, xhr) {
			imagesData = data;
		}, "json");	
}

var renderImages = function (data, container) {
	container.html("");
	for (var i = data.length - 1; i >= 0; i--) {
		container.append('<div class="col-sm-6 col-md-4"><a id="'+data[i].id+'" href="#" class="thumbnail galeria-thumbnail" data-index="'+i+'"><div class="galeria-imagen" style="background-image:url('+base_url+'img/usuarios/'+idUsuario+'/'+data[i].archivo+')"></div></a></div>');
	};
}

function mostrarAdicional () {
	contenedorAdicional.show();
}
function ocultarAdicional () {
	contenedorAdicional.hide();
}
var habilitarOpcionesImagen = function (contenedor) {
	contenedor.find('.opciones-imagen').removeAttr('disabled');
}
var desabilitarOpcionesImagen = function (contenedor) {
	contenedor.find('.opciones-imagen').attr('disabled', 'disabled');
}

var actualizarInput = function(selectedImages){
	var input = contenedorActivo.find('input.selected-images'); 
	input.val("");
	selectedImages.each(function(index, el) {
		input.val(input.val()+$(this).attr('id')+",");
	});
}

var actualizarContenedorActivo = function(){
	contenedorActivo = $("div.contenedor-imagenes.contenedor-imagenes-activo");
}

var actualizarImages = function(container){
	renderImages(images, contenedorAdicional.find('div#contenedor-imagenes div.row'));
}

$(function(){
	contenedores = $("div.contenedor-imagenes");
	contenedorActivo = $("div.contenedor-imagenes.contenedor-imagenes-activo");
	contenedorAdicional = $("div#contenedor-aplicacion-galeria-adicional");
	$("body").on('click', '.galeria-boton-agregar-imagen', function(event) {
		event.preventDefault();
		contenedorAdicional.html(ajax_load);
		$.post(base_url+'galeria/minima', function(data, textStatus, xhr) {
			$("body").append(data);
			contenedorAdicional.html('');
			$("div#galeria-minima-aplicacion").appendTo(contenedorAdicional);
			aplicacionGaleria = $("div#aplicacion-minima-galeria");
			$(".galeria-boton-agregar-imagen").attr('disabled', 'disabled');
			$.post(base_url+'galeria/get_images_data/'+idUsuario, function(data, textStatus, xhr) {
				imagesData = data;
				images = imagesData;
				renderImages(images, contenedorAdicional.find('div#contenedor-imagenes div.row'));
			}, "json");	
			$("form#galeria-subir-imagen").validate({
			  rules: {
			  	"galeria-imagen-nombre": {
			  		required: true,
			  		remote: {url: base_url+"galeria/check_nombre",
    		    			type: "post",
    		    			data: {
    		    			  nombre: function() {
    		    			    return $( "#galeria-imagen-nombre" ).val();
    		    			  }
    		    			}
    		    		}
			  	}
			  },
			  messages: {
			  		"galeria-imagen-nombre": {
			  			required: mensajesValidacion.errores.campoRequerido,
			  			remote: mensajesValidacion.errores.nombreImagenUsado
			  		},
			  		"galeria-imagen": mensajesValidacion.errores.campoRequerido
			  	},
			  validClass: "has-success",
			  success: function(label) {
  			    label.parent().addClass("has-success");
  			  },
  			   highlight: function(element, errorClass) {
  			    $(element).parent().addClass("has-error");
  			  },
  			  submitHandler: function(form) {
  			  	var input = document.getElementById("galeria-imagen"),  
		      	formdata = false;  
		      
		  		if (window.FormData) {  
		    		formdata = new FormData();  
		  		}
		
		  		var i = 0, len = input.files.length, img, reader, file;  
		       
		    	for ( ; i < len; i++ ) {  
			      file = input.files[i];   
		    	}
		
		    	if ( window.FileReader ) {  
				  reader = new FileReader();    
				  reader.readAsDataURL(file);  
				}  
				if (formdata) {  
				  formdata.append("galeria-imagen", file);
				  formdata.append('galeria-nombre', $(form).find("#galeria-imagen-nombre").val());
				  formdata.append('galeria-descripcion', $(form).find("#galeria-imagen-descripcion").val());
				  formdata.append('galeria-album', 0);
				}
				$("div#galeria-minima-upload-modal").find('div#galeria-subir-resultado').html(ajax_load);
				$.ajax({  
				  url: base_url+"galeria/upload_image/"+$(form).find("button#galeria-imagen-boton-subir").attr("id-usuario"),  
				  type: "POST",  
				  data: formdata,  
				  processData: false,  
				  contentType: false,  
				  success: function (res) {  
				  	$("div#galeria-minima-upload-modal").find('input#galeria-imagen').val('');
				  	$("div#galeria-minima-upload-modal").find('input#galeria-imagen-nombre').val('');
				  	$("div#galeria-minima-upload-modal").find('input#galeria-imagen-descripcion').val('');
				    $("div#galeria-minima-upload-modal").find('div#galeria-subir-resultado').html($(res).find('div#galeria-subir-resultado').html());   
				  }  
				});
  			  }
			});
			mostrarAdicional();
		});
	});

	$("div#presentaciones-parte-izquierda").on('click', 'a.galeria-thumbnail', function(event) {
		event.preventDefault();
		var parent = $(this).parent().parent().parent().parent();
		if ($(this).hasClass('galeria-thumbnail-selected') == true) {
			desabilitarOpcionesImagen(parent);
			$(this).removeClass('galeria-thumbnail-selected');
		} else{
			contenedorAdicional.find('a.galeria-thumbnail').each(function(index, el) {
				$(this).removeClass('galeria-thumbnail-selected');
			});
			if(contenedorAdicional.find('a#galeria-boton-detalles-imagen').attr('data-estado') == "activo"){
				detallesImagen($(this).attr('data-index'));
			}
			habilitarOpcionesImagen(parent);
			$(this).addClass('galeria-thumbnail-selected');

		};
	}).on('click', 'a#galeria-boton-eliminar-imagen', function(event) {
		event.preventDefault();
		var parent = $(this).parent().parent().parent().parent();
		var imgSelected = parent.find('a.galeria-thumbnail-selected');
		imgSelected.each(function(index, el) {
			$(this).removeClass('galeria-thumbnail-selected').parent().appendTo(contenedorAdicional.find('div.panel-body div.row')).removeClass('col-md-3').addClass('col-md-4');
		});
		actualizarInput(parent.find("a.galeria-thumbnail"));
		desabilitarOpcionesImagen(parent);
	});;

	contenedorAdicional.on('click', 'a.galeria-thumbnail', function(event) {
		event.preventDefault();
		if ($(this).hasClass('galeria-thumbnail-selected') == true) {
			desabilitarOpcionesImagen(contenedorAdicional);
			$(this).removeClass('galeria-thumbnail-selected');
		} else{
			contenedorAdicional.find('a.galeria-thumbnail').each(function(index, el) {
				$(this).removeClass('galeria-thumbnail-selected');
			});
			if(contenedorAdicional.find('a#galeria-boton-detalles-imagen').attr('data-estado') == "activo"){
				detallesImagen($(this).attr('data-index'));
			}
			habilitarOpcionesImagen(contenedorAdicional);
			$(this).addClass('galeria-thumbnail-selected');

		};
	}).on('click', 'a#galeria-minima-boton-seleccionar-imagen', function(event) {
		event.preventDefault();
		var imgSelected = contenedorAdicional.find('a.galeria-thumbnail-selected');
		imgSelected.each(function(index, el) {
			$(this).removeClass('galeria-thumbnail-selected').parent().appendTo(contenedorActivo.find('div.panel-body div.row')).removeClass('col-md-4').addClass('col-md-3');
		});
		actualizarInput(contenedorActivo.find("a.galeria-thumbnail"));
		desabilitarOpcionesImagen(contenedorAdicional);
	}).on('click', 'a#galeria-minima-boton-cerrar-galeria', function(event) {
		event.preventDefault();
		contenedorAdicional.hide();
		$('.galeria-boton-agregar-imagen').removeAttr('disabled');
	}).on('click', 'button#galeria-imagen-boton-subir', function(event) {
		event.preventDefault();
		/*var input = document.getElementById("galeria-imagen"),  
      	formdata = false;  
      
  		if (window.FormData) {  
    		formdata = new FormData();  
  		}

  		var i = 0, len = input.files.length, img, reader, file;  
       
    	for ( ; i < len; i++ ) {  
	      file = input.files[i];   
    	}

    	if ( window.FileReader ) {  
		  reader = new FileReader();    
		  reader.readAsDataURL(file);  
		}  
		if (formdata) {  
		  formdata.append("galeria-imagen", file);
		  formdata.append('galeria-nombre', contenedorAdicional.find("#galeria-imagen-nombre").val());
		  formdata.append('galeria-descripcion', contenedorAdicional.find("#galeria-imagen-descripcion").val());
		  formdata.append('galeria-album', 0);
		}
		$("div#galeria-minima-upload-modal").find('div#galeria-subir-resultado').html(ajax_load);
		$.ajax({  
		  url: base_url+"galeria/upload_image/"+$(this).attr("id-usuario"),  
		  type: "POST",  
		  data: formdata,  
		  processData: false,  
		  contentType: false,  
		  success: function (res) {  
		  	 $("div#galeria-minima-upload-modal").find('input#galeria-imagen').val('');
		  	 $("div#galeria-minima-upload-modal").find('input#galeria-imagen-nombre').val('');
		  	 $("div#galeria-minima-upload-modal").find('input#galeria-imagen-descripcion').val('');
		    $("div#galeria-minima-upload-modal").find('div#galeria-subir-resultado').html($(res).find('div#galeria-subir-resultado').html());   
		  }  
		});*/
	});
});