$(function () {
	var modal = $("div#modal-generar-facturas");

	$(".popover-user").popover();

	$("a.btn-detalles").click(function(event) {
		event.preventDefault();

		modal.find('.detalles').hide();
		modal.find('.detalles_'+$(this).attr("data-index")).show();

		modal.modal("show");
	});

	$("a.btn-eliminar").click(function(event) {
		event.preventDefault();
		var btn = $(this);
		$.post(btn.attr("href"), function(data, textStatus, xhr) {
			btn.parent().parent().remove();
		});
	});

	$("a.btn-enviar-correo").click(function(event) {
		event.preventDefault();
		var btn = $(this);
		$.post(btn.attr("href"), function(data, textStatus, xhr) {
		});
	});
});