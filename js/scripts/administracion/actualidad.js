$(function(){
	var container = $("div#form-container");

	$("a#btn-nueva-noticia").click(function(event) {
		event.preventDefault();
		container.find('input#tipo-accion').val("crear");
		container.find('input#titulo').val("");
		container.find('textarea#cuerpo').val("");
		container.find('input#id-noticia').val("");

		container.show();
	});

	$("a.btn-editar-noticia").click(function(event) {
		event.preventDefault();
		var boton = $(this);
		boton.attr('disabled', 'disabled').html("Cargando...");
		var idNoticia = boton.attr("data-idnoticia");
		container.find('input#tipo-accion').val("editar");
		$.post(base_url+'administracion/get_noticia/'+idNoticia, function(data, textStatus, xhr) {
			container.find('input#titulo').val(data.titulo);
			container.find('textarea#cuerpo').val(data.cuerpo);
			container.find('input#id-noticia').val(data.id);
			container.show();
			boton.removeAttr('disabled').html("Editar");
		}, "json");
	});

	$("input#btn-cancelar").click(function(event) {
		container.hide();
	});
});