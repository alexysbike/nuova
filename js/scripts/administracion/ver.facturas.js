$(function () {
	var modal = $("div#modal-ver-facturas");

	$(".popover-user").popover();

	$(".date").datepicker({format: "dd/mm/yyyy"});

	$("a.btn-detalles").click(function(event) {
		event.preventDefault();

		modal.find('.detalles').hide();
		modal.find('.detalles_'+$(this).attr("data-index")).show();

		modal.modal("show");
	});

	$("a.btn-eliminar").click(function(event) {
		event.preventDefault();
		var btn = $(this);
		$.post(btn.attr("href"), function(data, textStatus, xhr) {
			btn.parent().parent().remove();
		});
	});

	$("a.btn-enviar-correo").click(function(event) {
		event.preventDefault();
		var btn = $(this);
		$.post(btn.attr("href"), function(data, textStatus, xhr) {

		});
	});

	$("a.btn-marcar-pagada").click(function(event) {
		event.preventDefault();
		var btn = $(this);
		$.post(btn.attr("href"), function(data, textStatus, xhr) {
			btn.parent().parent().find('.status').html("Pagada");
			btn.remove();
		});
	});

	$("a#btn-consultar").click(function(event) {
		event.preventDefault();
		$("form#form-generar").hide();
		$("form#form-consultar").show();
	});
	$("a#btn-generar").click(function(event) {
		event.preventDefault();
		$("form#form-consultar").hide();
		$("form#form-generar").show();
	});
});