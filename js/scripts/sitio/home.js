$(function(){

	$("body").on('click', 'div.categoria-home', function(event) {
		event.preventDefault();
		$("div.categoria-home-grande").find('.detalles-categoria').hide();
		$("div.categoria-home-grande").find('.img-detalles').show();
		$("div.categoria-home-grande").removeClass('col-md-5').removeClass('categoria-home-grande').addClass('col-md-1').addClass('categoria-home');
		$(this).removeClass('col-md-1').removeClass('categoria-home').addClass('col-md-5').addClass('categoria-home-grande');
		$(this).find('.img-detalles').hide();
		$(this).find('.detalles-categoria').show();
	});

});