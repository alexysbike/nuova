<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists('split_cel_ve')) {
      
    function split_cel_ve($cel, $return_parte) {

      if(strlen($cel)!=11) { 
      	
      	return '';
      
      }	elseif($return_parte=='prefijo') {
    	
      	return substr($cel, 0, 4);
      	
      } elseif($return_parte=='sufijo') {
      	
      	return substr($cel, 4);
      	
      }
        
    }

}



if ( ! function_exists('dd_prefijos_cel_ve')) {
      
    function dd_prefijos_cel_ve( $defecto=FALSE ) {

    	 $ci =& get_instance();
   		 $ci->load->helper('form');
   		 
   		 return form_dropdown( 'dd_prefijos_cel_ve',
   		 	array(
   				'0414'=>'0414',
   				'0424'=>'0424',
   				'0416'=>'0416',
   				'0426'=>'0426',
   				'0412'=>'0412'
   		 	),
   		 	$defecto,
   		 	'id="pref_cel_vzla"'
   		 );
        
    }

}







?>