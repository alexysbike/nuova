<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists('id_imagenes_encode')) {
      
    function id_imagenes_encode($id_imagenes_array="") {
		$encode = "";
    	foreach ($id_imagenes_array as $id_imagen):
    		$encode.=$id_imagen->id.",";
    	endforeach;
        return $encode;
    }

}

if ( ! function_exists('id_imagenes_decode')) {
      
    function id_imagenes_decode($id_imagenes_string="") {
		return explode(",", $id_imagenes_string);
    }

}