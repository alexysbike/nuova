<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('decode_genero')) {
      
    function decode_genero($genero) {
      
     	if ($genero == "M"):
     		return "Masculino";
     	elseif($genero == "F"):
     		return "Femenino";
     	else:
     		return "Sin Especificar";
     	endif;
        
    }

}

if ( ! function_exists('get_generos_articulo')) {
      
    function get_generos_articulo() {
      
     	return array(
     		"Caballeros" => "Caballeros",
     		"Damas" => "Damas",
     		"Ninos" => "Ni&ntilde;os",
     		"Ninas" => "Ni&ntilde;as",
     		"Unisex" => "Unisex"
     		);
        
    }

}

if ( ! function_exists('get_categorias')) {
      
    function get_categorias() {
        
        return array(
            "0"=>"Todas",
            "1"=>"Alta Costura",
            "2"=>"Casual",
            "3"=>"Deportiva",
            "4"=>"Playera",
            "5"=>"&Iacute;ntima",
            "6"=>"Accesorios",
            "7"=>"Calzado"
            );
        
    }

}