<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists('sqldate_to_fecha')) {
      
    function sqldate_to_fecha($date) {
      
      $d = explode('-',$date);
      return $d['2'].'-'.$d['1'].'-'.$d['0'];
        
    }

}


if ( ! function_exists('datepicker_to_sqldate')) {
      
    function datepicker_to_date($datepicker) {
      
      if($datepicker==''){
      	  return '';
      } else {	
    	  $d = explode('/',$datepicker);
    	  return $d['2'].'-'.$d['1'].'-'.$d['0'];
      }
    }

}


if( ! function_exists('sqldate_to_datepicker') ) {
	
	function sqldate_to_datepicker($sqldate) {
	
		$d = explode('-', $sqldate);
		
		return $d[2].'/'.$d[1].'/'.$d[0];
		
	
	}
	
	
	
	
}




if (!function_exists('sqldate_to_escrita')) {
	
	function sqldate_to_escrita($sqldate) {
		
		$semana = array( 'Monday'=>'Lunes', 'Tuesday'=>'Martes', 'Wednesday'=>'Mi&eacute;rcoles', 'Thursday'=>'Jueves', 'Friday'=>'Viernes', 'Saturday'=>'Sábado', 'Sunday'=>'Domingo');
				
		$meses = array(
			 '01'	=>	'Enero',
			 '02'	=>	'Febrero',
			 '03'	=>	'Marzo',
			 '04'	=>	'Abril',
			 '05'	=>	'Mayo',
			 '06'	=>	'Junio',
			 '07'	=>	'Julio',
			 '08'	=>	'Agosto',
			 '09'	=>	'Septiembre',
			 '10'	=>	'Octubre',
			 '11'	=>	'Noviembre',
			 '12'	=>	'Diciembre'
		);
		
		$d = explode('-',$sqldate);		
		$h = mktime(0, 0, 0, $d['1'], $d['2'], $d['0']);
		$w = date("l", $h) ;		
		
		return /*$semana[$w].' '.*/$d['2'].' de '.$meses[ $d['1'] ].' de '.$d['0'];

	}

}

if (!function_exists('sqldate_to_escrita_corta')) {
	
	function sqldate_to_escrita_corta($sqldate) {
		
		$meses = array(
			 '01'	=>	'Enero',
			 '02'	=>	'Febrero',
			 '03'	=>	'Marzo',
			 '04'	=>	'Abril',
			 '05'	=>	'Mayo',
			 '06'	=>	'Junio',
			 '07'	=>	'Julio',
			 '08'	=>	'Agosto',
			 '09'	=>	'Septiembre',
			 '10'	=>	'Octubre',
			 '11'	=>	'Noviembre',
			 '12'	=>	'Diciembre'
		);
		
		$d = explode('-',$sqldate);		
		$h = mktime(0, 0, 0, $d['1'], $d['2'], $d['0']);
		
		return $d['2'].' de '.$meses[ $d['1'] ].' de '.$d['0'];

	}

}


if (!function_exists('fecha_corta')) {
	
	function fecha_corta($sqldate_1, $sqldate_2) {

		$d_1 = explode('-',$sqldate_1);
		$d_2 = explode('-',$sqldate_2);
		
		$ret = $d_1['2'];
		
		if ($sqldate_2!='0000-00-00' && $d_1['1']==$d_2['1'] ) {
			
			$ret .= ' y '.$d_2['2'];
			
		}
		
		$ret .= '-'.$d_1['1'].'-'.$d_1['0'];
		return $ret;
		
	}
	
}




if (!function_exists('mes_escrito')) {
    
    function mes_escrito($mes) {
        
        $meses = array(
             '1'   =>  'Enero',
             '01'   =>  'Enero',
             '2'   =>  'Febrero',
             '02'   =>  'Febrero',
             '3'   =>  'Marzo',
             '03'   =>  'Marzo',
             '4'   =>  'Abril',
             '04'   =>  'Abril',
             '5'   =>  'Mayo',
             '05'   =>  'Mayo',
             '6'   =>  'Junio',
             '06'   =>  'Junio',
             '7'   =>  'Julio',
             '07'   =>  'Julio',
             '8'   =>  'Agosto',
             '08'   =>  'Agosto',
             '9'   =>  'Septiembre',
             '09'   =>  'Septiembre',
             '10'   =>  'Octubre',
             '11'   =>  'Noviembre',
             '12'   =>  'Diciembre'
        );    
        
        return $meses[$mes];

    }
	
if (!function_exists('sqldate_to_escrita_sin_ano')){
	
	function sqldate_to_escrita_sin_ano($sqldate)
	{
	
		$semana = array( 'Monday'=>'Lunes', 'Tuesday'=>'Martes', 'Wednesday'=>'Mi&eacute;rcoles', 'Thursday'=>'Jueves', 'Friday'=>'Viernes', 'Saturday'=>'Sábado', 'Sunday'=>'Domingo');
				
		$meses = array(
			 '01'	=>	'Enero',
			 '02'	=>	'Febrero',
			 '03'	=>	'Marzo',
			 '04'	=>	'Abril',
			 '05'	=>	'Mayo',
			 '06'	=>	'Junio',
			 '07'	=>	'Julio',
			 '08'	=>	'Agosto',
			 '09'	=>	'Septiembre',
			 '10'	=>	'Octubre',
			 '11'	=>	'Noviembre',
			 '12'	=>	'Diciembre'
		);
		
		$d = explode('-',$sqldate);		
		$h = mktime(0, 0, 0, $d['1'], $d['2'], $d['0']);
		$w = date("l", $h) ;		
		
		return $d['2'].' de '.$meses[ $d['1'] ];
		
	}

}

if (!function_exists('sqldate_to_escrita_certificado')) {
	
	function sqldate_to_escrita_certificado($sqldate_1, $sqldate_2) {
				
		$meses = array(
			 '01'	=>	'Enero',
			 '02'	=>	'Febrero',
			 '03'	=>	'Marzo',
			 '04'	=>	'Abril',
			 '05'	=>	'Mayo',
			 '06'	=>	'Junio',
			 '07'	=>	'Julio',
			 '08'	=>	'Agosto',
			 '09'	=>	'Septiembre',
			 '10'	=>	'Octubre',
			 '11'	=>	'Noviembre',
			 '12'	=>	'Diciembre'
		);
		
		$d_1 = explode('-',$sqldate_1);		
		$d_2 = explode('-',$sqldate_2);
		$ret = $d_1['2'];
		if ($sqldate_2!='0000-00-00' && $d_1['1']==$d_2['1'] ) {
			$ret .= ' y '.$d_2['2'];
		}
		
		return $ret.' de '.$meses[ $d_1['1'] ].' de '.$d_1['0'];

	}

}


}




?>