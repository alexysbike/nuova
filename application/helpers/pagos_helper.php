<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists('get_bancos_array')) {
	function get_bancos_array()
	{
		$bancos = array(
			"100% Banco",
			"Arrendadora Financiera Empresarial, ANFICO",
			"Bancamiga Banco Microfinanciero",
			"BanCaribe",
			"Banco Activo",
			"Banco Agr&iacute;cola de Venezuela",
			"Banco Bicentenario",
			"Banco Caron&iacute;",
			"Banco de Comercio Exterior",
			"Banco de Venezuela",
			"Banco del Tesoro",
			"Banco Esp&iacute;rito Santo",
			"Banco Exterior",
			"Banco Guayana",
			"Banco Internacional de desarrollo",
			"Banco Mercantil",
			"Banco Nacional de Cr&eacute;dito",
			"Banco Nacional de Vivienda y el H&aacute;bitat",
			"Banco Occidental de Descuento",
			"Banco Plaza",
			"Banco Sofitasa",
			"Bancrecer SA",
			"Banesco",
			"Bangente",
			"Banplus",
			"BBVA Banco Provincial",
			"BFC Banco Fondo Com&uacute;n",
			"Citibank",
			"Corp Banca",
			"Del Sur",
			"Instituto Municipal de Cr&eacute;dito Popular",
			"Mi Banco",
			"Sofioccidente Banco de Inversi&oacute;n"
			);

		return $bancos;
	}
}