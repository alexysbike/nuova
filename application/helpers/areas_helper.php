<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists('areas_interes_checkbox_checked')) {
      
    function areas_interes_checkbox_checked($area, $areas_usuario) {
		
    	$areas_array = explode(',', $areas_usuario);    	
    	if (in_array($area, $areas_array)) return 'checked';
        
    }

}

if ( ! function_exists('get_categorias')) {
      
    function get_categorias() {
		
    	return $categorias = array(
    		"0"->"Todas",
    		"1"->"Alta Costura",
            "2"->"Casual",
            "3"->"Deportiva",
            "4"->"Playera",
            "5"->"&Iacute;ntima",
            "6"->"Accesorios",
            "7"->"Calzado"
    		);
        
    }

}










?>