<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Articulo extends CI_Controller {

	private $folder;

	public function __construct()
    {
        parent::__construct();
        // Your own constructor code
        $this->folder = "articulo/";
        $this->load->model(array("Galeria_model", "Articulo_model", "Usuario_model"));
        if($this->session->userdata('logueado')!=true) :
	    	redirect('usuario/login');
		endif;
    }

	public function index()
	{
		$this->load->view("templates/basica");	
	}

	public function detalles($id_articulo = '')
	{
		$data["articulo"] = $this->Articulo_model->get_articulo($id_articulo);
		$data["main_content"] = "templates/columnas/100";
		$data["columna_1"] = $this->folder."detalles";
		$data["scripts"][0] = "lightbox.js";
		$data["scripts"][1] = "../lib/jquery.validate.min.js";
		$data["scripts"][2] = "busqueda.tabla.js";
		$data["scripts"][3] = "articulo/detalles.articulo.js";
		$data["css"][0] = "lightbox.css";

		$this->load->view('templates/basica', $data);
	}

	public function imagenes($id_articulo = '')
	{
		$data["articulo"] = $this->Articulo_model->get_articulo($id_articulo);
		$data["main_content"] = "templates/columnas/100";
		$data["columna_1"] = $this->folder."img_manager";
		$data["scripts"][0] = "lightbox.js";
		$data["scripts"][1] = "../lib/jquery.validate.min.js";
		$data["scripts"][2] = "busqueda.tabla.js";
		$data["scripts"][3] = "articulo/imagenes.articulo.js";
		$data["css"][0] = "lightbox.css";

		$this->load->view('templates/basica', $data);
	}

	public function inventario($id_articulo = '', $id_presentacion = '')
	{
		$data["articulo"] = $this->Articulo_model->get_articulo($id_articulo);
		$data["presentacion_seleccionada"] = $id_presentacion;
		$data["main_content"] = "templates/columnas/100";
		$data["columna_1"] = $this->folder."inventario";
		$data["scripts"][0] = "lightbox.js";
		$data["scripts"][1] = "../lib/jquery.validate.min.js";
		$data["scripts"][2] = "busqueda.tabla.js";
		$data["scripts"][3] = "articulo/inventario.articulo.js";
		$data["css"][0] = "lightbox.css";

		$this->load->view('templates/basica', $data);
	}
	/*
	public function nuevo()
	{
		$data["main_content"] = "templates/columnas/100";
		$data["columna_1"] = $this->folder."formularios/nuevo_articulo";
		$data["scripts"][0] = "../lib/chosen/chosen.jquery.min.js";
		$data["scripts"][1] = "../lib/bootstrap-colorpicker.min.js";
		$data["scripts"][2] = "../lib/bootstrap-datepicker.js";
		$data["scripts"][3] = "../lib/jquery.validate.min.js";
		$data["scripts"][4] = "coleccion/widgets.coleccion.js";
		$data["scripts"][5] = "articulo/registro.articulo.js";
		$data["scripts"][6] = "galeria/aplicacion.articulos.js";
		$data["css"][0] = "chosen.min.css";
		$data["css"][1] = "bootstrap-colorpicker.min.css";
		$data["css"][2] = "datepicker3.css";
		$data["categorias"] = $this->Articulo_model->get_categorias();
		$data["colecciones"] = $this->Articulo_model->get_colecciones($this->session->userdata("id"));
		$this->load->view('templates/basica', $data);
	}
	*/
	public function nuevo()
	{
		$data["main_content"] = "templates/columnas/100";
		$data["columna_1"] = $this->folder."formularios/nuevo_articulo";
		$data["scripts"][0] = "../lib/chosen/chosen.jquery.min.js";
		$data["scripts"][1] = "../lib/bootstrap-colorpicker.min.js";
		$data["scripts"][2] = "../lib/bootstrap-datepicker.js";
		$data["scripts"][3] = "../lib/jquery.validate.min.js";
		$data["scripts"][4] = "coleccion/widgets.coleccion.js";
		$data["scripts"][5] = "articulo/registro.articulo.js";
		$data["scripts"][6] = "galeria/aplicacion.articulos.js";
		$data["css"][0] = "chosen.min.css";
		$data["css"][1] = "bootstrap-colorpicker.min.css";
		$data["css"][2] = "datepicker3.css";
		$data["categorias"] = $this->Articulo_model->get_categorias();
		$data["colecciones"] = $this->Articulo_model->get_colecciones($this->session->userdata("id"));
		$data["titulo"] = "Nuevo Art&iacute;culo";
		$this->load->view('templates/basica', $data);
	}

	public function editar($id_articulo='')
	{
		if ($id_articulo!="") :
			$data["main_content"] = "templates/columnas/100";
			$data["columna_1"] = $this->folder."formularios/nuevo_articulo";
			$data["scripts"][0] = "../lib/chosen/chosen.jquery.min.js";
			$data["scripts"][1] = "../lib/bootstrap-colorpicker.min.js";
			$data["scripts"][2] = "../lib/bootstrap-datepicker.js";
			$data["scripts"][3] = "../lib/jquery.validate.min.js";
			$data["scripts"][4] = "coleccion/widgets.coleccion.js";
			$data["scripts"][5] = "articulo/registro.articulo.js";
			$data["scripts"][6] = "galeria/aplicacion.articulos.js";
			$data["css"][0] = "chosen.min.css";
			$data["css"][1] = "bootstrap-colorpicker.min.css";
			$data["css"][2] = "datepicker3.css";
			$data["categorias"] = $this->Articulo_model->get_categorias();
			$data["colecciones"] = $this->Articulo_model->get_colecciones($this->session->userdata("id"));
			$data["titulo"] = "Editar Art&iacute;culo";
			$data["articulo"] = $this->Articulo_model->get_articulo($id_articulo);
			$this->load->view('templates/basica', $data);
		endif;
	}

	public function desactivar($id_articulo="")
	{
		$reg = array(
			"status" => -2
			);

		$this->db->where("id", $id_articulo)->update("articulo", $reg);

		redirect('usuario/cuenta/articulos');
	}

	public function activar($id_articulo="")
	{
		$total = $this->Articulo_model->get_total_piezas($id_articulo);
		if ($total == 0) :
			$reg = array(
			"status" => 0
			);
		else:
			$reg = array(
			"status" => 1
			);
		endif;

		$this->db->where("id", $id_articulo)->update("articulo", $reg);

		redirect('usuario/cuenta/articulos');
	}

	//VIA Ajax
	public function get_widget($widget='', $extra1='', $extra2='')
	{
		if ($widget == "nueva_coleccion") :
			$data["id_usuario"] = $extra1;
			$data["data_trigger"] = $extra2;
			$this->load->view($this->folder.'widgets/nueva_coleccion',$data);
		endif;
	}

	//Via AJAX
	public function crear_coleccion()
	{
		echo json_encode($this->Articulo_model->crear_coleccion($this->session->userdata("id")));
	}

	public function crear_coleccion_redirect()
	{
		if ($this->input->post("tipo") == "crear") :
			$this->Articulo_model->crear_coleccion($this->session->userdata("id"));
		elseif ($this->input->post("tipo") == "editar"):
			$this->Articulo_model->editar_coleccion($this->input->post("id_coleccion"));
		else:
			$this->Articulo_model->eliminar_coleccion($this->input->post("id_coleccion"));
		endif;

		redirect('usuario/cuenta/articulos');
	}

	//Via AJAX
	public function manejar_colecciones($id_usuario)
	{
		$data["colecciones"] = $this->Articulo_model->get_colecciones($id_usuario);

		$this->load->view($this->folder."informaciones/manejar_colecciones", $data);
	}
	/*
	public function crear_articulo()
	{
		$this->Articulo_model->crear_articulo($this->session->userdata("id"));

		$data["main_content"] = "templates/columnas/100";
		$data["columna_1"] = $this->folder."informaciones/creacion_articulo_completo";
		$this->load->view('templates/basica', $data);
	}
	*/
	public function crear_articulo()
	{
		$data["id_articulo"] = $this->Articulo_model->crear_articulo($this->session->userdata("id"));

		$data["main_content"] = "templates/columnas/100";
		$data["columna_1"] = $this->folder."informaciones/creacion_articulo_completo";
		$this->load->view('templates/basica', $data);	
	}

	public function editar_articulo($id_articulo)
	{
		$this->Articulo_model->editar_articulo($id_articulo);

		redirect('articulo/detalles/'.$id_articulo);
	}

	public function eliminar_articulo($id_articulo='')
	{
		$this->Articulo_model->eliminar_articulo($id_articulo);

		redirect('usuario/cuenta');
	}

	public function primera_activacion($id_articulo = '')
	{
		$articulo = $this->Articulo_model->get_articulo($id_articulo);
		if ($this->Articulo_model->primera_activacion($articulo)) :
			redirect('articulo/inventario/'.$id_articulo);
		endif;
	}

	/*//Via AJAX
	public function modificar_inventario()
	{
		$data["inventario"] = $this->Articulo_model->get_inventario_info($this->input->post("id_inventario"));
		$this->load->view($this->folder.'widgets/modificar_inventario', $data);
	}*/

	//Via AJAX
	public function modificar_inventario()
	{
		$data["presentacion"] = $this->Articulo_model->get_presentacion($this->input->post("id_presentacion"));
		$data["tallas"] = $this->Articulo_model->get_tallas($this->input->post("id_articulo"));
		foreach ($data["tallas"] as $talla) :
			$talla->inventario = $this->Articulo_model->get_inventario($this->input->post("id_articulo"), $this->input->post("id_presentacion"), $talla->id);
		endforeach;
		$this->load->view($this->folder.'widgets/modificar_inventario', $data);
	}

	/*public function operacion_inventario($id_inventario,$id_articulo)
	{
		$cantidad = $this->input->post("cantidad");
		if ($cantidad > 0) :
			$this->Articulo_model->agregar_piezas_inventario($id_inventario, $cantidad);
		elseif ($cantidad < 0) :
			$this->Articulo_model->eliminar_piezas_inventario($id_inventario, $cantidad);
		endif;
		
		redirect('articulo/inventario/'.$id_articulo);
	}*/

	public function operacion_inventario($id_articulo, $id_presentacion = "")
	{
		$id_inventario = $this->input->post("id_inventario");
		$cantidad = $this->input->post("cantidad");
		for ($i=0; $i < count($id_inventario); $i++) : 
			if ($cantidad[$i] > 0) :
				$this->Articulo_model->agregar_piezas_inventario($id_inventario[$i], $cantidad[$i]);
			elseif ($cantidad[$i] < 0) :
				$this->Articulo_model->eliminar_piezas_inventario($id_inventario[$i], $cantidad[$i]);
			endif;	
		endfor;
		
		redirect('articulo/inventario/'.$id_articulo.'/'.$id_presentacion);
	}

	public function reportar_pago()
	{
		$this->Articulo_model->registrar_pago($this->session->userdata("id"));

		redirect("usuario/cuenta/compras");
	}

	//Via AJAX
	public function get_acordion_pagos_reportados($id_compra='')
	{
		$data["pagos"] = $this->Articulo_model->get_pagos($id_compra);
		$data["compra"] = $this->Articulo_model->get_compra($id_compra);

		$this->load->view($this->folder."informaciones/acordion_pagos_reportados", $data);
	}

	public function marcar_favorito($id_articulo="")
	{
		echo $this->Articulo_model->marcar_favorito($this->session->userdata("id"), $id_articulo);
	}

	public function eliminar_favorito($id_lista_deseos="")
	{
		$this->db->where("id", $id_lista_deseos)->delete("lista_deseos");

		redirect("usuario/cuenta/deseos");
	}

	public function marcar_verificado($id_pago="")
	{
		echo $this->Articulo_model->marcar_verificado($id_pago);
	}

	public function get_direcciones_envio($id_usuario="", $id_compra="")
	{
		$data["perfil"] = $this->Usuario_model->get_perfil($id_usuario);
		$data["compra"] = $this->Articulo_model->get_compra($id_compra);

		$this->load->view($this->folder."informaciones/direccion_envios_compra", $data);
	}

	public function guardar_direccion_alt()
	{
		$this->Articulo_model->guardar_direccion_alt($this->input->post("id_compra"), $this->input->post("direccion_alt"));
	}

	public function cambiar_status_compra($id_compra='', $status="")
	{
		$compra = $this->Articulo_model->get_compra($id_compra);

		if ($compra->id_usuario_vendedor == $this->session->userdata("id")) :
			$this->Articulo_model->cambiar_status_compra($id_compra, $status);
		endif;

		redirect("usuario/cuenta/ventas");
	}

	public function marcar_enviado($id_compra='')
	{
		$this->Articulo_model->marcar_enviado($id_compra);

		redirect("usuario/cuenta/ventas");
	}

	//Via AJAX
	public function detalles_compra($id_compra='')
	{
		$compra = $this->Articulo_model->get_compra($id_compra);

		$this->load->view("articulo/widgets/detalles_compra",$compra);
	}

	//Via AJAX
	public function eliminar_imagen()
	{
		$this->Galeria_model->eliminar_imagen($this->input->post("id_imagen"));
		$this->Articulo_model->eliminar_articulofoto($this->input->post("id_articulofotos"));
		echo "1";
	}

	//Via AJAX
	public function formulario_calificacion_compra()
	{
		$data["compra"] = $this->Articulo_model->get_compra($this->input->post("id_compra"));

		$vendedor["disenador"] = $this->Usuario_model->get_perfil($this->input->post("id_usuario_vendedor"));
		$vendedor["disenador"]->usuario = $this->Usuario_model->get_usuario("","", $this->input->post("id_usuario_vendedor"));

		$data["vendedor"] = $vendedor;

		$this->load->view($this->folder."formularios/calificar_compra", $data);
	}

	public function formulario_calificacion_venta()
	{
		$data["compra"] = $this->Articulo_model->get_compra($this->input->post("id_compra"));

		$comprador["comprador"] = $this->Usuario_model->get_perfil($this->input->post("id_usuario_comprador"));
		$comprador["comprador"]->usuario = $this->Usuario_model->get_usuario("","", $this->input->post("id_usuario_comprador"));

		$data["comprador"] = $comprador;

		$data["calificacion"] = $this->Usuario_model->get_calificacion_compra($this->input->post("id_compra"), "compra");

		$this->load->view($this->folder."formularios/calificar_venta", $data);
	}

	public function calificar_compra()
	{
		$data = $this->input->post();

		$this->Usuario_model->modificar_reputacion($data["id-usuario-vendedor"], $data["tipo"], $this->session->userdata("id"), $data["comentario"], $data["id-compra"], $data["recibido"], "compra");

		if ($data["recibido"] == 0) :
			$this->Articulo_model->cambiar_status_compra($data["id-compra"], -1);
		else :
			$this->Articulo_model->cambiar_status_compra($data["id-compra"], 4);
		endif;

		$this->Articulo_model->__enviar_calificacion_comprador($data["id-compra"]);
		
		redirect("usuario/cuenta/compras");
	}

	public function calificar_venta()
	{
		$data = $this->input->post();

		$this->Usuario_model->modificar_reputacion($data["id-usuario-comprador"], $data["tipo"], $this->session->userdata("id"), $data["comentario"], $data["id-compra"], 0, "venta");
		$status = $this->Articulo_model->get_status_compra($data["id-compra"]);
		if ($status == -1) :
			$this->Articulo_model->cambiar_status_compra($data["id-compra"], -2);
		elseif ($status == 4) :
			$this->Articulo_model->cambiar_status_compra($data["id-compra"], 5);
		endif;

		$this->Articulo_model->__enviar_calificacion_vendedor($data["id-compra"]);
		
		redirect("usuario/cuenta/ventas");
	}

	public function ver_calificaciones()
	{
		if ($this->input->post("por") == "compra"):
			$data["propio"] = $this->Usuario_model->get_calificacion_compra($this->input->post("id_compra"), "compra");
			$data["nopropio"] = $this->Usuario_model->get_calificacion_compra($this->input->post("id_compra"), "venta");
			$data["nopropio"]->usuario = $this->Usuario_model->get_usuario("","",$data["nopropio"]->id_usuario_puntuador);
		else:
			$data["propio"] = $this->Usuario_model->get_calificacion_compra($this->input->post("id_compra"), "venta");
			$data["nopropio"] = $this->Usuario_model->get_calificacion_compra($this->input->post("id_compra"), "compra");
			$data["nopropio"]->usuario = $this->Usuario_model->get_usuario("","",$data["nopropio"]->id_usuario_puntuador);
		endif;

		$this->load->view($this->folder."informaciones/ver_calificaciones", $data);
	}

	public function hacer_pregunta()
	{
		$data["pregunta"] = $this->Articulo_model->guardar_pregunta($this->input->post("id_usuario"), $this->input->post("id_articulo"), $this->input->post("pregunta"));

		$this->Articulo_model->__avisar_usuario_pregunta($data["pregunta"]["id"]);

		$this->load->view($this->folder.'formularios/pregunta_hecha', $data);

	}

	public function hacer_respuesta()
	{
		$data["respuesta"] = $this->Articulo_model->guardar_respuesta($this->input->post("id_pregunta"), $this->input->post("respuesta"));

		$this->Articulo_model->__avisar_usuario_respuesta($this->input->post("id_pregunta"));

		$this->load->view($this->folder.'formularios/respuesta_hecha', $data);
	}

	public function poner_destacado($id_articulo)
	{
		$this->db->where("id", $id_articulo)->update("articulo", array("destacado" => 1));

		redirect("sitio/articulo/".$id_articulo);
	}

	public function quitar_destacado($id_articulo)
	{
		$this->db->where("id", $id_articulo)->update("articulo", array("destacado" => 0));

		redirect("sitio/articulo/".$id_articulo);
	}
}

/* End of file articulo.php */
/* Location: ./application/controllers/articulo.php */