<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Galeria extends CI_Controller {

	private $folder;

	public function __construct()
    {
        parent::__construct();
        $this->folder = "galeria/";
        $this->load->model(array("Galeria_model", "Usuario_model"));
        if($this->session->userdata('logueado')!=true) :
	    	redirect('usuario/login');
		endif;
    }

	public function index()
	{
		$data["main_content"] = "templates/columnas/100";
		$data["columna_1"] = $this->folder."aplicacion";
		$data["scripts"][0] = "../lib/chosen/chosen.jquery.min.js";
		$data["scripts"][1] = "galeria/aplicacion_maestra.js";
		$data["css"][0] = "chosen.min.css";
		$this->load->view('templates/basica', $data);
	}

	//Via AJAX
	public function adicional($seccion='', $extra='')
	{
		if ($seccion == "upload_image") :
			$data["resultado"] = $extra;
			$data["albums"] = $this->Galeria_model->get_albums($this->input->post("id_usuario"));
		endif;
		$data["id_usuario"] = $this->input->post("id_usuario");
		$this->load->view($this->folder."adicionales/".$seccion, $data);
	}

	//Via AJAX
	public function upload_image($id_usuario)
	{
		if($this->Galeria_model->upload_image($id_usuario)!= false):
			$this->adicional("upload_image",'<div class="alert alert-success"><strong>Subida Exitosa</strong></div>');
		else:
			$this->adicional("upload_image",'<div class="alert alert-danger"><strong>Error en subida</strong></div>');
		endif;
	}

	//Via AJAX
	public function upload_image_articulo($id_usuario, $id_articulo, $id_presentacion)
	{
		$id_imagen = $this->Galeria_model->upload_image_articulo($id_usuario, $id_articulo, $id_presentacion); 
		if($id_imagen != false):
			$data["imagen"] = $this->Galeria_model->get_image($id_imagen);
			$data["articulo_fotos"] = $this->db->where("id_articulo", $id_articulo)->where("id_presentacion", $id_presentacion)->where("id_imagen", $id_imagen)->get("articulo_fotos")->row();
			$data["mensaje"]= '<div class="alert alert-success"><strong>Subida Exitosa</strong></div>';
		else:
			$data["id_imagen"] = "";
			$data["articulo_fotos"]->id="";
			$data["mensaje"]= '<div class="alert alert-danger"><strong>Error en subida</strong></div>';
		endif;
		$this->load->view($this->folder."adicionales/resultado_articulo", $data);
	}

	//Via AJAX
	public function get_images_data($id_usuario)
	{
		echo json_encode($this->Galeria_model->get_images($id_usuario));
	}

	//Via AJAX
	public function minima($value='')
	{
		$this->load->view($this->folder."aplicacion_minima");
	}

	//Via AJAX
	public function check_nombre()
	{
		$nombre = $this->input->post("nombre");

		if ($this->Galeria_model->check_nombre($nombre) == true) :
			echo "false";
		else:
			echo "true";
		endif;
	}

	//Via AJAX
	public function upload_image_perfil()
	{
		$perfil = $this->Usuario_model->get_perfil($this->session->userdata("id"));

		$id_imagen_nueva = $this->Galeria_model->update_imagen_perfil($perfil->id_imagen);
		$this->Usuario_model->editar_imagen_perfil($perfil->id, $id_imagen_nueva);

		echo "ok";
	}

}

/* End of file galeria.php */
/* Location: ./application/controllers/galeria.php */