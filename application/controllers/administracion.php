<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administracion extends CI_Controller {

	private $folder;

	public function __construct()
    {
        parent::__construct();
        // Your own constructor code
        $this->folder = "administracion/";
        $this->load->model(array("Galeria_model", "Articulo_model", "Usuario_model", "Administracion_model"));
        if($this->session->userdata('logueado')!=true) :
	    	redirect('usuario/login');
	    elseif ($this->session->userdata('id_tipo')>=2):
	    	redirect('usuario/cuenta');
		endif;
    }

	public function index()
	{
		$data["main_content"] = "templates/columnas/100";
		$data["columna_1"] = $this->folder."home";
		$this->load->view("templates/admin", $data);
	}

	public function disenadores()
	{
		$data["disenadores"] = $this->Usuario_model->get_disenadores_todos();
		$data["main_content"] = "templates/columnas/100";
		$data["columna_1"] = $this->folder."ver_disenadores";
		$data["scripts"][0] = "busqueda.tabla.js";
		$this->load->view("templates/admin", $data);
	}

	public function bloquear_usuario($id_usuario="")
	{
		$this->Usuario_model->bloquear_usuario($id_usuario);

		redirect("administracion/disenadores");
	}

	public function activar_usuario($id_usuario="")
	{
		$this->Usuario_model->activar_usuario($id_usuario);

		redirect("administracion/disenadores");
	}

	public function poner_disenador_destacado($id_usuario='')
	{
		$this->Usuario_model->disenador_destacado($id_usuario, 1);

		redirect("administracion/disenadores");
	}

	public function quitar_disenador_destacado($id_usuario='')
	{
		$this->Usuario_model->disenador_destacado($id_usuario, 0);

		redirect("administracion/disenadores");
	}

	public function facturas()
	{
		if ($this->input->post("fecha-desde") != "" && $this->input->post("fecha-hasta") != ""):
			$data["facturas"] = $this->Administracion_model->get_facturas(datepicker_to_date($this->input->post("fecha-desde")), datepicker_to_date($this->input->post("fecha-hasta")));
		endif;
		$data["main_content"] = "templates/columnas/100";
		$data["columna_1"] = $this->folder."home_facturas";
		$data["scripts"][0] = "busqueda.tabla.js";
		$data["scripts"][1] = "../lib/bootstrap-datepicker.js";
		$data["scripts"][2] = $this->folder."ver.facturas.js";
		$data["css"][0] = "datepicker3.css";
		$this->load->view("templates/admin", $data);
	}

	public function generar_facturas(){
		if ($this->input->post("fecha-desde") != "" && $this->input->post("fecha-hasta") != ""):
			$data["facturas"] = $this->Administracion_model->generar_facturas(datepicker_to_date($this->input->post("fecha-desde")), datepicker_to_date($this->input->post("fecha-hasta")));
		endif;
		$data["main_content"] = "templates/columnas/100";
		$data["columna_1"] = $this->folder."generar_facturas";
		$data["scripts"][0] = "busqueda.tabla.js";
		$data["scripts"][1] = $this->folder."generar.facturas.js";
		$this->load->view("templates/admin", $data);	
	}

	//Via AJAX
	public function eliminar_factura($id_factura)
	{
		$this->db->where("id", $id_factura)->delete("servicio_factura");
		$compras = $this->db->where("id_factura", $id_factura)->get("servicio_factura_item")->result();
		foreach ($compras as $compra) :
			$this->db->where("id", $compra->id_compra)->update("compras", array("id_factura" => 0));
		endforeach;
		$this->db->where("id_factura", $id_factura)->delete("servicio_factura_item");
	}

	//Via AJA
	public function cambiar_status_factura($id_factura, $status)
	{
		$this->Administracion_model->cambiar_status_factura($id_factura, $status);

		echo "1";
	}

	//Via AJAX
	public function enviar_correo($id_factura)
	{
		$this->Administracion_model->__enviar_factura_cliente($id_factura);
	}

	public function enviar_correos_por_enviar()
	{
		$facturas = $this->db->where("aviso_correo",0)->get("servicio_factura")->result();

		foreach ($facturas as $factura) :
			$this->Administracion_model->__enviar_factura_cliente($factura->id);		
		endforeach;

		redirect("administracion/facturas");
	}

	public function actualidad()
	{
		$data["noticias"] = $this->Administracion_model->get_noticias();
		$data["main_content"] = "templates/columnas/100";
		$data["columna_1"] = $this->folder."formulario_actualidad";
		$data["scripts"][0] = "../lib/summernote.min.js";
		$data["scripts"][1] = $this->folder."actualidad.js";
		$data["css"][0] = "summernote.css";
		$data["css"][1] = "font-awesome.min.css";
		$this->load->view("templates/admin", $data);
	}

	public function editar_noticia()
	{
		if ($this->input->post("tipo-accion") == "crear") :
			$this->Administracion_model->crear_noticia();
		else:
			$this->Administracion_model->editar_noticia($this->input->post("id-noticia"));
		endif;

		redirect("administracion/actualidad");
	}

	public function eliminar_noticia($id_noticia)
	{
		$this->Administracion_model->eliminar_noticia($id_noticia);

		redirect("administracion/actualidad");
	}

	public function get_noticia($id_noticia)
	{
		$noticia = $this->Administracion_model->get_noticia($id_noticia);

		echo json_encode($noticia);
	}

	public function ventas()
	{
		if ($this->input->post("fecha-desde") != "" && $this->input->post("fecha-hasta") != ""):
			$data["ventas"] = $this->Administracion_model->get_ventas(datepicker_to_date($this->input->post("fecha-desde")), datepicker_to_date($this->input->post("fecha-hasta")));
		endif;
		$data["main_content"] = "templates/columnas/100";
		$data["columna_1"] = $this->folder."home_ventas";
		$data["scripts"][0] = "busqueda.tabla.js";
		$data["scripts"][1] = "../lib/bootstrap-datepicker.js";
		$data["scripts"][2] = $this->folder."ver.ventas.js";
		$data["css"][0] = "datepicker3.css";
		$this->load->view("templates/admin", $data);
	}

	public function correos_masivos()
	{
		$data["scripts"][0] = "../lib/summernote.min.js";
		$data["scripts"][1] = $this->folder."correos.masivos.js";
		$data["css"][0] = "summernote.css";
		$data["css"][1] = "summernote-bs3.css";
		$data["css"][2] = "font-awesome.min.css";

		if ($this->input->post('enviar') == "enviar") {
			$data["enviados"] = $this->Administracion_model->__enviar_masivo();
			$data["main_content"] = "templates/columnas/100";
			$data["columna_1"] = $this->folder."correos_masivos";
			$this->load->view("templates/admin", $data);
		} else {
			$data["main_content"] = "templates/columnas/100";
			$data["columna_1"] = $this->folder."correos_masivos";
			$this->load->view("templates/admin", $data);
		}
		
	}

}

/* End of file administracion.php */
/* Location: ./application/controllers/administracion.php */