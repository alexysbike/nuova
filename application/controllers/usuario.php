<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuario extends CI_Controller {

	private $folder;

	public function __construct()
    {
        parent::__construct();
        // Your own constructor code
        $this->load->model(array("Usuario_model", "Galeria_model", "Articulo_model"));
        $this->folder = "usuario/";
    }

	public function index()
	{
		redirect("usuario/cuenta");
	}

	public function nuevo($tipo='cliente')
	{	
		if($tipo == "cliente"):
			$data["tipo_usuario"] = 3;
		elseif($tipo == "disenador"):
			$data["tipo_usuario"] = 2;
		else:
			redirect("/error");
		endif;
		$data["main_content"] = "templates/columnas/50_50";
		$data["columna_1"] = $this->folder."formularios/nuevo_usuario";
		$data["columna_2"] = $this->folder."informaciones/ser_disenador";
		$data["scripts"][0] = "../lib/jquery.validate.min.js";
		$data["scripts"][1] = "../lib/bootstrap-datepicker.js";
		$data["scripts"][2] = "usuario/registro.usuario.js";
		$data["css"][0] = "datepicker3.css";
		$this->load->view('templates/basica', $data);
	}

	public function finalizar_registro($id_usuario = '')
	{
		$data["main_content"] = "templates/columnas/50_50";
		$data["columna_1"] = $this->folder."formularios/editar_perfil";
		$data["columna_2"] = $this->folder."informaciones/registro_perfil";
		$data["scripts"][0] = "../lib/jquery.validate.min.js";
		$data["scripts"][1] = "../lib/bootstrap-datepicker.js";
		$data["scripts"][2] = "usuario/registro.usuario.js";
		$data["css"][0] = "datepicker3.css";
		$data["tipo_perfil"] = "crear";
		$data["id_usuario"] = $id_usuario;
		$data["estados"] = $this->db->where("id_pais", 233)->get("geo_estados")->result();
		$this->load->view('templates/basica', $data);
	}

	public function cambiar_contrasena()
	{
		echo $this->Usuario_model->cambiar_contrasena($this->session->userdata("id"));
	}

	//Via AJAX
	public function check_correo()
	{
		$correo = $this->input->post("correo");

		if ($this->Usuario_model->check_correo($correo) == true) :
			echo "false";
		else:
			echo "true";
		endif;
	}

	//Via AJAX
	public function check_alias()
	{
		$alias = $this->input->post("alias");

		if ($this->Usuario_model->check_alias($alias) == true) :
			echo "false";
		else:
			echo "true";
		endif;
	}

	//Via AJAX
	public function crear_usuario()
	{
		if($this->Usuario_model->crear_usuario()):
			$data["id_usuario"] = $this->db->insert_id();
			$this->Galeria_model->create_user_folder($data["id_usuario"]);
			$data["estados"] = $this->db->where("id_pais", 233)->get("geo_estados")->result();
			$data["tipo_perfil"] = "crear";
			$this->load->view($this->folder."formularios/editar_perfil",$data);
		else:
			echo "error";
		endif;
	}

	public function editar_perfil($id_usuario = '')
	{
		if ($this->input->post("tipo-perfil") == "crear") :
			if($this->Usuario_model->crear_perfil()):
				$this->session->set_userdata("logueado", true);
				$this->session->set_userdata($this->Usuario_model->get_usuario("","",$this->input->post("id_usuario")));
				if ($this->session->userdata("id_tipo") == 2):
					$this->Usuario_model->__enviar_aviso($this->input->post("id_usuario"));
				endif;
				$data["main_content"] = "templates/columnas/100";
				$data["columna_1"] = $this->folder."informaciones/registro_usuario_completo";
				$data["tipo_usuario"] = $this->session->userdata("id_tipo");
				$data["scripts"][0] = "usuario/registro.usuario.js";
				$this->load->view('templates/basica', $data);
			else:
				echo "Error de Creacion de Perfil";
			endif;
		elseif ($this->input->post("tipo-perfil") == "editar") :
			$this->Usuario_model->editar_perfil($this->session->userdata("id"));
			redirect("usuario/cuenta");
		else :
			$data["main_content"] = "templates/columnas/50_50";
			$data["columna_1"] = $this->folder."formularios/editar_perfil";
			$data["columna_2"] = $this->folder."informaciones/publicidad";
			$data["scripts"][0] = "../lib/jquery.validate.min.js";
			$data["scripts"][1] = "../lib/bootstrap-datepicker.js";
			$data["scripts"][2] = "usuario/registro.usuario.js";
			$data["css"][0] = "datepicker3.css";
			$data["tipo_perfil"] = "editar";
			$data["id_usuario"] = $id_usuario;
			$data["perfil"] = $this->Usuario_model->get_perfil($id_usuario);
			$data["estados"] = $this->db->where("id_pais", 233)->get("geo_estados")->result();
			$this->load->view('templates/basica', $data);
		endif;
		
	}

	public function ver_articulos($id="")
	{
		$data["perfil"] = $this->Usuario_model->get_perfil($id);
		$data["perfil"]->usuario = $this->Usuario_model->get_usuario("","",$id);
		$data["articulos"] = $this->Articulo_model->get_articulos_usuario($id);
		$data["columna_1"] = $this->folder."articulos_usuario";
		$data["main_content"] = "templates/columnas/100";
		$this->load->view("templates/basica", $data);
	}

	public function ver_articulos_coleccion($id="")
	{
		$data["coleccion"] = $this->Articulo_model->get_coleccion($id);
		$data["articulos"] = $this->Articulo_model->get_articulos_coleccion($id);
		$data["columna_1"] = $this->folder."articulos_coleccion";
		$data["main_content"] = "templates/columnas/100";
		$this->load->view("templates/basica", $data);
	}

	public function ver_perfil($id)
	{
		$data["perfil"] = $this->Usuario_model->get_perfil($id);
		$data["perfil"]->usuario = $this->Usuario_model->get_usuario("","",$id);
		$data["colecciones"] = $this->Articulo_model->get_colecciones($id);
		$data["articulos"] = $this->Articulo_model->get_ultimos_articulos($id);
		$data["calificaciones"] = $this->Usuario_model->get_reputacion($id);
		$data["columna_1"] = $this->folder."perfil_publico";
		$data["main_content"] = "templates/columnas/100";
		$data["scripts"][0] = "lightbox.js";
		$data["scripts"][1] = "usuario/calificaciones.js";
		$data["css"][0] = "lightbox.css";
		$this->load->view("templates/basica", $data);	
	}

	//Via AJAX
	public function get_informacion($informacion='')
	{
		$this->load->view($this->folder."informaciones/".$informacion);
	}
	/*
	public function login()
	{
		if($this->input->post("login") == "login"):
			$usuario = $this->Usuario_model->get_usuario($this->input->post("correo"), $this->input->post("password"));
			if(!empty($usuario)):
				if (count($this->Usuario_model->get_perfil($usuario->id)) == 0):
					redirect("usuario/finalizar_registro/".$usuario->id);
				endif;
				$this->session->set_userdata("logueado", true);
				$this->session->set_userdata($usuario);
				redirect("usuario/cuenta");
			else:
				$data["main_content"] = "templates/columnas/50_50";
				$data["columna_1"] = $this->folder."formularios/login";
				$data["columna_2"] = $this->folder."informaciones/error_logeo";
				$this->load->view('templates/basica', $data);
			endif;
		else:
			$data["main_content"] = "templates/columnas/50_50";
			$data["columna_1"] = $this->folder."formularios/login";
			$data["columna_2"] = $this->folder."informaciones/ser_disenador";
			$this->load->view('templates/basica', $data);
		endif;
	}
	*/
	public function login()
	{
		if($this->input->post("login") == "login"):
			$usuario = $this->Usuario_model->get_usuario($this->input->post("correo"), $this->input->post("password"));
			if(!empty($usuario)):
				if (count($this->Usuario_model->get_perfil($usuario->id)) == 0):
					redirect("usuario/finalizar_registro/".$usuario->id);
				endif;
				$this->session->set_userdata("logueado", true);
				$this->session->set_userdata($usuario);
				redirect("usuario/cuenta");
			else:
				$data["main_content"] = "templates/columnas/50_50";
				$data["columna_1"] = $this->folder."formularios/login";
				$data["columna_2"] = $this->folder."informaciones/error_logeo";
				$this->load->view('templates/basica', $data);
			endif;
		else:
			$data["main_content"] = "templates/columnas/50_50";
			$data["columna_1"] = $this->folder."formularios/login";
			$data["columna_2"] = $this->folder."informaciones/ser_disenador";
			$this->load->view('templates/basica', $data);
		endif;
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect();
	}

	public function olvido_contrasena()
	{
		if($this->input->post("olvido") == "olvido"):
			$usuario = $this->Usuario_model->get_usuario_bycorreo($this->input->post("correo"));
			if (count($usuario) > 0):
				$this->Usuario_model->__enviar_contrasena($usuario->id);
				$data["reenvio"] = true;
				$data["main_content"] = "templates/columnas/50_50";
				$data["columna_1"] = $this->folder."formularios/login";
				$data["columna_2"] = $this->folder."informaciones/ser_disenador";
				$this->load->view('templates/basica', $data);
			else:
				$data["main_content"] = "templates/columnas/50_50";
				$data["columna_1"] = $this->folder."formularios/olvido_contrasena";
				$data["columna_2"] = $this->folder."informaciones/error_correo";
				$this->load->view('templates/basica', $data);
			endif;
		else:
			$data["main_content"] = "templates/columnas/50_50";
			$data["columna_1"] = $this->folder."formularios/olvido_contrasena";
			$data["columna_2"] = $this->folder."informaciones/ser_disenador";
			$this->load->view('templates/basica', $data);
		endif;
	}

	public function cuenta($modo = "")
	{
		if($this->session->userdata('logueado')!=true) :
	    	redirect('usuario/login');
		endif;
		$data["factura_pendiente"] = $this->Usuario_model->factura_pendiente($this->session->userdata("id"));
		$data["modo"] = $modo;
		$data["perfil"] = $this->Usuario_model->get_perfil($this->session->userdata("id"));
		$data["perfil"]->usuario = $this->Usuario_model->get_usuario("","",$this->session->userdata("id"));
		$data["articulos"] = $this->Articulo_model->get_articulos_cuenta($this->session->userdata("id"));
		$data["deseos"] = $this->Articulo_model->get_articulos_deseos($this->session->userdata("id"));
		$data["compras"] = $this->Articulo_model->get_compras($this->session->userdata("id"));
		$data["ventas"] = $this->Articulo_model->get_ventas($this->session->userdata("id"));
		$data["ventas_nuevas"] = count($this->Articulo_model->get_ventas_nuevas($this->session->userdata("id")));
		$data["datos_pago"] = $this->Usuario_model->get_datos_pago($this->session->userdata("id"));
		$data["n_preguntas_sc"] = $this->Articulo_model->get_preguntas_por_contestar($this->session->userdata("id"));
		$data["calificaciones"] = $this->Usuario_model->get_reputacion($this->session->userdata("id"));
		$data["main_content"] = "templates/columnas/100";
		$data["columna_1"] = $this->folder."cuenta";
		$data["scripts"][0] = "../lib/chosen/chosen.jquery.min.js";
		$data["scripts"][1] = "../lib/bootstrap-datepicker.js";
		$data["scripts"][2] = "../lib/jquery.validate.min.js";
		$data["scripts"][3] = "lightbox.js";
		$data["scripts"][4] = "busqueda.tabla.js";
		$data["scripts"][5] = "usuario/cuenta.js";
		$data["scripts"][6] = "usuario/calificaciones.js";
		$data["css"][0] = "chosen.min.css";
		$data["css"][1] = "datepicker3.css";
		$data["css"][3] = "lightbox.css";
		$this->load->view('templates/basica', $data);
	}

	//Via AJAX y normal
	public function verificar_login()
	{
		if ($this->session->userdata("logueado") == true) :
			echo $this->session->userdata("id");
		else :
			echo "0";
		endif;		
	}

	//Via AJAX
	public function get_acordion_datos_pago($id_usuario="")
	{
		$data["datos_pago"] = $this->Usuario_model->get_datos_pago($id_usuario);

		$this->load->view($this->folder."informaciones/acordion_datos_pago", $data);
	}

	public function editar_datos_pago($id_usuario="")
	{
		if ($this->input->post("tipo-guardar") == "nuevo"):
			$this->Usuario_model->agregar_datos_pago($id_usuario);
		elseif($this->input->post("tipo-guardar") == "borrar"):
			$this->Usuario_model->eliminar_datos_pago($id_usuario);
		else: 
			$this->Usuario_model->editar_datos_pago($id_usuario);;
		endif;

		redirect("usuario/cuenta");
	}

	//Via AJAX
	public function detalles_vendedor($id_usuario)
	{
		$data["disenador"] = $this->Usuario_model->get_perfil($id_usuario);
		$data["disenador"]->usuario = $this->Usuario_model->get_usuario("","", $id_usuario);
		$data["datos_pago"] = $this->Usuario_model->get_datos_pago($id_usuario);

		$this->load->view($this->folder."widgets/detalles_vendedor",$data);
	}

	//Via AJAX
	public function detalles_comprador($id_usuario)
	{
		$data["comprador"] = $this->Usuario_model->get_perfil($id_usuario);
		$data["comprador"]->usuario = $this->Usuario_model->get_usuario("","", $id_usuario);

		$this->load->view($this->folder."widgets/detalles_comprador",$data);
	}

}

/* End of file articulo.php */
/* Location: ./application/controllers/usuario.php */