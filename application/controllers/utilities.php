<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Utilities extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model(array("Galeria_model", "Articulo_model", "Usuario_model"));
		$this->load->library("email");
	}

	public function index()
	{
		
	}

	public function email_prueba()
	{
		$config["mailtype"] = "html";

		$this->email->initialize($config);

		$this->email->from('alexysbike@nuovavenezuela.com', 'AXEL');
		$this->email->to('alexysbike@gmail.com'); 

		$this->email->subject('Email Test');
		$this->email->message('Testing the email class.');	

		$this->email->send();

		echo $this->email->print_debugger();	
	}

	public function prueba_nueva_compra()
	{
		$config["mailtype"] = "html";

		$this->email->initialize($config);

		$this->email->from('alexysbike@nuovavenezuela.com', 'AXEL');
		$this->email->to('alexysbike@gmail.com'); 

		$this->email->subject('Email Test');
		$data["compra"] = $this->Articulo_model->get_compra(3);
		$data["disenador"] = $this->Usuario_model->get_perfil($data["compra"]->articulo["id_usuario"]);
		$data["disenador"]->usuario = $this->Usuario_model->get_usuario("","", $data["compra"]->articulo["id_usuario"]);
		$data["datos_pago"] = $this->Usuario_model->get_datos_pago($data["compra"]->articulo["id_usuario"]);
		$html = $this->load->view("templates/email_template", $data, TRUE);
		$this->email->message($html);	

		$this->email->send();

		echo $this->email->print_debugger();	
	}

	public function prueba_nueva_venta()
	{
		$config["mailtype"] = "html";

		$this->email->initialize($config);

		$this->email->from('alexysbike@nuovavenezuela.com', 'AXEL');
		$this->email->to('alexysbike@gmail.com'); 

		$this->email->subject('Email Test');
		$data["compra"] = $this->Articulo_model->get_compra(3);
		$data["comprador"] = $this->Usuario_model->get_perfil($data["compra"]->id_usuario_comprador);
		$data["comprador"]->usuario = $this->Usuario_model->get_usuario("","", $data["compra"]->id_usuario_comprador);
		$html = $this->load->view("templates/email_template", $data, TRUE);
		$this->email->message($html);	

		$this->email->send();

		echo $this->email->print_debugger();	
	}

	public function prueba_reporte_pago()
	{
		$config["mailtype"] = "html";

		$this->email->initialize($config);

		$this->email->from('alexysbike@nuovavenezuela.com', 'AXEL');
		$this->email->to('alexysbike@gmail.com'); 

		$this->email->subject('Email Test');
		$data["compra"] = $this->Articulo_model->get_compra(3);
		$data["comprador"] = $this->Usuario_model->get_perfil($data["compra"]->id_usuario_comprador);
		$data["comprador"]->usuario = $this->Usuario_model->get_usuario("","", $data["compra"]->id_usuario_comprador);
		$html = $this->load->view("templates/email_template", $data, TRUE);
		$this->email->message($html);	

		$this->email->send();

		echo $this->email->print_debugger();	
	}

	public function prueba_reporte_envio()
	{
		$config["mailtype"] = "html";

		$this->email->initialize($config);

		$this->email->from('alexysbike@nuovavenezuela.com', 'AXEL');
		$this->email->to('alexysbike@gmail.com'); 

		$this->email->subject('Email Test');
		$data["compra"] = $this->Articulo_model->get_compra(3);
		$data["disenador"] = $this->Usuario_model->get_perfil($data["compra"]->articulo["id_usuario"]);
		$data["disenador"]->usuario = $this->Usuario_model->get_usuario("","", $data["compra"]->articulo["id_usuario"]);
		$data["observaciones_envio"] = "XYvendedorGJH DSGFHHF DSGFHHF";
		$html = $this->load->view("templates/email_template", $data, TRUE);
		$this->email->message($html);	

		$this->email->send();

		echo $this->email->print_debugger();	
	}

	public function prueba_calificacion_comprador()
	{
		$config["mailtype"] = "html";

		$this->email->initialize($config);

		$this->email->from('alexysbike@nuovavenezuela.com', 'AXEL');
		$this->email->to('alexysbike@gmail.com'); 

		$this->email->subject('Email Test');
		$data["compra"] = $this->Articulo_model->get_compra(3);
		$data["comprador"] = $this->Usuario_model->get_perfil($data["compra"]->id_usuario_comprador);
		$data["comprador"]->usuario = $this->Usuario_model->get_usuario("","", $data["compra"]->id_usuario_comprador);
		$html = $this->load->view("templates/email_template", $data, TRUE);
		$this->email->message($html);	

		$this->email->send();

		echo $this->email->print_debugger();	
	}

	public function prueba_calificacion_vendedor()
	{
		$config["mailtype"] = "html";

		$this->email->initialize($config);

		$this->email->from('alexysbike@nuovavenezuela.com', 'AXEL');
		$this->email->to('alexysbike@gmail.com'); 

		$this->email->subject('Email Test');
		$data["compra"] = $this->Articulo_model->get_compra(3);
		$data["disenador"] = $this->Usuario_model->get_perfil($data["compra"]->articulo["id_usuario"]);
		$data["disenador"]->usuario = $this->Usuario_model->get_usuario("","", $data["compra"]->articulo["id_usuario"]);
		$html = $this->load->view("templates/email_template", $data, TRUE);
		$this->email->message($html);	

		$this->email->send();

		echo $this->email->print_debugger();	
	}

	/*public function prueba_carpeta($id='')
	{
		echo $this->Galeria_model->create_user_folder($id);
	}*/

}

/* End of file utilities.php */
/* Location: ./application/controllers/utilities.php */