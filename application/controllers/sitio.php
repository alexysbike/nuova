<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sitio extends CI_Controller {

	private $folder;

	function __construct(){
		parent::__construct();
		$this->load->model(array("Galeria_model", "Articulo_model", "Usuario_model", "Administracion_model"));
		$this->folder = "sitio/";
	}

	public function index2()
	{
		$this->load->view("inicio_provisional");
	}

	public function index()
	{
		$data["destacado_alta_costura"] = $this->Articulo_model->get_articulo_destacado_home(1);
		$data["destacado_casual"] = $this->Articulo_model->get_articulo_destacado_home(2);
		$data["destacado_deportiva"] = $this->Articulo_model->get_articulo_destacado_home(3);
		$data["destacado_playera"] = $this->Articulo_model->get_articulo_destacado_home(4);
		$data["destacado_intima"] = $this->Articulo_model->get_articulo_destacado_home(5);
		$data["destacado_accesorios"] = $this->Articulo_model->get_articulo_destacado_home(6);
		$data["destacado_calzado"] = $this->Articulo_model->get_articulo_destacado_home(7);
		$data["disenador_destacado"] = $this->Usuario_model->get_disenador_destacado();
		$data["mas_vendido_alta_costura"] = $this->Articulo_model->get_articulo_mas_vendido_home(1);
		$data["mas_vendido_casual"] = $this->Articulo_model->get_articulo_mas_vendido_home(2);
		$data["mas_vendido_deportiva"] = $this->Articulo_model->get_articulo_mas_vendido_home(3);
		$data["mas_vendido_playera"] = $this->Articulo_model->get_articulo_mas_vendido_home(4);
		$data["mas_vendido_intima"] = $this->Articulo_model->get_articulo_mas_vendido_home(5);
		$data["mas_vendido_accesorios"] = $this->Articulo_model->get_articulo_mas_vendido_home(6);
		$data["mas_vendido_calzado"] = $this->Articulo_model->get_articulo_mas_vendido_home(7);
		$data["noticia"] = $this->Administracion_model->get_noticia_aleatoria();
		$data["scripts"][0] = "lightbox.js";
		$data["scripts"][1] = "sitio/home.js";
		$this->load->view("templates/home", $data);
	}

	public function buscar($categoria='todo')
	{
		if ($this->input->post("busqueda")) :
			$data["busqueda"] = $this->input->post("busqueda");
		endif;
		if ($this->input->post("ordenar")) :
			$data["ordenar"] = $this->input->post("ordenar");
		else:
			$data["ordenar"] = "destacado";
		endif;
		if ($this->input->post("filtrar")) :
			$data["filtrar"] = $this->input->post("filtrar");
		else:
			$data["filtrar"] = "";
		endif;
		if ($this->input->post("valor")) :
			$data["valor"] = $this->input->post("valor");
		else:
			$data["valor"] = "";
		endif;
		if ($this->input->post("filtro")) :
			$data["filtro"] = $this->input->post("filtro");
		endif;
		if ($this->input->post("categoria")):
			$data["categoria"] = $this->input->post("categoria");
			$categoria = $this->input->post("categoria");
		else:
			$data["categoria"] = $categoria;
		endif;
		if ($this->input->post("pagina")):
			$data["pagina"] = $this->input->post("pagina");
		else:
			$data["pagina"] = 1;
		endif;
		$data["articulos"] = $this->Articulo_model->get_articulos($data["pagina"], $categoria, $this->input->post("busqueda"));
		$data["main_content"] = "templates/columnas/100";
		$data["columna_1"] = $this->folder."articulo/busqueda";
		$data["scripts"][0] = "lightbox.js";
		$data["scripts"][1] = "articulo/busqueda.articulo.js";
		$data["css"][0] = "lightbox.css";

		$this->load->view('templates/basica', $data);	
	}

	public function buscar_disenador()
	{
		if ($this->input->post("ordenar")) :
			$data["ordenar"] = $this->input->post("ordenar");
		else:
			$data["ordenar"] = "destacado";
		endif;
		if ($this->input->post("pagina")):
			$data["pagina"] = $this->input->post("pagina");
		else:
			$data["pagina"] = 1;
		endif;
		$data["disenadores"] = $this->Usuario_model->get_disenadores($data["pagina"], $this->input->post("busqueda"));
		$data["url_busqueda"] = base_url()."sitio/buscar_disenador";
		if ($this->input->post("busqueda")) :
			$data["busqueda"] = $this->input->post("busqueda");
		endif;
		$data["main_content"] = "templates/columnas/100";
		$data["columna_1"] = $this->folder."usuario/busqueda";
		$data["scripts"][0] = "articulo/busqueda.articulo.js";

		$this->load->view('templates/basica', $data);
	}

	public function articulo($id='')
	{
		$data["articulo"] = $this->Articulo_model->get_articulo($id);
		$data["disenador"] = $this->Usuario_model->get_perfil($data["articulo"]["id_usuario"]);
		$data["disenador"]->usuario = $this->Usuario_model->get_usuario("","",$data["articulo"]["id_usuario"]);
		if ($this->session->userdata("logueado") == true) :
			$data["es_favorito"] = $this->Articulo_model->es_favorito($this->session->userdata("id"), $id);
			if ($this->session->userdata("id") != $data["articulo"]["id_usuario"]):
				$data["preguntas_usuario"] = $this->Articulo_model->get_preguntas_usuario_articulo($this->session->userdata("id"), $id);
				$data["preguntas"] = $this->Articulo_model->get_preguntas_articulo($id, "todas", TRUE, $this->session->userdata("id"));
			else:
				$data["preguntas_contestar"] = $this->Articulo_model->get_preguntas_articulo($id, false);
				$data["preguntas"] = $this->Articulo_model->get_preguntas_articulo($id, true);
			endif;
		else :
			$data["es_favorito"] = false;
			$data["sin_contestar_usuario"] = array();
			$data["preguntas"] = $this->Articulo_model->get_preguntas_articulo($id);
		endif;
		$data["calificaciones"] = $this->Usuario_model->get_reputacion($data["articulo"]["id_usuario"]);
		$data["main_content"] = "templates/columnas/100";
		$data["columna_1"] = $this->folder."articulo/detalles";
		$data["scripts"][0] = "lightbox.js";
		$data["scripts"][1] = "../lib/thumbelina.js";
		$data["scripts"][2] = "articulo/publico.articulo.js";
		$data["scripts"][3] = "usuario/calificaciones.js";
		$data["css"][0] = "lightbox.css";
		$data["css"][1] = "thumbelina.css";

		$this->load->view('templates/basica', $data);
	}

	public function compra()
	{
		$this->Articulo_model->compra();

		$data["articulo"] = $this->Articulo_model->get_articulo($this->input->post("id-articulo"));
		if ($data["articulo"]["presentacion-tipo"] == "unica") :
			$data["imagen"] = $this->Articulo_model->get_imagenes($this->input->post("id-articulo"));
		else :
			$data["presentacion"] = $this->Articulo_model->get_presentacion($this->input->post("id-presentacion"))->nombre;
			$data["imagen"] = $this->Articulo_model->get_imagenes_presentaciones($this->input->post("id-presentacion"));
		endif;
		$data["talla"] = $this->Articulo_model->get_talla($this->input->post("id-talla"))->talla;
		$data["disenador"] = $this->Usuario_model->get_perfil($data["articulo"]["id_usuario"]);
		$data["main_content"] = "templates/columnas/100";
		$data["columna_1"] = $this->folder."articulo/compra_completa";
		$data["scripts"][0] = "lightbox.js";
		$data["scripts"][1] = "articulo/publico.articulo.js";
		$data["css"][0] = "lightbox.css";

		$this->load->view('templates/basica', $data);
	}

	public function quienes_somos()
	{
		$data["main_content"] = "templates/columnas/100";
		$data["columna_1"] = $this->folder."quienes_somos";
		$this->load->view('templates/basica', $data);
	}

	public function contacto()
	{
		$data["main_content"] = "templates/columnas/100";
		$data["columna_1"] = $this->folder."contacto";
		$this->load->view('templates/basica', $data);
	}

	public function tutoriales()
	{
		$data["main_content"] = "templates/columnas/100";
		$data["columna_1"] = $this->folder."tutoriales";
		$this->load->view('templates/basica', $data);
	}
}

/* End of file sitio.php */
/* Location: ./application/controllers/sitio.php */