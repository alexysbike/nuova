<div class="media">
  <? if(isset($perfil->imagen)): ?>
  	<a href="#" class="thumbnail imagen-perfil-pequena pull-left" style="background-image:url(<?=base_url()?>img/usuarios/<?=$perfil->id_usuario?>/<?=$perfil->imagen->archivo?>)"></a>
  <? else: ?>
  	<a href="#" class="thumbnail imagen-perfil-pequena pull-left" style="background-color:red"></a>
  <? endif; ?>
  <div class="media-body">
    <h3 class="media-heading"><?=$perfil->alias?></h3>
    <h4><small><?=$perfil->nombre?> <?=$perfil->apellido?></small></h4>
  </div>
</div>
<p>
	<strong>Correo Electr&oacute;nico:</strong> <?=$usuario->correo?><br>
	<strong>Telefono:</strong> <?=$perfil->telefono?><br>
	<strong>Celular:</strong> <?=$perfil->celular?>
</p>