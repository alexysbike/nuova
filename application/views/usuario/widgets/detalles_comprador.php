<div class="media">
  <? if(isset($comprador->imagen)): ?>
  	<a href="#" class="thumbnail imagen-perfil-pequena pull-left" style="background-image:url(<?=base_url()?>img/usuarios/<?=$comprador->id_usuario?>/<?=$comprador->imagen->archivo?>)"></a>
  <? else: ?>
  	<a href="#" class="thumbnail imagen-perfil-pequena pull-left" style="background-color:red"></a>
  <? endif; ?>
  <div class="media-body">
  	<input type="hidden" id="id-disenador" value="<?=$comprador->id_usuario?>">
    <h3 class="media-heading"><?=$comprador->alias?></h3>
    <h4><small><?=$comprador->nombre?> <?=$comprador->apellido?></small></h4>
  </div>
</div>
<p>
	<strong>Correo Electr&oacute;nico:</strong> <?=$comprador->usuario->correo?><br>
	<strong>Telefono:</strong> <?=$comprador->telefono?><br>
	<strong>Celular:</strong> <?=$comprador->celular?>
</p>
