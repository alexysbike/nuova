<!-- Modal -->
<div class="modal fade" id="modal-manejar-datos-pago" tabindex="-1" role="dialog" aria-hidden="true" data-show="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <form class="form-horizontal" role="form" id="formulario-datos-pago" method="post" action="<?=base_url()?>usuario/editar_datos_pago/<?=$this->session->userdata("id")?>">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Datos de Pagos</h4>
      </div>
      <div class="modal-body">
       <div class="contenedor-datos-pago">
        <p class="text-info">Hacer click para vizualizar informaci&oacute;n de la cuenta <a class="btn btn-xs btn-success pull-right" id="nuevo-datos-pago" title="Agregar Nuevo"><span class="glyphicon glyphicon-plus"></span></a></p>
        <div id="contenedor-datos"><? $this->load->view("usuario/informaciones/acordion_datos_pago_personal", array("dats_pago" => $datos_pago)) ?></div>
       </div>
       <div class="contenedor-agregar-datos" id="contenedor-datos" style="display:none">
        <legend>Nuevo</legend>
        <div class="form-group">
          <label for="banco" class="col-sm-4 control-label">Banco:</label>
          <div class="col-sm-6">
            <select class="form-control" name="banco" id="banco">
              <option value="" selected>Seleccione Banco</option>
              <? foreach (get_bancos_array() as $banco): ?>
                <option value="<?=$banco?>"><?=$banco?></option>
              <? endforeach; ?>
            </select>
          </div>
        </div>

        <div class="form-group">
          <label for="tipo-cuenta" class="col-sm-4 control-label">Tipo de Cuenta:</label>
          <div class="col-sm-6">
            <select class="form-control" name="tipo-cuenta" id="tipo-cuenta">
              <option value="" selected>Seleccione Tipo</option>
              <option value="corriente">Corriente</option>
              <option value="ahorro">Ahorro</option>
            </select>
          </div>
        </div>

         <div class="form-group">
           <label for="numero-cuenta" class="col-sm-4 control-label">Numero de Cuenta:</label>
           <div class="col-sm-6">
              <input type="text" class="form-control" name="numero-cuenta" id="numero-cuenta">
           </div>
         </div>
         
         <div class="form-group">
           <label for="titular-cuenta" class="col-sm-4 control-label">A Nombre de:</label>
           <div class="col-sm-6">
              <input type="text" class="form-control" name="titular-cuenta" id="titular-cuenta">
           </div>
         </div>

         <div class="form-group">
           <label for="cedula-cuenta" class="col-sm-4 control-label">Cedula:</label>
           <div class="col-sm-6">
              <input type="text" class="form-control" name="cedula-cuenta" id="cedula-cuenta">
           </div>
         </div>

         <div class="form-group">
           <label for="correo-cuenta" class="col-sm-4 control-label">Correo Electr&oacute;nico:</label>
           <div class="col-sm-6">
              <input type="text" class="form-control" name="correo-cuenta" id="correo-cuenta">
           </div>
         </div>

         <input type="hidden" name="tipo-guardar" id="tipo-guardar" value="nuevo">
         <input type="hidden" name="id-datos-pago" id="id-datos-pago" value="">

       </div>
      </div>
      <div class="modal-footer">
        <div class="contenedor-datos-pago">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
        <div class="contenedor-agregar-datos" style="display:none">
          <button type="submit" class="btn btn-primary" id="agregar-datos" name="agregar-datos">Guardar</button>
          <button type="button" class="btn btn-default" id="cerrar-datos">Cerrar</button>
        </div>
      </div>
      </form>
    </div>
  </div>
</div>