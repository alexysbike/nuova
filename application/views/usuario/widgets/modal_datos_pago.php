<!-- Modal -->
<div class="modal fade" id="modal-datos-pago" tabindex="-1" role="dialog" aria-hidden="true" data-show="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <form class="form-horizontal" role="form" id="formulario-reportar-pago" method="post" action="<?=base_url()?>articulo/reportar_pago">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Reporte de Pagos</h4>
      </div>
      <div class="modal-body">
      <div class="reportar-pago-formulario">
      <p class="text-info">Hacer click para seleccionar y/o vizualizar informaci&oacute;n de la cuenta para depositar</p>
      <div id="acordion-recibidor"></div>
      <p class="help-block">Banco Receptor Seleccionado: <span id="banco-seleccionado">-ninguno-</span></p>
        <input type="hidden" id="id-datos-pago" name="id-datos-pago" value="">
        <input type="hidden" id="id-compra-datos" name="id-compra" value="">
        <input type="hidden" id="id-usuario-receptor" name="id-usuario-receptor" value="">
        <legend>Datos de la Transacci&oacute;n <a href="#" class="btn btn-success btn-sm pull-right" id="n-pagos">X Pagos Reportados</a></legend>
        <div class="form-group">
          <label for="banco-emisor" class="col-sm-4 control-label">Banco Emisor:</label>
          <div class="col-sm-6">
            <select class="form-control" name="banco-emisor" id="banco-emisor">
              <option value="" selected>Seleccione Banco Emisor</option>
              <option value="Efectivo">Efectivo</option>
              <? foreach (get_bancos_array() as $banco): ?>
                <option value="<?=$banco?>"><?=$banco?></option>
              <? endforeach; ?>
            </select>
            <span class="help-block">Banco desde donde se realiz&oacute; <abbr title="Transferencia, Cheque, Deposito">la transacci&oacute;n</abbr></span>
          </div>
        </div>
        <div class="form-group">
          <label for="fecha-pago" class="col-sm-4 control-label">Fecha</label>
          <div class="col-sm-6">
            <input type="text" class="form-control" name="fecha-pago" id="fecha-pago" placeholder="Fecha del pago...">
          </div>
        </div>
        <div class="form-group">
          <label for="codigo-pago" class="col-sm-4 control-label">Codigo</label>
          <div class="col-sm-6">
            <input type="text" class="form-control" name="codigo-pago" placeholder="Codigo de tranferencia, cheque, deposito....">
          </div>
        </div>
        <div class="form-group">
          <label for="monto-pago" class="col-sm-4 control-label">Monto</label>
          <div class="col-sm-6">
            <input type="text" class="form-control" name="monto-pago" placeholder="Usar formato ##.##">
          </div>
        </div>
        <? /*
        <div class="form-group">
          <label for="imagen-pago" class="col-sm-4 control-label">Imagen del pago:</label>
          <div class="col-sm-6">
            <input type="file" class="form-control" name="imagen-pago" placeholder="Escojer imagen">
          </div>
        </div>
        */ ?>
        </div>
        <div class="pagos-reportados" id="contenedor-pagos" style="display:none">
        </div>
      </div>
      <div class="modal-footer">
        <div class="reportar-pago-formulario">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <button type="submit" class="btn btn-primary">Reportar Pago</button>
        </div>
        <div class="pagos-reportados" style="display:none">
          <button type="button" class="btn btn-default" id="regresar-formulario">Regresar al Formulario</button>
        </div>
      </div>
      </form>
    </div>
  </div>
</div>