<!-- Modal -->
<div class="modal fade modal-ver-calificaciones-usuario" id="modal-ver-calificaciones" tabindex="-1" role="dialog" aria-hidden="true" data-show="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="modal-titulo">Calificaciones</h4>
      </div>
      <div class="modal-body">
        <div class="panel panel-default">
          <div class="panel-heading">
          Ver: 
          <input type="radio" value="todas" name="ver-radio" id="ver-todas" class="ver-radio" checked> Todas    
          <input type="radio" value="positivas" name="ver-radio" id="ver-positivas" class="ver-radio"> Positivas
          <input type="radio" value="negativas" name="ver-radio" id="ver-negativas" class="ver-radio"> Negativas
          <input type="radio" value="neutrales" name="ver-radio" id="ver-neutrales" class="ver-radio"> Neutrales
          </div>
          <ul class="list-group" id="lista-calif" style="max-height:250px; overflow-y:scroll">
            <?php foreach ($calificaciones["todas"] as $calificacion): ?>
              <li class="list-group-item <? if($calificacion->tipo == 1): echo "list-group-item-success positiva"; elseif($calificacion->tipo == -1): echo "list-group-item-danger negativa"; else: echo "neutral"; endif;?>">
                <p class="help-block"><?=sqldate_to_datepicker($calificacion->fecha)?></p>
                <strong><? if($calificacion->tipo == 1): echo "Positiva"; elseif($calificacion->tipo == -1): echo "Negativa"; else: echo "Neutral"; endif;?></strong> - <?=$calificacion->comentario?>
              </li>
            <?php endforeach ?>
          </ul>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>