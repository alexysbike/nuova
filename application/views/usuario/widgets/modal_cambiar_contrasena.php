<!-- Modal -->
<div class="modal fade" id="modal-cambiar-contrasena" tabindex="-1" role="dialog" aria-hidden="true" data-show="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" method="post" action="<?=base_url()?>usuario/cambio_contrasena" class="form-horizontal">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="modal-titulo">Cambio de Contrase&ntilde;a</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label for="actual" class="col-sm-4 control-label">Contrase&ntilde;a Actual</label>
          <div class="col-sm-8">
            <input type="password" class="form-control" id="actual" name="actual" placeholder="Contrase&ntilde;a actual...">
            <span id="error-actual"></span>
          </div>
        </div>
        <div class="form-group">
          <label for="nueva" class="col-sm-4 control-label">Nueva Contrase&ntilde;a</label>
          <div class="col-sm-8">
            <input type="password" class="form-control" id="nueva" name="nueva" placeholder="Nueva contrase&ntilde;a...">
            <span id="error-nueva"></span>
          </div>
        </div>
        <div class="form-group">
          <label for="confirmacion" class="col-sm-4 control-label">Confirmaci&oacute;n Contrase&ntilde;a</label>
          <div class="col-sm-8">
            <input type="password" class="form-control" id="confirmacion" name="confirmacion" placeholder="Confirmaci&oacute;n de contrase&ntilde;a...">
            <span id="error-confirmacion"></span>
          </div>
        </div>
        <p id="error-cambiar" class="alert alert-danger" style="display:none">Ha ocurrido un error al cambiar la contrase&ntilde;a, revise la informaci&oacute;n.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="btn-cambiar">Cambiar</button>
      </div>
      </form>
    </div>
  </div>
</div>