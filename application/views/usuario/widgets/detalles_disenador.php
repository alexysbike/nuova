<div class="media">
  <? if(isset($disenador->imagen)): ?>
  	<a href="#" class="thumbnail imagen-perfil-pequena pull-left" style="background-image:url(<?=base_url()?>img/usuarios/<?=$articulo["id_usuario"]?>/<?=$disenador->imagen->archivo?>)"></a>
  <? else: ?>
  	<a href="#" class="thumbnail imagen-perfil-pequena pull-left" style="background-color:red"></a>
  <? endif; ?>
  <div class="media-body">
  	<input type="hidden" id="id-disenador" value="<?=$articulo["id_usuario"]?>">
    <h3 class="media-heading"><?=$disenador->alias?></h3>
    <h4><small><?=$disenador->nombre?> <?=$disenador->apellido?></small></h4>
  </div>
</div>
<p>
	<strong>Correo Electr&oacute;nico:</strong> <?=$disenador->usuario->correo?><br>
	<strong>Telefono:</strong> <?=$disenador->telefono?><br>
	<strong>Celular:</strong> <?=$disenador->celular?>
</p>
<p class="text-info">Click en link para ver informaci&oacute;n de cuentas</p>