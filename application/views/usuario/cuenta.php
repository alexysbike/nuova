<div class="row">
  <div class="col-md-8">
  	<div class="row">
        <div class="col-md-3">
            <?
              if ($perfil->id_imagen == 0) :
                $img_cuenta = base_url()."img/missing-img.png";
              else :
                $img_cuenta = base_url()."img/usuarios/".$perfil->id_usuario."/".$perfil->imagen->archivo;
              endif;
            ?>
            <a href="<?=$img_cuenta?>" class="thumbnail" id="imagen-usuario-cuenta" style="background-image:url(<?=$img_cuenta?>)" data-idimagen="<?=$perfil->id_imagen?>" data-target="#lightbox" url-img="<?=$img_cuenta?>">
              <p class="text-center" id="imagen-hover-cuenta" style="display:none"><strong>Cambiar Imagen</strong></p>
            </a>
        </div>
        <div class="col-md-9">
            <h4><?=$perfil->alias?></h4>
            <? if ($this->session->userdata("bloqueado") == 1): ?>
            	<span class="label label-danger">Bloqueado</span><br>
            <? endif ?>
            <? if ($factura_pendiente == true): ?>
              <span class="label label-warning">Tiene Factura Pendiente, se envi&oacute; a su correo la factura</span><br>
            <? endif ?>
            <small><cite title="<?=$perfil->lugar?>"><?=$perfil->lugar?> <span class="glyphicon glyphicon-map-marker">
            </span></cite></small>
            <p>
            	<span class="glyphicon glyphicon-user"></span> <?=$perfil->nombre.' '.$perfil->apellido?>
                <br />
                <span class="glyphicon glyphicon-envelope"></span> <?=$this->session->userdata("correo")?>
                <br />
                <span class="glyphicon glyphicon-globe"></span> <?=decode_genero($perfil->genero)?>
                <br />
                <span class="glyphicon glyphicon-gift"></span> <?=sqldate_to_escrita($perfil->fecha_nacimiento)?>
                  <p>
                  <a target="_blank" href="<?=$perfil->facebook?>"><img src="<?=base_url()?>img/socials/facebook-48.png"></a>
                  <a target="_blank" href="http://twitter.com/<?=$perfil->twitter?>"><img src="<?=base_url()?>img/socials/twitter-48.png"></a>
                  <a target="_blank" href="<?=$perfil->linkedin?>"><img src="<?=base_url()?>img/socials/linkedin-48.png"></a>
                  <a target="_blank" href="<?=$perfil->instagram?>"><img src="<?=base_url()?>img/socials/instagram-48.png" style="height: 46px;margin-top: -3px;"></a>
                  </p>
                </p>
            <!-- Split button -->
            <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    Opciones <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="<?=base_url()?>usuario/editar_perfil/<?=$this->session->userdata("id")?>">Editar Perfil</a></li>
                    <li><a href="#" id="btn-cambiar-img-perfil" data-idimagen="<?=$perfil->id_imagen?>">Cambiar Imagen de Perfil</a></li>
                    <li><a href="#" id="btn-cambiar-contrasena">Cambiar Contrase&ntilde;a</a></li>
                    <? if ($this->session->userdata("id_tipo")<=2): ?>
                    <li><a href="#" class="btn-manejar-datos-pago">Datos para Pagos</a></li>
                    <li><a href="<?=base_url()?>usuario/ver_perfil/<?=$this->session->userdata("id")?>" id="btn-ver-perfil-publico" target="_blank">Ver Perf&iacute;l P&uacute;blico</a></li>
                  <? endif; ?>
                </ul>
            </div>
        </div>
    </div>
  </div>
  <div class="col-md-4">
      <div class="row">
          <div class="col-md-5 text-right">
            Calificaciones:
          </div>
          <div class="col-md-7">
            <?
            $positivas = count($calificaciones["positivas"]);
            $negativas = count($calificaciones["negativas"]);
            $neutrales = count($calificaciones["neutrales"]);
            $total_calif = $positivas + $negativas + $neutrales;
            ?>
            <? if ($total_calif > 0): ?>
            <a href="#" title="<?=$positivas?> positivas <?=$total_calif?> calificaciones"><span class="label label-success"><?=($positivas / $total_calif) * 100?>%</span></a> : <a href="#" title="<?=$negativas?> negativas de <?=$total_calif?> calificaciones"><span class="label label-danger"><?=($negativas / $total_calif) * 100?>%</span></a>
            <? else: ?>
            Sin Calificar
            <? endif; ?>
          </div>
      </div>
      <? if ($total_calif > 0): ?>
        <p class="text-center"><a href="#" data-toggle="modal" data-target=".modal-ver-calificaciones-usuario">Ver Calificaciones</a></p>
      <? endif; ?>
    <? if ($this->session->userdata("id_tipo")<=2): ?>
	  <div class="row">
  		<div class="col-md-6 text-right">
  			Ventas:
  		</div>
  		<div class="col-md-6">
  			<strong class="text-success"><?=$ventas_nuevas?></strong> Nuevas
  		</div>
  	</div>
  	<div class="row">
  		<div class="col-md-6 text-right">
  			Preguntas
  		</div>
  		<div class="col-md-6">
  			<a href="#"><strong class="text-success"><?=count($n_preguntas_sc)?></strong></a> por constestar
  		</div>
  	</div>
    <? endif; ?>
  </div>
</div>

<? if ($this->session->userdata("bloqueado") == 0): ?>

<ul class="nav nav-tabs">
  <? if ($this->session->userdata("id_tipo")<=2): ?>
  <li class="<?=($modo=="articulos" || $modo=="") ? "active" : "" ?>"><a href="#articulos" data-toggle="tab">Mis Articulos</a></li>
  <li class="<?=($modo=="ventas") ? "active" : "" ?>"><a href="#ventas" data-toggle="tab">Ventas</a></li>
  <? endif; ?>
  <li class="<?=($modo=="compras" || ($modo=="" && $this->session->userdata("id_tipo") > 2)) ? "active" : "" ?>"><a href="#compras" data-toggle="tab">Compras</a></li>
  <li class="<?=($modo=="deseos") ? "active" : "" ?>"><a href="#deseos" data-toggle="tab">Listas de Deseos</a></li>
</ul>

<div class="tab-content" id="cuenta-tabs">
  <? if ($this->session->userdata("id_tipo")<=2): ?>
  <div class="tab-pane <?=($modo=="articulos" || $modo=="") ? "active" : "" ?>" id="articulos">
    <div class="panel panel-default">
      <!-- Default panel contents -->
      <div class="panel-heading">
      	<div class="row">
          <div class="col-md-8">
            <a href="<?=base_url()?>articulo/nuevo" class="btn btn-primary" <? if (count($datos_pago)<=0): ?> onclick="alert('Debes agregar los datos para pagos (en Opciones->Datos para Pagos)'); return false;" <? endif; ?>>Nuevo Articulo</a>
            <a href="#" class="btn btn-warning btn-manejar-datos-pago">Datos para Pagos</a>
            <a href="#" class="btn btn-success" id="btn-ver-colecciones" data-idusuario="<?=$this->session->userdata("id")?>">Colecciones</a>
          </div>
      		<div class="col-md-4">
      			<form action="#" method="get">
        		    <div class="input-group">
        		        <!-- USE TWITTER TYPEAHEAD JSON WITH API TO SEARCH -->
        		        <input class="form-control" id="system-search" name="q" placeholder="Buscar" required>
        		        <span class="input-group-btn">
        		            <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
        		        </span>
        		    </div>
        		</form>
        	</div>
        </div>
      </div>
      <!-- Table -->
      <table class="table table-list-search">
          <p class="text-info bg-info">Para Activar un Art&iacute;culo por primera vez se le debe agregar piezas en la secci&oacute;n de "Inventario"</p>
          <thead>
              <tr>
                  <th>Categor&iacute;a</th>
                  <th>Colecci&oacute;n</th>
                  <th>Nombre</th>
                  <th>Fecha Publ</th>
                  <th>Status</th>
                  <th>Piezas</th>
                  <th>Acciones</th>
              </tr>
          </thead>
          <tbody>
              <? foreach ($articulos as $articulo): ?>
              	<tr>
              		<td><?=$articulo->categoria?></td>
              		<td><?=$articulo->coleccion?></td>
              		<td><?=$articulo->nombre?></td>
              		<td><?=sqldate_to_datepicker($articulo->fecha)?></td>
              		<td><div class="label <?=($articulo->status == 0) ? "label-warning" : (($articulo->status == 1) ? "label-success" : "label-danger")  ?>"><?=$articulo->status_escrito?></div></td>
              		<td class="popover-user" data-toggle="popover" data-content='<?$this->load->view("articulo/widgets/detalles_inventario", $articulo)?>' data-html="true" data-placement="left" title="Detalles" data-trigger="hover" data-container="body"><?=$articulo->total_piezas?></td>
              		<td>
                   <? if ($articulo->status == -1): ?>
                    <a href="<?=base_url()?>articulo/primera_activacion/<?=$articulo->id?>" class="btn btn-sm btn-primary">Activar</a>
                    <a href="<?=base_url()?>articulo/eliminar_articulo/<?=$articulo->id?>" class="btn btn-sm btn-danger">Eliminar</a>
                   <? else: ?>
                    <a href="<?=base_url()?>articulo/imagenes/<?=$articulo->id?>" class="btn btn-default btn-sm">Detalles</a>
                    <a href="<?=base_url()?>articulo/imagenes/<?=$articulo->id?>" class="btn btn-default btn-sm">Imagenes</a>
                    <a href="<?=base_url()?>articulo/inventario/<?=$articulo->id?>" class="btn btn-primary btn-sm">Inventario</a>
                    <a href="<?=base_url()?>articulo/editar/<?=$articulo->id?>" class="btn btn-warning btn-sm">Editar</a>
                    <? if ($articulo->status!=-2): ?>
                    <a href="<?=base_url()?>articulo/desactivar/<?=$articulo->id?>" class="btn btn-danger btn-sm">Desactivar</a>
                    <? else: ?>
                    <a href="<?=base_url()?>articulo/activar/<?=$articulo->id?>" class="btn btn-danger btn-sm">Activar</a>
                    <? endif; ?>
                    <? if (count($articulo->preg_sc) > 0): ?>
                    <a href="<?=base_url()?>sitio/articulo/<?=$articulo->id?>" class="btn btn-primary btn-sm"><?=count($articulo->preg_sc)?> Preguntas sin contestar</a>
                    <? endif; ?>
                    <a href="<?=base_url()?>sitio/articulo/<?=$articulo->id?>" class="btn btn-success btn-sm">Pagina P&uacute;blica</a>
                   <? endif; ?>
                  </td>
              	</tr>
              <? endforeach ?>
          </tbody>
      </table>
    </div>
  </div>
  <div class="tab-pane <?=($modo=="ventas") ? "active" : "" ?>" id="ventas">
    <div class="panel panel-default">
      <!-- Default panel contents -->
      <div class="panel-heading">
        <div class="row">
          <div class="col-md-8">
          </div>
          <div class="col-md-4">
            <form action="#" method="get">
                <div class="input-group">
                    <!-- USE TWITTER TYPEAHEAD JSON WITH API TO SEARCH -->
                    <input class="form-control system-search" data-target="#tabla-ventas" id="system-search-ventas" name="q" placeholder="Buscar" required>
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                    </span>
                </div>
            </form>
          </div>
        </div>
      </div>

      <!-- Table -->
      <table class="table table-list-ventas" id="tabla-ventas">
          <thead>
              <tr>
                  <th>Comprador</th>
                  <th>Art&iacute;culo (Detalles de Venta)</th>
                  <th>Cant</th>
                  <th>Precio</th>
                  <th><abbr title="Fecha de Venta">Fech Vent</abbr></th>
                  <th>Status</th>
                  <th>Acciones</th>
              </tr>
          </thead>
          <tbody>
             <? foreach ($ventas as $venta): ?>
                <tr>
                  <td class="popover-user comprador" style="cursor:pointer" data-toggle="popover" data-content='<?$this->load->view("usuario/widgets/detalles_comprador",$venta)?>' data-html="true" data-placement="top" title="Detalles Comprador" data-trigger="hover" data-container="body"><a href="#" class="comprador" data-idcomprador="<?=$venta->comprador->id_usuario?>"><?=$venta->comprador->alias?></a></td>
                  <td class="popover-user" data-toggle="popover" data-content='<?$this->load->view("articulo/widgets/detalles_compra",$venta)?>' data-html="true" data-placement="top" title="Detalles de Venta" data-trigger="hover" data-container="body"><a href="<?=base_url()?>sitio/articulo/<?=$venta->id_articulo?>" target="_blank"><?=$venta->articulo["nombre"]?></a></td>
                  <td><?=$venta->cantidad?></td>
                  <td>Bs. <?=$venta->precio?></td>
                  <td><?=$venta->fecha_compra?></td>
                  <td><div class="label <?=($venta->status == 0) ? "label-info" : (($venta->status == 1) ? "label-warning" : (($venta->status <= -1) ? "label-danger" : "label-success" ))  ?>"><?=$venta->status_escrita?></div></td>
                  <td>
                    <a href="#" title="Detalles de la Venta" class="btn btn-default btn-xs btn-detalle-compra" data-idcompra="<?=$venta->id?>"><span class="glyphicon glyphicon-list-alt"></span></a>
                    <a href="#" title="Detalles del Comprador" class="btn btn-primary btn-xs btn-detalle-comprador" data-idusuario="<?=$venta->id_usuario_comprador?>"><span class="glyphicon glyphicon-user"></span></a>
                    <span class="btn-ver-pagos"><a href="#" title="Reporte de Pagos" class="btn btn-xs btn-info comprador" data-idcompra="<?=$venta->id?>" data-iddisenador="<?=$venta->comprador->id_usuario?>" data-npagos="<?=count($venta->pagos)?>"><span class="glyphicon glyphicon-usd"></span></a></span>
                    <? if ($venta->status < 4 && $venta->status >= 0): ?>
                    <a href="#" title="Direcci&oacute;n de Env&iacute;o" class="btn btn-default btn-xs btn-envios" data-idcompra="<?=$venta->id?>" data-idusuario="<?=$venta->id_usuario_comprador?>"><span class="glyphicon glyphicon-send"></span></a>
                    <? elseif ($venta->status == 4 || $venta->status == -1): ?>
                    <a href="#" title="Calificar Comprador" class="btn btn-success btn-xs btn-calificar-venta" data-idcompra="<?=$venta->id?>" data-idusuariocomprador="<?=$venta->id_usuario_comprador?>"><span class="glyphicon glyphicon-check"></span></a>
                  <? elseif($venta->status == 5 || $venta->status == -2): ?>
                      <a href="#" title="Ver Calificaciones" class="btn btn-success btn-xs btn-ver-calificaciones" data-idcompra="<?=$venta->id?>" data-por="venta"><span class="glyphicon glyphicon-check"></span></a>
                    <? endif; ?>
                  </td>
                </tr>
              <? endforeach; ?>
          </tbody>
      </table>
    </div>
  </div>
  <? endif; ?>
  <div class="tab-pane <?=($modo=="compras" || ($modo=="" && $this->session->userdata("id_tipo") > 2)) ? "active" : "" ?>" id="compras">
    <div class="panel panel-default">
      <!-- Default panel contents -->
      <div class="panel-heading">
        <div class="row">
          <div class="col-md-8">
          </div>
          <div class="col-md-4">
            <form action="#" method="get">
                <div class="input-group">
                    <!-- USE TWITTER TYPEAHEAD JSON WITH API TO SEARCH -->
                    <input class="form-control system-search" data-target="#tabla-compras" id="system-search-compras" name="q" placeholder="Buscar" required>
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                    </span>
                </div>
            </form>
          </div>
        </div>
      </div>

      <!-- Table -->
      <table class="table table-list-compras" id="tabla-compras">
          <thead>
              <tr>
                  <th>Dise&ntilde;ador</th>
                  <th>Art&iacute;culo (Detalles de Compra)</th>
                  <th>Cant</th>
                  <th>Precio</th>
                  <th><abbr title="Fecha de Compra">Fech Comp</abbr></th>
                  <th>Status</th>
                  <th>Acciones</th>
              </tr>
          </thead>
          <tbody>
              <? foreach ($compras as $compra): ?>
                <tr>
                  <td class="popover-user disenador" style="cursor:pointer" data-toggle="popover" data-content='<?$this->load->view("usuario/widgets/detalles_disenador",$compra)?>' data-html="true" data-placement="top" title="Detalles Dise&ntilde;ador" data-trigger="hover" data-container="body"><a href="#" class="disenador" data-idcompra="<?=$compra->id?>" data-iddisenador="<?=$compra->disenador->id_usuario?>" data-npagos="<?=count($compra->pagos)?>"><?=$compra->disenador->alias?></a></td>
                  <td class="popover-user" data-toggle="popover" data-content='<?$this->load->view("articulo/widgets/detalles_compra",$compra)?>' data-html="true" data-placement="top" title="Detalles Compra" data-trigger="hover" data-container="body"><a href="<?=base_url()?>sitio/articulo/<?=$compra->id_articulo?>" target="_blank"><?=$compra->articulo["nombre"]?></a></td>
                  <td><?=$compra->cantidad?></td>
                  <td>Bs. <?=$compra->precio?></td>
                  <td><?=$compra->fecha_compra?></td>
                  <td><div class="label <?=($compra->status == 0) ? "label-info" : (($compra->status == 1) ? "label-warning" : (($compra->status <= -1) ? "label-danger" : "label-success" ))  ?>"><?=$compra->status_escrita?></div></td>
                  <td>
                    <a href="#" title="Detalles de la Compra" class="btn btn-default btn-xs btn-detalle-compra" data-idcompra="<?=$compra->id?>"><span class="glyphicon glyphicon-list-alt"></span></a>
                    <a href="#" title="Detalles del Vendedor" class="btn btn-primary btn-xs btn-detalle-vendedor" data-idusuario="<?=$compra->id_usuario_vendedor?>"><span class="glyphicon glyphicon-user"></span></a>
                    <? if ($compra->status < 4 && $compra->status >= 0) : ?>
                    <span class="btn-reportar-pago"><a href="#" title="Reporte de Pagos" class="btn btn-xs btn-info disenador" data-idcompra="<?=$compra->id?>" data-iddisenador="<?=$compra->disenador->id_usuario?>" data-npagos="<?=count($compra->pagos)?>"><span class="glyphicon glyphicon-usd"></span></a></span>
                    <a href="#" title="Direcci&oacute;n de Env&iacute;o" class="btn btn-default btn-xs btn-envios" data-idcompra="<?=$compra->id?>" data-idusuario="<?=$this->session->userdata("id")?>"><span class="glyphicon glyphicon-send"></span></a>
                    <a href="#" title="Calificar Vendedor" class="btn btn-success btn-xs btn-calificar-compra" data-idcompra="<?=$compra->id?>" data-idusuariovendedor="<?=$compra->id_usuario_vendedor?>"><span class="glyphicon glyphicon-check"></span></a>
                    <? elseif($compra->status == 5 || $compra->status == -2): ?>
                      <a href="#" title="Ver Calificaciones" class="btn btn-success btn-xs btn-ver-calificaciones" data-idcompra="<?=$compra->id?>" data-por="compra"><span class="glyphicon glyphicon-check"></span></a>
                    <? endif; ?>
                  </td>
                </tr>
              <? endforeach; ?>
          </tbody>
      </table>
    </div>
  </div>
  <div class="tab-pane <?=($modo=="deseos") ? "active" : "" ?>" id="deseos">
    <div class="panel panel-default">
      <!-- Default panel contents -->
      <div class="panel-heading">
        <div class="row">
          <div class="col-md-8">
          </div>
          <div class="col-md-4">
            <form action="#" method="get">
                <div class="input-group">
                    <!-- USE TWITTER TYPEAHEAD JSON WITH API TO SEARCH -->
                    <input class="form-control system-search" id="system-search-deseos" data-target="#tabla-deseos" name="q" placeholder="Buscar" required>
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                    </span>
                </div>
            </form>
          </div>
        </div>
      </div>

      <!-- Table -->
      <table class="table table-list-deseos" id="tabla-deseos">
          <thead>
              <tr>
                  <th>Categor&iacute;a</th>
                  <th>Colecci&oacute;n</th>
                  <th>Nombre</th>
                  <th>Fecha Publ</th>
                  <th>Acciones</th>
              </tr>
          </thead>
          <tbody>
              <? foreach ($deseos as $deseo): ?>
                <tr>
                  <td><?=$deseo->categoria?></td>
                  <td><?=$deseo->coleccion?></td>
                  <td><?=$deseo->nombre?></td>
                  <td><?=$deseo->fecha?></td>
                  <td>
                    <a href="<?=base_url()?>articulo/eliminar_favorito/<?=$deseo->id_deseo?>" class="btn btn-primary btn-sm">Eliminar</a>
                    <a href="<?=base_url()?>sitio/articulo/<?=$deseo->id?>" class="btn btn-success btn-sm" target="_blank">Ver</a>
                  </td>
                </tr>
              <? endforeach ?>
          </tbody>
      </table>

    </div>
  </div>
</div>
<? else: ?>
  <h1>USUARIO BLOQUEADO <small>Pongase en contacto con el Administrador</small></h1>
<? endif; ?>
<?$this->load->view("usuario/widgets/modal_datos_pago");?>
<?$this->load->view("usuario/widgets/modal_cambiar_contrasena");?>
<?$this->load->view("usuario/widgets/modal_manejar_datos_pago");?>
<?$this->load->view("articulo/widgets/modal_envios_compra");?>
<?$this->load->view("articulo/widgets/modal_ver_pagos");?>
<?$this->load->view("articulo/widgets/modal_ver_colecciones");?>
<?$this->load->view("articulo/widgets/modal_ver_compra");?>
<?$this->load->view("galeria/adicionales/modal_upload_image");?>
<?$this->load->view("articulo/widgets/modal_calificar_compra");?>
<?$this->load->view("articulo/widgets/modal_calificar_venta");?>
<?$this->load->view("articulo/widgets/modal_ver_calificaciones");?>
<?$this->load->view("templates/lightbox");?>
<? $this->load->view("usuario/widgets/modal_ver_calificaciones"); ?>