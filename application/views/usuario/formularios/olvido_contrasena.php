<div class="panel panel-default">
<div class="panel-heading"><h2 class="text-center">Reenv&iacute;o de Contrase&ntilde;a</h2></div>
<div class="panel-body">
<form method="post" action="<?=base_url()?>usuario/olvido_contrasena" role="form" class="form-horizontal">
	<div class="form-group">
		<label for="correo" class="col-md-4 control-label">Correo Electr&oacute;nico:</label>
		<div class="col-md-8">
			<input type="email" name="correo" class="form-control" placeholder="Correo Electr&oacute;nico...">
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-6 col-md-offset-4">
			<button name="olvido" value="olvido" class="btn btn-primary">Reenviar</button>
		</div>
	</div>
</form>
</div>
</div>
