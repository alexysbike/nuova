<? if ($tipo_usuario == 2): ?>
<h3 class="text-center">Nuevo Dise&ntilde;ador</h3>
<? else: ?>
<h3 class="text-center">Nuevo Usuario</h3>
<? endif; ?>
<div class="panel panel-default">
  <div class="panel-body">
	<form role="form" method="post" action="<?=base_url()?>usuario/crear_usuario" id="registro-usuario">
		<div class="form-group">
	    	<label for="correo">Correo Electr&oacute;nico</label>
	    	<input type="email" class="form-control" id="correo" name="correo" placeholder="Ingrese su correo electr&oacute;nico" required>
	  	</div>
	  	<div class="form-group">
	    	<label for="confirmar-correo">Confirmar Correo Electr&oacute;nico</label>
	    	<input type="email" class="form-control" id="confirmar-correo" name="confirmar-correo" placeholder="Confirme su correo electr&oacute;nico">
	  	</div>
	  	<div class="form-group">
	    	<label for="password">Contrase&ntilde;a</label>
	    	<input type="password" class="form-control" id="password" name="password" placeholder="Ingrese su contrase&ntilde;a" required>
	  	</div>
	  	<div class="form-group">
	    	<label for="confirmar-password">Confirmar Contrase&ntilde;a</label>
	    	<input type="password" class="form-control" id="confirmar-password" name="confirmar-password" placeholder="Confirme su contrase&ntilde;a">
	  	</div>
	  	<div class="form-group">
	  		<p class="text-center"><a href="#" data-toggle="modal" data-target="#modal-terminos">Terminos y Condiciones</a> - <a href="#" data-toggle="modal" data-target="#modal-privacidad">Pol&iacute;ticas de privacidad y confidencialidad</a></p>
	  		<p class="alert alert-warning">Al darle click al bot&oacute;n "Crear" estas aceptando los Terminos y Condiciones y las pol&iacute;ticas de privacidad y confidencialidad</p>
	  	</div>
	  	<input type="hidden" name="tipo-usuario" value="<?=$tipo_usuario?>">
	  	<button type="submit" class="btn btn-primary col-md-4" id="crear-usuario">Crear</button>
	</form>
  </div>
</div>
<? $this->load->view("usuario/widgets/modal_terminos"); ?>
<? $this->load->view("usuario/widgets/modal_privacidad"); ?>