<div class="panel panel-default">
<div class="panel-heading"><h2 class="text-center">Ingreso al sistema Nuova</h2></div>
<div class="panel-body">
<form method="post" action="<?=base_url()?>usuario/login" role="form" class="form-horizontal">
	<div class="form-group">
		<label for="correo" class="col-md-4 control-label">Correo Electr&oacute;nico:</label>
		<div class="col-md-8">
			<input type="email" name="correo" class="form-control" placeholder="Correo Electr&oacute;nico...">
		</div>
	</div>
	<div class="form-group">
		<label for="password" class="col-md-4 control-label">Contrase&ntilde;a:</label>
		<div class="col-md-8">
			<input type="password" name="password" class="form-control" placeholder="Contrase&ntilde;a...">
		</div>
	</div>
	<?php if (isset($reenvio)): ?>
		<p class="alert alert-success">La informaci&oacute;n fue enviada a su correo</p>
	<?php endif ?>
	<p><a href="<?=base_url()?>usuario/olvido_contrasena">Olvide mi Contrase&ntilde;a</a></p>
	<p>Registrate con nosotros como: <a href="<?=base_url()?>usuario/nuevo">Comprador</a> o <a href="<?=base_url()?>usuario/nuevo/disenador">Dise&ntilde;ador</a></p>
	<div class="form-group">
		<div class="col-md-6 col-md-offset-4">
			<button name="login" value="login" class="btn btn-primary">Login</button>
		</div>
	</div>
</form>
</div>
</div>
