<h3 class="text-center">Perfil</h3>
<div class="panel panel-default">
  <div class="panel-body">
	<form id="editar-perfil" role="form" method="post" action="<?=base_url()?>usuario/editar_perfil" enctype="multipart/form-data">
		<input type="hidden" value="<?=$id_usuario?>" name="id_usuario" id="id_usuario">
		<div class="form-group">
	    	<label for="alias">Alias</label>
	    	<? if (isset($perfil)): ?>
	    		<p><?=$perfil->alias?></p>
	    	<? else: ?>
	    		<input type="text" class="form-control" id="alias" name="alias" id="alias" placeholder="Ingrese como lo ver&aacute;n los demas usuarios">
	    	<? endif; ?>
	    	
	  	</div>
	  	<div class="form-group">
	    	<label for="nombre">Datos Personales</label>
	    	<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombres..." value="<?=(isset($perfil)) ? $perfil->nombre : "" ?>">
	    	<input type="text" class="form-control" id="apellido" name="apellido" placeholder="Apellidos..." value="<?=(isset($perfil)) ? $perfil->apellido : "" ?>">
	    	<input type="text" class="form-control" id="cedula" name="cedula" placeholder="Cedula sin espacios ni guiones ni puntos" value="<?=(isset($perfil)) ? $perfil->cedula : "" ?>">
	  	</div>
	  	<div class="form-group">
	    	<label for="fecha-nacimiento">Fecha de Nacimiento</label>
	    	<div class="input-group date" id="dp3">
    		  <input class="form-control <?/*?>date<?*/?>" type="text" id="fecha-nacimiento" name="fecha-nacimiento" placeholder="Seleccione su fecha de Nacimiento" value="<?=(isset($perfil)) ? sqldate_to_datepicker($perfil->fecha_nacimiento) : "01/01/2014" ?>">
    		  <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
    		</div>
    		<p class="help-block">Use Formato dd/mm/aaaa (P.ej. 08/09/1988)</p>
	  	</div>
	  	<div class="form-group">
	    	<label for="genero">Genero</label>
	    	<div class="radio">
			  <label>
			    <input type="radio" name="genero" id="masculino" value="M" <?=(isset($perfil)) ? (($perfil->genero == "M") ? "checked" : "") : "" ?>>
    			Masculino
  			  </label>
			</div>
			<div class="radio">
			  <label>
			    <input type="radio" name="genero" id="femenino" value="F" <?=(isset($perfil)) ? (($perfil->genero == "F") ? "checked" : "") : "" ?>>
    			Femenino
  			  </label>
			</div>
			<div class="radio">
			  <label>
			    <input type="radio" name="genero" id="no" value="N" <?=(isset($perfil)) ? (($perfil->genero != "F" && $perfil->genero != "M") ? "checked" : "") : "checked" ?>>
    			No Especificar
  			  </label>
			</div>
	  	</div>
	  	<div class="form-group">
	    	<label for="pais">Pais</label>
	    	<select name="pais" id="pais" class="form-control">
	    		<option value="233" selected>Venezuela</option>
	    	</select>
	  	</div>
	  	<div class="form-group">
	    	<label for="estado">Estado</label>
	    	<select name="estado" id="estado" class="form-control">
	    		<? foreach ($estados as $estado): ?>
	    			<option value="<?=$estado->id?>" <?=(isset($perfil)) ? (($perfil->id_estado == $estado->id) ? "selected" : "") : "" ?>><?=$estado->estado?></option>
	    		<? endforeach; ?>
	    	</select>
	  	</div>
	  	<div class="form-group">
	    	<label for="ciudad">Ciudad</label>
	    	<input type="text" class="form-control" id="ciudad" name="ciudad" placeholder="Ciudad..." value="<?=(isset($perfil)) ? $perfil->ciudad : "" ?>">
	  	</div>
	  	<div class="form-group">
	    	<label for="direccion">Direcci&oacute;n</label>
	    	<textarea class="form-control" id="direccion" name="direccion" placeholder="Direcci&oacute;n..."><?=(isset($perfil)) ? $perfil->direccion : "" ?></textarea>
	    	<p class="help-block">&Eacute;sta se usara como direcci&oacute;n de env&iacute;o</p>
	  	</div>
	  	<div class="form-group">
	    	<label for="telefono">Telefono</label>
	    	<input type="text" class="form-control" id="telefono" name="telefono" placeholder="Telefono sin espacios ni guiones..." value="<?=(isset($perfil)) ? $perfil->telefono : "" ?>">
	  	</div>
	  	<div class="form-group">
	    	<label for="celular">Celular</label>
	    	<input type="text" class="form-control" id="celular" name="celular" placeholder="Celular sin espacios ni guiones..." value="<?=(isset($perfil)) ? $perfil->celular : "" ?>">
	  	</div>
	  	<div class="form-group">
	    	<label for="biografia">Biograf&iacute;a</label>
	    	<textarea class="form-control" id="biografia" name="biografia" placeholder="Biograf&iacute;a..."><?=(isset($perfil)) ? $perfil->biografia : "" ?></textarea>
	  	</div>
	  	<div class="form-group">
	    	<label for="facebook">Redes Sociales</label>
	    	<input type="text" class="form-control" id="facebook" name="facebook" value="<?=(isset($perfil)) ? $perfil->facebook : "http://facebook.com/" ?>" placeholder="Facebook...">
	    	<input type="text" class="form-control" id="twitter" name="twitter" value="<?=(isset($perfil)) ? $perfil->twitter : "@" ?>" placeholder="Twitter...">
	    	<input type="text" class="form-control" id="instagram" name="instagram" value="<?=(isset($perfil)) ? $perfil->instagram : "http://instagram.com/" ?>" placeholder="Instagram...">
	    	<input type="text" class="form-control" id="linkedin" name="linkedin" value="<?=(isset($perfil)) ? $perfil->linkedin : "http://linkedin.com/" ?>" placeholder="Linkedin...">
	  	</div>

	  	<button type="submit" class="btn btn-primary" id="tipo-perfil" name="tipo-perfil" value="<?=$tipo_perfil?>">Guardar</button>
	</form>
  </div>
</div>