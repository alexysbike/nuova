<div class="row">
    <div class="col-md-3">
        <?
          if ($perfil->id_imagen == 0) :
            $img_cuenta = base_url()."img/missing-img.png";
          else :
            $img_cuenta = base_url()."img/usuarios/".$perfil->id_usuario."/".$perfil->imagen->archivo;
          endif;
        ?>
        <a href="<?=$img_cuenta?>" class="thumbnail" id="imagen-usuario-publico" style="background-image:url(<?=$img_cuenta?>)" data-idimagen="<?=$perfil->id_imagen?>" data-target="#lightbox" url-img="<?=$img_cuenta?>">
          <p class="text-center" id="imagen-hover-cuenta" style="display:none"><strong>Cambiar Imagen</strong></p>
        </a>
    </div>
    <div class="col-md-5">
        <h2><?=$perfil->alias?></h2>
        <small><cite title="<?=$perfil->lugar?>"><?=$perfil->lugar?> <span class="glyphicon glyphicon-map-marker">
        </span></cite></small>
        <p>
        	<span class="glyphicon glyphicon-user"></span> <?=$perfil->nombre.' '.$perfil->apellido?>
            <p>
              <a target="_blank" href="<?=$perfil->facebook?>"><img src="<?=base_url()?>img/socials/facebook-48.png"></a>
              <a target="_blank" href="http://twitter.com/<?=$perfil->twitter?>"><img src="<?=base_url()?>img/socials/twitter-48.png"></a>
              <a target="_blank" href="<?=$perfil->linkedin?>"><img src="<?=base_url()?>img/socials/linkedin-48.png"></a>
              <a target="_blank" href="<?=$perfil->instagram?>"><img src="<?=base_url()?>img/socials/instagram-48.png" style="height: 46px;margin-top: -3px;"></a></p>
            </p>
        <p class="well" style="height:129px; overflow-y:scroll">
          <?=$perfil->biografia?>
        </p>
    </div>
    <div class="col-md-4">
      <div class="row">
          <div class="col-md-5 text-right">
            Calificaciones:
          </div>
          <div class="col-md-7">
            <?
            $positivas = count($calificaciones["positivas"]);
            $negativas = count($calificaciones["negativas"]);
            $neutrales = count($calificaciones["neutrales"]);
            $total_calif = $positivas + $negativas + $neutrales;
            ?>
            <? if ($total_calif > 0): ?>
            <a href="#" title="<?=$positivas?> positivas <?=$total_calif?> calificaciones"><span class="label label-success"><?=($positivas / $total_calif) * 100?>%</span></a> : <a href="#" title="<?=$negativas?> negativas de <?=$total_calif?> calificaciones"><span class="label label-danger"><?=($negativas / $total_calif) * 100?>%</span></a>
            <? else: ?>
            Sin Calificar
            <? endif; ?>
          </div>
      </div>
      <? if ($total_calif > 0): ?>
        <p class="text-center"><a href="#" data-toggle="modal" data-target="#modal-ver-calificaciones">Ver Calificaciones</a></p>
      <? endif; ?>
      <div class="panel panel-primary" style="margin-top:25px">
        <div class="panel-heading text-center">Colecciones</div>
        <div class="list-group" style="height: 213px;overflow-y: scroll">
          <? foreach ($colecciones as $coleccion): ?>
            <a class="list-group-item" href="<?=base_url()?>usuario/ver_articulos_coleccion/<?=$coleccion->id?>"><?=$coleccion->nombre?></a>
          <? endforeach; ?>
        </div>
      </div>
    </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">Ultimos Art&iacute;culos</div>
  <div class="panel-body">
    <? if(count($articulos) == 0): ?>
      <div class="alert alert-danger">No hay art&iacute;culos disponibles</div>
    <? else: ?>
    <div class="row">
      <? foreach ($articulos as $articulo): ?>
          <?
            if (count($articulo->imagenes)>0) :
              $url = base_url()."img/usuarios/".$articulo->id_usuario."/".$articulo->imagenes[0]->archivo;
            else :
              $url = base_url()."img/missing-img.png";
            endif;
          ?>
        <div class="col-md-2">
            <a href="<?=base_url()?>sitio/articulo/<?=$articulo->id?>" class="thumbnail thumbnail-ult-art" target="_blank">
              <div class="imagen-ult-art" style="background-image: url(<?=$url?>)">
              </div>
              <div class="caption">
                <? if($articulo->id_coleccion != 0): ?><cite title="Colecci&oacute;n"><?=$articulo->coleccion?></cite><? endif; ?>
                <h4><?=$articulo->nombre?></h4>
                <h3 class="text-center"><strong class="text-primary">Bs. <?=$articulo->precio->precio?></strong></h3>
              </div>
            </a>
          </div>
      <? endforeach; ?>
    </div>    
    <? endif; ?>
  </div>
  <div class="panel-footer text-right"><a href="<?=base_url()?>usuario/ver_articulos/<?=$perfil->id_usuario?>">Ver todos...</a></div>
</div>
<? $this->load->view("templates/lightbox"); ?>
<? $this->load->view("usuario/widgets/modal_ver_calificaciones"); ?>