		<legend class="h1 text-center">Art&iacute;culos de la Colecci&oacute;n "<?=$coleccion->nombre?>"</legend>
		<? if(count($articulos) == 0): ?>
      		<div class="alert alert-danger">No hay art&iacute;culos disponibles</div>
    	<? else: ?>
		<div class="row">
		  <? foreach($articulos as $articulo): ?>
		  <?
		  	if (count($articulo->imagenes)>0) :
		  		$url = base_url()."img/usuarios/".$articulo->id_usuario."/".$articulo->imagenes[0]->archivo;
		  	else :
		  		$url = base_url()."img/missing-img.png";
		  	endif;
		  ?>
		  <div class="col-sm-6 col-md-3">
		    <a href="<?=base_url()?>sitio/articulo/<?=$articulo->id?>" class="thumbnail thumbnail-busqueda">
		      <div class="imagen-busqueda" style="background-image: url(<?=$url?>)">
			  </div>
		      <div class="caption">
		      	<? if($articulo->id_coleccion != 0): ?><cite title="Colecci&oacute;n"><?=$articulo->coleccion?></cite><? endif; ?>
		      	<h4><?=$articulo->nombre?><br><small>Por <i><?=$articulo->usuario->alias?></i></small></h4>
		        <h3 class="text-center"><strong class="text-primary">Bs. <?=$articulo->precio->precio?></strong></h3>
		      </div>
		    </a>
		  </div>
		<? endforeach; ?>
		</div>
		<? endif; ?>