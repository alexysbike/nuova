<div class="panel-group" id="accordion-datos-pago">
  <? $i=0; foreach ($datos_pago as $datos): ?>
  <div class="panel panel-default">
    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion-datos-pago" href="#dato<?=$datos->id?>" style="cursor:pointer" data-iddatospago="<?=$datos->id?>" data-banco="<?=$datos->banco?>">
      <h4 class="panel-title">
          <?=$datos->banco?>
      </h4>
    </div>
    <div id="dato<?=$datos->id?>" class="panel-collapse <?= ($i==0) ? "in" : "collapse"?>">
      <div class="panel-body">
        <p>
          <strong>Banco: </strong><span class="banco"><?=$datos->banco?></span><br>
          <strong>Cuenta: </strong><span class="tipo-cuenta" value="<?=$datos->tipo_cuenta?>"><?=ucfirst($datos->tipo_cuenta)?></span><br>
          <strong>Numero: </strong><span class="numero-cuenta"><?=$datos->numero?></span><br>
          <strong>A nombre de: </strong><span class="titular-cuenta"><?=$datos->titular?></span><br>
          <strong>Cedula: </strong><span class="cedula-cuenta"><?=$datos->cedula?></span><br>
          <strong>Correo Electr&oacute;nico: </strong><span class="correo-cuenta"><?=$datos->correo?></span><br>
          <a href="#" class="btn btn-default editar-datos-pago" title="Editar" data-iddatospago="<?=$datos->id?>"><span class="glyphicon glyphicon-pencil"></span></a>
          <a href="#" class="btn btn-danger eliminar-datos-pago" title="Eliminar" data-iddatospago="<?=$datos->id?>"><span class="glyphicon glyphicon-remove"></span></a>
        </p>
      </div>
    </div>
  </div>
  <? $i++; endforeach; ?>
</div>