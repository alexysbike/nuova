<h1 class="text-center" style="margin-bottom:120px">Registro Completado! Bienvenido a Nuova!</h1>
<div class="row">
	<? if($tipo_usuario == 3): ?>
	<div class="col-md-5">
		<a href="#" class="btn btn-lg btn-block btn-success"><span class="glyphicon glyphicon-shopping-cart"></span> Explorar Articulos</a>
	</div>
	<div class="col-md-5 col-md-offset-2">
		<a href="<?=base_url()?>usuario/cuenta" class="btn btn-lg btn-block btn-primary">Ir A Mi Cuenta <span class="glyphicon glyphicon-user"></span></a>
	</div>
	<? else: ?>
	<div class="col-md-4">
		<a href="<?=base_url()?>sitio/buscar" class="btn btn-lg btn-block btn-success"><span class="glyphicon glyphicon-shopping-cart"></span> Explorar Articulos</a>
	</div>
	<div class="col-md-4">
		<a href="#" class="btn btn-lg btn-block btn-success"><span class="glyphicon glyphicon-shopping-book"></span> Tutoriales</a>
	</div>
	<div class="col-md-4">
		<a href="<?=base_url()?>usuario/cuenta" class="btn btn-lg btn-block btn-primary">Ir A Mi Cuenta <span class="glyphicon glyphicon-user"></span></a>
	</div>
	<? endif;?>
</div>
<? if($tipo_usuario == 2): ?>
<div class="row" style="margin-top:20px">
	<div class="col-md-6"><img src="<?=base_url()?>img/informaciones/registro-completo-1.png" class="img-responsive"></div>
	<div class="col-md-6"><img src="<?=base_url()?>img/informaciones/registro-completo-2.png" class="img-responsive"></div>
</div>
<? endif; ?>