<div class="panel-group" id="accordion-datos-pago">
  <? foreach ($datos_pago as $datos): ?>
  <div class="panel panel-default">
    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion-datos-pago" href="#dato<?=$datos->id?>" style="cursor:pointer" data-iddatospago="<?=$datos->id?>" data-banco="<?=$datos->banco?>">
      <h4 class="panel-title">
          <?=$datos->banco?>
      </h4>
    </div>
    <div id="dato<?=$datos->id?>" class="panel-collapse collapse">
      <div class="panel-body">
        <p>
          <strong>Banco: </strong><?=$datos->banco?><br>
          <strong>Cuenta: </strong><?=ucfirst($datos->tipo_cuenta)?><br>
          <strong>Numero: </strong><?=$datos->numero?><br>
          <strong>A nombre de: </strong><?=$datos->titular?><br>
          <strong>Cedula: </strong><?=$datos->cedula?><br>
          <strong>Correo Electr&oacute;nico: </strong><?=$datos->correo?>
        </p>
      </div>
    </div>
  </div>
  <? endforeach; ?>
</div>