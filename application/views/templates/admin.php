<!DOCTYPE html>
<html>
  <head>
    <title>Nuova Venezuela - Administracion</title>
    <link rel="shortcut icon" href="<?=base_url()?>img/favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?=base_url()?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>css/bootstrap-lightbox.min.css" rel="stylesheet">

    <link href="<?=base_url()?>css/base.css" rel="stylesheet">

    <? if(!empty($css)): foreach ($css as $cs): ?>
      <link href="<?=base_url()?>css/<?=$cs?>" rel="stylesheet">
    <? endforeach; endif; ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
      <div class="row" id="header">
        
        <div class="col-md-4 col-md-offset-4 text-center" id="logo"><a href="<?=base_url()?>"><img src="<?=base_url()?>img/logo.png"></a></div>
        <div class="col-md-4 text-right" id="opciones-usuario">
          <? if ($this->session->userdata("logueado") == true): ?>
            <? if ($this->session->userdata("id_tipo")<=2): ?>
              <a href="#" id="boton-micuenta" data-nombre="<?=$this->session->userdata("alias")?>"><span class="glyphicon glyphicon-user"></span> Mi Cuenta</a>
            <? else: ?>
              <a href="#" id="boton-micuenta-comprador" data-nombre="<?=$this->session->userdata("alias")?>"><span class="glyphicon glyphicon-user"></span> Mi Cuenta</a>
            <? endif; ?>
            <? if ($this->session->userdata("id_tipo") == 0): ?>
              <a href="<?=base_url()?>administracion"><span class="glyphicon glyphicon-flag"></span> Administracion</a>
            <? endif; ?>
            <a href="<?=base_url()?>usuario/cuenta/deseos"><span class="glyphicon glyphicon-heart"></span> Deseos</a>
          <? else: ?>
            <a href="#" id="boton-login"><span class="glyphicon glyphicon-user"></span> Login</a>
          <? endif; ?>
          </div>
        </div>
      </div>  
    </div>

    <nav class="navbar navbar-inverse" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li><a href="<?=base_url()?>administracion/actualidad">Actualidad</a></li>
              <li><a href="<?=base_url()?>administracion/disenadores">Dise&ntilde;adores</a></li>
              <li><a href="<?=base_url()?>administracion/facturas">Facturacion</a></li>
              <li><a href="<?=base_url()?>administracion/ventas">Ventas</a></li>
              <li><a href="<?=base_url()?>administracion/correos_masivos">Correos Masivos</a></li>
            </ul>
            
          </div><!-- /.navbar-collapse -->
        </div>
      </nav>
    
    <div class="container" id="contenido">
      <? $this->load->view($main_content); ?>
    </div>
    <div id="footer" class="navbar-inverse">
      <div class="container">
        &copy; 2014 Nuova Venezuela,C.A.   RIF: J-40336353-6
      </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?=base_url()?>js/lib/jquery-2.0.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=base_url()?>js/lib/bootstrap.min.js"></script>
    <script src="<?=base_url()?>js/lib/bootstrap-lightbox.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/lib/jquery.form.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/config.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/base.js"></script>
    <? if(!empty($scripts)): foreach ($scripts as $js): ?>
      <script type="text/javascript" src="<?=base_url()?>js/scripts/<?=$js?>"></script>
    <? endforeach; endif;?>
  </body>
</html>