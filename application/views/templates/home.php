<!DOCTYPE html>
<html>
  <head>
    <!--
    Realizado por: 
    Alexys Gonzalez
    alexysbike@gmail.com
    -->
    <title>Nuova Venezuela</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?=base_url()?>img/favicon.ico">
    <link href="<?=base_url()?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>css/bootstrap-lightbox.min.css" rel="stylesheet">

    <link href="<?=base_url()?>css/base.css" rel="stylesheet">

    <? if(!empty($css)): foreach ($css as $cs): ?>
      <link href="<?=base_url()?>css/<?=$cs?>" rel="stylesheet">
    <? endforeach; endif; ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body style="background-color:black">
    <div class="container">
      <div class="row" id="header">
        <div class="col-md-5" id="buscador">
        <div class="" id="opciones-usuario">
          <? if ($this->session->userdata("logueado") == true): ?>
            <? if ($this->session->userdata("id_tipo")<=2): ?>
              <a href="#" id="boton-micuenta" data-nombre="<?=$this->session->userdata("alias")?>"><span class="glyphicon glyphicon-user"></span> Mi Cuenta</a>
            <? else: ?>
              <a href="#" id="boton-micuenta-comprador" data-nombre="<?=$this->session->userdata("alias")?>"><span class="glyphicon glyphicon-user"></span> Mi Cuenta</a>
            <? endif; ?>
            <? if ($this->session->userdata("id_tipo") == 0): ?>
              <a href="<?=base_url()?>administracion"><span class="glyphicon glyphicon-flag"></span> Administracion</a>
            <? endif; ?>
            <a href="<?=base_url()?>usuario/cuenta/deseos"><span class="glyphicon glyphicon-heart"></span> Deseos</a>
          <? else: ?>
            <a href="#" id="boton-login"><span class="glyphicon glyphicon-user"></span> Login</a>
            <a href="<?=base_url()?>usuario/nuevo"><span class="glyphicon glyphicon-ok"></span> Registrar como Comprador</a>
            <a href="<?=base_url()?>usuario/nuevo/disenador"><span class="glyphicon glyphicon-star"></span> Registrar como Dise&ntilde;ador</a>
          <? endif; ?>
        </div><br>
        <?
          if(!isset($url_busqueda)):
            $url_busqueda = base_url()."sitio/buscar/";
          endif;
        ?>
        <form class="" id="busqueda-pagina" role="search" action="<?=$url_busqueda?>" method="post">
            <div class="input-group ">
              <input type="hidden" name="ordenar" id="ordenar" value="<? if(isset($ordenar)): echo $ordenar; endif;?>">
              <input type="hidden" name="filtrar" id="filtrar" value="<? if(isset($filtrar)): echo $filtrar; endif;?>">
              <input type="hidden" name="valor" id="valor" value="<? if(isset($valor)): echo $valor; endif;?>">
              <input type="hidden" name="categoria" id="categoria" value="<? if(isset($categoria)): echo $categoria; endif;?>">
              <input type="hidden" name="pagina" id="pagina" value="">
              <input type="text" class="form-control" placeholder="Busqueda" name="busqueda" id="busqueda-articulo" value="<?=(isset($busqueda)) ? $busqueda : "" ?>">
              <span class="input-group-btn"><button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button></span>
            </div>
            
          </form><br>
            <a href="<?=base_url()?>usuario/nuevo/disenador"><span class="glyphicon glyphicon-globe"></span> Publica con Nosotros</a>
            <a href="<?=base_url()?>sitio/contacto"><span class="glyphicon glyphicon-envelope"></span> Cont&aacute;cto</a>
            <a href="<?=base_url()?>sitio/quienes_somos"><span class="glyphicon glyphicon-info-sign"></span> Quienes Somos</a>
            <a href="<?=base_url()?>sitio/tutoriales"><span class="glyphicon glyphicon-book"></span> Tutoriales</a><br>
            <br>
            <a target="_blank" href="https://www.facebook.com/nuovavenezuela" style="min-width:82px; min-height:24px; text-align:left"><img style="vertical-align:middle; height:36px; margin-right:5px" src="<?=base_url()?>img/socials/facebook-48.png"></a>
            <a target="_blank" href="https://twitter.com/nuovavenezuela" style="min-width:82px; min-height:24px; text-align:left"><img style="vertical-align:middle; height:36px; margin-right:5px" src="<?=base_url()?>img/socials/twitter-48.png"></a>
            <a target="_blank" href="http://instagram.com/nuovavenezuela" style="min-width:82px; min-height:24px; text-align:left"><img style="vertical-align:middle; height:36px" src="<?=base_url()?>img/socials/instagram-48.png"></a>
        </div>
        <div class="col-md-7 text-right" id="logo"><a href="<?=base_url()?>"><img src="<?=base_url()?>img/logo-blanco.png" style="width:350px"></a></div>
        </div>
      </div>  
    </div>

      <nav class="navbar navbar-inverse" role="navigation" style="background-color:black;color:#E31082; min-height:555px;">
        <div class="container">
          <div class="row hidden-xs hidden-sm" style="color:#E31082">
            <div class="col-md-5 categoria-home-grande borde-categoria borde-categoria-izq">
              <img src="<?=base_url()?>img/categorias-rotadas/alta-costura-b.png" style="display:none" class="img-detalles">
              <div class="row detalles-categoria">
                <div class="col-md-2"><img src="<?=base_url()?>img/categorias-rotadas/alta-costura-p.png"></div>
                <div class="col-md-10">
                  <h3>Destacada</h3>
                  <? if (count($destacado_alta_costura) != 0): ?>
                  <a href="<?=base_url()?>sitio/articulo/<?=$destacado_alta_costura["id"]?>" class="thumbnail color-fucsia">
                  <div class="row">
                    <div class="col-md-5">
                      <?
                        if ($destacado_alta_costura["presentacion-tipo"] != "unica"):
                          $destacado_alta_costura["imagenes"] = $destacado_alta_costura["presentaciones"][0]["imagenes"];
                        endif;
                        if (count($destacado_alta_costura["imagenes"])>0) :
                          $url = base_url()."img/usuarios/".$destacado_alta_costura["id_usuario"]."/".$destacado_alta_costura["imagenes"][0]->archivo;
                        else :
                          $url = base_url()."img/missing-img.png";
                        endif;
                      ?>
                      <div class="thumbnails-home" style="background-image:url(<?=$url?>)"></div>
                    </div>
                    <div class="col-md-7">
                      <div class="caption">
                        <? if ($destacado_alta_costura["id_coleccion"] != 0): ?>
                        <cite title="Colección" class="h5"><?=$destacado_alta_costura["coleccion"]?></cite>
                        <? endif; ?>           
                        <h3 style="margin:0"><?=$destacado_alta_costura["nombre"]?><br><small>Por <i><?=$destacado_alta_costura["usuario"]->alias?></i></small></h3>
                      </div>
                    </div>
                  </div>
                  </a>
                  <? else: ?>
                  <div class="thumbnail" style="height:180px">
                    <h2 class="text-center">No hay art&iacute;culo destacado en este momento</h2>
                  </div>
                  <? endif; ?>
                  <h3>M&aacute;s Vendida</h3>
                  <? if (count($mas_vendido_alta_costura) != 0): ?>
                    <a href="<?=base_url()?>sitio/articulo/<?=$mas_vendido_alta_costura["id"]?>" class="thumbnail color-fucsia">
                    <div class="row">
                      <div class="col-md-5">
                        <?
                          if ($mas_vendido_alta_costura["presentacion-tipo"] != "unica"):
                            $mas_vendido_alta_costura["imagenes"] = $mas_vendido_alta_costura["presentaciones"][0]["imagenes"];
                          endif;
                          if (count($mas_vendido_alta_costura["imagenes"])>0) :
                            $url = base_url()."img/usuarios/".$mas_vendido_alta_costura["id_usuario"]."/".$mas_vendido_alta_costura["imagenes"][0]->archivo;
                          else :
                            $url = base_url()."img/missing-img.png";
                          endif;
                        ?>
                        <div class="thumbnails-home" style="background-image:url(<?=$url?>)"></div>
                      </div>
                      <div class="col-md-7">
                        <div class="caption">
                          <? if ($mas_vendido_alta_costura["id_coleccion"] != 0): ?>
                          <cite title="Colección" class="h5"><?=$mas_vendido_alta_costura["coleccion"]?></cite>
                          <? endif; ?>           
                          <h3 style="margin:0"><?=$mas_vendido_alta_costura["nombre"]?><br><small>Por <i><?=$mas_vendido_alta_costura["usuario"]->alias?></i></small></h3>
                        </div>
                      </div>
                    </div>
                  </a>
                  <? else: ?>
                  <div class="thumbnail" style="height:180px">
                    <h2 class="text-center">No hay art&iacute;culo m&aacute;s vendido en este momento</h2>
                  </div>
                  <? endif; ?>
                  <p class="text-right"><a href="<?=base_url()?>sitio/buscar/1">Ver todo...</a></p>
                </div>
              </div>
            </div>
            <div class="col-md-1 categoria-home borde-categoria">
            <img src="<?=base_url()?>img/categorias-rotadas/casual-b.png" class="img-detalles">
            <div class="row detalles-categoria" style="display:none">
                <div class="col-md-2"><img src="<?=base_url()?>img/categorias-rotadas/casual-p.png"></div>
                <div class="col-md-10">
                  <h3>Destacada</h3>
                  <? if (count($destacado_casual) != 0): ?>
                  <a href="<?=base_url()?>sitio/articulo/<?=$destacado_casual["id"]?>" class="thumbnail color-fucsia">
                  <div class="row">
                     <div class="col-md-5">
                      <?
                        if ($destacado_casual["presentacion-tipo"] != "unica"):
                          $destacado_casual["imagenes"] = $destacado_casual["presentaciones"][0]["imagenes"];
                        endif;
                        if (count($destacado_casual["imagenes"])>0) :
                          $url = base_url()."img/usuarios/".$destacado_casual["id_usuario"]."/".$destacado_casual["imagenes"][0]->archivo;
                        else :
                          $url = base_url()."img/missing-img.png";
                        endif;
                      ?>
                      <div class="thumbnails-home" style="background-image:url(<?=$url?>)"></div>
                    </div>
                    <div class="col-md-7">
                      <div class="caption">
                        <? if ($destacado_casual["id_coleccion"] != 0): ?>
                        <cite title="Colección" class="h5"><?=$destacado_casual["coleccion"]?></cite>
                        <? endif; ?>           
                        <h3 style="margin:0"><?=$destacado_casual["nombre"]?><br><small>Por <i><?=$destacado_casual["usuario"]->alias?></i></small></h3>
                      </div>
                    </div>
                  </div>
                  </a>
                  <? else: ?>
                  <div class="thumbnail" style="height:180px">
                    <h2 class="text-center">No hay art&iacute;culo destacado en este momento</h2>
                  </div>
                  <? endif; ?>
                  <h3>M&aacute;s Vendida</h3>
                  <? if (count($mas_vendido_casual) != 0): ?>
                  <a href="<?=base_url()?>sitio/articulo/<?=$mas_vendido_casual["id"]?>" class="thumbnail color-fucsia">
                  <div class="row">
                     <div class="col-md-5">
                      <?
                        if ($mas_vendido_casual["presentacion-tipo"] != "unica"):
                          $mas_vendido_casual["imagenes"] = $mas_vendido_casual["presentaciones"][0]["imagenes"];
                        endif;
                        if (count($mas_vendido_casual["imagenes"])>0) :
                          $url = base_url()."img/usuarios/".$mas_vendido_casual["id_usuario"]."/".$mas_vendido_casual["imagenes"][0]->archivo;
                        else :
                          $url = base_url()."img/missing-img.png";
                        endif;
                      ?>
                      <div class="thumbnails-home" style="background-image:url(<?=$url?>)"></div>
                    </div>
                    <div class="col-md-7">
                      <div class="caption">
                        <? if ($mas_vendido_casual["id_coleccion"] != 0): ?>
                        <cite title="Colección" class="h5"><?=$mas_vendido_casual["coleccion"]?></cite>
                        <? endif; ?>           
                        <h3 style="margin:0"><?=$mas_vendido_casual["nombre"]?><br><small>Por <i><?=$mas_vendido_casual["usuario"]->alias?></i></small></h3>
                      </div>
                    </div>
                  </div>
                  </a>
                  <? else: ?>
                  <div class="thumbnail" style="height:180px">
                    <h2 class="text-center">No hay art&iacute;culo m&aacute;s vendido en este momento</h2>
                  </div>
                  <? endif; ?>
                  <p class="text-right"><a href="<?=base_url()?>sitio/buscar/2">Ver todo...</a></p>
                </div>
              </div>
            </div>
            <div class="col-md-1 categoria-home borde-categoria">
              <img src="<?=base_url()?>img/categorias-rotadas/deportiva-b.png" class="img-detalles">
              <div class="row detalles-categoria" style="display:none">
                <div class="col-md-2"><img src="<?=base_url()?>img/categorias-rotadas/deportiva-p.png"></div>
                <div class="col-md-10">
                  <h3>Destacada</h3>
                  <? if (count($destacado_deportiva) != 0): ?>
                    <a href="<?=base_url()?>sitio/articulo/<?=$destacado_deportiva["id"]?>" class="thumbnail color-fucsia">
                    <div class="row">
                      <div class="col-md-5">
                        <?
                          if ($destacado_deportiva["presentacion-tipo"] != "unica"):
                            $destacado_deportiva["imagenes"] = $destacado_deportiva["presentaciones"][0]["imagenes"];
                          endif;
                          if (count($destacado_deportiva["imagenes"])>0) :
                            $url = base_url()."img/usuarios/".$destacado_deportiva["id_usuario"]."/".$destacado_deportiva["imagenes"][0]->archivo;
                          else :
                            $url = base_url()."img/missing-img.png";
                          endif;
                        ?>
                        <div class="thumbnails-home" style="background-image:url(<?=$url?>)"></div>
                      </div>
                      <div class="col-md-7">
                        <div class="caption">
                          <? if ($destacado_deportiva["id_coleccion"] != 0): ?>
                          <cite title="Colección" class="h5"><?=$destacado_deportiva["coleccion"]?></cite>
                          <? endif; ?>           
                          <h3 style="margin:0"><?=$destacado_deportiva["nombre"]?><br><small>Por <i><?=$destacado_deportiva["usuario"]->alias?></i></small></h3>
                        </div>
                      </div>
                    </div>
                    </a>
                  <? else: ?>
                  <div class="thumbnail" style="height:180px">
                    <h2 class="text-center">No hay art&iacute;culo destacado en este momento</h2>
                  </div>
                  <? endif; ?>
                  <h3>M&aacute;s Vendida</h3>
                  <? if (count($mas_vendido_deportiva) != 0): ?>
                    <a href="<?=base_url()?>sitio/articulo/<?=$mas_vendido_deportiva["id"]?>" class="thumbnail color-fucsia">
                    <div class="row">
                      <div class="col-md-5">
                        <?
                          if ($mas_vendido_deportiva["presentacion-tipo"] != "unica"):
                            $mas_vendido_deportiva["imagenes"] = $mas_vendido_deportiva["presentaciones"][0]["imagenes"];
                          endif;
                          if (count($mas_vendido_deportiva["imagenes"])>0) :
                            $url = base_url()."img/usuarios/".$mas_vendido_deportiva["id_usuario"]."/".$mas_vendido_deportiva["imagenes"][0]->archivo;
                          else :
                            $url = base_url()."img/missing-img.png";
                          endif;
                        ?>
                        <div class="thumbnails-home" style="background-image:url(<?=$url?>)"></div>
                      </div>
                      <div class="col-md-7">
                        <div class="caption">
                          <? if ($mas_vendido_deportiva["id_coleccion"] != 0): ?>
                          <cite title="Colección" class="h5"><?=$mas_vendido_deportiva["coleccion"]?></cite>
                          <? endif; ?>           
                          <h3 style="margin:0"><?=$mas_vendido_deportiva["nombre"]?><br><small>Por <i><?=$mas_vendido_deportiva["usuario"]->alias?></i></small></h3>
                        </div>
                      </div>
                    </div>
                    </a>
                  <? else: ?>
                  <div class="thumbnail" style="height:180px">
                    <h2 class="text-center">No hay art&iacute;culo m&aacute;s vendido en este momento</h2>
                  </div>
                  <? endif; ?>
                  <p class="text-right"><a href="<?=base_url()?>sitio/buscar/3">Ver todo...</a></p>
                </div>
              </div>
            </div>
            <div class="col-md-1 categoria-home borde-categoria">
              <img src="<?=base_url()?>img/categorias-rotadas/playera-b.png" class="img-detalles">
              <div class="row detalles-categoria" style="display:none">
                <div class="col-md-2"><img src="<?=base_url()?>img/categorias-rotadas/playera-p.png"></div>
                <div class="col-md-10">
                  <h3>Destacada</h3>
                  <? if (count($destacado_playera) != 0): ?>
                  <a href="<?=base_url()?>sitio/articulo/<?=$destacado_playera["id"]?>" class="thumbnail color-fucsia">
                  <div class="row">
                    <div class="col-md-5">
                      <?
                        if ($destacado_playera["presentacion-tipo"] != "unica"):
                          $destacado_playera["imagenes"] = $destacado_playera["presentaciones"][0]["imagenes"];
                        endif;
                        if (count($destacado_playera["imagenes"])>0) :
                          $url = base_url()."img/usuarios/".$destacado_playera["id_usuario"]."/".$destacado_playera["imagenes"][0]->archivo;
                        else :
                          $url = base_url()."img/missing-img.png";
                        endif;
                      ?>
                      <div class="thumbnails-home" style="background-image:url(<?=$url?>)"></div>
                    </div>
                    <div class="col-md-7">
                      <div class="caption">
                        <? if ($destacado_playera["id_coleccion"] != 0): ?>
                        <cite title="Colección" class="h5"><?=$destacado_playera["coleccion"]?></cite>
                        <? endif; ?>           
                        <h3 style="margin:0"><?=$destacado_playera["nombre"]?><br><small>Por <i><?=$destacado_playera["usuario"]->alias?></i></small></h3>
                      </div>
                    </div>
                  </div>
                  </a>
                  <? else: ?>
                  <div class="thumbnail" style="height:180px">
                    <h2 class="text-center">No hay art&iacute;culo destacado en este momento</h2>
                  </div>
                  <? endif; ?>
                  <h3>M&aacute;s Vendida</h3>
                  <? if (count($mas_vendido_playera) != 0): ?>
                  <a href="<?=base_url()?>sitio/articulo/<?=$mas_vendido_playera["id"]?>" class="thumbnail color-fucsia">
                  <div class="row">
                    <div class="col-md-5">
                      <?
                        if ($mas_vendido_playera["presentacion-tipo"] != "unica"):
                          $mas_vendido_playera["imagenes"] = $mas_vendido_playera["presentaciones"][0]["imagenes"];
                        endif;
                        if (count($mas_vendido_playera["imagenes"])>0) :
                          $url = base_url()."img/usuarios/".$mas_vendido_playera["id_usuario"]."/".$mas_vendido_playera["imagenes"][0]->archivo;
                        else :
                          $url = base_url()."img/missing-img.png";
                        endif;
                      ?>
                      <div class="thumbnails-home" style="background-image:url(<?=$url?>)"></div>
                    </div>
                    <div class="col-md-7">
                      <div class="caption">
                        <? if ($mas_vendido_playera["id_coleccion"] != 0): ?>
                        <cite title="Colección" class="h5"><?=$mas_vendido_playera["coleccion"]?></cite>
                        <? endif; ?>           
                        <h3 style="margin:0"><?=$mas_vendido_playera["nombre"]?><br><small>Por <i><?=$mas_vendido_playera["usuario"]->alias?></i></small></h3>
                      </div>
                    </div>
                  </div>
                  </a>
                  <? else: ?>
                  <div class="thumbnail" style="height:180px">
                    <h2 class="text-center">No hay art&iacute;culo m&aacute;s vendido en este momento</h2>
                  </div>
                  <? endif; ?>
                  <p class="text-right"><a href="<?=base_url()?>sitio/buscar/4">Ver todo...</a></p>
                </div>
              </div>
            </div>
            <div class="col-md-1 categoria-home borde-categoria">
              <img src="<?=base_url()?>img/categorias-rotadas/intima-b.png" class="img-detalles">
              <div class="row detalles-categoria" style="display:none">
                <div class="col-md-2"><img src="<?=base_url()?>img/categorias-rotadas/intima-p.png"></div>
                <div class="col-md-10">
                  <h3>Destacada</h3>
                  <? if (count($destacado_intima) != 0): ?>
                  <a href="<?=base_url()?>sitio/articulo/<?=$destacado_intima["id"]?>" class="thumbnail color-fucsia">
                  <div class="row">
                    <div class="col-md-5">
                      <?
                        if ($destacado_intima["presentacion-tipo"] != "unica"):
                          $destacado_intima["imagenes"] = $destacado_intima["presentaciones"][0]["imagenes"];
                        endif;
                        if (count($destacado_intima["imagenes"])>0) :
                          $url = base_url()."img/usuarios/".$destacado_intima["id_usuario"]."/".$destacado_intima["imagenes"][0]->archivo;
                        else :
                          $url = base_url()."img/missing-img.png";
                        endif;
                      ?>
                      <div class="thumbnails-home" style="background-image:url(<?=$url?>)"></div>
                    </div>
                    <div class="col-md-7">
                      <div class="caption">
                        <? if ($destacado_intima["id_coleccion"] != 0): ?>
                        <cite title="Colección" class="h5"><?=$destacado_intima["coleccion"]?></cite>
                        <? endif; ?>           
                        <h3 style="margin:0"><?=$destacado_intima["nombre"]?><br><small>Por <i><?=$destacado_intima["usuario"]->alias?></i></small></h3>
                      </div>
                    </div>
                  </div>
                  </a>
                  <? else: ?>
                  <div class="thumbnail" style="height:180px">
                    <h2 class="text-center">No hay art&iacute;culo destacado en este momento</h2>
                  </div>
                  <? endif; ?>
                  <h3>M&aacute;s Vendida</h3>
                  <? if (count($mas_vendido_intima) != 0): ?>
                  <a href="<?=base_url()?>sitio/articulo/<?=$mas_vendido_intima["id"]?>" class="thumbnail color-fucsia">
                  <div class="row">
                    <div class="col-md-5">
                      <?
                        if ($mas_vendido_intima["presentacion-tipo"] != "unica"):
                          $mas_vendido_intima["imagenes"] = $mas_vendido_intima["presentaciones"][0]["imagenes"];
                        endif;
                        if (count($mas_vendido_intima["imagenes"])>0) :
                          $url = base_url()."img/usuarios/".$mas_vendido_intima["id_usuario"]."/".$mas_vendido_intima["imagenes"][0]->archivo;
                        else :
                          $url = base_url()."img/missing-img.png";
                        endif;
                      ?>
                      <div class="thumbnails-home" style="background-image:url(<?=$url?>)"></div>
                    </div>
                    <div class="col-md-7">
                      <div class="caption">
                        <? if ($mas_vendido_intima["id_coleccion"] != 0): ?>
                        <cite title="Colección" class="h5"><?=$mas_vendido_intima["coleccion"]?></cite>
                        <? endif; ?>           
                        <h3 style="margin:0"><?=$mas_vendido_intima["nombre"]?><br><small>Por <i><?=$mas_vendido_intima["usuario"]->alias?></i></small></h3>
                      </div>
                    </div>
                  </div>
                  </a>
                  <? else: ?>
                  <div class="thumbnail" style="height:180px">
                    <h2 class="text-center">No hay art&iacute;culo m&aacute;s vendido en este momento</h2>
                  </div>
                  <? endif; ?>
                  <p class="text-right"><a href="<?=base_url()?>sitio/buscar/5">Ver todo...</a></p>
                </div>
              </div>
            </div>
            <div class="col-md-1 categoria-home borde-categoria">
              <img src="<?=base_url()?>img/categorias-rotadas/accesorios-b.png" class="img-detalles">
              <div class="row detalles-categoria" style="display:none">
                <div class="col-md-2"><img src="<?=base_url()?>img/categorias-rotadas/accesorios-p.png"></div>
                <div class="col-md-10">
                  <h3>Destacada</h3>
                  <? if (count($destacado_accesorios) != 0): ?>
                  <a href="<?=base_url()?>sitio/articulo/<?=$destacado_accesorios["id"]?>" class="thumbnail color-fucsia">
                  <div class="row">
                    <div class="col-md-5">
                      <?
                        if ($destacado_accesorios["presentacion-tipo"] != "unica"):
                          $destacado_accesorios["imagenes"] = $destacado_accesorios["presentaciones"][0]["imagenes"];
                        endif;
                        if (count($destacado_accesorios["imagenes"])>0) :
                          $url = base_url()."img/usuarios/".$destacado_accesorios["id_usuario"]."/".$destacado_accesorios["imagenes"][0]->archivo;
                        else :
                          $url = base_url()."img/missing-img.png";
                        endif;
                      ?>
                      <div class="thumbnails-home" style="background-image:url(<?=$url?>)"></div>
                    </div>
                    <div class="col-md-7">
                      <div class="caption">
                        <? if ($destacado_accesorios["id_coleccion"] != 0): ?>
                        <cite title="Colección" class="h5"><?=$destacado_accesorios["coleccion"]?></cite>
                        <? endif; ?>           
                        <h3 style="margin:0"><?=$destacado_accesorios["nombre"]?><br><small>Por <i><?=$destacado_accesorios["usuario"]->alias?></i></small></h3>
                      </div>
                    </div>
                  </div>
                  </a>
                  <? else: ?>
                  <div class="thumbnail" style="height:180px">
                    <h2 class="text-center">No hay art&iacute;culo destacado en este momento</h2>
                  </div>
                  <? endif; ?>
                  <h3>M&aacute;s Vendida</h3>
                  <? if (count($mas_vendido_accesorios) != 0): ?>
                  <a href="<?=base_url()?>sitio/articulo/<?=$mas_vendido_accesorios["id"]?>" class="thumbnail color-fucsia">
                  <div class="row">
                    <div class="col-md-5">
                      <?
                        if ($mas_vendido_accesorios["presentacion-tipo"] != "unica"):
                          $mas_vendido_accesorios["imagenes"] = $mas_vendido_accesorios["presentaciones"][0]["imagenes"];
                        endif;
                        if (count($mas_vendido_accesorios["imagenes"])>0) :
                          $url = base_url()."img/usuarios/".$mas_vendido_accesorios["id_usuario"]."/".$mas_vendido_accesorios["imagenes"][0]->archivo;
                        else :
                          $url = base_url()."img/missing-img.png";
                        endif;
                      ?>
                      <div class="thumbnails-home" style="background-image:url(<?=$url?>)"></div>
                    </div>
                    <div class="col-md-7">
                      <div class="caption">
                        <? if ($mas_vendido_accesorios["id_coleccion"] != 0): ?>
                        <cite title="Colección" class="h5"><?=$mas_vendido_accesorios["coleccion"]?></cite>
                        <? endif; ?>           
                        <h3 style="margin:0"><?=$mas_vendido_accesorios["nombre"]?><br><small>Por <i><?=$mas_vendido_accesorios["usuario"]->alias?></i></small></h3>
                      </div>
                    </div>
                  </div>
                  </a>
                  <? else: ?>
                  <div class="thumbnail" style="height:180px">
                    <h2 class="text-center">No hay art&iacute;culo m&aacute;s vendido en este momento</h2>
                  </div>
                  <? endif; ?>
                  <p class="text-right"><a href="<?=base_url()?>sitio/buscar/6">Ver todo...</a></p>
                </div>
              </div>
            </div>
            <div class="col-md-1 categoria-home borde-categoria">
              <img src="<?=base_url()?>img/categorias-rotadas/calzado-b.png" class="img-detalles">
              <div class="row detalles-categoria" style="display:none">
                <div class="col-md-2"><img src="<?=base_url()?>img/categorias-rotadas/calzado-p.png"></div>
                <div class="col-md-10">
                  <h3>Destacada</h3>
                  <? if (count($destacado_calzado) != 0): ?>
                  <a href="<?=base_url()?>sitio/articulo/<?=$destacado_calzado["id"]?>" class="thumbnail color-fucsia">
                  <div class="row">
                    <div class="col-md-5">
                      <?
                        if ($destacado_calzado["presentacion-tipo"] != "unica"):
                          $destacado_calzado["imagenes"] = $destacado_calzado["presentaciones"][0]["imagenes"];
                        endif;
                        if (count($destacado_calzado["imagenes"])>0) :
                          $url = base_url()."img/usuarios/".$destacado_calzado["id_usuario"]."/".$destacado_calzado["imagenes"][0]->archivo;
                        else :
                          $url = base_url()."img/missing-img.png";
                        endif;
                      ?>
                      <div class="thumbnails-home" style="background-image:url(<?=$url?>)"></div>
                    </div>
                    <div class="col-md-7">
                      <div class="caption">
                        <? if ($destacado_calzado["id_coleccion"] != 0): ?>
                        <cite title="Colección" class="h5"><?=$destacado_calzado["coleccion"]?></cite>
                        <? endif; ?>           
                        <h3 style="margin:0"><?=$destacado_calzado["nombre"]?><br><small>Por <i><?=$destacado_calzado["usuario"]->alias?></i></small></h3>
                      </div>
                    </div>
                  </div>
                  </a>
                  <? else: ?>
                  <div class="thumbnail" style="height:180px">
                    <h2 class="text-center">No hay art&iacute;culo destacado en este momento</h2>
                  </div>
                  <? endif; ?>
                  <h3>M&aacute;s Vendida</h3>
                  <? if (count($mas_vendido_calzado) != 0): ?>
                  <a href="<?=base_url()?>sitio/articulo/<?=$mas_vendido_calzado["id"]?>" class="thumbnail color-fucsia">
                  <div class="row">
                    <div class="col-md-5">
                      <?
                        if ($mas_vendido_calzado["presentacion-tipo"] != "unica"):
                          $mas_vendido_calzado["imagenes"] = $mas_vendido_calzado["presentaciones"][0]["imagenes"];
                        endif;
                        if (count($mas_vendido_calzado["imagenes"])>0) :
                          $url = base_url()."img/usuarios/".$mas_vendido_calzado["id_usuario"]."/".$mas_vendido_calzado["imagenes"][0]->archivo;
                        else :
                          $url = base_url()."img/missing-img.png";
                        endif;
                      ?>
                      <div class="thumbnails-home" style="background-image:url(<?=$url?>)"></div>
                    </div>
                    <div class="col-md-7">
                      <div class="caption">
                        <? if ($mas_vendido_calzado["id_coleccion"] != 0): ?>
                        <cite title="Colección" class="h5"><?=$mas_vendido_calzado["coleccion"]?></cite>
                        <? endif; ?>           
                        <h3 style="margin:0"><?=$mas_vendido_calzado["nombre"]?><br><small>Por <i><?=$mas_vendido_calzado["usuario"]->alias?></i></small></h3>
                      </div>
                    </div>
                  </div>
                  </a>
                  <? else: ?>
                  <div class="thumbnail" style="height:180px">
                    <h2 class="text-center">No hay art&iacute;culo m&aacute;s vendido en este momento</h2>
                  </div>
                  <? endif; ?>
                  <p class="text-right"><a href="<?=base_url()?>sitio/buscar/7">Ver todo...</a></p>
                </div>
              </div>
            </div>
            <div class="col-md-1 categoria-home borde-categoria">
              <img src="<?=base_url()?>img/categorias-rotadas/disenadores-b.png" style="margin-top: 10px;" class="img-detalles">
              <div class="row detalles-categoria" style="display:none">
                <div class="col-md-2"><img src="<?=base_url()?>img/categorias-rotadas/disenadores-p.png" style="margin-top: 10px;"></div>
                <div class="col-md-10">
                  <?
                    if ($disenador_destacado->id_imagen == 0) :
                      $img_cuenta = base_url()."img/missing-img.png";
                    else :
                      $img_cuenta = base_url()."img/usuarios/".$disenador_destacado->id_usuario."/".$disenador_destacado->imagen->archivo;
                    endif;
                  ?>
                  <h3>Destacado</h3>
                  <a href="<?=base_url()?>usuario/ver_perfil/<?=$disenador_destacado->id_usuario?>" class="thumbnail color-fucsia">
                  <div class="row">
                    <div class="col-md-5">
                      <div class="thumbnails-home" style="background-image:url(<?=$img_cuenta?>)"></div>
                    </div>
                    <div class="col-md-7">
                      <div class="caption">
                        <cite title="Colección" class="h5"><?=$disenador_destacado->nombre." ".$disenador_destacado->apellido?></cite>           
                        <h3 style="margin:0"><?=$disenador_destacado->alias?></h3>
                      </div>
                    </div>
                  </div>
                  </a>
                  <? /*<h3>Mejores Ventas</h3>
                  <a href="http://localhost/nuova/img/usuarios/10/10_fghjk.jpg" class="thumbnail color-fucsia">
                  <div class="row">
                    <div class="col-md-5">
                      <div class="thumbnails-home" style="background-image:url(http://localhost/nuova/img/usuarios/10/10_fghjk.jpg)"></div>
                    </div>
                    <div class="col-md-7">
                      <div class="caption">
                        <cite title="Colección" class="h5">White Ropes 2013</cite>           
                        <h3 style="margin:0">camisa blanca<br><small>Por <i>Azraeldk</i></small></h3>
                      </div>
                    </div>
                  </div>
                  </a>
                  */?>
                  <p class="text-right"><a href="<?=base_url()?>sitio/buscar_disenador">Ver todo...</a></p> 
                  </div>
                </div>
              </div>
            </div>
            <div class="visible-xs visible-sm">
              <p class="text-center"><a href="<?=base_url()?>sitio/buscar/1"><img src="<?=base_url()?>img/categorias/alta-costura-p.png" class="img-responsive center-block"></a></p>
              <p class="text-center"><a href="<?=base_url()?>sitio/buscar/2"><img src="<?=base_url()?>img/categorias/casual-p.png" class="img-responsive center-block"></a></p>
              <p class="text-center"><a href="<?=base_url()?>sitio/buscar/3"><img src="<?=base_url()?>img/categorias/deportiva-p.png" class="img-responsive center-block"></a></p>
              <p class="text-center" style="margin-bottom:0"><a href="<?=base_url()?>sitio/buscar/4"><img src="<?=base_url()?>img/categorias/playera-p.png" class="img-responsive center-block"></a></p>
              <p class="text-center"><a href="<?=base_url()?>sitio/buscar/5"><img src="<?=base_url()?>img/categorias/intima-p.png" class="img-responsive center-block"></a></p>
              <p class="text-center"><a href="<?=base_url()?>sitio/buscar/6"><img src="<?=base_url()?>img/categorias/accesorios-p.png" class="img-responsive center-block"></a></p>
              <p class="text-center"><a href="<?=base_url()?>sitio/buscar/7"><img src="<?=base_url()?>img/categorias/calzado-p.png" class="img-responsive center-block"></a></p>
              <p class="text-center"><a href="<?=base_url()?>sitio/buscar_disenador"><img src="<?=base_url()?>img/categorias/disenadores-p.png" class="img-responsive center-block"></a></p>
            </div>         
          </div>
        </div>
      </nav>
    <div class="container text-center" style="margin-top: 20px; margin-bottom: 20px">
      <div class="row">
        <div class="col-md-6"><img src="<?=base_url()?>img/informaciones/pagar-publicidad.png" class="img-responsive"></div>
        <div class="col-md-6"><img src="<?=base_url()?>img/informaciones/pagar-publicidad.png" class="img-responsive"></div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="panel panel-default" style="min-height:341px">
            <div class="panel-heading">Actualidad</div>
            <div class="panel-body" style="height:298px; overflow-y:scroll">
            <? if (count($noticia) != 0):?>
            <h2><?=$noticia->titulo?></h2>
            <strong class="text-primary">(<?=sqldate_to_datepicker($noticia->fecha)?>)</strong> <?=$noticia->cuerpo?>
            <? else: ?>
            <strong>No hay noticias de actualidad en este momento...</strong>
            <? endif; ?>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <a href="<?=base_url()?>usuario/nuevo/disenador"><img src="<?=base_url()?>img/informaciones/disenador-publica.png" class="img-responsive"></a>
        </div>
      </div>
    </div>
    <div id="footer" class="navbar-inverse" style="background-color:black; margin-top:20px">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <p>
              &copy; 2014 Nuova Venezuela,C.A.   RIF: J-40336353-6<br>
              <a href="mailto:info@nuovavenezuela.com">info@nuovavenezuela.com</a><br>
              Tel&eacute;fono: +584149474836<br>
              <a target="_blank" href="https://www.facebook.com/nuovavenezuela" style="min-width:82px; min-height:24px; text-align:left"><img style="vertical-align:middle" src="<?=base_url()?>img/socials/facebook-48.png"></a>
            <a target="_blank" href="https://twitter.com/nuovavenezuela" style="min-width:82px; min-height:24px; text-align:left"><img style="vertical-align:middle" src="<?=base_url()?>img/socials/twitter-48.png"></a>
            <a target="_blank" href="http://instagram.com/nuovavenezuela" style="min-width:82px; min-height:24px; text-align:left"><img style="vertical-align:middle" src="<?=base_url()?>img/socials/instagram-48.png"></a>
            </p>
          </div>
          <div class="col-md-6">
            <p>
              <strong>Mapa del Sitio:</strong><br>
              <a href="<?=base_url()?>sitio/buscar">Art&iacute;culos</a><br>
              <a href="<?=base_url()?>sitio/quienes_somos">Quienes Somos</a><br>
              <a href="<?=base_url()?>sitio/contacto">Contacto</a><br>
              <a href="<?=base_url()?>sitio/tutoriales">Tutoriales</a>
            </p>
          </div>
        </div>
      </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?=base_url()?>js/lib/jquery-2.0.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=base_url()?>js/lib/bootstrap.min.js"></script>
    <script src="<?=base_url()?>js/lib/bootstrap-lightbox.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/lib/jquery.form.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/config.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/base.js"></script>
    <? if(!empty($scripts)): foreach ($scripts as $js): ?>
      <script type="text/javascript" src="<?=base_url()?>js/scripts/<?=$js?>"></script>
    <? endforeach; endif;?>
  </body>
</html>