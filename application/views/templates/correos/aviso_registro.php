<!DOCTYPE html>
<html>
  <head>
    <title>Nuova Venezuela</title>
  </head>
  <body>
    <img src="<?=base_url()?>img/logo_grande.png" width="300">
    <h1>Registro de Nuevo Dise&ntilde;ador</h1>
    <p>
      <strong>Nombre y Apellido:</strong> <?=$perfil->nombre." ".$perfil->apellido?><br>
      <strong>Correo:</strong> <?=$usuario->correo?><br>
      <strong>Telefono:</strong> <?=$perfil->telefono?><br>
      <strong>Celular:</strong> <?=$perfil->celular?>
    </p>
    <div id="footer">
      <div class="container">
        &copy; 2014 Nuova Venezuela,C.A.   RIF: J-40336353-6
      </div>
    </div>
  </body>
</html>