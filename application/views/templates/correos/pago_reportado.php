<!DOCTYPE html>
<html>
  <head>
    <title>Nuova Venezuela</title>
  </head>
  <body>
    <img src="<?=base_url()?>logo_grande.png" width="300">
    <h1>Han Reportado un Pago!</h1>
    <p>Han reportado un pago para el art&iacute;culo:</p>
    <? if ($compra->articulo["presentacion-tipo"] == "varias") :
            $imagen = $compra->presentacion->imagenes;
    else:
            $imagen = $compra->imagenes;
    endif;?>
    <div class="media">
      <img width="75" src="<?=base_url()?>img/usuarios/<?=$compra->articulo["id_usuario"]?>/<?=$imagen[0]->archivo?>">
      <div class="media-body">
        <h3 class="media-heading"><?=$compra->articulo["nombre"]?></h3>
        <h4><small>Colecci&oacute;n <?=$compra->articulo["coleccion"]?></small></h4>
      </div>
    </div>
    <p>
      <strong>Fecha: </strong><?=sqldate_to_datepicker($compra->fecha_compra)?><br>
      <? if ($compra->articulo["presentacion-tipo"] == "varias"): ?>
      <strong>Presentacion: </strong><span id="confirmacion-presentacion"><?=$compra->presentacion->nombre?></span><br>
      <? endif; ?>
      <strong>Talla: </strong><span id="confirmacion-talla"><?=$compra->talla->talla?></span><br>
      <strong>Cantidad: </strong><span id="confirmacion-cantidad"><?=$compra->cantidad?> Unidades</span><br>
      <strong>Precio: </strong>Bs. <?=$compra->precio?><br>
      <strong>Total: </strong>Bs. <?=$compra->precio*$compra->cantidad?>
    </p>
    <p>Datos del Comprador:</p>
    <div class="media">
      <? if(isset($comprador->imagen)): ?>
        <img width="75" src="<?=base_url()?>img/usuarios/<?=$comprador->id_usuario?>/<?=$comprador->imagen->archivo?>">
      <? endif; ?>
      <div class="media-body">
        <h3 class="media-heading"><?=$comprador->alias?></h3>
        <h4><small><?=$comprador->nombre?> <?=$comprador->apellido?></small></h4>
      </div>
    </div>
    <p>
      <strong>Correo Electr&oacute;nico:</strong> <?=$comprador->usuario->correo?><br>
      <strong>Telefono:</strong> <?=$comprador->telefono?><br>
      <strong>Celular:</strong> <?=$comprador->celular?>
    </p>
    <h3>Por favor dirigete al panel de las <a href="<?=base_url()?>usuario/cuenta/ventas">ventas</a> y verificalo en la opcion correspondiente. Luego de verificado el pago puedes proceder a marcar la venta como Verificada y proceder a realizar el env&iacute;o, para esto puedes verificar la direcci&oacute;n de env&iacute;o a trav&eacute;s del panel de <a href="<?=base_url()?>usuario/cuenta/ventas">ventas</a></h3>
    <div id="footer">
      <div class="container">
        &copy; 2014 Nuova Venezuela,C.A.   RIF: J-40336353-6
      </div>
    </div>
  </body>
</html>