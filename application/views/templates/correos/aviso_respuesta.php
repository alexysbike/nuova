<!DOCTYPE html>
<html>
  <head>
    <title>Nuova Venezuela</title>
  </head>
  <body>
    <img src="<?=base_url()?>img/logo_grande.png" width="300">
    <h1>Te han respondido</h1>
    <p>Te respondieron una pregunta que hicistes en el siguiente Art&iacute;culo: </p>
    <? if ($articulo["presentacion-tipo"] == "varias") :
            $imagen = $articulo["presentaciones"]["imagenes"];
    else:
            $imagen = $articulo["imagenes"];
    endif;?>
    <div class="media">
      <img width="75" src="<?=base_url()?>img/usuarios/<?=$articulo["id_usuario"]?>/<?=$imagen[0]->archivo?>">
      <div class="media-body">
        <h3 class="media-heading"><?=$articulo["nombre"]?></h3>
        <h4><small>Colecci&oacute;n <?=$articulo["coleccion"]?></small></h4>
      </div>
    </div>
    <h3>Haz click <a href="<?=base_url()?>sitio/articulo/<?=$articulo["id"]?>">aqu&iacute;</a> para que veas la respuesta</h3>
    <div id="footer">
      <div class="container">
        &copy; 2014 Nuova Venezuela,C.A.   RIF: J-40336353-6
      </div>
    </div>
  </body>
</html>