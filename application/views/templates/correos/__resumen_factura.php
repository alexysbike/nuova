<!DOCTYPE html>
<html>
  <head>
    <title>Nuova Venezuela</title>
  </head>
  <body>
    <img src="<?=base_url()?>img/logo_grande.png" width="300">
    <h1>Res&uacute;men de la factura del <?=sqldate_to_datepicker($factura->factura["fecha"])?></h1>
    <p>
      <strong>Total de Piezas Vendidas:</strong> <?=$factura->n_piezas?><br>
      <strong>Monto:</strong> Bs. <?=$factura->factura["monto"]?><br>
      <strong>Iva:</strong> Bs. <?=$factura->factura["monto"] * ($factura->factura["porc_iva"]/100)?><br>
      <strong>Total:</strong> Bs.   <?=$factura->factura["monto"] * (($factura->factura["porc_iva"]/100)+1)?>
    </p>

    <?php foreach ($factura->items as $item): ?>
      <?=var_dump($item)?>
      <? if ($item->articulo["presentacion-tipo"] == "varias") :
              $imagen = $item->presentacion->imagenes;
      else:
              $imagen = $item->imagenes;
      endif;?>
      <div class="media">
        <img width="75" src="<?=base_url()?>img/usuarios/<?=$item->articulo["id_usuario"]?>/<?=$imagen[0]->archivo?>">
        <div class="media-body">
          <h3 class="media-heading"><?=$item->articulo["nombre"]?></h3>
          <h4><small>Colecci&oacute;n <?=$item->articulo["coleccion"]?></small></h4>
        </div>
      </div>
      <p>
        <? if ($item->articulo["presentacion-tipo"] == "varias"): ?>
        <strong>Presentacion: </strong><span id="confirmacion-presentacion"><?=$item->presentacion->nombre?></span><br>
        <? endif; ?>
        <strong>Talla: </strong><span id="confirmacion-talla"><?=$item->talla->talla?></span><br>
        <strong>Cantidad: </strong><span id="confirmacion-cantidad"><?=$item->cantidad?> Unidades</span><br>
        <strong>Precio: </strong>Bs. <?=$item->precio?><br>
        <strong>Total: </strong>Bs. <?=$item->precio*$item->cantidad?>
      </p>
    <?php endforeach ?>

    <div id="footer">
      <div class="container">
        &copy; 2014 Nuova Venezuela,C.A.   RIF: J-40336353-6
      </div>
    </div>
  </body>
</html>