<!DOCTYPE html>
<html>
  <head>
    <title>Nuova Venezuela</title>
  </head>
  <body>
    <img src="<?=base_url()?>img/logo_grande.png" width="300">
    <h1>Res&uacute;men de la factura del <?=sqldate_to_datepicker($factura->fecha)?></h1>
    <div style="border-bottom: solid 1px gray;margin-bottom: 15px;">
      <strong>Total de Piezas Vendidas:</strong> <?=$factura->n_piezas?><br>
      <strong>Monto:</strong> Bs. <?=$factura->monto?><br>
      <strong>Iva:</strong> Bs. <?=$factura->monto * ($factura->porc_iva/100)?><br>
      <strong>Total:</strong> Bs. <?=$factura->monto * (($factura->porc_iva/100)+1)?>
    </div>
    <?php foreach ($factura->items as $item): ?>
    <div style="border-bottom: solid 1px gray;margin-bottom: 15px;">
      <? if ($item->compra->articulo["presentacion-tipo"] == "varias") :
              $imagen = $item->compra->presentacion->imagenes;
      else:
              $imagen = $item->compra->imagenes;
      endif;?>
      <div class="media">
        <img width="75" src="<?=base_url()?>img/usuarios/<?=$item->compra->articulo["id_usuario"]?>/<?=$imagen[0]->archivo?>" style="float:left">
        <div style="float:left; margin-left:10px">
          <h3 class="media-heading"><?=$item->compra->articulo["nombre"]?></h3>
          <h4><small>Colecci&oacute;n <?=$item->compra->articulo["coleccion"]?></small></h4>
        </div>
      </div>
      <div style="clear:both"></div>
      <div>
        <? if ($item->compra->articulo["presentacion-tipo"] == "varias"): ?>
        <strong>Presentacion: </strong><span id="confirmacion-presentacion"><?=$item->compra->presentacion->nombre?></span><br>
        <? endif; ?>
        <strong>Talla: </strong><span id="confirmacion-talla"><?=$item->compra->talla->talla?></span><br>
        <strong>Cantidad: </strong><span id="confirmacion-cantidad"><?=$item->compra->cantidad?> Unidades</span><br>
        <strong>Precio: </strong>Bs. <?=$item->compra->precio?><br>
        <strong>Total: </strong>Bs. <?=$item->compra->precio*$item->compra->cantidad?>
      </div>
    </div>
    <?php endforeach ?>

    <div id="footer">
      <div class="container">
        &copy; 2014 Nuova Venezuela,C.A.   RIF: J-40336353-6
      </div>
    </div>
  </body>
</html>