<!DOCTYPE html>
<html>
  <head>
    <title>Nuova Venezuela</title>
  </head>
  <body>
    <img src="<?=base_url()?>img/logo_grande.png" width="300">
    <h1>Compra Realizada con &Eacute;xito!</h1>
    <p>Res&uacute;men de la compra:</p>
    <? if ($compra->articulo["presentacion-tipo"] == "varias") :
            $imagen = $compra->presentacion->imagenes;
    else:
            $imagen = $compra->imagenes;
    endif;?>
    <div class="media">
      <img width="75" src="<?=base_url()?>img/usuarios/<?=$compra->articulo["id_usuario"]?>/<?=$imagen[0]->archivo?>">
      <div class="media-body">
        <h3 class="media-heading"><?=$compra->articulo["nombre"]?></h3>
        <h4><small>Colecci&oacute;n <?=$compra->articulo["coleccion"]?></small></h4>
      </div>
    </div>
    <p>
      <? if ($compra->articulo["presentacion-tipo"] == "varias"): ?>
      <strong>Presentacion: </strong><span id="confirmacion-presentacion"><?=$compra->presentacion->nombre?></span><br>
      <? endif; ?>
      <strong>Talla: </strong><span id="confirmacion-talla"><?=$compra->talla->talla?></span><br>
      <strong>Cantidad: </strong><span id="confirmacion-cantidad"><?=$compra->cantidad?> Unidades</span><br>
      <strong>Precio: </strong>Bs. <?=$compra->precio?><br>
      <strong>Total: </strong>Bs. <?=$compra->precio*$compra->cantidad?>
    </p>
    <p>Datos del Vendedor:</p>
    <div class="media">
      <? if(isset($disenador->imagen)): ?>
        <img width="75" src="<?=base_url()?>img/usuarios/<?=$disenador->id_usuario?>/<?=$disenador->imagen->archivo?>">
      <? endif; ?>
      <div class="media-body">
        <h3 class="media-heading"><?=$disenador->alias?></h3>
        <h4><small><?=$disenador->nombre?> <?=$disenador->apellido?></small></h4>
      </div>
    </div>
    <p>
      <strong>Correo Electr&oacute;nico:</strong> <?=$disenador->usuario->correo?><br>
      <strong>Telefono:</strong> <?=$disenador->telefono?><br>
      <strong>Celular:</strong> <?=$disenador->celular?>
    </p>
    <p>Datos para realizar el pago:</p>
    <ul>
      <? foreach ($datos_pago as $datos): ?>
      <li>
          <strong>Banco: </strong><?=$datos->banco?><br>
          <strong>Cuenta: </strong><?=ucfirst($datos->tipo_cuenta)?><br>
          <strong>Numero: </strong><?=$datos->numero?><br>
          <strong>A nombre de: </strong><?=$datos->titular?><br>
          <strong>Cedula: </strong><?=$datos->cedula?><br>
          <strong>Correo Electr&oacute;nico: </strong><?=$datos->correo?>
      </li>
      <? endforeach; ?>
    </ul>
    <h3>Luego de realizar el pago deber&aacute; reportarlo a trav&eacute;s de la p&aacute;gina en el panel de las compras (Bot&oacute;n de reporte de pagos de la compra), puedes ingresar al panel a trav&eacute;s de este <a href="<?=base_url()?>usuario/cuenta/compras">link</a></h3>
    <div id="footer">
      <div class="container">
        &copy; 2014 Nuova Venezuela,C.A.   RIF: J-40336353-6
      </div>
    </div>
  </body>
</html>