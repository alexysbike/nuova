<!DOCTYPE html>
<html>
  <head>
    <title>Nuova Venezuela</title>
  </head>
  <body>
    <img src="<?=base_url()?>img/logo_grande.png" width="300">
    <h1>Informaci&oacute;n de cuenta</h1>
    <p>
      <strong>Correo:</strong> <?=$usuario->correo?><br>
      <strong>Contrase&ntilde;a:</strong> <?=$usuario->password?>
    </p>
    <div id="footer">
      <div class="container">
        &copy; 2014 Nuova Venezuela,C.A.   RIF: J-40336353-6
      </div>
    </div>
  </body>
</html>