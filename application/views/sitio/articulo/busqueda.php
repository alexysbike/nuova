<div class="row">
	<div class="col-md-3">
		<div class="well well-lg">
			<ul class="nav" id="nav-ordenar">
				<strong style="text-decoration:underline">Ordenar por</strong>
				<li <?=($ordenar == "destacado" || $ordenar == "") ? 'style="font-weight:bold"' : ''?>><a href="#" data-ordenar="destacado">Destacado</a></li>
				<li <?=($ordenar == "ventas" || $ordenar == "") ? 'style="font-weight:bold"' : ''?>><a href="#" data-ordenar="ventas">M&aacute;s vendido</a></li>
				<li <?=($ordenar == "nuevo" || $ordenar == "") ? 'style="font-weight:bold"' : ''?>><a href="#" data-ordenar="nuevo">Lo m&aacute;s nuevo</a></li>
				<li <?=($ordenar == "coleccion") ? 'style="font-weight:bold"' : ''?>><a href="#" data-ordenar="coleccion">Colecciones</a></li>
			</ul>
			<ul class="nav" id="nav-filtrar">
				<strong style="text-decoration:underline">Filtrar por</strong>
				<li>
					<span style="padding-left:10px">G&eacute;nero:</span>
					<ul class="nav" id="nav-genero">
						<li <?=($valor == "") ? 'style="font-weight:bold"' : ''?>><a href="#" data-filtrar="genero" data-valor="">Todos</a></li>
						<li <?=($valor == "Caballeros") ? 'style="font-weight:bold"' : ''?>><a href="#" data-filtrar="genero" data-valor="Caballeros">Caballeros</a></li>
						<li <?=($valor == "Damas") ? 'style="font-weight:bold"' : ''?>><a href="#" data-filtrar="genero" data-valor="Damas">Damas</a></li>
						<li <?=($valor == "Ninos") ? 'style="font-weight:bold"' : ''?>><a href="#" data-filtrar="genero" data-valor="Ninos">Ni&ntilde;os</a></li>
						<li <?=($valor == "Ninas") ? 'style="font-weight:bold"' : ''?>><a href="#" data-filtrar="genero" data-valor="Ninas">Ni&ntilde;as</a></li>
						<li <?=($valor == "Unisex") ? 'style="font-weight:bold"' : ''?>><a href="#" data-filtrar="genero" data-valor="Unisex">Unisex</a></li>
					</ul>
				</li>
				<li>
					<span style="padding-left:10px">Categor&iacute;a:</span>
					<ul class="nav" id="nav-categoria">
						<li <?=($categoria == "todo" || $categoria == "") ? 'style="font-weight:bold"' : ''?>><a href="#" data-filtrar="categoria" data-valor="todo">Todas</a></li>
						<li <?=($categoria == "1") ? 'style="font-weight:bold"' : ''?>><a href="#" data-filtrar="categoria" data-valor="1">Alta Costura</a></li>
						<li <?=($categoria == "2") ? 'style="font-weight:bold"' : ''?>><a href="#" data-filtrar="categoria" data-valor="2">Casual</a></li>
			            <li <?=($categoria == "3") ? 'style="font-weight:bold"' : ''?>><a href="#" data-filtrar="categoria" data-valor="3">Deportiva</a></li>
			            <li <?=($categoria == "4") ? 'style="font-weight:bold"' : ''?>><a href="#" data-filtrar="categoria" data-valor="4">Playera</a></li>
			            <li <?=($categoria == "5") ? 'style="font-weight:bold"' : ''?>><a href="#" data-filtrar="categoria" data-valor="5">&Iacute;ntima</a></li>
			            <li <?=($categoria == "6") ? 'style="font-weight:bold"' : ''?>><a href="#" data-filtrar="categoria" data-valor="6">Accesorios</a></li>
			            <li <?=($categoria == "7") ? 'style="font-weight:bold"' : ''?>><a href="#" data-filtrar="categoria" data-valor="7">Calzado</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<div class="col-md-9">
		<? if($this->input->post("busqueda")): ?>
      		<div class="alert alert-info">
      		Resultados de <span class="text-success">"<?=$this->input->post("busqueda")?>"</span>
      		<? if ($categoria != 0 && $categoria != "" && $categoria != 'todo'): ?>
      		en la Categor&iacute;a <span class="text-success"><? $categorias = get_categorias(); echo $categorias[$categoria]; ?></span>.
      		<? else: ?>
      		en todas las Categor&iacute;as.
      		<? endif; ?>
      		<? if ($ordenar == "coleccion"): ?>
      		Ordenado por Colecci&oacute;n.
      		<? endif ?>
      		<? if($filtrar != "" && $valor != ""): ?>
      			Filtros Adicionales:
      			<? if($filtrar == "genero"): ?>
      			G&eacute;nero <span class="text-primary">"<? $genero = get_generos_articulo(); echo $genero[$this->input->post("valor")]; ?>"</span>.
      			<? endif; ?>
      		<? endif; ?>
      		<span class="text-warning"><?=count($articulos)?> Resultados</span>
      		</div>
    	<? endif; ?>
		<div class="row">
		  <? foreach($articulos as $articulo): ?>
		  <?
		  	if (count($articulo->imagenes)>0) :
		  		$url = base_url()."img/usuarios/".$articulo->id_usuario."/".$articulo->imagenes[0]->archivo;
		  	else :
		  		$url = base_url()."img/missing-img.png";
		  	endif;
		  ?>
		  <div class="col-sm-6 col-md-4">
		    <a href="<?=base_url()?>sitio/articulo/<?=$articulo->id?>" class="thumbnail thumbnail-busqueda <?=($articulo->destacado == 1) ? "destacado" : "" ?>">
		      <div class="imagen-busqueda" style="background-image: url(<?=$url?>)">
			  </div>
		      <div class="caption">
		      	<? if($articulo->id_coleccion != 0): ?><cite title="Colecci&oacute;n"><?=$articulo->coleccion?></cite><? endif; ?>
		      	<h4><?=$articulo->nombre?><br><small>Por <i><?=$articulo->alias?></i></small></h4>
		        <h3 class="text-center"><strong class="text-primary">Bs. <?=$articulo->precio->precio?></strong></h3>
		      </div>
		    </a>
		  </div>
		<? endforeach; ?>
		</div>
		<? if (count($articulos)>0): ?>
			<? 
			$total = $articulos[0]->total;
			$n_paginas = ceil($total/12);
			if($n_paginas >= 10):
				if ($pagina > 5 && $pagina < $n_paginas-4):
					$start = $pagina - 4;
				elseif ($pagina >= $n_paginas - 4):
					$start = $n_paginas - 8;
				else:
					$start = 1;
				endif;

				if ($pagina > 5 && $pagina < $n_paginas-4):
					$end = $pagina + 4;
				elseif ($pagina >= $n_paginas - 4):
					$end = $n_paginas;
				else:
					$end = 9;
				endif;
			else:
				$start = 1;
				$end = $n_paginas;
			endif;

			?>
		<div class="text-center">
			<ul class="pagination">
				<li class="<?= ($pagina == 1) ? "disabled" : ""?>"><a href="#" data-pagina="<?= ($pagina > 1) ? $pagina-1 : 1?>">&laquo;</a></li>
				<? if ($pagina > 5): ?>
				<li class="disabled"><a href="#">...</a></li>
				<? endif; ?>
				<? for ($i=$start; $i <= $end; $i++) : ?> 
				<li class="<?= ($pagina == $i) ? "active" : ""?>"><a href="#" data-pagina="<?=$i?>"><?=$i?></a></li>
				<? endfor;?>
				<? if ($pagina < $n_paginas-4 && $n_paginas >=10): ?>
				<li class="disabled"><a href="#">...</a></li>
				<? endif; ?>
				<li class="<?= ($pagina == $n_paginas) ? "disabled" : ""?>"><a href="#" data-pagina="<?= ($pagina < $n_paginas) ? $pagina+1 : $n_paginas?>">&raquo;</a></li>
			</ul>
			<p class="help-block"><?=$total?> Art&iacute;culos encontrados</p>
		</div>
		<? else: ?>
			<div class="alert alert-warning">
      		No se encontro ningun elemento con ese par&aacute;metro de busqueda<br>
      		Por favor intente con algo diferente
      		</div>
		<? endif; ?>
	</div>
</div>