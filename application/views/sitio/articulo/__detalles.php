<div class="row">
	<div class="col-md-3">

		<div id="imagenes-articulo" class="carousel slide" data-ride="carousel" style="height:325px" data-interval="999999">
		  <span id="items-ocultos" style="display:none;"></span>
		  <!-- Indicators -->
		  <?/*<ol class="carousel-indicators">
		    <? if ($articulo["presentacion-tipo"] == "unica"): ?>
		    	<? $i=0; foreach ($articulo['imagenes'] as $imagen): ?>
		    		<li data-target="#imagenes-articulo" data-slide-to="<?=$i?>" class="<?=($i==0) ? "active" : "" ?>"></li>
		    	<? $i++; endforeach; ?>
		    <? else: ?>
		    	<? $i=0; foreach ($articulo["presentaciones"] as $presentacion): ?>
		    		<? foreach ($presentacion['imagenes'] as $imagen): ?>
		    			<li data-target="#imagenes-articulo" data-slide-to="<?=$i?>" class="<?=($i==0) ? "active" : "" ?>" data-idpresentacion="<?=$presentacion["id"]?>"></li>
		    		<? $i++; endforeach; ?>
		    	<? endforeach; ?>
		    <? endif; ?>
		  </ol>*/?>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner" id="carousel-imagenes">
		    <? if ($articulo["presentacion-tipo"] == "unica"): ?>
		    	<? if (count($articulo["imagenes"]) == 0): ?>
		    		<div class="item active">
		    			  <a href="<?=base_url()?>img/missing-img.png" style="background-image: url(<?=base_url()?>img/missing-img.png); height:325px; width: 100%;display: block;" class="imagen-articulo" data-target="#lightbox" url-img="<?=base_url()?>img/missing-img.png" title=""></a>
		    			</div>
		    	<? else: ?>
		    	<? $i=0; foreach ($articulo['imagenes'] as $imagen): ?>
		    		<div class="item <?=($i==0) ? "active" : "" ?>">
		    			  <a href="<?=base_url()?>img/usuarios/<?=$articulo["id_usuario"]?>/<?=$imagen->archivo?>" style="background-image: url(<?=base_url()?>img/usuarios/<?=$articulo["id_usuario"]?>/<?=$imagen->archivo?>); height:325px; width: 100%;display: block;" class="imagen-articulo" data-target="#lightbox" url-img="<?=base_url()?>img/usuarios/<?=$articulo["id_usuario"]?>/<?=$imagen->archivo?>" title=""></a>
		    			</div>
		    	<? $i++; endforeach; ?>
		    	<? endif; ?>
		    <? else: ?>
		    	<? $i=0; foreach ($articulo["presentaciones"] as $presentacion): ?>
		    		<? foreach ($presentacion['imagenes'] as $imagen): ?>
		    			<div class="item <?=($i==0) ? "active" : "" ?>" data-idpresentacion="<?=$presentacion["id"]?>">
		    			  <a href="<?=base_url()?>img/usuarios/<?=$articulo["id_usuario"]?>/<?=$imagen->archivo?>" style="background-image: url(<?=base_url()?>img/usuarios/<?=$articulo["id_usuario"]?>/<?=$imagen->archivo?>); height:325px; width: 100%;display: block;" class="imagen-articulo" data-target="#lightbox" url-img="<?=base_url()?>img/usuarios/<?=$articulo["id_usuario"]?>/<?=$imagen->archivo?>" title=""></a>
		    			</div>
		    		<? $i++; endforeach; ?>
		    	<? endforeach; ?>
		    	<? if ($i == 0): ?>
		    		<div class="item active">
		    			  <a href="<?=base_url()?>img/missing-img.png" style="background-image: url(<?=base_url()?>img/missing-img.png); height:325px; width: 100%;display: block;" class="imagen-articulo" data-target="#lightbox" url-img="<?=base_url()?>img/missing-img.png" title=""></a>
		    			</div>
		    	<? endif; ?>
		    <? endif; ?>
		  </div>

		  <!-- Controls -->
		  <a class="left carousel-control" href="#imagenes-articulo" data-slide="prev">
		    <span class="glyphicon glyphicon-chevron-left"></span>
		  </a>
		  <a class="right carousel-control" href="#imagenes-articulo" data-slide="next">
		    <span class="glyphicon glyphicon-chevron-right"></span>
		  </a>
		</div>
	</div>
	<div class="col-md-6">
		<div class="row">
			<div class="col-md-8">
				<? if ($articulo["id_coleccion"] != 0): ?>
        			<h4><cite title="Colecci&oacute;n">Colecci&oacute;n <?=$articulo["coleccion"]?></cite></h4>
        		<? endif ?>
            	<h3><?=$articulo["nombre"]?></h3>
            	<small><cite title="Categor&iacute;a"><?=$articulo["categoria"]?> <span class="glyphicon glyphicon-tag"></span></cite></small>
            	<? if ($es_favorito == false): ?>
            		<a href="#" title="Marcar Favorito" style="color: grey" id="marcar-favorito" data-idarticulo="<?=$articulo["id"]?>"><h4 style="margin:5px 0 0 0"><span class="glyphicon glyphicon-star-empty"></span></h4></a>
            	<? else: ?>
            		<a href="#" title="Desmarcar Favorito" style="color: goldenrod" id="marcar-favorito" data-idarticulo="<?=$articulo["id"]?>"><h4 style="margin:5px 0 0 0"><span class="glyphicon glyphicon-star"></span></h4></a>
            	<? endif; ?>
            	<span class="glyphicon glyphicon-user"></span> <?$array = get_generos_articulo(); echo $array[$articulo["genero"]]?>
			</div>
			<div class="col-md-4">
				<h2>Bs. <?=$articulo["precio"]->precio?></h2>
			</div>
		</div>
		<? if ($articulo["presentacion-tipo"] == "unica"): ?>
		<? else: ?>
		<h5>Presentaciones:</h5>
		<ul class="nav nav-pills" id="presentaciones">
			<? foreach ($articulo["presentaciones"] as $index => $presentacion): ?>
				<li class="<?=($index==0) ? "" : "" ?>" data-idpresentacion="<?=$presentacion["id"]?>"><a href="#presentacion-<?=$presentacion["id"]?>" title="<?=$presentacion["nombre"]?>" data-toggle="tab"><i class="presentacion-square img-thumbnail" style="background-color: <?=$presentacion["color_icono"]?>;"></i></a></li>
			<? endforeach ?>
        </ul>
        <p class="help-block" id="nombre-presentacion">Todas las presentaciones</p>
        <? endif ?>
        <input type="hidden" id="presentacion-tipo" value="<?=$articulo["presentacion-tipo"]?>">
        <!-- Tab panes -->
		<div class="tab-content" id="select-presentaciones">
		  <? if ($articulo["presentacion-tipo"] == "unica"): ?>
		  	<div class="tab-pane active" id="presentacion-unica" style="padding:0">
		  		<div class="form-group">
  				  <label class="col-sm-2 control-label"><h4>Talla:</h4></label>
  				  <div class="col-sm-10">
  				    <select class="form-control">
  				    	<option value="" data-cantidad="0">Elija Talla</option>
					  <? foreach ($articulo["inventario"] as $id_presentacion => $talla): ?>
		  				<? foreach ($talla as $id_talla => $inv): ?>
		  					<? if($inv->cantidad != 0): ?>
		  						<option value="<?=$id_talla?>" data-cantidad="<?=$inv->cantidad?>"><?=$this->Articulo_model->get_talla($id_talla)->talla?></option>
		  					<? endif; ?>
		  				<? endforeach ?>
		  			<? endforeach ?>
					</select>
  				  </div>
  				</div>
		  	</div>
		  <? else: ?>
		  		<div id="a-esconder">
		  			<div class="form-group">
  					  <label class="col-sm-2 control-label"><h4>Talla:</h4></label>
  					  <div class="col-sm-10">
  					    <h4><span class="label label-info">Debe escojer una presentaci&oacute;n</span></h4>
  					  </div>
  					</div> 
		  		</div>
		  	<? foreach ($articulo["presentaciones"] as $index => $presentacion): ?>
		  		<div class="tab-pane <?=($index==0) ? "" : "" ?>" id="presentacion-<?=$presentacion["id"]?>" style="padding:0">
		  			<div class="form-group">
  					  <label class="col-sm-2 control-label"><h4>Talla:</h4></label>
  					  <div class="col-sm-10">
  					    <select class="form-control">
  					      <option value="" data-cantidad="0">Elija Talla</option>
						  <? foreach ($articulo["inventario"][$presentacion["id"]] as $id_talla => $inv): ?>
						  	<? if($inv->cantidad != 0): ?>
		  					<option value="<?=$id_talla?>" data-cantidad="<?=$inv->cantidad?>"><?=$this->Articulo_model->get_talla($id_talla)->talla?></option>
		  				<? endif; ?>
		  				<? endforeach ?>
						</select>
  					  </div>
  					</div> 
		  		</div>
		  	<? endforeach; ?>
		  <? endif; ?>
		</div>
		<div class="clearfix"></div>
		<div class="form-group">
  		  <label class="col-sm-2 control-label"><h4>Cantidad:</h4></label>
  		  <div class="col-sm-6 col-sm-offset-2">
  		  	<div class="input-group">
  		    	<span class="input-group-btn">
  		    		<button id="menos-cantidad" class="btn btn-default"><span class="glyphicon glyphicon-minus"></span></button>
  		    	</span>
  		    	<input class="form-control" id="cantidad" type="text" value="0">
  		    	<span class="input-group-btn">
  		    		<button id="mas-cantidad" class="btn btn-default"><span class="glyphicon glyphicon-plus"></span></button>
  		    	</span>
            </div>
            <p class="help-block" id="unidades-disponibles">Elija la talla</p>
  		  </div>
  		</div>
  		<div class="clearfix"></div>
  		<? if ($articulo["usuario_bloqueado"]->bloqueado == 1): ?>
  			<div class="panel panel-danger">
				<div class="panel-body">
					<h1 class="text-center">Usuario Bloqueado</h1>
				</div>
			</div>
  		<? elseif ($articulo["status"] > 0): ?>
  		<div class="row">
  			<div class="col-md-8 col-md-offset-2">
  				<button class="btn btn-primary btn-block btn-lg" id="boton-comprar"><span class="glyphicon glyphicon-shopping-cart"></span> Comprar</button>
  				<p class="text-center"><span id="mensaje-error-compra" class="label label-danger" style="display:none">Debes Escojer una presentacion</span></p>
  			</div>
  		</div>
  		<? elseif ($articulo["status"] == 0): ?>
			<div class="panel panel-danger">
				<div class="panel-body">
					<h1 class="text-center">Sin Existencias</h1>
				</div>
			</div>
  		<? else: ?>
			<div class="panel panel-danger">
				<div class="panel-body">
					<h1 class="text-center">Articulo Desactivado</h1>
				</div>
			</div>
  		<? endif; ?>
	</div>
	<div class="col-md-3">
		<p class="bg-info">
		<div class="media">
		  <? if(isset($disenador->imagen)): ?>
		  	<a href="#" url-img="<?=base_url()?>img/usuarios/<?=$articulo["id_usuario"]?>/<?=$disenador->imagen->archivo?>" class="thumbnail imagen-perfil-pequena pull-left" data-target="#lightbox" style="background-image:url(<?=base_url()?>img/usuarios/<?=$articulo["id_usuario"]?>/<?=$disenador->imagen->archivo?>)"></a>
		  <? else: ?>
		  	<a href="#" url-img="<?=base_url()?>img/missing-img.png" data-target="#lightbox" class="thumbnail imagen-perfil-pequena pull-left" style="background-image:url(<?=base_url()?>img/missing-img.png)"></a>
		  <? endif; ?>
		  <div class="media-body">
		  	<input type="hidden" id="id-disenador" value="<?=$articulo["id_usuario"]?>">
		    <h3 class="media-heading"><?=$disenador->alias?></h3>
		    <h4><small><?=$disenador->nombre?> <?=$disenador->apellido?></small></h4>
		  </div>
		</div>
		<div class="row">
  			<div class="col-md-6 text-right">
  				Reputaci&oacute;n:
  			</div>
  			<div class="col-md-6">
  				<?=$disenador->usuario->reputacion?>pt
  			</div>
  		</div>
  		<div class="row" style="padding-top: 5px">
  			<div class="col-md-12">
			  	<div class="progress progress-striped active">
				  <div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
				    <span class="sr-only">100% Complete</span>
				  </div>
				</div>
			</div>
		</div>
		</p>
		<h4>Descripci&oacute;n del Art&iacute;culo:</h4>
		<p id="descripcion-articulo" class="bg-primary">
			<?=$articulo["descripcion"]?>
		</p>
	</div>
</div>
<div class="row">
	<div class="col-md-6 col-md-offset-3" id="contenedor-preguntas">
		<h2>Preguntas y Respuestas</h2>
		<ul class="list-group">
			<? if($this->session->userdata("logueado")): ?>
				<? if ($this->session->userdata("id") != $articulo["id_usuario"]): ?>
					<li class="list-group-item list-group-item-success">
						<div class="form-group">
							<textarea class="form-control" rows="2" id="pregunta" placeholder="Escribe tu pregunta aqui..."></textarea>
						</div>
						<p class="text-right"><a href="#" class="btn btn-primary" id="btn-preguntar" data-idusuario="<?=$this->session->userdata("id")?>" data-idarticulo="<?=$articulo["id"]?>">Preguntar</a></p>
					</li>
					<? foreach ($preguntas_usuario as $pregunta): ?>
						<li class="list-group-item list-group-item-info">
							<p class="text-info"><span class="glyphicon glyphicon-question-sign"></span> <?=$pregunta->pregunta?> <span class="label label-info pull-right"><?=sqldate_to_datepicker($pregunta->fecha_pregunta)?></span></p>
							<? if ($pregunta->respuesta != ""): ?>
							<p class="text-success" style="margin-left:15px"><span class="glyphicon glyphicon-question-sign"></span> <?=$pregunta->respuesta?> <span class="label label-success pull-right"><?=sqldate_to_datepicker($pregunta->fecha_respuesta)?></span></p>
							<? endif; ?>
						</li>
					<? endforeach ?>
					<? foreach ($preguntas as $pregunta): ?>
						<li class="list-group-item">
							<p class="text-info"><span class="glyphicon glyphicon-question-sign"></span> <?=$pregunta->pregunta?> <span class="label label-info pull-right"><?=sqldate_to_datepicker($pregunta->fecha_pregunta)?></span></p>
							<? if ($pregunta->respuesta != ""): ?>
							<p class="text-success" style="margin-left:15px"><span class="glyphicon glyphicon-question-sign"></span> <?=$pregunta->respuesta?> <span class="label label-success pull-right"><?=sqldate_to_datepicker($pregunta->fecha_respuesta)?></span></p>
							<? endif; ?>
						</li>
					<? endforeach ?>
				<? else: ?>
					<? foreach ($preguntas_contestar as $pregunta): ?>
						<li class="list-group-item list-group-item-info">
							<p class="text-info"><span class="glyphicon glyphicon-question-sign"></span> <?=$pregunta->pregunta?> <span class="label label-info pull-right"><?=sqldate_to_datepicker($pregunta->fecha_pregunta)?></span></p>
							
							<div class="text-success" style="margin-left:15px">
								<div class="form-group"><textarea class="form-control respuesta" rows="2" placeholder="Escriba la respuesta para esta pregunta aqui..."></textarea></div>
								<p class="text-right"><a href="#" class="btn btn-primary btn-responder" data-idpregunta="<?=$pregunta->id?>">Responder</a></p>
							</div>
							
						</li>
					<? endforeach ?>
					<? foreach ($preguntas as $pregunta): ?>
						<li class="list-group-item">
							<p class="text-info"><span class="glyphicon glyphicon-question-sign"></span> <?=$pregunta->pregunta?> <span class="label label-info pull-right"><?=sqldate_to_datepicker($pregunta->fecha_pregunta)?></span></p>
							<? if ($pregunta->respuesta != ""): ?>
							<p class="text-success" style="margin-left:15px"><span class="glyphicon glyphicon-question-sign"></span> <?=$pregunta->respuesta?> <span class="label label-success pull-right"><?=sqldate_to_datepicker($pregunta->fecha_respuesta)?></span></p>
							<? endif; ?>
						</li>
					<? endforeach ?>
				<? endif; ?>
			<? else: ?>
				<li class="list-group-item list-group-item-success">
					Debes estar logueado para poder realizar una pregunta
				</li>
				<? foreach ($preguntas as $pregunta): ?>
					<li class="list-group-item">
						<p class="text-info"><span class="glyphicon glyphicon-question-sign"></span> <?=$pregunta->pregunta?> <span class="label label-info pull-right"><?=sqldate_to_datepicker($pregunta->fecha_pregunta)?></span></p>
						<? if ($pregunta->respuesta != ""): ?>
						<p class="text-success" style="margin-left:15px"><span class="glyphicon glyphicon-question-sign"></span> <?=$pregunta->respuesta?> <span class="label label-success pull-right"><?=sqldate_to_datepicker($pregunta->fecha_respuesta)?></span></p>
						<? endif; ?>
					</li>
				<? endforeach ?>
			<? endif; ?>
		</ul>
	</div>
</div>
<? $this->load->view("templates/lightbox"); ?>
<? $this->load->view("sitio/widgets/nueva_compra"); ?>