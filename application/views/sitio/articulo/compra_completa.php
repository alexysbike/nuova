<h1 class="text-center" style="margin-bottom:120px">Compra Exitosa!</h1>
<div class="row">
	<div class="col-md-6">
		<h2>Art&iacute;culo</h2>
		<legend></legend>
		<div class="media">
		  <a href="#" url-img="<?=base_url()?>img/usuarios/<?=$articulo["id_usuario"]?>/<?=$imagen[0]->archivo?>" class="thumbnail imagen-articulo-pequena pull-left" style="background-image:url(<?=base_url()?>img/usuarios/<?=$articulo["id_usuario"]?>/<?=$imagen[0]->archivo?>)" data-target="#lightbox"></a>
		  <div class="media-body">
		    <h3 class="media-heading"><?=$articulo["nombre"]?></h3>
		    <h4><small>Colecci&oacute;n <?=$articulo["coleccion"]?></small></h4>
		    <p>
        	  <? if ($articulo["presentacion-tipo"] == "varias"): ?>
        	  <strong>Presentacion: </strong><span id="confirmacion-presentacion"><?=$presentacion?></span><br>
        	  <? endif; ?>
        	  <strong>Talla: </strong><span id="confirmacion-talla"><?=$talla?></span><br>
        	  <strong>Cantidad: </strong><span id="confirmacion-cantidad"><?=$this->input->post("articulo-cantidad")?> Unidades</span><br>
        	  <strong>Precio: </strong>Bs. <?=$this->input->post("articulo-precio")?><br>
        	  <strong>Total: </strong>Bs. <?=$this->input->post("articulo-precio")*$this->input->post("articulo-cantidad")?>
        	</p>
		  </div>
		</div>
	</div>
	<div class="col-md-6">
	<h2>Dise&ntilde;ador</h2>
	<legend></legend>
		<div class="media">
		  <? if(isset($disenador->imagen)): ?>
		  	<a href="#" class="thumbnail imagen-perfil-pequena pull-left" style="background-image:url(<?=base_url()?>img/usuarios/<?=$articulo["id_usuario"]?>/<?=$disenador->imagen->archivo?>)"></a>
		  <? else: ?>
		  	<a href="#" class="thumbnail imagen-perfil-pequena pull-left" style="background-color:red"></a>
		  <? endif; ?>
		  <div class="media-body">
		  	<input type="hidden" id="id-disenador" value="<?=$articulo["id_usuario"]?>">
		    <h3 class="media-heading"><?=$disenador->alias?></h3>
		    <h4><small><?=$disenador->nombre?> <?=$disenador->apellido?></small></h4>
		  </div>
		</div>
		<p>
			<strong>Correo Electr&oacute;nico:</strong> <?=$this->Usuario_model->get_usuario("", "", $disenador->id_usuario)->correo?><br>
			<strong>Telefono:</strong> <?=$disenador->telefono?><br>
			<strong>Celular:</strong> <?=$disenador->celular?>
		</p>
	</div>
</div>
<div class="row">
	<div class="col-md-5 col-md-offset-1">
		<p>Ponte en Contacto con el Disenador para que concreten la forma de pago y el envio</p>
		<p>Luego de realizar el pago en una de las cuentas bancarias del disenador, debes reportar el pago a traves de esta plataforma</p>
		<p>Luego de reportado el pago el disenador procedera a enviarte la mercancia a su direccion de envio</p>
	</div>
	<div class="col-md-5">
		<p>Otros articulos del Disenador</p>
	</div>
</div>
<? $this->load->view("templates/lightbox"); ?>