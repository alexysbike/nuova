<legend class="h1 text-center">Cont&aacute;ctanos</legend>

<div class="row">
	<div class="col-md-5 col-md-offset-1" style="min-height:300px">
		<div class="well">
		<h4><strong>Correos Electr&oacute;nicos</strong></h4>
		<ol>
			<li>Informaci&#243;n para errores de p&#225;gina, preguntas de facturaci&#243;n y funcionamiento de la p&#225;gina (compra, env&#237;os): <br>
			<a href="mailto:administracion@nuovavenezuela.com">administracion@nuovavenezuela.com</a></li>
			<li>Informaci&#243;n General <br>
			<a href="mailto:info@nuovavenezuela.com">info@nuovavenezuela.com</a></li>
			<li>Reportes de pagos por el servicio prestado y preguntas con respecto a la facturaci&#243;n:<br>
			<a href="mailto:pagos@nuovavenezuela.com">pagos@nuovavenezuela.com</a></li>
			<li>Gerente General:<br>
			<a href="mailto:jeka0489@gmail.com">Jessika Carrillo jeka0489@gmail.com</a></li>
		</ol>
		</div>
	</div>
	<div class="col-md-5" style="min-height:300px">
		<div class="well">
		<h4><strong>N&uacute;meros Telef&oacute;nicos</strong></h4>
		<p>
			Gerente General: Whatsapp Activo<br>
			+584149474836
		</p>
		</div>
	</div>
</div>