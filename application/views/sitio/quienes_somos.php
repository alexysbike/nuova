<div class="jumbotron">

<legend class="text-center h1">Venezuela dise&#241;a, viste NUOVA</legend>
<br>
<br>
<br>
<p style="text-align:justify">La p&#225;gina web&#160;NUOVA, es producto de un emprendimiento del dise&#241;ador arag&#252;e&#241;o Francisco Carrillo, egresado del Instituto de Dise&#241;o Centro Gr&#225;fico de Tecnolog&#237;a. Nuova, es una tienda online exclusiva para dise&#241;adores de moda venezolanos, d&#243;nde no s&#243;lo podr&#225;n exhibir y vender sus creaciones, tambi&#233;n podr&#225;n posicionarse en el mercado como dise&#241;adores y marcas consolidadas.</p>

<p style="text-align:justify">La tienda online busca romper los esquemas y la cultura que tiene el venezolano de comprar prendas de marcas internacionales ya reconocidas sin darle ninguna importancia al talento nacional.&#160;</p>

<p style="text-align:justify">El objetivo principal de Nuova Venezuela C. A. es impulsar en el mercado nacional a los dise&#241;adores de moda venezolanos, brind&#225;ndoles una mano para que no solo su inclusi&#243;n en el mercado sea efectiva y estable con el tiempo, sino que los dise&#241;adores que ya est&#225;n en dicho mercado puedan lograr un mayor &#233;xito del que ya puedan tener. </p>

</div>