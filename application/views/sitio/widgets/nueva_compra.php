<!-- Modal -->
<div class="modal fade" id="nueva-compra" tabindex="-1" role="dialog" aria-hidden="true" data-show="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form role="form" method="post" action="<?=base_url()?>sitio/compra">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Confirmaci&oacute;n de Compra</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="id-usuario-comprador" value="0">
        <input type="hidden" name="id-usuario-vendedor" value="<?=$articulo["id_usuario"]?>">
        <input type="hidden" name="id-articulo" value="<?=$articulo["id"]?>">
        <input type="hidden" name="presentacion-tipo" value="<?=$articulo["presentacion-tipo"]?>">
        <input type="hidden" name="id-presentacion" value="0">
        <input type="hidden" name="id-talla" value="0">
        <input type="hidden" name="articulo-cantidad" value="0">
        <input type="hidden" name="articulo-precio" value="<?=$articulo["precio"]->precio?>">

        <p>Usted esta a punto de comprar lo siguiente:</p>
        <div class="well">
        <div class="row">
          <div class="col-md-3">
            <a href="#" class="thumbnail" id="imagen-articulo-pequena" style=""></a>
          </div>
          <div class="col-md-9">
            <h3 style="margin-top: 0"><?=$articulo["nombre"]?></h3>
            <p>
              <? if ($articulo["presentacion-tipo"] == "varias"): ?>
              <strong>Presentacion: </strong><span id="confirmacion-presentacion">presentacion</span><br>
              <? endif; ?>
              <strong>Talla: </strong><span id="confirmacion-talla">talla</span><br>
              <strong>Cantidad: </strong><span id="confirmacion-cantidad">cantidad</span><br>
              <strong>Precio: </strong><span id="confirmacion-precio">Bs. <?=$articulo["precio"]->precio?></span><br>
              <strong>Total: </strong><span id="confirmacion-total"></span>
            </p>
          </div>

        </div>
        </div>
        <p>Verifique todos los datos antes de hacer click en "Confirmar Compra"</p>
        <div id="datos-envio">
          <legend>Direcci&oacute;n de Env&iacute;o</legend>
          <div class="row">
            <div class="col-md-6">
            <address class="well">
              <?
                if ($this->session->userdata("logueado")):
                  $perfil = $this->Usuario_model->get_perfil($this->session->userdata("id"));
              ?>
              <?=$perfil->direccion?><br>
              <?=$perfil->ciudad?><br>
              <?=$perfil->lugar?>
            <? endif; ?>
            </address>
            </div>
          </div>
          <label class="checkbox-inline">
            <input type="checkbox" name="usar-direccion-alt" id="usar-direccion-alt" value="1"> Usar una direcci&oacute;n diferente para esta compra
          </label>
          <div id="contenedor-direccion-alt" style="display:none">
            <textarea name="direccion-alternativa" class="form-control" row="3"></textarea>
            <p class="help-block">Aqui puedes usar una direcci&oacute;n diferente para esta compra en particular.</p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-primary">Confirmar Compra</button>
      </div>
      </form>
    </div>
  </div>
</div>