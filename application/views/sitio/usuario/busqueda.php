<div class="row">
	<div class="col-md-3">
		<div class="well well-lg">
			<ul class="nav" id="nav-ordenar">
				<strong style="text-decoration:underline">Ordenar por</strong>
				<li <?=($ordenar == "destacado" || $ordenar == "") ? 'style="font-weight:bold"' : ''?>><a href="#" data-ordenar="destacado">Destacado</a></li>
				<li <?=($ordenar == "ventas" || $ordenar == "") ? 'style="font-weight:bold"' : ''?>><a href="#" data-ordenar="ventas">M&aacute;s vendido</a></li>
				<li <?=($ordenar == "nuevo" || $ordenar == "") ? 'style="font-weight:bold"' : ''?>><a href="#" data-ordenar="nuevo">Lo m&aacute;s nuevo</a></li>
			</ul>
		</div>
	</div>
	<div class="col-md-9">
		<legend class="h1 text-center">Dise&ntilde;adores</legend>
		<? if(count($disenadores) == 0): ?>
      		<div class="alert alert-warning">
      		No se encontro ningun elemento con ese par&aacute;metro de busqueda<br>
      		Por favor intente con algo diferente
      		</div>
    	<? else: ?>
		<div class="row">
		  <? foreach($disenadores as $disenador): ?>
		  <?
		  	if ($disenador->id_imagen != 0) :
		  		$url = base_url()."img/usuarios/".$disenador->id."/".$disenador->imagen->archivo;
		  	else :
		  		$url = base_url()."img/missing-img.png";
		  	endif;
		  ?>
		  <div class="col-sm-6 col-md-4">
		    <a href="<?=base_url()?>usuario/ver_perfil/<?=$disenador->id?>" class="thumbnail thumbnail-busqueda-disenador <?=($disenador->destacado == 1) ? "destacado" : "" ?>">
		      <div class="imagen-busqueda" style="background-image: url(<?=$url?>)">
			  </div>
		      <div class="caption">
		      	<cite title="Lugar"><?=$disenador->estado.", ".$disenador->pais?></cite>
		      	<h4><?=$disenador->alias?><br><small><i><?=$disenador->nombre." ".$disenador->apellido?></i></small></h4>
		      </div>
		    </a>
		  </div>
		<? endforeach; ?>
		</div>
				<? 
				$total = $disenadores[0]->total;
				$n_paginas = ceil($total/12);
				if($n_paginas >= 10):
					if ($pagina > 5 && $pagina < $n_paginas-4):
						$start = $pagina - 4;
					elseif ($pagina >= $n_paginas - 4):
						$start = $n_paginas - 8;
					else:
						$start = 1;
					endif;

					if ($pagina > 5 && $pagina < $n_paginas-4):
						$end = $pagina + 4;
					elseif ($pagina >= $n_paginas - 4):
						$end = $n_paginas;
					else:
						$end = 9;
					endif;
				else:
					$start = 1;
					$end = $n_paginas;
				endif;

				?>
			<div class="text-center">
				<ul class="pagination">
					<li class="<?= ($pagina == 1) ? "disabled" : ""?>"><a href="#" data-pagina="<?= ($pagina > 1) ? $pagina-1 : 1?>">&laquo;</a></li>
					<? if ($pagina > 5): ?>
					<li class="disabled"><a href="#">...</a></li>
					<? endif; ?>
					<? for ($i=$start; $i <= $end; $i++) : ?> 
					<li class="<?= ($pagina == $i) ? "active" : ""?>"><a href="#" data-pagina="<?=$i?>"><?=$i?></a></li>
					<? endfor;?>
					<? if ($pagina < $n_paginas-4 && $n_paginas >=10): ?>
					<li class="disabled"><a href="#">...</a></li>
					<? endif; ?>
					<li class="<?= ($pagina == $n_paginas) ? "disabled" : ""?>"><a href="#" data-pagina="<?= ($pagina < $n_paginas) ? $pagina+1 : $n_paginas?>">&raquo;</a></li>
				</ul>
				<p class="help-block"><?=$total?> Art&iacute;culos encontrados</p>
			</div>
		<? endif; ?>
	</div>
</div>