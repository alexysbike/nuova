<!DOCTYPE html>
<html>
  <head>
    <title>Nuova Venezuela</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?=base_url()?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>css/bootstrap-lightbox.min.css" rel="stylesheet">

    <link href="<?=base_url()?>css/base.css" rel="stylesheet">

    <? if(!empty($css)): foreach ($css as $cs): ?>
      <link href="<?=base_url()?>css/<?=$cs?>" rel="stylesheet">
    <? endforeach; endif; ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body style="background-color:#e6007f">

    <div class="container" style="background-color:white"><p class="text-center"><img src="<?=base_url()?>img/logo_grande.png"></p></div>

    <div class="container" style="background-color:white">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="alert alert-success">
            <p class="text-center">Haz sido seleccionad@ para participar en la <span class="label label-primary">Beta Cerrada</span> de la nueva p&aacute;gina de Nuova.</p>
            <p class="text-center">En esta <span class="label label-primary">Beta</span> podr&aacute;s crear tu usuario de Dise&ntilde;ador, crear articulos, manejar el inventario, subir imagenes, etc...</p>
            <p class="text-center">Su participaci&oacute;n nos ayudar&aacute; a mejorar la plataforma. Cualquier error detectado o oportunidad de mejora, por favor h&aacute;znosla saber por nuestros correos electr&oacute;nicos: <span class="label label-danger">sistemas@nuovavenezuela.com</span> e <span class="label label-danger">info@nuovavenezuela.com</span></p>
            <p class="text-center">Agradecidos por su colaboraci&oacute;n, puede comenzar registrando su cuenta</p>
          </div>
        </div>
      </div>
    </div>
    
    <div class="container" style="background-color:white">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <? $this->load->view("usuario/formularios/login") ?>
        </div>
      </div>
    </div>

    

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?=base_url()?>js/lib/jquery-2.0.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=base_url()?>js/lib/bootstrap.min.js"></script>
    <script src="<?=base_url()?>js/lib/bootstrap-lightbox.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/lib/jquery.form.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/config.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/base.js"></script>
    <? if(!empty($scripts)): foreach ($scripts as $js): ?>
      <script type="text/javascript" src="<?=base_url()?>js/scripts/<?=$js?>"></script>
    <? endforeach; endif;?>
  </body>
</html>