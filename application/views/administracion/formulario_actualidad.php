<legend class="h1 text-center">Noticias de Actualidad</legend>

<div id="form-container" style="display:none">
	<form role="form" method="post" action="<?=base_url()?>administracion/editar_noticia">
	  <div class="form-group">
	    <label for="titulo">T&iacute;tulo</label>
	    <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Titulo de la noticia...">
	  </div>
	  <div class="form-group">
	    <label for="cuerpo">Cuerpo</label>
	    <textarea id="cuerpo" name="cuerpo" class="form-control"></textarea>
	  </div>
	  <div class="form-group">
	  	<input type="hidden" id="tipo-accion" name="tipo-accion" value="crear">
	  	<input type="hidden" id="id-noticia" name="id-noticia" value="">
	  	<div class="row">
	  		<div class="col-md-4 col-md-offset-2"><input type="button" class="btn btn-default form-control" id="btn-cancelar" value="Cancelar"></div>
	  		<div class="col-md-4"><input type="submit" class="btn btn-primary form-control" id="guardar" name="guardar" value="Guardar"></div>
	  	</div>
	  </div>
	</form>
</div>
<p class="text-right"><a href="#" class="btn btn-success" id="btn-nueva-noticia">Nueva Noticia</a></p>
<table class="table">
	<thead>
	  <tr>
	      <th>Fecha</th>
	      <th>T&iacute;tulo</th>
	      <th>Acciones</th>
	  </tr>
	</thead>
	<tbody>
		<? foreach ($noticias as $noticia): ?>
		  <tr>
		  	<td><?=sqldate_to_datepicker($noticia->fecha)?></td>
		  	<td><?=$noticia->titulo?></td>
		  	<td>
		  		<a href="#" class="btn bt-sm btn-warning btn-editar-noticia" data-idnoticia="<?=$noticia->id?>">Editar</a>
		  		<a href="<?=base_url()?>administracion/eliminar_noticia/<?=$noticia->id?>" class="btn bt-sm btn-danger">Eliminar</a>
		  	</td>
		  </tr>	
		<? endforeach ?>
	</tbody>
</table>
