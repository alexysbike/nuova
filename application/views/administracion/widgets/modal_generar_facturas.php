<!-- Modal -->
<div class="modal fade" id="modal-generar-facturas" tabindex="-1" role="dialog" aria-hidden="true" data-show="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Detalles</h4>
      </div>
      <div class="modal-body">
        <? $i=0; foreach ($facturas as $factura): ?>
          <div class="detalles_<?=$i?> detalles" style="display:none">
            <ul class="list-group">
              <? foreach ($factura->items as $item): ?>
                <li class="list-group-item">
                  <div class="row">
                    <div class="col-md-6">
                      <strong>Articulo:</strong> <?=$item->articulo["nombre"]?><br>
                      <strong>Cantidad:</strong> <?=$item->cantidad?><br>
                      <strong>Total:</strong> <?=$item->cantidad*$item->precio?>
                    </div>
                    <div class="col-md-6">
                      <strong>Fecha Compra:</strong> <?=sqldate_to_datepicker($item->fecha_compra)?><br>
                      <strong>Precio:</strong> <?=$item->precio?> <br>
                      <strong>% Servicio:</strong> <?=round(($item->cantidad*$item->precio)*0.1, 2)?>
                    </div>
                  </div>
                  
                </li>
              <? endforeach ?>
            </ul>
          </div>
        <? $i++; endforeach; ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>