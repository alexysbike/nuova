<form action="<?=base_url()?>administracion/ventas" method="post" role="form" id="form-consultar">
	<div class="form-group">
	  <label for="fecha-desde">Desde</label>
	  <input type="text" class="form-control date" name="fecha-desde" placeholder="Fecha..." value="<?=$this->input->post("fecha-desde")?>">
	</div>
	<div class="form-group">
	  <label for="fecha-hasta">Hasta</label>
	  <input type="text" class="form-control date" name="fecha-hasta" placeholder="Fecha..." value="<?=$this->input->post("fecha-hasta")?>">
	</div>
	<button type="submit" class="btn btn-primary">Consultar</button>
</form>

<?if (isset($ventas)): ?>
<div class="panel panel-default">
      <!-- Default panel contents -->
      <div class="panel-heading">
        <div class="row">
          <div class="col-md-8">
          </div>
          <div class="col-md-4">
            <form action="#" method="get">
                <div class="input-group">
                    <!-- USE TWITTER TYPEAHEAD JSON WITH API TO SEARCH -->
                    <input class="form-control system-search" data-target="#tabla-ventas" id="system-search-ventas" name="q" placeholder="Buscar" required>
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                    </span>
                </div>
            </form>
          </div>
        </div>
      </div>

      <!-- Table -->
      <table class="table table-list-ventas" id="tabla-ventas">
          <thead>
              <tr>
                  <th>Comprador</th>
                  <th>Vendedor</th>
                  <th>Art&iacute;culo (Detalles de Venta)</th>
                  <th>Cant</th>
                  <th>Precio</th>
                  <th><abbr title="Fecha de Venta">Fech Vent</abbr></th>
                  <th>Status</th>
                  <th>Facturado</th>
              </tr>
          </thead>
          <tbody>
             <? foreach ($ventas as $venta): ?>
                <tr>
                  <td class="popover-user comprador" style="cursor:pointer" data-toggle="popover" data-content='<?$this->load->view("usuario/widgets/detalles_comprador",$venta)?>' data-html="true" data-placement="top" title="Detalles Comprador" data-trigger="hover" data-container="body"><a href="#" class="comprador" data-idcomprador="<?=$venta->comprador->id_usuario?>"><?=$venta->comprador->alias?></a></td>
                  <td class="popover-user disenador" style="cursor:pointer" data-toggle="popover" data-content='<?$this->load->view("usuario/widgets/detalles_disenador",$venta)?>' data-html="true" data-placement="top" title="Detalles Dise&ntilde;ador" data-trigger="hover" data-container="body"><a href="#" class="disenador" data-iddisenador="<?=$venta->disenador->id_usuario?>"><?=$venta->disenador->alias?></a></td>
                  <td class="popover-user" data-toggle="popover" data-content='<?$this->load->view("articulo/widgets/detalles_compra",$venta)?>' data-html="true" data-placement="top" title="Detalles de Venta" data-trigger="hover" data-container="body"><a href="<?=base_url()?>sitio/articulo/<?=$venta->id_articulo?>" target="_blank"><?=$venta->articulo["nombre"]?></a></td>
                  <td><?=$venta->cantidad?></td>
                  <td>Bs. <?=$venta->precio?></td>
                  <td><?=sqldate_to_datepicker($venta->fecha_compra)?></td>
                  <td><div class="label <?=($venta->status == 0) ? "label-info" : (($venta->status == 1) ? "label-warning" : (($venta->status <= -1) ? "label-danger" : "label-success" ))  ?>"><?=$venta->status_escrita?></div></td>
                  <td><?=($venta->facturado == 0) ? "No" : "Si"?></td>
                </tr>
              <? endforeach; ?>
          </tbody>
      </table>
</div>
<? endif; ?>