<legend class="h1 text-center">Generaci&oacute;n de Facturas</legend>

<? if (isset($facturas)): ?>
<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">
  	<div class="row">
      <div class="col-md-8">
      	<a href="<?=base_url()?>administracion/enviar_correos_por_enviar" class="btn btn-primary">Enviar Todos Los Correos</a>
      </div>
  		<div class="col-md-4">
  			<form action="#" method="get">
    		    <div class="input-group">
    		        <!-- USE TWITTER TYPEAHEAD JSON WITH API TO SEARCH -->
    		        <input class="form-control system-search" data-target="#tabla-facturas" name="q" placeholder="Buscar" required>
    		        <span class="input-group-btn">
    		            <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
    		        </span>
    		    </div>
    		</form>
    	</div>
    </div>
  </div>
  <!-- Table -->
  <table class="table table-list-facturas" id="tabla-facturas">
      <thead>
          <tr>
              <th>Usuario</th>
              <th>Piezas</th>
              <th>Monto</th>
              <th>Iva</th>
              <th>Total</th>
              <th>Acciones</th>
          </tr>
      </thead>
      <tbody>
      	  <? if (count($facturas) == 0): ?>
      	  <p class="alert alert-danger">Ninguna factura generada</p>
      	  <? endif; ?>
          <? $i=0; foreach ($facturas as $factura): ?>
          	<tr>
          		<td class="popover-user" data-togle="popover" data-content='<?$this->load->view("usuario/widgets/detalles_disenador_factura", $factura)?>' data-html="true" data-placement="top" title="Detalles Dise&ntilde;ador" data-trigger="hover" data-container="body"><a href="<?=base_url()?>usuario/ver_perfil/<?=$factura->perfil->id_usuario?>" target="_blank"><?=$factura->perfil->alias?></a></td>
          		<td><?=$factura->n_piezas?></td>
          		<td><?=$factura->factura["monto"]?></td>
          		<td><?=$factura->factura["monto"] * ($factura->factura["porc_iva"]/100)?></td>
          		<td><?=$factura->factura["monto"] * (($factura->factura["porc_iva"]/100)+1)?></td>
          		<td>
          			<a href="#" class="btn btn-warning btn-detalles" data-index="<?=$i?>">Detalles</a>
          			<a href="<?=base_url()?>administracion/eliminar_factura/<?=$factura->factura["id"]?>" class="btn btn-danger btn-eliminar">Eliminar</a>
          			<a href="<?=base_url()?>administracion/enviar_correo/<?=$factura->factura["id"]?>" class="btn btn-primary btn-enviar-correo">Enviar Correo</a>
          		</td>
          	</tr>
          <? $i++; endforeach; ?>
      </tbody>
  </table>
</div>
<? $this->load->view("administracion/widgets/modal_generar_facturas"); ?>
<? else: ?>
	<p class="alert alert-error">Error en las fechas</p>
<? endif; ?>