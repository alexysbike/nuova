<legend class="h1 text-center">Envio de Correos Masivos</legend>

<?php if (isset($enviados)): ?>
	<div class="alert alert-success">
		<?=$enviados?> correos enviados satisfactoriamente.
	</div>
<?php endif ?>

<form role="form" action="" method="post">
	<div class="form-group">
		<label class="control-label" for="destinatarios">Destinatarios</label>
		<div class="radio">
		  <label>
		    <input type="radio" name="destinatarios" id="destinatarios1" value="disenadores" checked>
		    Dise&ntilde;adores
		  </label>
		</div>
		<div class="radio">
		  <label>
		    <input type="radio" name="destinatarios" id="destinatarios2" value="compradores">
		    Compradores
		  </label>
		</div>
		<div class="radio">
		  <label>
		    <input type="radio" name="destinatarios" id="destinatarios2" value="todo">
		    Todos
		  </label>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label" for="asunto">Asunto:</label>
		<input type="text" class="form-control" id="asunto" name="asunto" placeholder="Asunto..." required>
	</div>
	<div class="form-group">
		<label class="control-label" for="mensaje">Mensaje:</label>
		<textarea class="form-control" name="mensaje" id="mensaje" placeholder="Mensaje..." required></textarea>
	</div>
	<div class="form-group">
		<button type="submit" class="btn btn-primary" name="enviar" value="enviar">Enviar</button>
	</div>
</form>