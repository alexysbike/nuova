<div class="row">
	<div class="col-md-8">	
		<h1>Dise&ntilde;adores</h1>
	</div>
	<div class="col-md-4">
		<h1><form action="#" method="get">
		    <div class="input-group">
		        <!-- USE TWITTER TYPEAHEAD JSON WITH API TO SEARCH -->
		        <input class="form-control" id="system-search" name="q" placeholder="Buscar" required>
		        <span class="input-group-btn">
		            <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
		        </span>
		    </div>
		</form></h1>
	</div>
</div>
<table class="table table-list-search">
  <thead>
      <tr>
          <th>Alias</th>
          <th>Nombre</th>
          <th>Cedula</th>
          <th>Correo</th>
          <th>Telefonos</th>
          <th>Status</th>
          <th>Acciones</th>
      </tr>
  </thead>
  <tbody>
      <? foreach ($disenadores as $disenador): ?>
      	<? if($disenador->perfil): ?>
      	<tr>
      		<td><?=$disenador->perfil->alias?></td>
      		<td><?=$disenador->perfil->nombre." ".$disenador->perfil->apellido?></td>
      		<td><?=$disenador->perfil->cedula?></td>
      		<td><?=$disenador->correo?></td>
      		<td><?=$disenador->perfil->telefono?><br><?=$disenador->perfil->celular?></td>
      		<td><?=($disenador->bloqueado == 1) ? '<span class="label label-danger">Bloqueado</span>' : '<span class="label label-success">Activo</span>' ?></td>
      		<td>
      			<? if ($disenador->bloqueado == 1): ?>
      				<a href="<?=base_url()?>administracion/activar_usuario/<?=$disenador->id?>" class="btn btn-success">Activar</a>
      			<? else: ?>
      				<a href="<?=base_url()?>administracion/bloquear_usuario/<?=$disenador->id?>" class="btn btn-danger">Bloquear</a>
      			<? endif ?>

            <? if ($disenador->perfil->destacado == 1): ?>
              <a href="<?=base_url()?>administracion/quitar_disenador_destacado/<?=$disenador->id?>" class="btn btn-danger">Quitar Destacado</a>
            <? else: ?>
              <a href="<?=base_url()?>administracion/poner_disenador_destacado/<?=$disenador->id?>" class="btn btn-success">Poner Destacado</a>
            <? endif ?>
      		</td>
      	</tr>
      	<? endif; ?>
      <? endforeach ?>
  </tbody>
</table>