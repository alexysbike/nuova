<a href="#" class="btn btn-success" id="btn-generar">Generar Factura</a> <a href="#" class="btn btn-primary" id="btn-consultar">Consulta de Facturas</a> 

<form action="<?=base_url()?>administracion/generar_facturas" method="post" role="form" id="form-generar" style="display:none">
	<div class="form-group">
	  <label for="fecha-desde">Desde</label>
	  <input type="text" class="form-control date" name="fecha-desde" placeholder="Fecha...">
	</div>
	<div class="form-group">
	  <label for="fecha-hasta">Hasta</label>
	  <input type="text" class="form-control date" name="fecha-hasta" placeholder="Fecha...">
	</div>
	<button type="submit" class="btn btn-primary">Generar</button>
</form>


<form action="<?=base_url()?>administracion/facturas" method="post" role="form" id="form-consultar" style="display:none">
	<div class="form-group">
	  <label for="fecha-desde">Desde</label>
	  <input type="text" class="form-control date" name="fecha-desde" placeholder="Fecha...">
	</div>
	<div class="form-group">
	  <label for="fecha-hasta">Hasta</label>
	  <input type="text" class="form-control date" name="fecha-hasta" placeholder="Fecha...">
	</div>
	<button type="submit" class="btn btn-primary">Consultar</button>
</form>

<? if (isset($facturas)): ?>
<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">
  	<div class="row">
      <div class="col-md-8">
      	<a href="<?=base_url()?>administracion/enviar_correos_por_enviar" class="btn btn-primary">Enviar Todos Los Correos</a>
      </div>
  		<div class="col-md-4">
  			<form action="#" method="get">
    		    <div class="input-group">
    		        <!-- USE TWITTER TYPEAHEAD JSON WITH API TO SEARCH -->
    		        <input class="form-control system-search" data-target="#tabla-facturas" name="q" placeholder="Buscar" required>
    		        <span class="input-group-btn">
    		            <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
    		        </span>
    		    </div>
    		</form>
    	</div>
    </div>
  </div>
  <!-- Table -->
  <table class="table table-list-facturas" id="tabla-facturas">
      <thead>
          <tr>
              <th>Usuario</th>
              <th>Piezas</th>
              <th>Monto</th>
              <th>Iva</th>
              <th>Total</th>
              <th>Enviado</th>
              <th>Status</th>
              <th>Acciones</th>
          </tr>
      </thead>
      <tbody>
          <? $i=0; foreach ($facturas as $factura): ?>
          	<tr>
          		<td class="popover-user" data-togle="popover" data-content='<?$this->load->view("usuario/widgets/detalles_disenador_factura", $factura)?>' data-html="true" data-placement="top" title="Detalles Dise&ntilde;ador" data-trigger="hover" data-container="body"><a href="<?=base_url()?>usuario/ver_perfil/<?=$factura->perfil->id_usuario?>" target="_blank"><?=$factura->perfil->alias?></a></td>
          		<td><?=$factura->n_piezas?></td>
          		<td><?=$factura->monto?></td>
          		<td><?=$factura->monto * ($factura->porc_iva/100)?></td>
          		<td><?=$factura->monto * (($factura->porc_iva/100)+1)?></td>
          		<td><?= ($factura->aviso_correo == 1) ? "Si" : "No" ;?></td>
              <td class="status"><?=($factura->status == 0) ? "Por Pagar" : "Pagada" ;?></td>
          		<td>
          			<a href="#" class="btn btn-warning btn-detalles" data-index="<?=$i?>">Detalles</a>
          			<a href="<?=base_url()?>administracion/eliminar_factura/<?=$factura->id?>" class="btn btn-danger btn-eliminar">Eliminar</a>
          			<a href="<?=base_url()?>administracion/enviar_correo/<?=$factura->id?>" class="btn btn-primary btn-enviar-correo">Enviar Correo</a>
                <? if ($factura->status == 0): ?>
                  <a href="<?=base_url()?>administracion/cambiar_status_factura/<?=$factura->id?>/1" class="btn btn-primary btn-marcar-pagada">Marcar Pagada</a>
                <? else: ?>
                  <a href="<?=base_url()?>administracion/cambiar_status_factura/<?=$factura->id?>/0" class="btn btn-primary btn-marcar-no-pagada">Marcar No Pagada</a>
                <? endif ?>
          		</td>
          	</tr>
          <? $i++; endforeach; ?>
      </tbody>
  </table>
</div>
<? $this->load->view("administracion/widgets/modal_ver_facturas"); ?>
<? endif; ?>