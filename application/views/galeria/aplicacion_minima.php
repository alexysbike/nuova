<div id="galeria-minima-aplicacion">
<h5><strong>Galeria de Imagenes</strong></h5>
<div id="aplicacion-minima-galeria" data-idusuario="<?=$this->session->userdata("id")?>">
	<div id="contenedor-aplicacion">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="btn-group" id="galeria-botonera">
				  <a href="#" class="btn btn-default" id="galeria-minima-boton-upload" data-estado="normal" title="Subir Imagen" data-toggle="modal" data-target="#galeria-minima-upload-modal"><span class="glyphicon glyphicon-cloud-upload"></span></a>
				  <a href="#" class="btn btn-default" id="galeria-minima-boton-refresh" data-estado="normal" title="Actualizar"><span class="glyphicon glyphicon-refresh"></span></a>
				  <a href="#" class="btn btn-default opciones-imagen" id="galeria-minima-boton-detalles-imagen" data-estado="normal" title="Detalles" disabled="disabled"><span class="glyphicon glyphicon-list-alt"></span></a>
				  <a href="#" class="btn btn-default opciones-imagen" id="galeria-minima-boton-seleccionar-imagen" data-estado="normal" title="Seleccionar" disabled="disabled"><span class="glyphicon glyphicon-arrow-left"></span></a>
				  <a href="#" class="btn btn-default" id="galeria-minima-boton-cerrar-galeria" data-estado="normal" title="Cerrar Galeria"><span class="glyphicon glyphicon-remove"></span></a>
				</div><!-- /.navbar-collapse -->
			</div>
			<div class="panel-body" id="contenedor-imagenes" style="overflow:auto; height:470px;">
				<div class="row">
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<!-- Modal -->
<div class="modal fade" id="galeria-minima-upload-modal" tabindex="-1" role="dialog" aria-labelledby="UploadImage" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <form action="<?=base_url()?>galeria/upload_image/<?=$this->session->userdata("id")?>" role="form" enctype="multipart/form-data" method="post" id="galeria-subir-imagen">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Subir Imagen</h4>
      </div>
      <div class="modal-body" style="padding-left: 50px; padding-right: 50px">
        
			<div class="form-group">
				<input type="file" class="form-control" id="galeria-imagen" name="galeria-imagen" accept="image/*" required>
			</div>
			<div class="form-group">
				<label for="galeria-imagen-nombre">Nombre:</label>
				<input type="text" name="galeria-imagen-nombre" id="galeria-imagen-nombre" class="form-control" placeholder="Unico para cada archivo...">
			</div>
			<div class="form-group">
				<label for="galeria-imagen-descripcion">Descripcion:</label>
				<textarea class="form-control" id="galeria-imagen-descripcion" name="galeria-imagen-descripcion" placeholder="Descripcion de la imagen... (Opcional)"></textarea>
			</div>
		<div id="galeria-subir-resultado"></div>
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-default" name="galeria-imagen-boton-cerrar" id="galeria-imagen-boton-cerrar" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary" name="galeria-imagen-boton-subir" id="galeria-imagen-boton-subir" id-usuario="<?=$this->session->userdata("id")?>">Subir</button>
      </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->