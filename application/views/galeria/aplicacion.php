<div id="aplicacion-galeria" data-idusuario="<?=$this->session->userdata("id")?>">
	<div class="row">
		<div id="contenedor-adicional" class="col-md-3" style="display:none">
			
		</div>
		<div id="contenedor-aplicacion" class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<nav class="navbar navbar-default" role="navigation" style="margin-bottom: 0">
					  <!-- Brand and toggle get grouped for better mobile display -->
					  <div class="navbar-header">
					    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#galeria-botonera">
					      <span class="sr-only">Toggle navigation</span>
					      <span class="icon-bar"></span>
					      <span class="icon-bar"></span>
					      <span class="icon-bar"></span>
					    </button>
					  </div>
					  <!-- Collect the nav links, forms, and other content for toggling -->
					  <div class="collapse navbar-collapse" id="galeria-botonera">
					    <a href="#" class="btn btn-default navbar-btn" id="galeria-boton-upload" data-estado="normal" title="Subir Imagen"><span class="glyphicon glyphicon-cloud-upload"></span></a>
					    <a href="#" class="btn btn-default navbar-btn" id="galeria-boton-refresh" data-estado="normal" title="Actualizar"><span class="glyphicon glyphicon-refresh"></span></a>

					    <a href="#" class="btn btn-default navbar-btn navbar-right opciones-imagen" id="galeria-boton-detalles-imagen" data-estado="normal" title="Detalles" disabled="disabled"><span class="glyphicon glyphicon-list-alt"></span></a>
					  </div><!-- /.navbar-collapse -->
					</nav>
				</div>
				<div class="panel-body" id="contenedor-imagenes">
					<div class="row">

					</div>
				</div>
			</div>
		</div>
	</div>
</div>