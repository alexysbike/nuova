<div class="panel panel-default">
	<div class="panel-heading"><h3 class="panel-title">Detalles de la Imagen</h3></div>
	<div class="panel-body">
		<a data-toggle="lightbox" href="<?=base_url()?>img/usuarios/<?=$id_usuario?>/<?=$this->input->post("archivo")?>" target="_blank" id="galeria-imagen-detalle" style="background-image:url(<?=base_url()?>img/usuarios/<?=$id_usuario?>/<?=$this->input->post("archivo")?>)" title="Ver Grande"></a>
		<h3 class="text-center"><?=$this->input->post("nombre_imagen")?></h3>
		<p><?=$this->input->post("mime")?></p>
		<p><strong>Album: </strong><?=$this->input->post("nombre_album")?></p>		
		<p><strong>Descripci&oacute;n: </strong><?=$this->input->post("descripcion")?></p>
		<p><strong>Tama&ntilde;o: </strong><?=$this->input->post("size")?> Kb</p>
	</div>
</div>
