<!-- Modal -->
<div class="modal fade" id="galeria-minima-upload-modal" tabindex="-1" role="dialog" aria-labelledby="UploadImage" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <form action="<?=base_url()?>galeria/upload_image/<?=$this->session->userdata("id")?>" role="form" enctype="multipart/form-data" method="post" id="galeria-subir-imagen">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Subir Imagen</h4>
      </div>
      <div class="modal-body" style="padding-left: 50px; padding-right: 50px">
        
			<div class="form-group">
				<input type="file" class="form-control" id="galeria-imagen" name="galeria-imagen" accept="image/*" required>
			</div>
			<div class="form-group">
				<label for="galeria-imagen-nombre">Nombre:</label>
				<input type="text" name="galeria-imagen-nombre" id="galeria-imagen-nombre" class="form-control" placeholder="Unico para cada archivo...">
			</div>
			<div class="form-group">
				<label for="galeria-imagen-descripcion">Descripcion:</label>
				<textarea class="form-control" id="galeria-imagen-descripcion" name="galeria-imagen-descripcion" placeholder="Descripcion de la imagen... (Opcional)"></textarea>
			</div>
		<div id="galeria-subir-resultado"></div>
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-default" name="galeria-imagen-boton-cerrar" id="galeria-imagen-boton-cerrar" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary" name="galeria-imagen-boton-subir" id="galeria-imagen-boton-subir" id-usuario="<?=$this->session->userdata("id")?>">Subir</button>
      </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->