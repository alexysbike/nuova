<div class="panel panel-default">
	<div class="panel-heading"><h3 class="panel-title">Subir Imagen</h3></div>
	<div class="panel-body">
		<form action="<?=base_url()?>galeria/upload_image/<?=$id_usuario?>" role="form" enctype="multipart/form-data" method="post" id="galeria-subir-imagen">
			<div class="form-group">
				<input type="file" class="form-control" id="galeria-imagen" name="galeria-imagen" accept="image/*">
			</div>
			<div class="form-group">
				<label for="galeria-imagen-album">Album:</label>
				<select class="form-control chosen-select" id="galeria-imagen-album" name="galeria-imagen-album">
					<option value="0" selected>Ninguno</option>
					<? foreach ($albums as $album): ?>
						<option value="<?=$album->id?>"><?=$album->nombre?></option>
					<? endforeach; ?>
				</select>
			</div>
			<div class="form-group">
				<label for="galeria-imagen-nombre">Nombre:</label>
				<input type="text" name="galeria-imagen-nombre" id="galeria-imagen-nombre" class="form-control" placeholder="Unico para cada archivo...">
			</div>
			<div class="form-group">
				<label for="galeria-imagen-descripcion">Descripcion:</label>
				<textarea class="form-control" id="galeria-imagen-descripcion" name="galeria-imagen-descripcion" placeholder="Descripcion de la imagen... (Opcional)"></textarea>
			</div>
			<div class="form-group text-center">
				<button type="submit" class="btn btn-primary" name="galeria-imagen-boton-subir" id="galeria-imagen-boton-subir">Subir</button>
				<button type="button" class="btn btn-default" name="galeria-imagen-boton-cerrar" id="galeria-imagen-boton-cerrar">Cerrar</button>
			</div>
		</form>
		<div id="galeria-subir-resultado"><? if($resultado != ""): ?><?=$resultado?><? endif; ?></div>
	</div>
</div>