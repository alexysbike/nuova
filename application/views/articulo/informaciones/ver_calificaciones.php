<legend>Mi Calificaci&oacute;n</legend>
<p>
	<strong>Calificacion:</strong> 
	<span class="<?=($propio->tipo == 1) ? "text-success" : (($propio->tipo == 0) ? "" : "text-danger" ) ?>">
		<? if ($propio->tipo == 1): ?>
			Positiva
		<? elseif ($propio->tipo == 0): ?>
			Neutral
		<? else: ?>
			Negativa
		<? endif ?>	
	</span>
	<br>
	<strong>Comentarios:</strong> <?=$propio->comentario?>
</p>
<legend>Calificaci&oacute;n de <?=$nopropio->usuario->alias?></legend>
<p>
	<strong>Calificacion:</strong> 
	<span class="<?=($nopropio->tipo == 1) ? "text-success" : (($nopropio->tipo == 0) ? "" : "text-danger" ) ?>">
		<? if ($nopropio->tipo == 1): ?>
			Positiva
		<? elseif ($nopropio->tipo == 0): ?>
			Neutral
		<? else: ?>
			Negativa
		<? endif ?>	
	</span>
	<br>
	<strong>Comentarios:</strong> <?=$nopropio->comentario?>
</p>