<h1 class="text-center" style="margin-bottom:120px">Creaci&oacute;n de Articulo Completado!</h1>
<div class="row">
	<div class="col-md-4">
		<a href="#" class="btn btn-lg btn-block btn-success"><span class="glyphicon glyphicon-tags"></span> Ir a Mis Art&iacute;culos</a>
	</div>
	<div class="col-md-4">
		<a href="<?=base_url()?>articulo/imagenes/<?=$id_articulo?>" class="btn btn-lg btn-block btn-success"><span class="glyphicon glyphicon-plus"></span> Agregar Imagenes</a>
	</div>
	<div class="col-md-4">
		<a href="<?=base_url()?>articulo/inventario/<?=$id_articulo?>" class="btn btn-lg btn-block btn-primary"><span class="glyphicon glyphicon-list"></span> Inventario del Art&iacute;culo</a>
	</div>
</div>
<div class="row" style="margin-top:20px">
	<div class="col-md-5 col-md-offset-1">
		<img class="img-responsive" src="<?=base_url()?>img/informaciones/activar-articulo-1.png">
	</div>
	<div class="col-md-5">
		<img class="img-responsive" src="<?=base_url()?>img/informaciones/activar-articulo-2.png">
	</div>
</div>