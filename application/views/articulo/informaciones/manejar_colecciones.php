<div class="contenedor-colecciones">
  <p class="text-info">Hacer click para vizualizar informaci&oacute;n de la colecci&oacute;n <a class="btn btn-xs btn-success pull-right" id="nueva-coleccion" title="Agregar Nueva Colecci&oacute;n"><span class="glyphicon glyphicon-plus"></span></a></p>
  <div class="panel-group" id="accordion-colecciones">
    <? $i=0; foreach ($colecciones as $coleccion): ?>
    <div class="panel panel-default">
      <div class="panel-heading" data-toggle="collapse" data-parent="#accordion-colecciones" href="#coleccion<?=$coleccion->id?>" style="cursor:pointer" data-idcoleccion="<?=$coleccion->id?>">
        <h4 class="panel-title">
            <?=$coleccion->nombre?>
        </h4>
      </div>
      <div id="coleccion<?=$coleccion->id?>" class="panel-collapse <?= ($i==0) ? "in" : "collapse"?>">
        <div class="panel-body">
          <p>
            <strong>Nombre: </strong><?=$coleccion->nombre?><br>
            <strong>Descripci&oacute;n: </strong><?=$coleccion->descripcion?><br>
            <strong>Fecha de Creaci&oacute;n: </strong><?=sqldate_to_datepicker($coleccion->fecha_creacion)?><br>
            <a href="#" class="btn btn-default editar-coleccion" title="Editar" data-idcoleccion="<?=$coleccion->id?>" data-nombre="<?=$coleccion->nombre?>" data-descripcion="<?=$coleccion->descripcion?>"><span class="glyphicon glyphicon-pencil"></span></a>
            <a href="#" class="btn btn-danger eliminar-coleccion" title="Eliminar" data-idcoleccion="<?=$coleccion->id?>"><span class="glyphicon glyphicon-remove"></span></a>
          </p>
        </div>
      </div>
    </div>
    <? $i++; endforeach; ?>
  </div>
</div>
<div class="contenedor-nueva-coleccion" style="display:none">
  <form class="form-horizontal" role="form" method="post" action="<?=base_url()?>articulo/crear_coleccion_redirect" id="registro-coleccion">
      <fieldset>
      <legend id="titulo-formulario-coleccion">Nueva Colecci&oacute;n</legend>
      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="nombre_coleccion">Nombre</label>  
        <div class="col-md-6">
        <input id="nombre_coleccion" name="nombre_coleccion" type="text" placeholder="Nombre de la colección..." class="form-control input-md" required="">
        <span id="error-nombre"></span>  
        </div>

      </div>
      
      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="descripcion_coleccion">Descripción</label>  
        <div class="col-md-6">
        <input id="descripcion_coleccion" name="descripcion_coleccion" type="text" placeholder="Descripción de la colección..." class="form-control input-md">
        <span id="error-descripcion"></span>
        </div>
      </div>

      <legend></legend>
      <input type="hidden" id="id_coleccion" name="id_coleccion" value="">
      <input type="hidden" id="tipo" name="tipo" value="">
      <div class="text-center">
        <button type="submit" id="agregar-nueva-coleccion" class="btn btn-primary">Aceptar</button>
        <button id="cancelar-nueva-coleccion" class="btn btn-default">Cancelar</button>
      </div>
      </fieldset>
    </form>
</div>