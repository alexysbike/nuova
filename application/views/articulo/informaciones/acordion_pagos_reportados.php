<? if (count($pagos)>0): ?>
<div class="panel-group" id="accordion-pagos-reportados">
  <? $k=0; foreach ($pagos  as $pago): ?>
  <div class="panel panel-default">
    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion-pagos-reportados" href="#pago<?=$pago->id?>" style="cursor:pointer">
      <h4 class="panel-title">
          Pago del <?=sqldate_to_escrita_corta($pago->fecha_pago)?> - <?=$pago->banco_emisor?>
      </h4>
    </div>
    <div id="pago<?=$pago->id?>" class="panel-collapse collapse <?= ($k == 0) ? "in" : "" ?>">
      <div class="panel-body">
        <p>
          <strong>Fecha: </strong><?=sqldate_to_escrita_corta($pago->fecha_pago)?><br>
          <strong>Desde: </strong><?=$pago->banco_emisor?><br>
          <strong>Al: </strong><?=$pago->datos_pago->banco?><br>
          <strong>Codigo de Transacci&oacute;n: </strong><?=$pago->codigo_pago?><br>
          <strong>Monto: </strong>Bs. <?=$pago->monto_total?>
        </p>
          <? if ($pago->verificado == false): ?>
            <span class="label label-danger label-verificado">Por Verificar</span>
          <? else: ?>
            <span class="label label-success label-verificado">Verificado</span>
          <? endif; ?>
        <? if ($pago->id_usuario_receptor == $this->session->userdata("id")): ?>
          <? if ($pago->verificado == false): ?>
            <button type="button" class="btn btn-success btn-xs marcar-verificado" data-idpago="<?=$pago->id?>"><span class="glyphicon glyphicon-ok"></span> Marcar como Verificado</button>
          <? else: ?>
            <button type="button" class="btn btn-danger btn-xs marcar-verificado" data-idpago="<?=$pago->id?>"><span class="glyphicon glyphicon-remove"></span> Desmarcar como Verificado</button>
          <? endif; ?>
        <? endif; ?>
      </div>
    </div>
  </div>
  <? $k++; endforeach; ?>
</div>
<p>
  <? if ($pagos[0]->id_usuario_receptor == $this->session->userdata("id")): ?>
    <? if ($compra->status <= 1 && $compra->status >=0): ?>
      <a href="<?=base_url()?>articulo/cambiar_status_compra/<?=$pagos[0]->id_compra?>/2" class="btn btn-block btn-success btn-lg" id="marcar-por-enviar">Pasar Venta al Estado <span class="label label-default"><?=$this->Articulo_model->get_status_compra_escrito(2)?></span></a>  
    <? elseif ($compra->status == 2): ?>
      <a href="<?=base_url()?>articulo/cambiar_status_compra/<?=$pagos[0]->id_compra?>/1" class="btn btn-block btn-danger btn-lg" id="marcar-por-enviar">Regresar Venta al Estado <span class="label label-warning"><?=$this->Articulo_model->get_status_compra_escrito(1)?></span></a>
    <? endif; ?>
  <? endif; ?>
</p>
<? else: ?>
  <p class="well">No existen pagos reportados...</p>
<? endif; ?>