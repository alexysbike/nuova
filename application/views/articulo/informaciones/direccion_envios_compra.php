<? if ($compra->id_usuario_vendedor == $this->session->userdata("id")): ?>
	<? if ($compra->direccion_envio_alt == ""): ?>
		<legend>Direcci&oacute;n de env&iacute;o</legend>
		<div class="row">
			<div class="col-md-6">
			<address class="well">
				<?=$perfil->direccion?><br>
				<?=$perfil->ciudad?><br>
				<?=$perfil->lugar?>
			</address>
			</div>
		</div>		
	<? else: ?>
		<legend>Direcci&oacute;n de env&iacute;o</legend>
		<div class="row">
			<div class="col-md-6">
			<address class="well">
				<?=$compra->direccion_envio_alt?>
			</address>
			</div>
		</div>
		<p class="help-block">Esta direcci&oacute;n a sido modificada por el comprador</p>
	<? endif; ?>
	<form role="form" action="<?=base_url()?>articulo/marcar_enviado/<?=$compra->id?>" method="post">
		<legend>Informaci&oacute;n sobre el Env&iacute;o</legend>
		<textarea name="observaciones-envio" class="form-control" row="3"><?=$compra->observaciones_envio?></textarea>
		<p class="help-block">Coloque en el recuadro informaci&oacute;n sobre el env&iacute;o del paquete (Numero de tracking, Compa&ntilde;&iacute;a, etc...)</p>
		<button class="btn btn-lg- btn-success"><span class="glyphicon glyphicon-ok"></span> Guardar y Marcar Venta como <span class="label label-primary"><?=$this->Articulo_model->get_status_compra_escrito(3)?></span></button>
	</form>
<? else: ?>
<legend>Direcci&oacute;n</legend>
<div class="row">
	<div class="col-md-6">
	<address class="well">
		<?=$perfil->direccion?><br>
		<?=$perfil->ciudad?><br>
		<?=$perfil->lugar?>
	</address>
	</div>
</div>
<legend>Direcci&oacute;n Alternativa</legend>
<textarea id="direccion-alternativa" class="form-control" row="3"><?=$compra->direccion_envio_alt?></textarea>
<p class="help-block">Aqui puedes usar una direcci&oacute;n diferente para esta compra en particular. De lo contrario se usar&aacute; la direcci&oacute;n de su perf&iacute;l</p>
<button id="guardar-direccion-alt" data-idcompra="<?=$compra->id?>">Guardar</button><br>
<legend>Informaci&oacute;n de Env&iacute;o</legend>
<? if ($compra->status < 3): ?>
	<p class="bg-danger">El art&iacute;culo no ha sido enviado</p>
<? else: ?>
	<p class="well"><?=$compra->observaciones_envio?></p>
<? endif; ?>
<? endif; ?>