<li class="list-group-item list-group-item-success">
	<div class="form-group">
		<textarea class="form-control" rows="2" id="pregunta" placeholder="Escribe tu pregunta aqui..."></textarea>
	</div>
	<p class="text-right"><a href="#" class="btn btn-primary" id="btn-preguntar" data-idusuario="<?=$pregunta["id_usuario_preg"]?>" data-idarticulo="<?=$pregunta["id_articulo"]?>">Preguntar</a></p>
</li>
<li class="list-group-item list-group-item-info">
	<p class="text-info"><span class="glyphicon glyphicon-question-sign"></span> <?=$pregunta["pregunta"]?> <span class="label label-info pull-right"><?=sqldate_to_datepicker($pregunta["fecha_pregunta"])?></span></p>
</li>