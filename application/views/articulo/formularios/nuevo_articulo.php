<? if (isset($articulo)): ?>
<form class="form-horizontal" role="form" method="post" action="<?=base_url()?>articulo/editar_articulo/<?=$articulo["id"]?>" id="registro-articulo" data-idusuario="<?=$this->session->userdata("id")?>">
<? else: ?>
<form class="form-horizontal" role="form" method="post" action="<?=base_url()?>articulo/crear_articulo" id="registro-articulo" data-idusuario="<?=$this->session->userdata("id")?>">
<? endif; ?>
	<!-- Form Name -->
  <h2 style="float:left; margin:0; padding:0;"><small><?=$titulo?></small></h2>
	<legend class="text-center"><h1 id="titulo-seccion">Datos Generales</h1></legend>
	<div class="row">
		<div class="col-md-8">
			<fieldset>
		  <div id="contenedor-datos">
		   <div style="min-height:350px">
			<? if (isset($articulo)): ?>
				<input type="hidden" id="id_articulo" name="id_articulo" value="<?=$articulo['id']?>">
			<? endif; ?>
			<!-- Select Basic -->
			<div class="form-group">
			  <label class="col-md-3 control-label" for="id_coleccion">Colecci&oacute;n</label>
			  <div class="col-md-9">
			    <select id="id_coleccion" name="id_coleccion" class="form-control chosen">
			      <option value="0">Ninguna</option>
			      <option value="-1">Crear Nueva Colecci&oacute;n</option>
			      <? foreach ($colecciones as $coleccion): ?>
			    	<option value="<?=$coleccion->id?>" <? if (isset($articulo)): if($articulo["id_coleccion"] == $coleccion->id): echo 'selected'; endif; endif; ?>><?=$coleccion->nombre?></option>
			      <? endforeach; ?>
			    </select>
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-3 control-label" for="nombre">Nombre</label>  
			  <div class="col-md-9">
			  <input id="nombre" name="nombre" type="text" placeholder="Nombre del Art&iacute;culo..." class="form-control input-md" required="required" value="<? if (isset($articulo)): echo $articulo['nombre']; endif;?>">
			    
			  </div>
			</div>
			
			<!-- Textarea -->
			<div class="form-group">
			  <label class="col-md-3 control-label" for="descripcion">Descripci&oacute;n</label>
			  <div class="col-md-9">                     
			    <textarea class="form-control" id="descripcion" name="descripcion"><? if (isset($articulo)): echo $articulo['descripcion']; endif;?></textarea>
			  </div>
			</div>
			
			<!-- Select Basic -->
			<div class="form-group">
			  <label class="col-md-3 control-label" for="id_categoria">Categor&iacute;a</label>
			  <div class="col-md-9">
			    <select id="id_categoria" name="id_categoria" class="form-control chosen">
			    <? foreach ($categorias as $categoria): ?>
			    	<option value="<?=$categoria->id?>" <? if (isset($articulo)): if($articulo["id_categoria"] == $categoria->id): echo 'selected'; endif; endif; ?>><?=$categoria->categoria?></option>
			    <? endforeach; ?>
			    </select>
			  </div>
			</div>

			<!-- Select Basic -->
			<div class="form-group">
			  <label class="col-md-3 control-label" for="id_categoria">Genero</label>
			  <div class="col-md-9">
			    <select id="genero" name="genero" class="form-control">
			    <? foreach (get_generos_articulo() as $index => $genero): ?>
			    	<option value="<?=$index?>" <? if (isset($articulo)): if($articulo["genero"] == $index): echo 'selected'; endif; endif; ?>><?=$genero?></option>
			    <? endforeach; ?>
			    </select>
			  </div>
			</div>
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-3 control-label" for="fecha">Fecha</label>  
			  <div class="col-md-9">
			  <input id="fecha" name="fecha" type="text" placeholder="Fecha de publicaci&oacute;n..." class="form-control input-md date" required="required" value="<? if (isset($articulo)): echo sqldate_to_datepicker($articulo['fecha']); else: echo date('d/m/Y'); endif;?>">
			  <span class="help-block">Indicar&aacute; desde que fecha estar&aacute; disponible el art&iacute;culo</span>  
			  </div>
			</div>
			
			<!-- Appended Input-->
			<div class="form-group">
			  <label class="col-md-3 control-label" for="precio">Precio</label>
			  <div class="col-md-9">
			    <div class="input-group">
			      <input id="precio" name="precio" class="form-control" placeholder="Precio del art&iacute;culo..." type="text" required="required" value="<? if (isset($articulo)): echo $articulo['precio']->precio; endif;?>">
			      <span class="input-group-addon">Bs</span>
			    </div>
			    <p class="help-block">Usar el formato ##.##</p>
			  </div>
			</div>
		   </div>
				
			<legend></legend>
			<!-- Button (Double) -->
			<div class="form-group">
			  <div class="col-md-12">
			    <a href="<?=base_url()?>usuario/cuenta" class="btn btn-default btn-lg col-md-4">Cancelar</a>
			    <button type="button" id="btn-siguiente-datos" class="btn btn-primary btn-lg col-md-4 col-md-offset-4">Siguiente</button>
			  </div>
			</div>
		  </div>
		  <div id="contenedor-tallas" style="display:none;">
			<? if (!isset($articulo)) : ?>
			<!-- Button Drop Down -->
			<div style="min-height:350px">
				<p class="text-info text-center well col-md-10 col-md-offset-1">
					Especif&iacute;que 1 o m&aacute;s tallas para el art&iacute;culo<br>
					P.ej: Unica, L, M, S, 12, 14, etc...<br>
					Use el bot&oacute;n de Agregar para adicionar m&aacute;s tallas<br>
					Tambien puede probar estas diferentes <strong>plantillas</strong> o crear sus prorias tallas: <br>
					<button class="btn btn-primary" id="plantilla-basica">B&aacute;sicas</button> 
					<button class="btn btn-warning" id="plantilla-unica">Talla &Uacute;nica</button>
					<button class="btn btn-default" id="plantilla-vacia">Vac&iacute;a</button>
				</p>
				<div class="form-group" id="inputs-tallas">
				  <div class="col-md-6 col-md-offset-3">
				    <div class="input-group">
				      <input id="talla[]" name="talla[]" class="form-control" placeholder="Ingrese tallas del art&iacute;culo..." type="text" required="required">
				      <div class="input-group-btn">
				        <button type="button" class="btn btn-default btn-eliminar-talla">
				          Eliminar
				        </button>
				      </div>
				    </div>
					
				    <a href="#" id="agregar-talla"><strong>Agregar Talla</strong></a>
				    <br>
				    <span id="tallas-error"></span>
				   </div>
			    </div>
			</div>
			<legend></legend>
			<!-- Button (Double) -->
			<div class="form-group">
			  <div class="col-md-12">
			    <button type="button" id="btn-regresar-tallas" class="btn btn-default btn-lg col-md-4">Regresar</button>
			    <button type="button" id="btn-siguiente-tallas" class="btn btn-primary btn-lg col-md-4 col-md-offset-4">Siguiente</button>
			  </div>
			</div>
			<? else: ?>
			<div style="min-height:350px">
				<p class="text-info text-center well col-md-12">
					Luego de activado el art&iacute;culo no se pueden agregar nuevas o eliminar tallas existentes.
					Solo le ser&aacute; permitido modificar el nombre.
				</p>
				<div class="form-group" id="inputs-tallas">
				  <div class="col-md-6 col-md-offset-3">
				    <? $i=1; foreach ($articulo["tallas"] as $talla): ?>
					<div class="input-group">
					  <span class="input-group-addon">Talla <?=$i?></span>
				      <input id="talla_existente[]" name="talla_existente[]" class="form-control" placeholder="Ingrese tallas del art&iacute;culo..." type="text" required="required" value="<?=$talla->talla?>">
				      <input id="id_talla[]" name="id_talla[]" type="hidden" value="<?=$talla->id?>">
			    	</div>							
					<? $i++; endforeach; ?>
				    <br>
				    <span id="tallas-error"></span>
				   </div>
			    </div>
			</div>
					

			<legend></legend>
			<!-- Button (Double) -->
			<div class="form-group">
			  <div class="col-md-12">
			    <button type="button" id="btn-regresar-tallas" class="btn btn-default btn-lg col-md-4">Regresar</button>
			    <button type="button" id="btn-siguiente-tallas" class="btn btn-primary btn-lg col-md-4 col-md-offset-4">Siguiente</button>
			  </div>
			</div>
			<? endif; ?>

		  </div>
		  <div id="contenedor-presentaciones" style="display:none">
			<? if (!isset($articulo)) : ?>
			<div style="min-height:350px">
				<p class="text-info text-center well col-md-12">
					En esta secci&oacute;n puede especificar varias presentaciones para un mismo art&iacute;culo 
					P.ej: Una misma prenda en diferentes colores<br>
					Si el art&iacute;culo tiene una sola presentaci&oacute;n marque la opci&oacute;n de "Unica".
					Si tiene varias presentaciones elija "Varias" y proceda a ingresar la informaci&oacute;n.
					Para ello puede escojer un color de icono y un nombre para cada presentaci&oacute;n<br>
				</p>
				<div class="form-group" id="inputs-presentaciones">
					<div class="col-md-10 col-md-offset-1">						
							<div class="radio">
							  <label>
							    <input type="radio" name="presentacion-tipo" value="unica" checked>
							    Unica
							  </label>
							</div>
							<div id="contenedor-presentacion-unica" style="display:none">
								<div class="row">
									<div class="col-md-4">
										<strong>Color del Icono</strong>
									</div>
									<div class="col-md-8">
										<strong>Nombre de la Presentaci&oacute;n</strong>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="input-group color-picker">
											<span class="input-group-addon"><i></i></span>
							    			<input type="text" value="#ffffff" class="form-control" disabled="disabled"/>
										</div>
									</div>
									<div class="col-md-8">
										<input type="text" class="form-control input-md" disabled="disabled" value="Unica">
									</div>
								</div>
							</div>
							<div class="radio">
							  <label>
							    <input type="radio" name="presentacion-tipo" value="varias">
							    Varias
							  </label>
							</div>
							<div id="contenedor-presentaciones-varias" style="display:none">
								<div class="row">
									<div class="col-md-4">
										<strong>Color del Icono</strong>
									</div>
									<div class="col-md-8">
										<strong>Nombre de la Presentaci&oacute;n</strong>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="input-group color-picker">
											<span class="input-group-addon"><i></i></span>
							    			<input type="text" value="#ffffff" class="form-control" name="presentacion-color[]"/>
										</div>
									</div>
									<div class="col-md-8">
										<div class="input-group">
											<input type="text" class="form-control input-md" name="presentacion-nombre[]" value="" placeholder="Nombre de la Presentaci&oacute;n...">
											<div class="input-group-btn">
										        <button type="button" class="btn btn-default btn-eliminar-presentacion">
										          Eliminar
										        </button>
										      </div>
										</div>
									</div>
								</div>
								<a href="#" id="agregar-presentacion">Agregar Presentaci&oacute;n</a>
								<br><span id="presentaciones-error"></span>
							</div>

					</div>
				</div>
			</div>
					<? else: ?>
				<div style="min-height:350px">
				<p class="text-info text-center well col-md-12">
					Luego de activado el art&iacute;culo no se pueden agregar nuevas o eliminar presentaciones existentes.
					Solo le ser&aacute; permitido modificar el nombre y el color del icono de las mismas.
				</p>
				<div class="form-group" id="inputs-presentaciones">
					<div class="col-md-10 col-md-offset-1">						
							<? if($articulo["presentacion-tipo"] == "unica"): ?>
								<div class="well">Presentaci&oacute;n &Uacute;nica</div>
							<? else: ?>
								<div id="contenedor-presentaciones-varias">
									<div class="row">
										<div class="col-md-4">
											<strong>Color del Icono</strong>
										</div>
										<div class="col-md-8">
											<strong>Nombre de la Presentaci&oacute;n</strong>
										</div>
									</div>
									<? foreach ($articulo["presentaciones"] as $presentacion) : ?>
									<div class="row">
										<input id="id_presentacion[]" name="id_presentacion[]" type="hidden" value="<?=$presentacion["id"]?>">
										<div class="col-md-4">
											<div class="input-group color-picker">
												<span class="input-group-addon"><i></i></span>
								    			<input type="text" value="<?=$presentacion["color_icono"]?>" class="form-control" name="presentacion-color[]"/>
											</div>
										</div>
										<div class="col-md-8">
											<div class="input-group">
												<input type="text" class="form-control input-md" name="presentacion-nombre[]" value="<?=$presentacion["nombre"]?>" placeholder="Nombre de la Presentaci&oacute;n...">
												<span class="input-group-addon"></span>
											</div>
										</div>
									</div>
									<? endforeach; ?>
									<br><span id="presentaciones-error"></span>
								</div>
							<? endif; ?>

					</div>
				</div>
			</div>
					<? endif; ?>
			<legend></legend>
			<!-- Button (Double) -->
			<div class="form-group">
			  <div class="col-md-12">
			    <button type="button" id="btn-regresar-presentaciones" name="btn-regresar-presentaciones" class="btn btn-default btn-lg col-md-4">Regresar</button>
			    <button id="btn-crear-articulo" name="btn-crear-articulo" class="btn btn-primary btn-lg col-md-4 col-md-offset-4" value="crear"><?=(isset($articulo)) ? "Editar" : "Crear"?></button>
			  </div>
			</div>
		  </div>
			</fieldset>
			</div>
			<div class="col-md-4">
				<img class="img-responsive" src="<?=base_url()?>img/informaciones/nuevo-articulo-datos-generales.png" id="datos">
				<img class="img-responsive" src="<?=base_url()?>img/informaciones/nuevo-articulo-tallas.png" id="tallas" style="display:none">
				<img class="img-responsive" src="<?=base_url()?>img/informaciones/nuevo-articulo-presentaciones.png" id="presentaciones" style="display:none">
			</div>
		</div>
		<fieldset>
			
		</fieldset>
	</form>