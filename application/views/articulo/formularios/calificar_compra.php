<div class="row">
	<div class="col-md-6">
		<? $this->load->view("articulo/widgets/detalles_compra", $compra); ?>
	</div>
	<div class="col-md-6">
		<? $this->load->view("usuario/widgets/detalles_vendedor_minimal", $vendedor); ?>
	</div>
</div>
<form role="form" class="form-horizontal" method="post" action="<?=base_url()?>articulo/calificar_compra" id="calificar-compra">
	<fieldset>
		<legend></legend>

		<div class="form-group">
			<label for="tipo" class="control-label col-md-4">Recibi&oacute; el art&iacute;culo:</label>
			<div class="col-md-7">
				<div class="radio-inline">
				  <label class="text-success">
				    <input type="radio" name="recibido" id="recibido-calificacion3" value="1">
				    Si
				  </label>
				</div>
				<div class="radio-inline">
				  <label class="text-danger">
				    <input type="radio" name="recibido" id="recibido-calificacion1" value="0">
				    No
				  </label>
				</div>
				<br>
				<span id="error-tipo"></span>
			</div>
		</div>

		<div class="form-group">
			<label for="tipo" class="control-label col-md-4">Calificaci&oacute;n</label>
			<div class="col-md-7">
				<div class="radio">
				  <label class="text-success">
				    <input type="radio" name="tipo" id="tipo-calificacion3" value="1">
				    Positiva
				  </label>
				</div>
				<div class="radio">
				  <label>
				    <input type="radio" name="tipo" id="tipo-calificacion2" value="0">
				    Neutral
				  </label>
				</div>
				<div class="radio">
				  <label class="text-danger">
				    <input type="radio" name="tipo" id="tipo-calificacion1" value="-1">
				    Negativa
				  </label>
				</div>
				<span id="error-tipo"></span>
			</div>
		</div>

		<div class="form-group">
			<label for="comentario" class="control-label col-md-4">Comentario</label>
			<div class="col-md-7">
				<textarea class="form-control" rows="3" name="comentario" id="comentario-calificacion"></textarea>
				<span id="error-comentario"></span>
			</div>
		</div>

		<input type="hidden" name="id-usuario-vendedor" value="<?=$this->input->post("id_usuario_vendedor")?>">
		<input type="hidden" name="id-compra" value="<?=$this->input->post("id_compra")?>">

		<legend></legend>

		<div class="col-md-4 col-md-offset-2">
			<button type="button" class="btn btn-default btn-block btn-lg" data-dismiss="modal">Cerrar</button>
		</div>
		<div class="col-md-4">
        	<button type="submit" class="btn btn-primary btn-block btn-lg btn-calificar" name="calificar">Calificar</button>
        </div>
	</fieldset>
</form>