<form class="form-horizontal" role="form" method="post" action="<?=base_url()?>articulo/crear_articulo" id="registro-articulo" data-idusuario="<?=$this->session->userdata("id")?>">
	<!-- Form Name -->
	<legend class="text-center"><h1>Nuevo Art&iacute;culo</h1></legend>
	<div class="row">
		<div class="col-md-8">
			<fieldset>
			<? if (isset($articulo)): ?>
				<input type="hidden" id="id_articulo" name="id_articulo" value="<?=$articulo['id']?>">
			<? endif; ?>
			<!-- Select Basic -->
			<div class="form-group">
			  <label class="col-md-3 control-label" for="id_coleccion">Colecci&oacute;n</label>
			  <div class="col-md-8">
			    <select id="id_coleccion" name="id_coleccion" class="form-control chosen">
			      <option value="0">Ninguna</option>
			      <option value="-1">Crear Nueva Colecci&oacute;n</option>
			      <? foreach ($colecciones as $coleccion): ?>
			    	<option value="<?=$coleccion->id?>" <? if (isset($articulo)): if($articulo["id_coleccion"] == $coleccion->id): echo 'selected'; endif; endif; ?>><?=$coleccion->nombre?></option>
			      <? endforeach; ?>
			    </select>
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-3 control-label" for="nombre">Nombre</label>  
			  <div class="col-md-8">
			  <input id="nombre" name="nombre" type="text" placeholder="Nombre del Art&iacute;culo..." class="form-control input-md" required="required" value="<? if (isset($articulo)): echo $articulo['nombre']; endif;?>">
			    
			  </div>
			</div>
			
			<!-- Textarea -->
			<div class="form-group">
			  <label class="col-md-3 control-label" for="descripcion">Descripci&oacute;n</label>
			  <div class="col-md-8">                     
			    <textarea class="form-control" id="descripcion" name="descripcion"><? if (isset($articulo)): echo $articulo['nombre']; endif;?></textarea>
			  </div>
			</div>
			
			<!-- Select Basic -->
			<div class="form-group">
			  <label class="col-md-3 control-label" for="id_categoria">Categor&iacute;a</label>
			  <div class="col-md-8">
			    <select id="id_categoria" name="id_categoria" class="form-control chosen">
			    <? foreach ($categorias as $categoria): ?>
			    	<option value="<?=$categoria->id?>" <? if (isset($articulo)): if($articulo["id_categoria"] == $categoria->id): echo 'selected'; endif; endif; ?>><?=$categoria->categoria?></option>
			    <? endforeach; ?>
			    </select>
			  </div>
			</div>
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-3 control-label" for="fecha">Fecha</label>  
			  <div class="col-md-8">
			  <input id="fecha" name="fecha" type="text" placeholder="Fecha de publicaci&oacute;n..." class="form-control input-md date" required="required" value="<? if (isset($articulo)): echo $articulo['fecha']; endif;?>">
			  <span class="help-block">Indicar&aacute; desde que fecha estar&aacute; disponible el art&iacute;culo</span>  
			  </div>
			</div>
			
			<!-- Appended Input-->
			<div class="form-group">
			  <label class="col-md-3 control-label" for="precio">Precio</label>
			  <div class="col-md-8">
			    <div class="input-group">
			      <input id="precio" name="precio" class="form-control" placeholder="Precio del art&iacute;culo..." type="text" required="required" value="<? if (isset($articulo)): echo $articulo['precio']->precio; endif;?>">
			      <span class="input-group-addon">Bs</span>
			    </div>
			    <p class="help-block">Usar el formato ##.##</p>
			  </div>
			</div>
			<!-- Button Drop Down -->
			<div class="form-group">
			  <label class="col-md-3 control-label" for="talla[]">Tallas</label>
			  <div class="col-md-8">
			  	<? if (!isset($articulo)) : ?>
			    <div class="input-group">
			      <input id="talla[]" name="talla[]" class="form-control" placeholder="Ingrese tallas del art&iacute;culo..." type="text" required="required">
			      <div class="input-group-btn">
			        <button type="button" class="btn btn-default">
			          Eliminar
			        </button>
			      </div>
			    </div>
				<? else: ?>
					<? foreach ($articulo["tallas"] as $talla): ?>
					<div class="input-group">
				      <input id="talla_existente[]" name="talla_existente[]" class="form-control" placeholder="Ingrese tallas del art&iacute;culo..." type="text" required="required" value="<?=$talla->talla?>">
				      <input id="id_talla[]" name="id_talla[]" type="hidden" value="<?=$talla->id?>">
			    	</div>							
					<? endforeach; ?>
				<? endif; ?>
			    <a href="#" id="agregar-talla">Agregar Talla</a>
			  </div>
			</div>
			
			</fieldset>
			</div>
			<div class="col-md-4">
				Informacion o cualquier cosa
			</div>
		</div>

		<div class="row">
			<legend class="text-center"><h3>Presentaciones</h3></legend>
			<div class="col-md-8" id="presentaciones-parte-izquierda">
				<fieldset>
				

				<ul id="presentaciones-tipo" class="nav nav-pills nav-justified">
	             <li class="<? if(isset($articulo)): if($articulo['presentacion-tipo'] == "unica"): echo "active"; endif; else: echo "active"; endif;?>"><a href="#presentaciones-unica" data-toggle="tab" data-tipo="unica">Unica</a></li>
	             <li class="<? if(isset($articulo)): if($articulo['presentacion-tipo'] != "unica"): echo "active"; endif; endif;?>"><a href="#presentaciones-varias" data-toggle="tab" data-tipo="varias">Varias</a></li>
	           </ul>
	           <input type="hidden" name="presentacion-tipo" id="presentacion-tipo" value="unica">
	           <div class="tab-content">
	             <div class="tab-pane <? if(isset($articulo)): if($articulo['presentacion-tipo'] == "unica"): echo "active"; endif; else: echo "active"; endif;?>" id="presentaciones-unica">
	             <div class="panel panel-default">
	             <div class="panel-body">
	             	<div class="form-group">
					  <label class="col-md-3 control-label">Nombre</label>  
					  <div class="col-md-8">
					  	<input type="text" class="form-control input-md" disabled="disabled" value="Unica">
					  </div>
					</div>
	             	<div class="form-group">
	            		<label class="col-md-3 control-label">Color del Icono</label>
			  			<div class="col-md-8">
	             			<div class="input-group color-picker">
				    			<input type="text" value="#ffffff" class="form-control" disabled="disabled" />
	    						<span class="input-group-addon"><i></i></span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div id="contenedor-imagenes-unica" class="col-md-10 col-md-offset-1 contenedor-imagenes contenedor-imagenes-activo">
							<input type="hidden" name="id_imagenes_unica" id="id_imagenes_unica" class="selected-images" value="">
							<div class="panel panel-default">
								<div class="panel-heading">
									<strong>Imagenes</strong>
									<div class="btn-group">
									  <a href="#" class="btn btn-default galeria-boton-agregar-imagen" id="" data-estado="normal" title="Agregar Imagenes"><span class="glyphicon glyphicon-plus"></span></a>
									  <a href="#" class="btn btn-default opciones-imagen" id="galeria-boton-eliminar-imagen" data-estado="normal" title="Eliminar Imagenes" disabled="disabled"><span class="glyphicon glyphicon-minus"></span></a>
									  <a href="#" class="btn btn-default opciones-imagen" id="galeria-boton-detalles-imagen" data-estado="normal" title="Detalles" disabled="disabled"><span class="glyphicon glyphicon-list-alt"></span></a>
									</div>
								</div>
								<div class="panel-body" style="overflow:auto; height:300px;">
									<div class="row">
										<? if (isset($articulo)): ?>
											<? if ($articulo["presentacion-tipo"] == "unica"): ?>
												<? foreach ($articulo["imagenes"] as $imagen): ?>
													<div class="col-sm-6 col-md-3"><a id="<?=$imagen->id?>" href="#" class="thumbnail galeria-thumbnail" data-index="2"><div class="galeria-imagen" style="background-image:url(http://localhost/nuova/img/usuarios/<?=$this->session->userdata("id")?>/<?=$imagen->archivo?>)"></div></a></div>	
												<? endforeach; ?>
											<? endif; ?>
										<? endif; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				 </div>
				 </div>
	             </div>
	             <div class="tab-pane <? if(isset($articulo)): if($articulo['presentacion-tipo'] != "unica"): echo "active"; endif; endif;?>" id="presentaciones-varias">
	             	<!-- Nav tabs -->
					<ul id="presentaciones-varias-navs" class="nav nav-tabs">
					  <? if(isset($articulo)) : 
					  		if($articulo['presentacion-tipo'] != "unica"):
					  			$i=1; ?>
								<? foreach ($articulo["presentaciones"] as $presentacion): ?>
									<li class="<?=($i==1) ? "active" : "" ;?>"><a href="#presentacion-<?=$i?>" data-toggle="tab"><?=$i?></a></li>
									<? $i++; ?>
								<? endforeach; ?>
					     <? endif; 
					     else: ?>
						 <li class="active"><a href="#presentacion-1" data-toggle="tab">1</a></li>
					  <? endif;?>
					  <li><a href="#" id="btn-agregar-presentacion"><span class="glyphicon glyphicon-plus"></span></a></li>
					</ul>
					
					<!-- Tab panes -->
					<div class="tab-content">
					  <? if(!isset($articulo)): ?>
					  <div class="tab-pane active" id="presentacion-1">

						<div class="panel panel-default">
	            		 <div class="panel-body">
	            		 	<a href="#" class="btn btn-danger btn-xs btn-eliminar-presentacion"><span class="glyphicon glyphicon-minus"></span> Eliminar Presentaci&oacute;n</a>
	            		 	<div class="form-group">
							  <label class="col-md-3 control-label">Nombre</label>  
							  <div class="col-md-8">
							  	<input type="text" name="presentacion-nombre[]" class="form-control input-md" placeholder="Nombre de la Presentaci&oacute;n...">
							  </div>
							</div>
	            		 	<div class="form-group">
	            				<label class="col-md-3 control-label">Color del Icono</label>
			  					<div class="col-md-8">
	            		 			<div class="input-group color-picker">
						    			<input type="text" value="#ffffff" class="form-control" name="presentacion-color[]" />
	    								<span class="input-group-addon"><i></i></span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div id="contenedor-imagenes-1" class="col-md-10 col-md-offset-1 contenedor-imagenes">
									<input type="hidden" name="id_imagenes[]" class="selected-images" value="">
									<div class="panel panel-default">
										<div class="panel-heading">
											<strong>Imagenes</strong>
											<div class="btn-group">
											  <a href="#" class="btn btn-default galeria-boton-agregar-imagen" id="" data-estado="normal" title="Agregar Imagenes"><span class="glyphicon glyphicon-plus"></span></a>
											  <a href="#" class="btn btn-default opciones-imagen" id="galeria-boton-eliminar-imagen" data-estado="normal" title="Eliminar Imagenes" disabled="disabled"><span class="glyphicon glyphicon-minus"></span></a>
											  <a href="#" class="btn btn-default opciones-imagen" id="galeria-boton-detalles-imagen" data-estado="normal" title="Detalles" disabled="disabled"><span class="glyphicon glyphicon-list-alt"></span></a>
											</div>
										</div>
										<div class="panel-body" style="overflow:auto; height:300px;">
											<div class="row"></div>
										</div>
									</div>
								</div>
							</div>
						 </div>
						</div>





					  </div>
					<? else: $i = 1;?>
					<? foreach ($articulo["presentaciones"] as $presentacion): ?>
					<div class="tab-pane <?=($i == 1) ? "active" : "" ?>" id="presentacion-<?=$i?>">
					  <div class="panel panel-default">
	            		 <div class="panel-body">
	            		 </div>
	            	  </div>
					</div>	
					<? endforeach; ?>
					<? endif; ?>
					</div>
	             </div>
	           </div>

				</fieldset>
			</div>
			<div class="col-md-4" id="presentaciones-parte-derecha">
				<div id="contenedor-aplicacion-galeria-adicional" style="display:none"></div>
			</div>
		</div>
		<fieldset>
			<legend></legend>
			<!-- Button (Double) -->
			<div class="form-group">
			  <div class="col-md-12">
			    <button id="btn-cancelar-articulo" name="btn-cancelar-articulo" class="btn btn-default btn-lg col-md-2 col-md-offset-2">Cancelar</button>
			    <button id="btn-crear-articulo" name="btn-crear-articulo" class="btn btn-primary btn-lg col-md-2 col-md-offset-1" value="crear">Crear</button>
			  </div>
			</div>
		</fieldset>
	</form>