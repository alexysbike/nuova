<div class="row">
	<div class="col-md-6">
		<? $this->load->view("articulo/widgets/detalles_compra", $compra); ?>
	</div>
	<div class="col-md-6">
		<? $this->load->view("usuario/widgets/detalles_comprador", $comprador); ?>
	</div>
</div>
<legend>Calificaci&oacute;n del Comprador</legend>
<p>
	<strong>Calificacion:</strong> 
	<span class="<?=($calificacion->tipo == 1) ? "text-success" : (($calificacion->tipo == 0) ? "" : "text-danger" ) ?>">
		<? if ($calificacion->tipo == 1): ?>
			Positiva
		<? elseif ($calificacion->tipo == 0): ?>
			Neutral
		<? else: ?>
			Negativa
		<? endif ?>	
	</span>
	<br>
	<strong>Comentarios:</strong> <?=$calificacion->comentario?>
</p>
<form role="form" class="form-horizontal" method="post" action="<?=base_url()?>articulo/calificar_venta" id="calificar-venta">
	<fieldset>
		<legend></legend>

		<div class="form-group">
			<label for="tipo" class="control-label col-md-4">Calificaci&oacute;n</label>
			<div class="col-md-7">
				<div class="radio">
				  <label class="text-success">
				    <input type="radio" name="tipo" id="tipo-calificacion3" value="1">
				    Positiva
				  </label>
				</div>
				<div class="radio">
				  <label>
				    <input type="radio" name="tipo" id="tipo-calificacion2" value="0">
				    Neutral
				  </label>
				</div>
				<div class="radio">
				  <label class="text-danger">
				    <input type="radio" name="tipo" id="tipo-calificacion1" value="-1">
				    Negativa
				  </label>
				</div>
				<span id="error-tipo"></span>
			</div>
		</div>

		<div class="form-group">
			<label for="comentario" class="control-label col-md-4">Comentario</label>
			<div class="col-md-7">
				<textarea class="form-control" rows="3" name="comentario" id="comentario-calificacion"></textarea>
				<span id="error-comentario"></span>
			</div>
		</div>

		<input type="hidden" name="id-usuario-comprador" value="<?=$this->input->post("id_usuario_comprador")?>">
		<input type="hidden" name="id-compra" value="<?=$this->input->post("id_compra")?>">

		<legend></legend>

		<div class="col-md-4 col-md-offset-2">
			<button type="button" class="btn btn-default btn-block btn-lg" data-dismiss="modal">Cerrar</button>
		</div>
		<div class="col-md-4">
        	<button type="submit" class="btn btn-primary btn-block btn-lg btn-calificar" name="calificar">Calificar</button>
        </div>
	</fieldset>
</form>