<!-- Modal -->
<div class="modal fade" id="modal-envios-compra" tabindex="-1" role="dialog" aria-hidden="true" data-show="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Env&iacute;os</h4>
      </div>
      <div class="modal-body">
        <div id="direccion-envio"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>