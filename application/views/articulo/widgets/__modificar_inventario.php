<form class="form-horizontal" role="form" method="post" action="<?=base_url()?>articulo/operacion_inventario/<?=$inventario->id?>/<?=$inventario->id_articulo?>">
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
  <h4 class="modal-title" id="myModalLabel">Modificar Inventario</h4>
</div>
<div class="modal-body" style="padding: 20px 50px">
  <p><strong>Presentaci&oacute;n:</strong> <?=$inventario->presentacion?></p>
  <p><strong>Talla:</strong> <?=$inventario->talla?></p>
  <div class="form-group">
    <label for="cantidad">Cantidad</label>
    <input class="form-control" placeholder="nro de piezas..." value="0" type="number" name="cantidad" min="-<?=$inventario->cantidad?>">
    <p class="help-block">Nota: Use n&uacute;meros negativos para eliminar piezas</p>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
  <button type="submit" class="btn btn-primary">Continuar</button>
</div>
</form>