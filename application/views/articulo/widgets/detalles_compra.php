<? if ($articulo["presentacion-tipo"] == "varias") :
        $imagen = $presentacion->imagenes;
else:
        $imagen = $imagenes;
endif;?>
<div class="media">
  <a href="#" url-img="<?=base_url()?>img/usuarios/<?=$articulo["id_usuario"]?>/<?=$imagen[0]->archivo?>" class="thumbnail imagen-perfil-pequena pull-left" style="background-image:url(<?=base_url()?>img/usuarios/<?=$articulo["id_usuario"]?>/<?=$imagen[0]->archivo?>)" data-target="#lightbox"></a>
  <div class="media-body">
    <h3 class="media-heading"><?=$articulo["nombre"]?></h3>
    <h4><small>Colecci&oacute;n <?=$articulo["coleccion"]?></small></h4>
  </div>
</div>
<p>
  <? if ($articulo["presentacion-tipo"] == "varias"): ?>
  <strong>Presentacion: </strong><span id="confirmacion-presentacion"><?=$presentacion->nombre?></span><br>
  <? endif; ?>
  <strong>Talla: </strong><span id="confirmacion-talla"><?=$talla->talla?></span><br>
  <strong>Cantidad: </strong><span id="confirmacion-cantidad"><?=$cantidad?> Unidades</span><br>
  <strong>Precio: </strong>Bs. <?=$precio?><br>
  <strong>Total: </strong>Bs. <?=$precio*$cantidad?>
</p>