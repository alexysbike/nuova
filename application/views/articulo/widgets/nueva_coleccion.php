<div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Nueva Colecci&oacute;n</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form" method="post" action="<?=base_url()?>articulo/crear_coleccion" id="registro-coleccion">
			<fieldset>
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="nombre_coleccion">Nombre</label>  
			  <div class="col-md-6">
			  <input id="nombre_coleccion" name="nombre_coleccion" type="text" placeholder="Nombre de la colección..." class="form-control input-md" required="">
			    
			  </div>
			</div>
			
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="descripcion_coleccion">Descripción</label>  
			  <div class="col-md-6">
			  <input id="descripcion_coleccion" name="descripcion_coleccion" type="text" placeholder="Descripción de la colección..." class="form-control input-md">
			    
			  </div>
			</div>
			
			</fieldset>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" id="wcb-cerrar-modal-nueva-coleccion" data-trigger="<?=$data_trigger?>" class="btn btn-default">Cancelar</button>
        <button type="button" id="wcb-crear-coleccion" data-trigger="<?=$data_trigger?>" class="btn btn-primary">Crear</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->