<form class="form-horizontal" role="form" method="post" action="<?=base_url()?>articulo/operacion_inventario/<?=$this->input->post("id_articulo")?>/<?=$this->input->post("id_presentacion")?>">
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
  <h4 class="modal-title" id="myModalLabel">Modificar Inventario</h4>
</div>
<div class="modal-body" style="padding: 20px 50px">
  <h3 class="text-center"><strong>Presentaci&oacute;n:</strong> <i class="presentacion-square-inline img-thumbnail" style="background-color: <?=$presentacion->color_icono?>;"></i> <?=$presentacion->nombre?></h3>
  <div class="well">
  <? foreach ($tallas as $talla): ?>
    <div class="form-group">
      <label for="cantidad" class="control-label col-sm-6"><strong>Talla:</strong> <?=$talla->talla?> - Cantidad:</label>
      <input type="hidden" name="id_inventario[]" value="<?=$talla->inventario->id?>">
      <div class="col-sm-3">
        <input class="form-control" placeholder="nro de piezas..." value="0" type="number" name="cantidad[]" <?=(date("Y-m-d") > date("Y-m-d", strtotime($talla->inventario->fecha_activacion." + 12 day")))? 'min="-'.$talla->inventario->cantidad.'"' : 'min="0"' ?>>   
      </div>
    </div>
  <? endforeach; ?>
  </div>
  <p class="help-block">Nota: Use n&uacute;meros negativos para eliminar piezas</p>
  <? if (date("Y-m-d") <= date("Y-m-d", strtotime($talla->inventario->fecha_activacion." + 12 day"))): ?>
    <p class="help-block">Debes de esperar un minimo de 12 dias luego de activado el art&iacute;culo para eliminar piezas</p>
  <? endif ?>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
  <button type="submit" class="btn btn-primary">Continuar</button>
</div>
</form>