<legend class="text-center"><h1>Detalles del Art&iacute;culo</h1></legend>
<div class="row">
  <div class="col-md-4">
  	<div class="row">
        <div class="col-sm-6 col-md-4">
            <img src="http://placehold.it/380x500" alt="" class="img-rounded img-responsive" />
        </div>
        <div class="col-sm-6 col-md-8">
        	<? if ($articulo["id_coleccion"] != 0): ?>
        		<h6><cite title="Colecci&oacute;n">Colecci&oacute;n <?=$articulo["coleccion"]?></cite></h6>
        	<? endif ?>
            <h4><?=$articulo["nombre"]?></h4>
            <small><cite title="Categor&iacute;a"><?=$articulo["categoria"]?> <span class="glyphicon glyphicon-tag">
            </span></cite></small>
            <p>
            	<span class="glyphicon glyphicon-calendar"></span> <?=sqldate_to_escrita($articulo["fecha"])?>
                <br />
                <span class="label <?=($articulo["status"] == 0) ? "label-warning" : (($articulo["status"] == 1) ? "label-success" : "label-danger")  ?>"><?=$articulo["status_escrito"]?></span>
                <br />
                <span class="glyphicon glyphicon-usd"></span> <?=$articulo["precio"]->precio?> BsF
                <br />
                <span class="glyphicon glyphicon-list"></span> Inventario -> <?=$articulo["total_piezas"]?> Piezas
            </p>
            <!-- Split button -->
            <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    Opciones <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Editar Articulo</a></li>
                </ul>
            </div>
        </div>
    </div>
  </div>
  <div class="col-md-8">
  	<div class="panel panel-info">
  		<div class="panel-heading">Presentaciones</div>
	    <!-- Nav tabs -->
		<ul class="nav nav-tabs">
		  <? if ($articulo["presentacion-tipo"] == "unica"): ?>
		  	<li class="active"><a href="#presentacion-unica" data-toggle="tab"><i class="presentacion-square img-thumbnail" style="background-color: #fff;"></i></a></li>
		  <? else: ?>
		  	<? foreach ($articulo["presentaciones"] as $index => $presentacion): ?>
		  		<li class="<?=($index==0) ? "active" : "" ?>"><a href="#presentacion-<?=$presentacion["id"]?>" data-toggle="tab"><i class="presentacion-square img-thumbnail" style="background-color: <?=$presentacion["color_icono"]?>;"></i></a></li>
		  	<? endforeach ?>
		  <? endif ?>
		</ul>
		
		<!-- Tab panes -->
		<div class="tab-content">
		  <? if ($articulo["presentacion-tipo"] == "unica"): ?>
		  	<div class="tab-pane active" id="presentacion-unica" style="padding:0 10px 5px 10px">
		  		<h2>UNICA</h2>
          <? if ($articulo["status"] != 0): ?>
		  		<h3 class="text-center">Inventario <small class="pull-right" style="padding-top:15px">N Piezas</small></h3>
		  		<ul class="list-group">
		  			<? foreach ($articulo["inventario"] as $id_presentacion => $talla): ?>
		  				<? foreach ($talla as $id_talla => $inv): ?>
		  					<li class="list-group-item talla-inventario-detalles">Talla <?=$this->Articulo_model->get_talla($id_talla)->talla?> <a class="opcion-agregar" href="" style="display:none" id-inventario="<?=$inv->id?>"><span class="label label-info">Agregar/Eliminar Piezas</span></a> <span class="badge"><?=$inv->cantidad?></span></li>
		  				<? endforeach ?>
		  			<? endforeach ?>
		  		</ul>
          <? else: ?>
            <h3 class="text-center">Esperando Activaci&oacute;n</h3>
          <? endif ;?>
		  	</div>
		  <? else: ?>
		  	<? foreach ($articulo["presentaciones"] as $index => $presentacion): ?>
		  		<div class="tab-pane <?=($index==0) ? "active" : "" ?>" id="presentacion-<?=$presentacion["id"]?>" style="padding:0 10px 5px 10px">
		  			<h2><?=$presentacion["nombre"]?></h2>
		  			<? if ($articulo["status"] != 0): ?>
		  			<h3 class="text-center">Inventario <small class="pull-right" style="padding-top:15px">N Piezas</small></h3>
		  			<ul class="list-group">
		  			<? foreach ($articulo["inventario"][$presentacion["id"]] as $id_talla => $inv): ?>
		  				<li class="list-group-item talla-inventario-detalles">Talla <?=$this->Articulo_model->get_talla($id_talla)->talla?> <a class="opcion-agregar" href="" style="display:none" id-inventario="<?=$inv->id?>"><span class="label label-info">Agregar/Eliminar Piezas</span></a> <span class="badge"><?=$inv->cantidad?></span></li>
		  			<? endforeach ?>
		  			<? else: ?>
		  			<h3 class="text-center">Esperando Activaci&oacute;n</h3>
		  			<? endif ;?>
		  		</ul>	
		  		</div>
		  	<? endforeach; ?>
		  <? endif; ?>
		</div>
		<div class="panel-footer">*Nota: Posiciona el puntero sobre las tallas para ver mas opciones</div>
	</div> 	
  </div>
</div>
<? if ($articulo["status"] == 0): ?>
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<a href="<?=base_url()?>articulo/primera_activacion/<?=$articulo["id"]?>" class="btn btn-lg btn-block btn-primary">Activar Articulo</a>
	</div>
</div>
<? endif ?>

<div class="modal fade" id="agregar-inventario-detalles" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <p>Cargando...</p>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->