<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuario_model extends CI_Model {

	

	public function crear_usuario()
	{
		$valores = array("correo" => $this->input->post("correo"),
						 "password" => $this->input->post("password"),
						 "id_tipo" => $this->input->post("tipo-usuario"),
						 "reputacion" => 0,
						 "bloqueado" => 0
			             );

		return $this->db->insert("usuario", $valores);
	}

	public function crear_perfil()
	{
		/*Upload Image Here*/

		$valores = array("id_usuario"=>$this->input->post("id_usuario"),
						 "alias"=>$this->input->post("alias"),
						 "nombre"=>$this->input->post("nombre"),
						 "apellido"=>$this->input->post("apellido"),
						 "cedula"=>$this->input->post("cedula"),
						 "fecha_nacimiento"=>($this->input->post("fecha-nacimiento")!="" && $this->input->post("fecha-nacimiento")!=false) ? datepicker_to_date($this->input->post("fecha-nacimiento")) : "2014-01-01",
						 "genero"=>$this->input->post("genero"),
						 "id_pais"=>$this->input->post("pais"),
						 "id_estado"=>$this->input->post("estado"),
						 "ciudad"=>$this->input->post("ciudad"),
						 "direccion"=>$this->input->post("direccion"),
						 "telefono"=>$this->input->post("telefono"),
						 "celular"=>$this->input->post("celular"),
						 "biografia"=>$this->input->post("biografia"),
						 "facebook"=>$this->input->post("facebook"),
						 "twitter"=>$this->input->post("twitter"),
						 "instagram"=>$this->input->post("instagram"),
						 "linkedin"=>$this->input->post("linkedin"),
						 "id_imagen"=>0,
						 "fecha_registro"=>date('Y-m-d'));

		return $this->db->insert("perfil",$valores);
		
	}

	public function editar_perfil($id_usuario)
	{
		$valores = array(
						 "nombre"=>$this->input->post("nombre"),
						 "apellido"=>$this->input->post("apellido"),
						 "cedula"=>$this->input->post("cedula"),
						 "fecha_nacimiento"=>($this->input->post("fecha-nacimiento")!="" && $this->input->post("fecha-nacimiento")!=false) ? datepicker_to_date($this->input->post("fecha-nacimiento")) : "2014-01-01",
						 "genero"=>$this->input->post("genero"),
						 "id_pais"=>$this->input->post("pais"),
						 "id_estado"=>$this->input->post("estado"),
						 "ciudad"=>$this->input->post("ciudad"),
						 "direccion"=>$this->input->post("direccion"),
						 "telefono"=>$this->input->post("telefono"),
						 "celular"=>$this->input->post("celular"),
						 "biografia"=>$this->input->post("biografia"),
						 "facebook"=>$this->input->post("facebook"),
						 "twitter"=>$this->input->post("twitter"),
						 "instagram"=>$this->input->post("instagram"),
						 "linkedin"=>$this->input->post("linkedin")
						 );

		return $this->db->where("id_usuario", $id_usuario)->update("perfil", $valores);
	}

	public function editar_imagen_perfil($id_perfil, $id_imagen='')
	{
		return $this->db->where("id", $id_perfil)->update("perfil", array("id_imagen" => $id_imagen));
	}

	public function get_usuario($correo="", $password="", $id="")
	{
		if ($id == "") :
			$usuario = $this->db->where("correo", $correo)->where("password", $password)->get("usuario")->row();
			if(!empty($usuario)):
				$perfil = $this->get_perfil($usuario->id);
				if(!empty($perfil)): 
					$usuario->alias = $perfil->alias;	
				endif;
			endif;
		else :
			$usuario = $this->db->where("id", $id)->get("usuario")->row();
			if(!empty($usuario)):
				$perfil = $this->get_perfil($usuario->id);
				if(!empty($perfil)): 
					$usuario->alias = $perfil->alias;	
				endif;
			endif;
		endif;

		return $usuario;
		
	}

	public function get_usuario_bycorreo($correo)
	{
		return $this->db->where("correo", $correo)->get("usuario")->row();
	}

	public function get_usuarios_por_tipo($tipo = '')
	{
		if ($tipo == "disenadores") {
			return $this->db->where('id_tipo', 2)->get('usuario')->result();
		} elseif ($tipo == "compradores") {
			return $this->db->where('id_tipo', 3)->get('usuario')->result();
		} else{
			return $this->db->where('id_tipo >=', 2)->get('usuario')->result();
		}
		
	}

	public function get_disenador_destacado()
	{
		$where = array(
			"us.bloqueado =" => 0,
			"us.id_tipo =" => 2
			);
		$disenador = $this->db->select("pe.id_usuario")
							 ->from("perfil AS pe")
							 ->join("usuario AS us", "us.id = pe.id_usuario", "inner")
							 ->where($where)
							 ->where("pe.destacado", 1)
							 ->get()
							 ->result();
		if (count($disenador)<=0) :
			$disenador = $this->db->select("pe.id_usuario")
							 ->from("perfil AS pe")
							 ->join("usuario AS us", "us.id = pe.id_usuario", "inner")
							 ->where($where)
							 ->get()
							 ->result();
		endif;
		$id_usuario = $disenador[rand(0,count($disenador)-1)]->id_usuario;
		$perfil = $this->get_perfil($id_usuario);
		$perfil->usuario = $this->get_usuario("","",$id_usuario);
		return $perfil;
	}

	public function cambiar_contrasena($id='')
	{
		$usuario = $this->get_usuario("","", $id);

		if ($usuario->password != $this->input->post("actual")) :
			return false;
		else:
			if ($this->db->where("id", $id)->update("usuario", array("password"=>$this->input->post("nueva")))) :
				return true;
			else:
				return false;
			endif;
		endif;
	}

	public function get_perfil($id='')
	{
		$perfil = $this->db->where("id_usuario", $id)->get("perfil")->row();

		if (count($perfil) > 0) :
			$perfil->lugar = $this->db->where("id", $perfil->id_estado)->get("geo_estados")->row()->estado.", ".$this->db->where("id", $perfil->id_pais)->get("geo_paises")->row()->pais;
			if ($perfil->id_imagen != 0) :
				$perfil->imagen = $this->db->where("id", $perfil->id_imagen)->get("galeria_imagen")->row();
			endif;
		endif;
			

		return $perfil;
	}

	public function get_disenadores_todos()
	{
		$disenadores = $this->db->where("id_tipo", 2)->get("usuario")->result();

		foreach ($disenadores as $disenador) :
			$disenador->perfil = $this->Usuario_model->get_perfil($disenador->id);
		endforeach;

		return $disenadores;
	}

	public function get_disenadores($pagina, $busqueda = "")
	{
		$where = array(
			"u.id_tipo" => 2,
			"u.bloqueado" => 0
			);

		$disenadores = $this->db->select("u.id, p.alias, p.nombre, p.apellido, p.ciudad, e.estado, pa.pais, p.id_imagen, p.destacado, (SELECT COUNT(*) FROM compras AS c WHERE p.id_usuario=c.id_usuario_vendedor AND c.status >=4) AS ventas")
								->from("usuario AS u")
								->join("perfil AS p", "u.id = p.id_usuario", "INNER")
								->join("geo_estados AS e", "e.id = p.id_estado", "INNER")
								->join("geo_paises AS pa", "pa.id = p.id_pais")
								->where($where);

		if ($busqueda != "" && $busqueda != false):
			$disenadores = $disenadores->where("(CONCAT_WS(' ', p.nombre, p.apellido) LIKE '%".$busqueda."%' OR p.alias LIKE '%".$busqueda."%' OR CONCAT_WS(', ', e.estado, pa.pais) LIKE '%".$busqueda."%' OR p.ciudad LIKE '%".$busqueda."%')");
		endif;

		if ($this->input->post("ordenar") == "nuevo"):
			$disenadores = $disenadores->order_by("p.fecha_registro", "desc");
		elseif ($this->input->post("ordenar") == "ventas"):
			$disenadores = $disenadores->order_by("ventas desc, p.fecha_registro asc");
		else:
			$disenadores = $disenadores->order_by("p.destacado desc, p.fecha_registro asc");
		endif;

		$total = clone($disenadores);

		$total = count($total->get()->result());

		$start = ($pagina - 1)*12;

		$disenadores = $disenadores->limit(12,$start)->get()->result();

		foreach ($disenadores as $disenador) :
			$disenador->total = $total;
			if ($disenador->id_imagen != 0) :
				$disenador->imagen = $this->db->where("id", $disenador->id_imagen)->get("galeria_imagen")->row();
			endif;
		endforeach;

		return $disenadores;
	}

	public function check_correo($correo)
	{
		$check = $this->db->where("correo", $correo)->get("usuario")->result_array();

		if (count($check) > 0) :
			return true;
		else:
			return false;
		endif;
	}

	public function check_alias($alias)
	{
		$check = $this->db->where("alias", $alias)->get("perfil")->result_array();

		if (count($check) > 0) :
			return true;
		else:
			return false;
		endif;
	}

	public function get_datos_pago($id='')
	{
		return $this->db->where("id_usuario", $id)->where("deleted", 0)->get("usuario_datos_pago")->result();
	}

	public function get_dato_pago($id='')
	{
		return $this->db->where("id", $id)->get("usuario_datos_pago")->row();
	}

	public function agregar_datos_pago($id_usuario="")
	{
		$datos = $this->input->post();

		$registro = array(
			"id_usuario" => $id_usuario,
			"banco" => $datos["banco"],
			"tipo_cuenta" => $datos["tipo-cuenta"],
			"numero" => $datos["numero-cuenta"],
			"titular" => $datos["titular-cuenta"],
			"cedula" => $datos["cedula-cuenta"],
			"correo" => $datos["correo-cuenta"]
			);

		return $this->db->insert("usuario_datos_pago", $registro);
	}

	public function editar_datos_pago($id_usuario="")
	{
		$datos = $this->input->post();

		$registro = array(
			"banco" => $datos["banco"],
			"tipo_cuenta" => $datos["tipo-cuenta"],
			"numero" => $datos["numero-cuenta"],
			"titular" => $datos["titular-cuenta"],
			"cedula" => $datos["cedula-cuenta"],
			"correo" => $datos["correo-cuenta"]
			);

		return $this->db->where("id", $datos["id-datos-pago"])->update("usuario_datos_pago", $registro);
	}

	public function eliminar_datos_pago($id_usuario="")
	{
		return $this->db->where("id", $this->input->post("id-datos-pago"))->update("usuario_datos_pago", array("deleted"=>1));
	}

	public function modificar_reputacion($id_usuario, $tipo, $id_usuario_puntuador, $comentario, $id_compra, $recibido, $por)
	{
		if ($tipo != 0):
			$usuario = $this->get_usuario("","",$id_usuario);
			$reputacion = $usuario->reputacion;
			$reputacion+=$tipo;
			$registro = array("reputacion" => $reputacion);

			$this->db->where("id", $id_usuario)->update("usuario", $registro);
		endif;

		$historial = array(
			"id_usuario" => $id_usuario,
			"tipo" => $tipo,
			"id_usuario_puntuador" => $id_usuario_puntuador,
			"comentario" => $comentario,
			"fecha" => date("Y-m-d"),
			"id_compra" => $id_compra,
			"recibido" => $recibido,
			"por" => $por
			);

		return $this->db->insert("usuario_reputacion_historial", $historial);
	}

	public function get_reputacion($id)
	{
		$reputacion = array(
			"positivas" => $this->get_calificaciones_positivas($id),
			"negativas" => $this->get_calificaciones_negativas($id),
			"neutrales" => $this->get_calificaciones_neutrales($id),
			"todas" => $this->get_calificaciones($id)
			);

		return $reputacion;
	}

	public function get_calificaciones_positivas($id_usuario='')
	{
		$where = array(
			"id_usuario" => $id_usuario,
			"tipo" => 1,
			"fecha >" => date('Y-m-d', strtotime('-6 month'))
			);

		$positivas = $this->db->where($where)->order_by("fecha", "desc")->get("usuario_reputacion_historial")->result();

		foreach ($positivas as $calificacion) :
			$calificacion->usuario_puntuador = $this->get_perfil($calificacion->id_usuario_puntuador);
		endforeach;

		return $positivas;
	}

	public function get_calificaciones_negativas($id_usuario='')
	{
		$where = array(
			"id_usuario" => $id_usuario,
			"tipo" => -1,
			"fecha >" => date('Y-m-d', strtotime('-6 month'))
			);

		$negativas = $this->db->where($where)->order_by("fecha", "desc")->get("usuario_reputacion_historial")->result();

		foreach ($negativas as $calificacion) :
			$calificacion->usuario_puntuador = $this->get_perfil($calificacion->id_usuario_puntuador);
		endforeach;

		return $negativas;
	}

	public function get_calificaciones_neutrales($id_usuario='')
	{
		$where = array(
			"id_usuario" => $id_usuario,
			"tipo" => 0,
			"fecha >" => date('Y-m-d', strtotime('-6 month'))
			);

		$neutrales = $this->db->where($where)->order_by("fecha", "desc")->get("usuario_reputacion_historial")->result();

		foreach ($neutrales as $calificacion) :
			$calificacion->usuario_puntuador = $this->get_perfil($calificacion->id_usuario_puntuador);
		endforeach;

		return $neutrales;
	}

	public function get_calificaciones($id_usuario='')
	{
		$where = array(
			"id_usuario" => $id_usuario,
			"fecha >" => date('Y-m-d', strtotime('-6 month'))
			);

		$calificaciones = $this->db->where($where)->order_by("fecha", "desc")->get("usuario_reputacion_historial")->result();

		foreach ($calificaciones as $calificacion) :
			$calificacion->usuario_puntuador = $this->get_perfil($calificacion->id_usuario_puntuador);
		endforeach;

		return $calificaciones;
	}

	public function get_calificacion_compra($id_compra, $por)
	{
		return $this->db->where("id_compra",$id_compra)->where("por", $por)->get("usuario_reputacion_historial")->row();
	}

	public function bloquear_usuario($id_usuario="")
	{
		$this->db->where("id", $id_usuario)->update("usuario", array("bloqueado" => 1));
	}

	public function activar_usuario($id_usuario="")
	{
		$this->db->where("id", $id_usuario)->update("usuario", array("bloqueado" => 0));
	}

	public function disenador_destacado($id_usuario, $destacado)
	{
		return $this->db->where("id_usuario", $id_usuario)->update("perfil", array("destacado" => $destacado));
	}

	public function factura_pendiente($id_usuario)
	{
		$factura = $this->db->where("id_usuario", $id_usuario)->where("status", 0)->get("servicio_factura")->result();

		if (count($factura) == 0) :
			return false;
		else :
			return true;
		endif;
		
	}

	public function __enviar_aviso($id_usuario)
	{
		$data["perfil"] = $this->get_perfil($id_usuario);
		$data["usuario"] = $this->get_usuario("","", $id_usuario);

		//Correo al Vendedor
		$config["mailtype"] = "html";

		$this->email->initialize($config);
		
		$this->email->from('sitemas@nuovavenezuela.com', 'Nuova Venezuela');
		$this->email->to("administracion@nuovavenezuela.com"); 

		$this->email->subject('Registro de Nuevo Diseñador');
		$html = $this->load->view("templates/correos/aviso_registro", $data, TRUE);
		$this->email->message($html);	

		$this->email->send();
	}

	public function __enviar_contrasena($id_usuario)
	{
		$data["usuario"] = $this->get_usuario("","", $id_usuario);

		//Correo al Vendedor
		$config["mailtype"] = "html";

		$this->email->initialize($config);
		
		$this->email->from('sitemas@nuovavenezuela.com', 'Nuova Venezuela');
		$this->email->to($data["usuario"]->correo); 

		$this->email->subject('Informacion de Cuenta Nuova Venezuela');
		$html = $this->load->view("templates/correos/reenvio_contrasena", $data, TRUE);
		$this->email->message($html);	

		$this->email->send();
	}

}

/* End of file usuario_model.php */
/* Location: ./application/models/usuario_model.php */