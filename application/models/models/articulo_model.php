<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Articulo_model extends CI_Model {

	public function get_status_escrito($status='')
	{
		if ($status == -1) :	
			return "Por Activar";
		elseif ($status == 0):
			return "Sin Existencias";
		elseif ($status == 1):
			return "Activado";
		else:
			return "Desactivado";
		endif;
	}

	public function get_categorias()
	{
		return $this->db->get("articulo_categorias")->result();
	}

	public function get_categoria($id)
	{
		return $this->db->where("id", $id)->get("articulo_categorias")->row();
	}

	public function get_colecciones($id_usuario="")
	{
		if ($id_usuario != "") :
			return $this->db->where("id_usuario", $id_usuario)->order_by("id", "desc")->get("articulo_coleccion")->result();
		endif;
	}

	public function get_coleccion($id='')
	{
		return $this->db->where("id", $id)->get("articulo_coleccion")->row();
	}

	public function get_articulo($id_articulo='')
	{
		$articulo = $this->db->where("id", $id_articulo)->get("articulo")->row_array();
		$articulo["usuario"] = $this->db->where("id_usuario", $articulo["id_usuario"])->get("perfil")->row();
		$articulo["usuario_bloqueado"] = $this->Usuario_model->get_usuario("","",$articulo["id_usuario"]);
		$articulo["categoria"] = $this->get_categoria($articulo["id_categoria"])->categoria;
		$articulo["coleccion"] = $this->get_coleccion($articulo["id_coleccion"])->nombre;
		$articulo["status_escrito"] = $this->get_status_escrito($articulo["status"]);
		$articulo["tallas"] = $this->get_tallas($id_articulo);
		$articulo["precio"] = $this->get_precios($id_articulo);
		$articulo["presentacion-tipo"] = $this->get_presentacion_tipo($id_articulo);
		if ($articulo["presentacion-tipo"] == "unica") :
			$articulo["imagenes"] = $this->get_imagenes($id_articulo);
		else:
			$articulo["presentaciones"] = $this->get_presentaciones($id_articulo);
		endif;
		if ($articulo["status"] >= 0):
			$articulo["inventario"] = $this->get_articulo_inventario_formateada($id_articulo);
			$articulo["total_piezas"] = $this->sumar_total_piezas($this->get_articulo_inventario($id_articulo));
		else:
			$articulo["inventario"] = '';
			$articulo["total_piezas"] = 0;
		endif;
		return $articulo;
	}

	public function get_articulos_cuenta($id_usuario='')
	{
		$articulos = $this->db->where("id_usuario", $id_usuario)->order_by("fecha", "desc")->get("articulo")->result();

		foreach ($articulos as $articulo) :
			$articulo->categoria = $this->get_categoria($articulo->id_categoria)->categoria;
			$articulo->coleccion = $this->get_coleccion($articulo->id_coleccion)->nombre;
			$articulo->presentacion_tipo = $this->get_presentacion_tipo($articulo->id);
			$articulo->preg_sc = $this->get_preguntas_articulo($articulo->id, false);
			$articulo->status_escrito = $this->get_status_escrito($articulo->status);
			if ($articulo->status >= 0):
				$articulo->inventario = $this->get_articulo_inventario($articulo->id);
				$articulo->total_piezas = $this->sumar_total_piezas($articulo->inventario);
			else:
				$articulo->inventario = '';
				$articulo->total_piezas = 0;
			endif;
		endforeach;

		return $articulos;
	}

	public function get_articulo_destacado_home($id_categoria='')
	{
		$where = array(
			"fecha <=" => date('Y-m-d'),
			"status >" => 0
			);
		$articulo = $this->db->where($where)->where("destacado", 1)->where("id_categoria",$id_categoria)->get("articulo")->result();
		if (count($articulo)<=0) :
			$articulo = $this->db->where($where)->where("id_categoria",$id_categoria)->get("articulo")->result();
		endif;
		$id_articulo = $articulo[rand(0,count($articulo)-1)]->id;
		return $this->get_articulo($id_articulo);
	}

	public function get_articulo_mas_vendido_home($id_categoria='')
	{
		$where = array(
			"a.fecha <=" => date('Y-m-d'),
			"a.status >" => 0
			);
		$articulo = $this->db->select("a.id AS id, (SELECT COUNT(*) FROM compras AS c WHERE a.id=c.id_articulo AND c.status >=4) AS ventas")
							  ->from("articulo AS a")
							  ->where("a.id_categoria", $id_categoria)
							  ->where($where)
							  ->order_by("ventas desc, a.fecha desc")
							  ->get()->result();
		$id_articulo = $articulo[0]->id;
		return $this->get_articulo($id_articulo);
	}

	public function get_articulos($pagina, $categoria, $busqueda="")
	{
		$articulos = $this->db->select("a.*, p.alias, (SELECT COUNT(*) FROM compras AS c WHERE a.id=c.id_articulo AND c.status >=4) AS ventas")
							  ->from("articulo AS a")
							  ->join("perfil AS p", "a.id_usuario = p.id_usuario", "inner")
							  ->join("usuario AS usr", "a.id_usuario = usr.id", "inner")
							  ->join("articulo_coleccion AS ac", "a.id_coleccion = ac.id", "inner");
		$where = array(
			"a.fecha <=" => date('Y-m-d'),
			"a.status >" => 0,
			"usr.bloqueado" => 0
			);

		if ($categoria != "todo"):
			$where["a.id_categoria"] = $categoria;
		endif;
		if ($this->input->post("filtrar") == "genero" && $this->input->post("valor") != ""):
			$where["a.genero"] = $this->input->post("valor");
		endif;
		
		$articulos = $articulos->where($where);
		
		if ($busqueda != "" && $busqueda != false):
			$articulos = $articulos->where("(a.nombre LIKE '%".$busqueda."%' OR CONCAT_WS(' ', p.nombre, p.apellido) LIKE '%".$busqueda."%' OR p.alias LIKE '%".$busqueda."%' OR ac.nombre LIKE '%".$busqueda."%')");
		endif;

		if ($this->input->post("ordenar") == "nuevo"):
			$articulos = $articulos->order_by("a.fecha", "desc");
		elseif ($this->input->post("ordenar") == "ventas"):
			$articulos = $articulos->order_by("ventas desc, a.fecha desc");
		elseif ($this->input->post("ordenar") == "coleccion"):
			$articulos = $articulos->order_by("ac.nombre asc, a.fecha desc");
		else:
			$articulos = $articulos->order_by("a.destacado desc, a.fecha desc");
		endif;

		$total = clone($articulos);

		$total = count($total->get()->result());

		$start = ($pagina - 1)*12;

		$articulos = $articulos->limit(12,$start)->get()->result();

		foreach ($articulos as $articulo) :
			$articulo->total = $total;
			$articulo->categoria = $this->get_categoria($articulo->id_categoria)->categoria;
			$articulo->coleccion = $this->get_coleccion($articulo->id_coleccion)->nombre;
			$articulo->precio = $this->get_precios($articulo->id);
			$articulo->presentacion_tipo = $this->get_presentacion_tipo($articulo->id);
			$articulo->status_escrito = $this->get_status_escrito($articulo->status);
			if ($articulo->presentacion_tipo == "unica") :
				$articulo->imagenes = $this->get_imagenes($articulo->id);
			else:
				$articulo->presentaciones = $this->get_presentaciones($articulo->id);
				$articulo->imagenes = array();
				foreach ($articulo->presentaciones as $presentacion) :
					foreach ($presentacion["imagenes"] as $imagen) :
						array_push($articulo->imagenes, $imagen);
					endforeach;
				endforeach;
			endif;
			if ($articulo->status > 0):
				$articulo->inventario = $this->get_articulo_inventario($articulo->id);
				$articulo->total_piezas = $this->sumar_total_piezas($articulo->inventario);
			else:
				$articulo->inventario = '';
				$articulo->total_piezas = 0;
			endif;
		endforeach;

		return $articulos;
	}

	public function get_ultimos_articulos($id)
	{
		$where = array(
			"fecha <=" => date('Y-m-d'),
			"status >" => 0,
			"id_usuario" => $id
			);

		$articulos = $this->db->where($where)->order_by("fecha", "desc")->get("articulo", 6)->result();

		foreach ($articulos as $articulo) :
			$articulo->categoria = $this->get_categoria($articulo->id_categoria)->categoria;
			$articulo->coleccion = $this->get_coleccion($articulo->id_coleccion)->nombre;
			$articulo->precio = $this->get_precios($articulo->id);
			$articulo->presentacion_tipo = $this->get_presentacion_tipo($articulo->id);
			$articulo->status_escrito = $this->get_status_escrito($articulo->status);
			if ($articulo->presentacion_tipo == "unica") :
				$articulo->imagenes = $this->get_imagenes($articulo->id);
			else:
				$articulo->presentaciones = $this->get_presentaciones($articulo->id);
				$articulo->imagenes = array();
				foreach ($articulo->presentaciones as $presentacion) :
					foreach ($presentacion["imagenes"] as $imagen) :
						array_push($articulo->imagenes, $imagen);
					endforeach;
				endforeach;
			endif;
			if ($articulo->status > 0):
				$articulo->inventario = $this->get_articulo_inventario($articulo->id);
				$articulo->total_piezas = $this->sumar_total_piezas($articulo->inventario);
			else:
				$articulo->inventario = '';
				$articulo->total_piezas = 0;
			endif;
		endforeach;

		return $articulos;
	}

	public function get_articulos_usuario($id)
	{
		$where = array(
			"fecha <=" => date('Y-m-d'),
			"status >" => 0,
			"id_usuario" => $id
			);

		$articulos = $this->db->where($where)->order_by("fecha", "desc")->get("articulo")->result();

		foreach ($articulos as $articulo) :
			$articulo->usuario = $this->Usuario_model->get_perfil($articulo->id_usuario);
			$articulo->categoria = $this->get_categoria($articulo->id_categoria)->categoria;
			$articulo->coleccion = $this->get_coleccion($articulo->id_coleccion)->nombre;
			$articulo->precio = $this->get_precios($articulo->id);
			$articulo->presentacion_tipo = $this->get_presentacion_tipo($articulo->id);
			$articulo->status_escrito = $this->get_status_escrito($articulo->status);
			if ($articulo->presentacion_tipo == "unica") :
				$articulo->imagenes = $this->get_imagenes($articulo->id);
			else:
				$articulo->presentaciones = $this->get_presentaciones($articulo->id);
				$articulo->imagenes = array();
				foreach ($articulo->presentaciones as $presentacion) :
					foreach ($presentacion["imagenes"] as $imagen) :
						array_push($articulo->imagenes, $imagen);
					endforeach;
				endforeach;
			endif;
			if ($articulo->status > 0):
				$articulo->inventario = $this->get_articulo_inventario($articulo->id);
				$articulo->total_piezas = $this->sumar_total_piezas($articulo->inventario);
			else:
				$articulo->inventario = '';
				$articulo->total_piezas = 0;
			endif;
		endforeach;

		return $articulos;
	}

	public function get_articulos_coleccion($id)
	{
		$where = array(
			"fecha <=" => date('Y-m-d'),
			"status >" => 0,
			"id_coleccion" => $id
			);

		$articulos = $this->db->where($where)->order_by("fecha", "desc")->get("articulo")->result();

		foreach ($articulos as $articulo) :
			$articulo->usuario = $this->Usuario_model->get_perfil($articulo->id_usuario);
			$articulo->categoria = $this->get_categoria($articulo->id_categoria)->categoria;
			$articulo->coleccion = $this->get_coleccion($articulo->id_coleccion)->nombre;
			$articulo->precio = $this->get_precios($articulo->id);
			$articulo->presentacion_tipo = $this->get_presentacion_tipo($articulo->id);
			$articulo->status_escrito = $this->get_status_escrito($articulo->status);
			if ($articulo->presentacion_tipo == "unica") :
				$articulo->imagenes = $this->get_imagenes($articulo->id);
			else:
				$articulo->presentaciones = $this->get_presentaciones($articulo->id);
				$articulo->imagenes = array();
				foreach ($articulo->presentaciones as $presentacion) :
					foreach ($presentacion["imagenes"] as $imagen) :
						array_push($articulo->imagenes, $imagen);
					endforeach;
				endforeach;
			endif;
			if ($articulo->status > 0):
				$articulo->inventario = $this->get_articulo_inventario($articulo->id);
				$articulo->total_piezas = $this->sumar_total_piezas($articulo->inventario);
			else:
				$articulo->inventario = '';
				$articulo->total_piezas = 0;
			endif;
		endforeach;

		return $articulos;
	}

	public function get_articulos_deseos($id_usuario='')
	{
		$articulos = $this->db->select("articulo.*, lista_deseos.id as id_deseo")
							  ->from("articulo")
							  ->join("lista_deseos", "articulo.id = lista_deseos.id_articulo")
							  ->where("lista_deseos.id_usuario", $id_usuario)
							  ->get()
							  ->result();

		foreach ($articulos as $articulo) :
			$articulo->categoria = $this->get_categoria($articulo->id_categoria)->categoria;
			$articulo->coleccion = $this->get_coleccion($articulo->id_coleccion)->nombre;
			$articulo->presentacion_tipo = $this->get_presentacion_tipo($articulo->id);
			$articulo->status_escrito = $this->get_status_escrito($articulo->status);
			if ($articulo->status > 0):
				$articulo->inventario = $this->get_articulo_inventario($articulo->id);
				$articulo->total_piezas = $this->sumar_total_piezas($articulo->inventario);
			else:
				$articulo->inventario = '';
				$articulo->total_piezas = 0;
			endif;
		endforeach;

		return $articulos;
	}

	public function get_articulo_inventario($id_articulo='')
	{
		return $this->db->where("id_articulo", $id_articulo)->get("articulo_inventario")->result();
	}

	public function get_articulo_inventario_formateada($id_articulo='')
	{
		$registros = $this->db->where("id_articulo", $id_articulo)->get("articulo_inventario")->result();
		$inventario = array();
		foreach ($registros as $registro) :
			$inventario[$registro->id_presentacion][$registro->id_talla] = new StdClass();
			$inventario[$registro->id_presentacion][$registro->id_talla]->cantidad = $registro->cantidad;
			$inventario[$registro->id_presentacion][$registro->id_talla]->id = $registro->id;
		endforeach;
		return $inventario;
	}

	public function get_inventario($id_articulo, $id_presentacion, $id_talla)
	{
		return $this->db->select("*")
						->from("articulo_inventario")
						->where("id_articulo", $id_articulo)
						->where("id_presentacion", $id_presentacion)
						->where("id_talla", $id_talla)
						->get()
						->row();
	}

	public function get_inventario_info($id_inventario)
	{
		$inventario = $this->db->where("id", $id_inventario)->get("articulo_inventario")->row();

		$inventario->talla = $this->get_talla($inventario->id_talla)->talla;
		$inventario->presentacion = $this->get_presentacion($inventario->id_presentacion)->nombre;

		return $inventario;
	}

	public function get_total_piezas($id_articulo)
	{
		$inventario = $this->db->where("id_articulo", $id_articulo)->get("articulo_inventario")->result();

		return $this->sumar_total_piezas($inventario);
	}

	public function sumar_total_piezas($inventario = '')
	{
		if ($inventario != ''):
			$total = 0;
			foreach ($inventario as $registro) :
				$total += $registro->cantidad;
			endforeach;
			return $total;
		endif;
	}

	public function get_imagenes($id_articulo='')
	{
		return $this->db->select("galeria_imagen.id, galeria_imagen.archivo, galeria_imagen.nombre, articulo_fotos.id AS id_articulo_fotos")
						->from("articulo_fotos")
						->join("galeria_imagen", "galeria_imagen.id = articulo_fotos.id_imagen")
						->where("articulo_fotos.id_articulo", $id_articulo)
						->get()
						->result();
	}

	public function get_imagenes_presentaciones($id_presentacion = '')
	{
		return $this->db->select("galeria_imagen.id, galeria_imagen.archivo, galeria_imagen.nombre, articulo_fotos.id AS id_articulo_fotos")
						->from("articulo_fotos")
						->join("galeria_imagen", "galeria_imagen.id = articulo_fotos.id_imagen")
						->where("articulo_fotos.id_presentacion", $id_presentacion)
						->get()
						->result(); 
	}

	public function get_tallas($id_articulo='')
	{
		return $this->db->where("id_articulo", $id_articulo)->get("articulo_tallas")->result();
	}
	public function get_talla($id)
	{
		return $this->db->where("id", $id)->get("articulo_tallas")->row();
	}

	public function get_precios($id_articulo='')
	{
		return $this->db->where("id_articulo", $id_articulo)->get("articulo_precios")->row();
	}

	public function get_presentacion_tipo($id_articulo='')
	{
		if (count($this->db->where("id_articulo",$id_articulo)->get("articulo_presentaciones")->result_array()) <= 0) :
			return "unica";
		else:
			return "varias";
		endif;
	}

	public function get_presentaciones($id_articulo='')
	{
		$presentaciones = $this->db->select("id, nombre, color_icono")
						->from("articulo_presentaciones")
						->where("id_articulo", $id_articulo)
						->get()
						->result_array();

		for ($i=0; $i < count($presentaciones); $i++) : 
			$presentaciones[$i]["imagenes"] = $this->get_imagenes_presentaciones($presentaciones[$i]["id"]);
		endfor;	

		return $presentaciones;
	}

	public function get_presentacion($id='')
	{
		$presentacion = $this->db->where("id", $id)->get("articulo_presentaciones")->row();
		$presentacion->imagenes = $this->get_imagenes_presentaciones($id);

		return $presentacion;
	}

	public function crear_coleccion($id_usuario="")
	{
		if ($id_usuario != "") :
			$ins = array("id_usuario" => $id_usuario,
						 "nombre" => $this->input->post("nombre_coleccion"),
						 "descripcion" => $this->input->post("descripcion_coleccion"),
						 "fecha_creacion" => date('Y-m-d'),
						 "codigo_mostrar" => "");
			if($this->db->insert("articulo_coleccion", $ins)):
				$ins["id"] = $this->db->insert_id();
				return $ins;
			else:
				return false;
			endif;
		endif;
	}

	public function editar_coleccion($id="")
	{
		if ($id != "") :
			$ins = array(
						 "nombre" => $this->input->post("nombre_coleccion"),
						 "descripcion" => $this->input->post("descripcion_coleccion")
						 );
			if($this->db->where("id", $id)->update("articulo_coleccion", $ins)):
				return $id;
			else:
				return false;
			endif;
		endif;
	}

	public function eliminar_coleccion($id="")
	{
		if ($id != "") :
			$ins = array(
						 "id_coleccion" => 0
						 );
			$this->db->where("id_coleccion", $id)->update("articulo", $ins);

			$this->db->where("id", $id)->delete("articulo_coleccion");
			
		endif;
	}
	/*
	public function crear_articulo($id_usuario="")
	{
		if ($id_usuario != "") :
			$data = $this->input->post();

			//Creando el articulo
			$articulo = array("id_usuario" => $id_usuario,
							  "id_categoria" => $data["id_categoria"],
							  "id_coleccion" => $data["id_coleccion"],
							  "nombre" => $data["nombre"],
							  "descripcion" => $data["descripcion"],
							  "fecha" => datepicker_to_date($data["fecha"]),
							  "status" => 0,
							  "modo_precio" => 1,
							  "modo_inventario" => ($data["presentacion-tipo"] == "unica") ? 1 : 2);

			if($this->db->insert("articulo", $articulo)):
				$id_articulo = $this->db->insert_id();

				//Guardando Precio

				$precio = array("id_articulo" => $id_articulo,
								"precio" => $data["precio"]);
				$this->db->insert("articulo_precios", $precio);

				//Guardando Tallas
				foreach ($data["talla"] as $talla):
					$ins = array("id_articulo" => $id_articulo,
								 "talla" => $talla,
								 "descripcion" => "");
					$this->db->insert("articulo_tallas", $ins);
				endforeach;

				//Guardando Presentaciones con sus fotos
				if ($data["presentacion-tipo"] == "unica") :
					$imagenes = explode(",", $data["id_imagenes_unica"]);
					foreach ($imagenes as $imagen):
						if ($imagen != "") :
							$ins = array("id_articulo" => $id_articulo,
								 "id_presentacion" => 0,
								 "id_imagen" => $imagen);
							$this->db->insert("articulo_fotos", $ins);
						endif;
					endforeach;
				else :
					for ($i=0; $i < count($data["presentacion-nombre"]); $i++) : 
						$ins = array("id_articulo" => $id_articulo,
									 "nombre" => $data["presentacion-nombre"][$i],
									 "color_icono" => $data["presentacion-color"][$i],
									 "descripcion" => "");
						;

						if ($this->db->insert("articulo_presentaciones", $ins)) :
							$id_presentacion = $this->db->insert_id();

							$imagenes = explode(",", $data["id_imagenes"][$i]);
							foreach ($imagenes as $imagen):
								if ($imagen != "") :
									$ins = array("id_articulo" => $id_articulo,
										 "id_presentacion" => $id_presentacion,
										 "id_imagen" => $imagen);
									$this->db->insert("articulo_fotos", $ins);
								endif;
							endforeach;
						endif;
					endfor;
				endif;
			endif;
		endif;
	}
	*/
	public function crear_articulo($id_usuario="")
	{
		if ($id_usuario != "") :
			$data = $this->input->post();

			//Creando el articulo
			$articulo = array("id_usuario" => $id_usuario,
							  "id_categoria" => $data["id_categoria"],
							  "id_coleccion" => $data["id_coleccion"],
							  "nombre" => $data["nombre"],
							  "descripcion" => $data["descripcion"],
							  "genero" => $data["genero"],
							  "fecha" => datepicker_to_date($data["fecha"]),
							  "status" => -1,
							  "modo_precio" => 1,
							  "modo_inventario" => ($data["presentacion-tipo"] == "unica") ? 1 : 2);

			if($this->db->insert("articulo", $articulo)):
				$id_articulo = $this->db->insert_id();

				//Guardando Precio

				$precio = array("id_articulo" => $id_articulo,
								"precio" => $data["precio"]);
				$this->db->insert("articulo_precios", $precio);

				//Guardando Tallas
				foreach ($data["talla"] as $talla):
					$ins = array("id_articulo" => $id_articulo,
								 "talla" => strtoupper($talla),
								 "descripcion" => "");
					$this->db->insert("articulo_tallas", $ins);
				endforeach;

				//Guardando Presentaciones
				if ($data["presentacion-tipo"] != "unica") :
					for ($i=0; $i < count($data["presentacion-nombre"]); $i++) : 
						$ins = array("id_articulo" => $id_articulo,
									 "nombre" => $data["presentacion-nombre"][$i],
									 "color_icono" => $data["presentacion-color"][$i],
									 "descripcion" => "");
						;

						$this->db->insert("articulo_presentaciones", $ins);
					endfor;
				endif;
			endif;
			return $id_articulo;
		endif;
	}

	public function editar_articulo($id_articulo)
	{
		$data = $this->input->post();

		//Creando el articulo
		$articulo = array(
						  "id_categoria" => $data["id_categoria"],
						  "id_coleccion" => $data["id_coleccion"],
						  "nombre" => $data["nombre"],
						  "descripcion" => $data["descripcion"],
						  "genero" => $data["genero"],
						  "fecha" => datepicker_to_date($data["fecha"])
						  );

		$this->db->where("id", $id_articulo)->update("articulo", $articulo);

		$precio = array("precio" => $data["precio"]);

		$this->db->where("id_articulo", $id_articulo)->update("articulo_precios", $precio);

		for ($i=0; $i < count($data["id_talla"]); $i++) :
			$talla = array(
				"talla" => strtoupper($data["talla_existente"][$i])
				);
			$this->db->where("id", $data["id_talla"][$i])->update("articulo_tallas", $talla);
		endfor;

		for ($i=0; $i < count($data["id_presentacion"]); $i++) :
			$presentacion = array(
					 "nombre" => $data["presentacion-nombre"][$i],
					 "color_icono" => $data["presentacion-color"][$i],
					 );

			$this->db->where("id", $data["id_presentacion"][$i])->update("articulo_presentaciones", $presentacion);	
		endfor;

		return true;
	}

	public function eliminar_articulo($id_articulo='')
	{
		if ($id_articulo != '') :
			$articulo = $this->db->where("id", $id_articulo)->get("articulo")->row();
			if ($articulo->status == -1) :
				if ($this->get_presentacion_tipo($id_articulo) == "varias") :
					$this->db->where("id_articulo", $id_articulo)->delete("articulo_presentaciones");
				endif;
				$this->db->where("id_articulo", $id_articulo)->delete("articulo_tallas");
				$imagenes = $this->db->where("id_articulo", $id_articulo)->get("articulo_fotos")->result();
				foreach ($imagenes as $imagen):
					$this->Galeria_model->eliminar_imagen($imagen->id_imagen);
				endforeach;
				$this->db->where("id_articulo", $id_articulo)->delete("articulo_fotos");
				$this->db->where("id_articulo", $id_articulo)->delete("articulo_precios");
				$this->db->where("id", $id_articulo)->delete("articulo");
				return true;
			else:
				return false;
			endif;
		else:
			return false;
		endif;
	}

	public function primera_activacion($articulo = '')
	{
		if ($articulo != ''):
			if ($articulo["presentacion-tipo"] == "unica") :
				foreach ($articulo["tallas"] as $talla):
					$reg = array("id_articulo" => $articulo["id"],
								 "id_talla" => $talla->id,
								 "id_presentacion" => 0,
								 "cantidad" => 0,
								 "fecha_ultima_act" => date('Y-m-d'),
								 "fecha_activacion" => date('Y-m-d'));
					$this->db->insert("articulo_inventario", $reg);
				endforeach;
			else:
				foreach ($articulo["presentaciones"] as $presentacion) :
					foreach ($articulo["tallas"] as $talla):
						$reg = array("id_articulo" => $articulo["id"],
									 "id_talla" => $talla->id,
									 "id_presentacion" => $presentacion["id"],
									 "cantidad" => 0,
									 "fecha_ultima_act" => date('Y-m-d'));
						$this->db->insert("articulo_inventario", $reg);
					endforeach;
				endforeach;
			endif;
			$this->db->where("id", $articulo["id"])->update("articulo", array("status" => 0));
			return true;
		else:
			return false;
		endif;
	}

	public function agregar_piezas_inventario($id_inventario, $cantidad)
	{
		if ($cantidad > 0) :
			$inventario = $this->get_inventario_info($id_inventario);
			$nueva_cantidad = $inventario->cantidad + $cantidad;
			if ($this->db->where("id",$id_inventario)->update("articulo_inventario", array("cantidad" => $nueva_cantidad, "fecha_ultima_act" => date('Y-m-d')))) :
				$registro = array("id_articulo" => $inventario->id_articulo,
								  "id_articulo_inventario" => $id_inventario,
								  "cantidad" => $cantidad,
								  "descripcion" => "Piezas Agregadas por ".$this->session->userdata("correo"),
								  "id_usuario_movimiento" => $this->session->userdata("id"),
								  "tipo_movimiento" => 1,
								  "id_extra" => 0,
								  "fecha" => date('Y-m-d'));
				if ($this->db->insert("articulo_inventario_historial", $registro)) :
					$this->db->where("id", $inventario->id_articulo)->update("articulo", array("status" => 1));
					return true;
				else :
					return false;
				endif;
				
			else :
				return false;
			endif;
		endif;
	}


	//Cantidad debe ser negativa
	public function eliminar_piezas_inventario($id_inventario, $cantidad, $compra=false)
	{
		if ($cantidad < 0) :
			$inventario = $this->get_inventario_info($id_inventario);
			$nueva_cantidad = $inventario->cantidad + $cantidad;
			if ($nueva_cantidad < 0) :
				return false;
			else :
				if ($this->db->where("id",$id_inventario)->update("articulo_inventario", array("cantidad" => $nueva_cantidad, "fecha_ultima_act" => date('Y-m-d')))) :
					$registro = array("id_articulo" => $inventario->id_articulo,
									  "id_articulo_inventario" => $id_inventario,
									  "cantidad" => $cantidad,
									  "descripcion" => ($compra != false) ? "Compra hecha por ".$this->session->userdata("correo") :"Piezas eliminadas por ".$this->session->userdata("correo"),
									  "id_usuario_movimiento" => $this->session->userdata("id"),
									  "tipo_movimiento" => ($compra != false) ? 3 : 2,
									  "id_extra" => ($compra != false) ? $compra : 0,
									  "fecha" => date('Y-m-d'));
					if ($this->db->insert("articulo_inventario_historial", $registro)) :
						$total = $this->get_total_piezas($inventario->id_articulo);
						if ($total == 0) :
							$this->db->where("id", $inventario->id_articulo)->update("articulo", array("status" => 0));
						endif;
						return true;
					else :
						return false;
					endif;
					
				else :
					return false;
				endif;
			endif;
		
		endif;
	}

	public function compra()
	{
		$compra = $this->input->post();

		$compra = $this->registrar_compra($compra);

		$this->eliminar_piezas_inventario($this->get_inventario($compra["id-articulo"], $compra["id-presentacion"], $compra["id-talla"])->id, ($compra["articulo-cantidad"]*-1), $compra["id-compra"]);

		$this->__enviar_confirmaciones_compra($compra["id-compra"]);

	}

	public function get_ventas($id_usuario)
	{
		$ventas = $this->db->where("id_usuario_vendedor", $id_usuario)->get("compras")->result();

		foreach ($ventas as $venta):
			$venta->status_escrita = $this->get_status_compra_escrito($venta->status);
			$venta->articulo = $this->get_articulo($venta->id_articulo);
			$venta->talla = $this->get_talla($venta->id_talla);
			if ($venta->articulo["presentacion-tipo"] == "unica"):
				$venta->imagenes = $this->get_imagenes($venta->id_articulo);
			else:
				$venta->presentacion = $this->get_presentacion($venta->id_presentacion);
			endif;
			$venta->comprador = $this->Usuario_model->get_perfil($venta->id_usuario_comprador);
			$venta->comprador->usuario = $this->Usuario_model->get_usuario("","",$venta->id_usuario_comprador);
			$venta->pagos = $this->get_pagos($venta->id);
		endforeach;

		return $ventas;
	}

	public function get_ventas_nuevas($id_usuario)
	{
		$ventas = $this->db->where("id_usuario_vendedor", $id_usuario)->where("status", 0)->get("compras")->result();

		foreach ($ventas as $venta):
			$venta->status_escrita = $this->get_status_compra_escrito($venta->status);
			$venta->articulo = $this->get_articulo($venta->id_articulo);
			$venta->talla = $this->get_talla($venta->id_talla);
			if ($venta->articulo["presentacion-tipo"] == "unica"):
				$venta->imagenes = $this->get_imagenes($venta->id_articulo);
			else:
				$venta->presentacion = $this->get_presentacion($venta->id_presentacion);
			endif;
			$venta->comprador = $this->Usuario_model->get_perfil($venta->id_usuario_comprador);
			$venta->comprador->usuario = $this->Usuario_model->get_usuario("","",$venta->id_usuario_comprador);
			$venta->pagos = $this->get_pagos($venta->id);
		endforeach;

		return $ventas;
	}

	public function registrar_compra($data)
	{
		$registro = array(
			"id_usuario_comprador" => $data["id-usuario-comprador"],
			"id_articulo" => $data["id-articulo"],
			"id_presentacion" => ($data["presentacion-tipo"] == "unica") ? 0 : $data["id-presentacion"],
			"id_talla" => $data["id-talla"],
			"cantidad" => $data["articulo-cantidad"],
			"precio" => $data["articulo-precio"],
			"fecha_compra" => date("Y-m-d"),
			"status" => 0,
			"facturado" => 0,
			"id_usuario_vendedor" => $data["id-usuario-vendedor"],
			"direccion_envio_alt" => (isset($data["usar-direccion-alt"])) ? $data["direccion-alternativa"] : "" 
			);

		$this->db->insert("compras", $registro);

		$data["id-compra"] = $this->db->insert_id();

		return $data;
	}

	public function get_compras($id_usuario="")
	{
		$compras = $this->db->where("id_usuario_comprador", $id_usuario)->get("compras")->result();

		foreach ($compras as $compra):
			$compra->status_escrita = $this->get_status_compra_escrito($compra->status);
			$compra->articulo = $this->get_articulo($compra->id_articulo);
			$compra->talla = $this->get_talla($compra->id_talla);
			if ($compra->articulo["presentacion-tipo"] == "unica"):
				$compra->imagenes = $this->get_imagenes($compra->id_articulo);
			else:
				$compra->presentacion = $this->get_presentacion($compra->id_presentacion);
			endif;
			$compra->disenador = $this->Usuario_model->get_perfil($compra->articulo["id_usuario"]);
			$compra->disenador->usuario = $this->Usuario_model->get_usuario("","",$compra->articulo["id_usuario"]);
			$compra->pagos = $this->get_pagos($compra->id);
		endforeach;

		return $compras;
	}

	public function get_compra($id="")
	{
		$compra = $this->db->where("id", $id)->get("compras")->row();
		$compra->status_escrita = $this->get_status_compra_escrito($compra->status);
		$compra->articulo = $this->get_articulo($compra->id_articulo);
		$compra->talla = $this->get_talla($compra->id_talla);
		if ($compra->articulo["presentacion-tipo"] == "unica"):
			$compra->imagenes = $this->get_imagenes($compra->id_articulo);
		else:
			$compra->presentacion = $this->get_presentacion($compra->id_presentacion);
		endif;
		$compra->disenador = $this->Usuario_model->get_perfil($compra->articulo["id_usuario"]);
		$compra->disenador->usuario = $this->Usuario_model->get_usuario("","",$compra->articulo["id_usuario"]);
		$compra->pagos = $this->get_pagos($compra->id);
		return $compra;
	}

	public function get_status_compra($id)
	{
		$compra = $this->db->where("id", $id)->get("compras")->row();

		return $compra->status;
	}

	public function get_status_compra_escrito($status="")
	{
		if ($status == 0) :
			return "Nueva";
		elseif ($status == 1) :
			return "Verificando Pagos";
		elseif ($status == 2) :
			return "Verificado/Por Enviar";
		elseif ($status == 3) :
			return "Enviado/Por Valorar";
		elseif ($status == 4) :
			return "Por Valorar Vendedor";
		elseif ($status == 5) :
			return "Recibido/Completado";
		elseif ($status == -1 || $status == -2) :
			return "Cancelado";
		endif;
		
	}

	public function cambiar_status_compra($id_compra='', $status="")
	{
		if ($status == -2 || $status == 5) :
			$this->db->where("id", $id_compra)->update("compras", array("status" => $status, "fecha_finalizacion_compra" => date("Y-m-d")));
		else:
			$this->db->where("id", $id_compra)->update("compras", array("status" => $status));
		endif;
	}

	public function registrar_pago($id_usuario='')
	{
		$registro = array(
			"id_compra" => $this->input->post("id-compra"),
			"monto_total" => $this->input->post("monto-pago"),
			"fecha_pago" => datepicker_to_date($this->input->post("fecha-pago")),
			"banco_emisor" => $this->input->post("banco-emisor"),
			"id_datos_pago" => $this->input->post("id-datos-pago"),
			"id_usuario_emisor" => $id_usuario,
			"codigo_pago" => $this->input->post("codigo-pago"),
			"fecha_registro" => date("Y-m-d"),
			"id_usuario_receptor" => $this->input->post("id-usuario-receptor"),
			"imagen" => ""
			);

		if ($this->db->insert("pagos", $registro)):
			$this->cambiar_status_compra($this->input->post("id-compra"), 1);
			return true;
		else:
			return false;
		endif;

		$this->__enviar_reporte_pago($this->input->post("id-compra"));
	}

	public function get_pagos($id_compra)
	{
		$pagos = $this->db->where("id_compra", $id_compra)->get("pagos")->result();
		foreach ($pagos as $pago) :
			$pago->datos_pago = $this->Usuario_model->get_dato_pago($pago->id_datos_pago);
		endforeach;
		return $pagos;
	}

	public function get_pago($id_pago)
	{
		$pago = $this->db->where("id", $id_pago)->get("pagos")->row();
		$pago->datos_pago = $this->Usuario_model->get_dato_pago($pago->id_datos_pago);
		return $pago;
	}

	public function marcar_favorito($id_usuario, $id_articulo)
	{
		$marca = $this->db->where("id_usuario", $id_usuario)
						  ->where("id_articulo", $id_articulo)
						  ->get("lista_deseos")
						  ->row_array();

		if (count($marca) > 0) :
			$this->db->where("id", $marca["id"])->delete("lista_deseos");
			return 0;
		else :
			$registro=array(
				"id_usuario" => $id_usuario,
				"id_articulo" => $id_articulo,
				"fecha" => date("Y-m-d")
				);
			$this->db->insert("lista_deseos", $registro);
			return 1;
		endif;
		
	}

	public function es_favorito($id_usuario, $id_articulo)
	{
		$marca = $this->db->where("id_usuario", $id_usuario)
						  ->where("id_articulo", $id_articulo)
						  ->get("lista_deseos")
						  ->row_array();

		if (count($marca) > 0) :
			return true;
		else :
			return false;
		endif;
	}

	public function marcar_verificado($id_pago)
	{
		$pago = $this->get_pago($id_pago);

		if ($pago->verificado == true) :
			$this->db->where("id", $id_pago)->update("pagos", array("verificado" => false));
			return 0;
		else :
			$this->db->where("id", $id_pago)->update("pagos", array("verificado" => true));
			return 1;
		endif;
		
	}

	public function guardar_direccion_alt($id_compra, $direccion_alt)
	{
		$this->db->where("id", $id_compra)->update("compras", array("direccion_envio_alt" => $direccion_alt));
	}

	public function marcar_enviado($id_compra='')
	{
		$this->db->where("id", $id_compra)->update("compras", array("observaciones_envio" => $this->input->post("observaciones-envio")));

		$this->cambiar_status_compra($id_compra, 3);

		$this->__enviar_confirmacion_envio($id_compra, $this->input->post("observaciones-envio"));
	}

	public function eliminar_articulofoto($id)
	{
		$this->db->where("id", $id)->delete("articulo_fotos");
	}

	public function guardar_pregunta($id_usuario="", $id_articulo="", $pregunta="")
	{
		$registro = array(
			"id_usuario_preg" => $id_usuario,
			"id_articulo" => $id_articulo,
			"fecha_pregunta" => date("Y-m-d"),
			"fecha_respuesta" => "0000-00-00",
			"pregunta" => $pregunta,
			"respuesta" => ""
			);

		$this->db->insert("articulo_preguntas", $registro);

		$registro["id"] = $this->db->insert_id();

		return $registro;
	}

	public function guardar_respuesta($id_pregunta="", $respuesta="")
	{
		$registro = array(
			"fecha_respuesta" => date("Y-m-d"),
			"respuesta" => $respuesta
			);

		$this->db->where("id", $id_pregunta)->update("articulo_preguntas", $registro);

		return $registro;
	}

	public function get_pregunta($id_pregunta='')
	{
		return $this->db->where("id", $id_pregunta)->get("articulo_preguntas")->result();
	}

	public function get_preguntas_usuario($id_usuario, $contestadas = "todas")
	{
		$where = array(
			"id_usuario_preg" => $id_usuario
			);

		$preguntas = $this->db->where($where);

		/*if ($contestadas == false):
			$preguntas = $preguntas->where("respuesta = ''");
		elseif($contestadas == true):
			$preguntas = $preguntas->where("respuesta != ''");
		endif;*/

		return $preguntas->order_by("fecha_pregunta", "desc")->get("articulo_preguntas")->result();
	}

	public function get_preguntas_articulo($id_articulo, $contestadas = "todas", $ex_us = FALSE, $id_usuario="")
	{
		$where = array(
			"id_articulo" => $id_articulo
			);

		$preguntas = $this->db->where($where);

		if ($ex_us) :
			$preguntas = $preguntas->where("id_usuario_preg != ".$id_usuario);
		endif;

		if ($contestadas == false):
			$preguntas = $preguntas->where("respuesta = ''");
		elseif($contestadas == true):
			$preguntas = $preguntas->where("respuesta != ''");
		endif;

		return $preguntas->order_by("fecha_pregunta", "desc")->get("articulo_preguntas")->result();
	}

	public function get_preguntas_por_contestar($id_usuario)
	{
		$preguntas = $this->db->select("ap.*")
							  ->from("articulo_preguntas AS ap")
							  ->join("articulo AS a", "a.id = ap.id_articulo", "inner")
							  ->where("a.id_usuario = ".$id_usuario)
							  ->where("ap.respuesta = ''")
							  ->order_by("fecha_pregunta", "desc")
							  ->get()
							  ->result();
		return $preguntas;
	}

	public function __enviar_confirmaciones_compra($id_compra='')
	{

		$data["compra"] = $this->Articulo_model->get_compra($id_compra);
		$data["disenador"] = $this->Usuario_model->get_perfil($data["compra"]->articulo["id_usuario"]);
		$data["disenador"]->usuario = $this->Usuario_model->get_usuario("","", $data["compra"]->articulo["id_usuario"]);
		$data["comprador"] = $this->Usuario_model->get_perfil($data["compra"]->id_usuario_comprador);
		$data["comprador"]->usuario = $this->Usuario_model->get_usuario("","", $data["compra"]->id_usuario_comprador);
		$data["datos_pago"] = $this->Usuario_model->get_datos_pago($data["compra"]->articulo["id_usuario"]);

		//Correo al Comprador
		$config["mailtype"] = "html";

		$this->email->initialize($config);
		
		$this->email->from('no-reply@nuovavenezuela.com', 'Nuova Venezuela');
		$this->email->to($data["comprador"]->usuario->correo); 

		$this->email->subject('Confirmación de Compra');
		$html = $this->load->view("templates/correos/compra_exitosa", $data, TRUE);
		$this->email->message($html);	

		$this->email->send();

		$this->email->clear();

		//Correo al Vendedor
		
		$config["mailtype"] = "html";

		$this->email->initialize($config);
		
		$this->email->from('no-reply@nuovavenezuela.com', 'Nuova Venezuela');
		$this->email->to($data["disenador"]->usuario->correo); 

		$this->email->subject('Confirmación de Venta');
		$html = $this->load->view("templates/correos/venta_exitosa", $data, TRUE);
		$this->email->message($html);	

		$this->email->send();		
	}

	public function __enviar_reporte_pago($id_compra='')
	{

		$data["compra"] = $this->Articulo_model->get_compra($id_compra);
		$data["disenador"] = $this->Usuario_model->get_perfil($data["compra"]->articulo["id_usuario"]);
		$data["disenador"]->usuario = $this->Usuario_model->get_usuario("","", $data["compra"]->articulo["id_usuario"]);
		$data["comprador"] = $this->Usuario_model->get_perfil($data["compra"]->id_usuario_comprador);
		$data["comprador"]->usuario = $this->Usuario_model->get_usuario("","", $data["compra"]->id_usuario_comprador);

		//Correo al Vendedor
		$config["mailtype"] = "html";

		$this->email->initialize($config);
		
		$this->email->from('no-reply@nuovavenezuela.com', 'Nuova Venezuela');
		$this->email->to($data["disenador"]->usuario->correo); 

		$this->email->subject('Reporte de Pago');
		$html = $this->load->view("templates/correos/pago_reportado", $data, TRUE);
		$this->email->message($html);	

		$this->email->send();

	}

	public function __enviar_confirmacion_envio($id_compra='', $observaciones="")
	{

		$data["compra"] = $this->Articulo_model->get_compra($id_compra);
		$data["disenador"] = $this->Usuario_model->get_perfil($data["compra"]->articulo["id_usuario"]);
		$data["disenador"]->usuario = $this->Usuario_model->get_usuario("","", $data["compra"]->articulo["id_usuario"]);
		$data["comprador"] = $this->Usuario_model->get_perfil($data["compra"]->id_usuario_comprador);
		$data["comprador"]->usuario = $this->Usuario_model->get_usuario("","", $data["compra"]->id_usuario_comprador);
		$data["observaciones_envio"] = $observaciones;

		//Correo al Comprador
		$config["mailtype"] = "html";

		$this->email->initialize($config);
		
		$this->email->from('no-reply@nuovavenezuela.com', 'Nuova Venezuela');
		$this->email->to($data["comprador"]->usuario->correo); 

		$this->email->subject('Confirmación de Envío');
		$html = $this->load->view("templates/correos/envio_reportado", $data, TRUE);
		$this->email->message($html);	

		$this->email->send();

	}

	public function __enviar_calificacion_comprador($id_compra)
	{
		$data["compra"] = $this->Articulo_model->get_compra($id_compra);
		$data["disenador"] = $this->Usuario_model->get_perfil($data["compra"]->articulo["id_usuario"]);
		$data["disenador"]->usuario = $this->Usuario_model->get_usuario("","", $data["compra"]->articulo["id_usuario"]);
		$data["comprador"] = $this->Usuario_model->get_perfil($data["compra"]->id_usuario_comprador);
		$data["comprador"]->usuario = $this->Usuario_model->get_usuario("","", $data["compra"]->id_usuario_comprador);

		//Correo al Vendedor
		$config["mailtype"] = "html";

		$this->email->initialize($config);
		
		$this->email->from('no-reply@nuovavenezuela.com', 'Nuova Venezuela');
		$this->email->to($data["disenador"]->usuario->correo); 

		$this->email->subject('Te Calificaron');
		$html = $this->load->view("templates/correos/calificacion_comprador", $data, TRUE);
		$this->email->message($html);	

		$this->email->send();
	}

	public function __enviar_calificacion_vendedor($id_compra)
	{
		$data["compra"] = $this->Articulo_model->get_compra($id_compra);
		$data["disenador"] = $this->Usuario_model->get_perfil($data["compra"]->articulo["id_usuario"]);
		$data["disenador"]->usuario = $this->Usuario_model->get_usuario("","", $data["compra"]->articulo["id_usuario"]);
		$data["comprador"] = $this->Usuario_model->get_perfil($data["compra"]->id_usuario_comprador);
		$data["comprador"]->usuario = $this->Usuario_model->get_usuario("","", $data["compra"]->id_usuario_comprador);

		//Correo al Vendedor
		$config["mailtype"] = "html";

		$this->email->initialize($config);
		
		$this->email->from('no-reply@nuovavenezuela.com', 'Nuova Venezuela');
		$this->email->to($data["comprador"]->usuario->correo); 

		$this->email->subject('Te Calificaron');
		$html = $this->load->view("templates/correos/calificacion_vendedor", $data, TRUE);
		$this->email->message($html);	

		$this->email->send();
	}

	public function __avisar_usuario_pregunta($id_pregunta="")
	{

		$data["pregunta"] = $this->get_pregunta($id_pregunta);
		$data["articulo"] = $this->get_articulo($data["pregunta"]->id_articulo);
		$data["disenador"] = $this->Usuario_model->get_perfil($data["articulo"]["id_usuario"]);
		$data["disenador"]->usuario = $this->Usuario_model->get_usuario("","", $data["articulo"]["id_usuario"]);
		$data["comprador"] = $this->Usuario_model->get_perfil($data["pregunta"]->id_usuario_preg);
		$data["comprador"]->usuario = $this->Usuario_model->get_usuario("","", $data["pregunta"]->id_usuario_preg);

		//Correo al Vendedor
		$config["mailtype"] = "html";

		$this->email->initialize($config);
		
		$this->email->from('no-reply@nuovavenezuela.com', 'Nuova Venezuela');
		$this->email->to($data["disenador"]->usuario->correo); 

		$this->email->subject('Te hicieron una pregunta');
		$html = $this->load->view("templates/correos/aviso_pregunta", $data, TRUE);
		$this->email->message($html);	

		$this->email->send();
	}

	public function __avisar_usuario_respuesta($id_pregunta="")
	{

		$data["pregunta"] = $this->get_pregunta($id_pregunta);
		$data["articulo"] = $this->get_articulo($data["pregunta"]->id_articulo);
		$data["disenador"] = $this->Usuario_model->get_perfil($data["articulo"]["id_usuario"]);
		$data["disenador"]->usuario = $this->Usuario_model->get_usuario("","", $data["articulo"]["id_usuario"]);
		$data["comprador"] = $this->Usuario_model->get_perfil($data["pregunta"]->id_usuario_preg);
		$data["comprador"]->usuario = $this->Usuario_model->get_usuario("","", $data["pregunta"]->id_usuario_preg);

		//Correo al Vendedor
		$config["mailtype"] = "html";

		$this->email->initialize($config);
		
		$this->email->from('no-reply@nuovavenezuela.com', 'Nuova Venezuela');
		$this->email->to($data["comprador"]->usuario->correo); 

		$this->email->subject('Respondieron una pregunta que hiciste');
		$html = $this->load->view("templates/correos/aviso_respuesta", $data, TRUE);
		$this->email->message($html);	

		$this->email->send();
	}

}

/* End of file articulo_model.php */
/* Location: ./application/models/articulo_model.php */