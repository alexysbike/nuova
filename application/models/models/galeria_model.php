<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Galeria_model extends CI_Model {

	public function create_user_folder($id_usuario='')
	{
		if ($id_usuario != "") :
			$path = 'img/usuarios/'.$id_usuario.'/';
			return mkdir($path);
		endif;
	}

	public function get_albums($id_usuario='')
	{
		return $this->db->where("id_usuario", $id_usuario)->get("galeria_album")->result();
	}

	public function get_images($id_usuario='')
	{
		return $this->db->select("galeria_imagen.id, galeria_imagen.id_album, galeria_album.nombre as nombre_album, galeria_imagen.nombre as nombre_imagen, galeria_imagen.descripcion, galeria_imagen.archivo, galeria_imagen.mime, galeria_imagen.width, galeria_imagen.height, galeria_imagen.size, galeria_imagen.fecha")
						   ->from("galeria_imagen")
						   ->join("galeria_album", "galeria_album.id = galeria_imagen.id_album")
						   ->where("galeria_imagen.id_usuario", $id_usuario)
						   ->get()
						   ->result_array();
	}

	public function get_image($id)
	{
		return $this->db->where("id", $id)->get("galeria_imagen")->row();
	}

	public function upload_image($id_usuario='')
	{
		$config['upload_path'] = realpath('img/usuarios/'.$id_usuario.'/');
		$config['max_size']	= '5120';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['file_name'] = $id_usuario."_".preg_replace('/\s\s+/', '', $this->input->post("galeria-nombre"));
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload("galeria-imagen"))
		{
			$error = array('error' => $this->upload->display_errors());

			var_dump($error);

			return false;
		}
		else
		{
			$data = $this->upload->data();
			$ins = array("id_usuario" => $id_usuario,
						 "id_album" => $this->input->post("galeria-album"),
						 "archivo" => $data["file_name"],
						 "nombre" => $this->input->post("galeria-nombre"),
						 "descripcion" => $this->input->post("galeria-descripcion"),
						 "mime" => $data["file_type"],
						 "width" => $data["image_width"],
						 "height" => $data["image_height"],
						 "size" => $data["file_size"],
						 "fecha" => date("Y-m-d"));
			return $this->db->insert("galeria_imagen", $ins);
		}
	}

	public function upload_image_articulo($id_usuario='', $id_articulo, $id_presentacion)
	{
		$config['upload_path'] = realpath('img/usuarios/'.$id_usuario.'/');
		$config['max_size']	= '5120';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['file_name'] = $id_usuario."_".preg_replace('/\s\s+/', '', $this->input->post("galeria-nombre"));
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload("galeria-imagen"))
		{
			$error = array('error' => $this->upload->display_errors());

			var_dump($error);

			return false;
		}
		else
		{
			$data = $this->upload->data();
			$ins = array("id_usuario" => $id_usuario,
						 "id_album" => $this->input->post("galeria-album"),
						 "archivo" => $data["file_name"],
						 "nombre" => $this->input->post("galeria-nombre"),
						 "descripcion" => $this->input->post("galeria-descripcion"),
						 "mime" => $data["file_type"],
						 "width" => $data["image_width"],
						 "height" => $data["image_height"],
						 "size" => $data["file_size"],
						 "fecha" => date("Y-m-d"));
			$this->db->insert("galeria_imagen", $ins);

			$id_imagen = $this->db->insert_id();

			$ins = array("id_articulo" => $id_articulo,
								 "id_presentacion" => $id_presentacion,
								 "id_imagen" => $id_imagen);

			$this->db->insert("articulo_fotos", $ins);

			return $id_imagen;
		}
	}

	public function check_nombre($nombre = '')
	{
		$check = $this->db->where('nombre', $nombre)
		                  ->where('id_usuario', $this->session->userdata('id'))
		                  ->get('galeria_imagen')
		                  ->result();

		if (count($check) > 0) :
			return true;
		else:
			return false;
		endif;
	}

	public function eliminar_imagen($id="")
	{
		$imagen = $this->get_image($id);

		$archivo = 'img/usuarios/'.$imagen->id_usuario.'/'.$imagen->archivo;

		unlink($archivo);

		$this->db->where("id", $id)->delete("galeria_imagen");
	}

	public function update_imagen_perfil($id_imagen_vieja)
	{

		$this->upload_image($this->session->userdata("id"));	
		$id_imagen_nueva = $this->db->insert_id();
		if ($id_imagen_vieja != 0) :
			$this->eliminar_imagen($id_imagen_vieja);
		endif;
		return $id_imagen_nueva;
	}

}

/* End of file galeria_model.php */
/* Location: ./application/models/galeria_model.php */