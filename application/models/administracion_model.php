<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administracion_model extends CI_Model {

	public function get_noticias()
	{
		return $this->db->get("noticias")->result();
	}

	public function get_noticia($id_noticia="")
	{
		return $this->db->where("id", $id_noticia)->get("noticias")->row();
	}

	public function get_noticia_aleatoria()
	{
		$noticias = $this->get_noticias();
		if (count($noticias) > 0):
			return $noticias[rand(0,count($noticias)-1)];
		else:
			return array();
		endif;
	}

	public function crear_noticia()
	{
		$registro = array(
			"fecha" => date('Y-m-d'),
			"titulo" => $this->input->post("titulo"),
			"cuerpo" => $this->input->post("cuerpo")
			);

		return $this->db->insert("noticias", $registro);
	}

	public function editar_noticia($id_noticia="")
	{
		$registro = array(
			"titulo" => $this->input->post("titulo"),
			"cuerpo" => $this->input->post("cuerpo")
			);

		return $this->db->where("id", $id_noticia)->update("noticias", $registro);
	}

	public function eliminar_noticia($id_noticia="")
	{
		return $this->db->where("id", $id_noticia)->delete("noticias");
	}

	public function generar_facturas($desde, $hasta)
	{
		$where = array(
			"id_factura" => 0,
			"status >=" => 3,
			"fecha_finalizacion_compra <=" => $hasta
			);

		$compras = $this->db->where($where)->get("compras")->result();

		$facturas = array();

		foreach ($compras as $compra) :

			if (!isset($facturas[$compra->id_usuario_vendedor])):
				$facturas[$compra->id_usuario_vendedor] = new StdClass;
			endif;

			if (!isset($facturas[$compra->id_usuario_vendedor]->factura)):
				$facturas[$compra->id_usuario_vendedor]->factura = array(
					"id_usuario" => $compra->id_usuario_vendedor,
					"fecha" => date('Y-m-d'),
					"concepto" => "Factura de las ventas desde ".sqldate_to_datepicker($desde)." hasta ".sqldate_to_datepicker($hasta),
					"monto" => 0,
					"porc_iva" => 12,
					"status" => 0,
					"aviso_correo" => 0
					);
				$facturas[$compra->id_usuario_vendedor]->perfil = $this->Usuario_model->get_perfil($compra->id_usuario_vendedor);
				$facturas[$compra->id_usuario_vendedor]->usuario = $this->Usuario_model->get_usuario("","",$compra->id_usuario_vendedor);
			endif;

			if (!isset($facturas[$compra->id_usuario_vendedor]->items)):
				$facturas[$compra->id_usuario_vendedor]->items = array();
				$facturas[$compra->id_usuario_vendedor]->n_piezas = 0;
			endif;

			$compra->articulo = $this->Articulo_model->get_articulo($compra->id_articulo);
			array_push($facturas[$compra->id_usuario_vendedor]->items, $compra);

			$facturas[$compra->id_usuario_vendedor]->factura["monto"] += (($compra->cantidad * $compra->precio)*0.1);
			$facturas[$compra->id_usuario_vendedor]->n_piezas += $compra->cantidad;
		endforeach;

		foreach ($facturas as $factura) :
			$this->db->insert("servicio_factura", $factura->factura);
			$id_factura = $this->db->insert_id();
			$factura->factura["id"] = $id_factura;
			foreach ($factura->items as $item) :
				$registro = array(
					"id_factura" => $id_factura,
					"id_compra" => $item->id,
					"monto_servicio" => ($item->cantidad * $item->precio)*0.1
					);
				
				$this->db->insert("servicio_factura_item", $registro);

				$this->db->where("id", $item->id)->update("compras", array("id_factura" => $id_factura));
			endforeach;
		endforeach;

		return $facturas;
	}

	public function get_facturas($desde, $hasta, $status = FALSE)
	{
		$where = array(
			"fecha >=" => $desde,
			"fecha <=" => $hasta
			);
		if ($status) :
			$where["status"] = $status;
		endif;
		$facturas = $this->db->where($where)->get("servicio_factura")->result();
		foreach ($facturas as $factura) :
			$factura->perfil = $this->Usuario_model->get_perfil($factura->id_usuario);
			$factura->usuario = $this->Usuario_model->get_usuario("","",$factura->id_usuario);
			$factura->n_piezas = 0;
			$factura->items = $this->db->where("id_factura", $factura->id)->get("servicio_factura_item")->result();
			foreach ($factura->items as $item) :
				$item->compra = $this->Articulo_model->get_compra($item->id_compra);
				$factura->n_piezas += $item->compra->cantidad;
			endforeach;
		endforeach;

		return $facturas;
	}

	public function get_factura($id_factura, $status = FALSE)
	{
		$factura = $this->db->where("id", $id_factura)->get("servicio_factura")->row();			
		
		$factura->perfil = $this->Usuario_model->get_perfil($factura->id_usuario);
		$factura->usuario = $this->Usuario_model->get_usuario("","",$factura->id_usuario);
		$factura->n_piezas = 0;
		$factura->items = $this->db->where("id_factura", $id_factura)->get("servicio_factura_item")->result();
		foreach ($factura->items as $item) :
			$item->compra = $this->Articulo_model->get_compra($item->id_compra);
			$factura->n_piezas += $item->compra->cantidad;
		endforeach;

		return $factura;
	}

	public function cambiar_status_factura($id_factura, $status)
	{
		$this->db->where("id", $id_factura)->update("servicio_factura", array("status" => $status));
	}

	public function __enviar_factura_cliente($id_factura='')
	{
		$data["factura"] = $this->get_factura($id_factura);

		//Correo al Vendedor
		$config["mailtype"] = "html";

		$this->email->initialize($config);
		
		$this->email->from('administracion@nuovavenezuela.com', 'Nuova Venezuela');
		$this->email->to($data["factura"]->usuario->correo); 

		$this->email->subject('Resumen de Factura del '.sqldate_to_datepicker($data["factura"]->fecha));
		$html = $this->load->view("templates/correos/resumen_factura", $data, TRUE);
		$this->email->message($html);	

		$this->email->send();

		$this->db->where("id", $id_factura)->update("servicio_factura", array("aviso_correo" => 1));
	}

	public function get_ventas($desde, $hasta, $status = FALSE)
	{
		$where = array(
			"fecha_compra >=" => $desde,
			"fecha_compra <=" => $hasta
			);
		if ($status) :
			$where["status"] = $status;
		endif;
		$ventas = $this->db->where($where)->get("compras")->result();
		foreach ($ventas as $venta) :
			$venta->status_escrita = $this->Articulo_model->get_status_compra_escrito($venta->status);
			$venta->articulo = $this->Articulo_model->get_articulo($venta->id_articulo);
			$venta->talla = $this->Articulo_model->get_talla($venta->id_talla);
			if ($venta->articulo["presentacion-tipo"] == "unica"):
				$venta->imagenes = $this->Articulo_model->get_imagenes($venta->id_articulo);
			else:
				$venta->presentacion = $this->Articulo_model->get_presentacion($venta->id_presentacion);
			endif;
			$venta->disenador = $this->Usuario_model->get_perfil($venta->id_usuario_vendedor);
			$venta->disenador->usuario = $this->Usuario_model->get_usuario("","",$venta->id_usuario_vendedor);
			$venta->comprador = $this->Usuario_model->get_perfil($venta->id_usuario_comprador);
			$venta->comprador->usuario = $this->Usuario_model->get_usuario("","",$venta->id_usuario_comprador);
		endforeach;

		return $ventas;
	}

	public function __enviar_masivo()
	{
		$destinatarios = $this->Usuario_model->get_usuarios_por_tipo($this->input->post('destinatarios'));

		foreach ($destinatarios as $destinatario) {
			$config["mailtype"] = "html";

			$this->email->initialize($config);
			
			$this->email->from('administracion@nuovavenezuela.com', 'Nuova Venezuela');
			$this->email->to($destinatario->correo); 

			$this->email->subject($this->input->post('asunto'));
			$data = array();
			$html = $this->load->view("templates/correos/correo_masivo", $data, TRUE);
			$this->email->message($html);	

			$this->email->send();

			$this->email->clear();
		}

		return count($destinatarios);

	}

}

/* End of file Administracion_model.php */
/* Location: ./application/models/Administracion_model.php */